﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="prescription.aspx.cs" Inherits="prescription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        var maskDate = "99-99-9999";

        function masking() {
            $("#TextBox_Keyword").val("");

            if ($('#DDL_SerchBy').val() == "Date") {
                $("#TextBox_Keyword").attr("placeholder", "DD-MM-YYYY");
                $("#TextBox_Keyword").mask(maskDate);
            }
            else if ($('#DDL_SerchBy').val() != "Date") {
                $("#TextBox_Keyword").attr("placeholder", "");
                $("#TextBox_Keyword").unmask(maskDate);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_General" NavigateUrl="~/glass.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Glass</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_contactlenses" NavigateUrl="~/contactlens.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Contact lens</asp:HyperLink></li>
          <li>
            <asp:HyperLink ID="HL_manageinventory" NavigateUrl="~/manageinventory.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Manage Inventory</asp:HyperLink></li>
         <li>
            <asp:HyperLink ID="HL_Supplier" NavigateUrl="~/managesupplier.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Supplier</asp:HyperLink></li>
        
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-success message"></asp:Label>
    </div>
    <div class="form-inline col-lg-12" style="margin-bottom: 10px; padding-left: 0px !important;">
        <div class="form-group">
            <label for="DDL_SerchBy">Search By:</label>
            <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server" onchange="masking();">
                <asp:ListItem Value="PrescriptionType">Type</asp:ListItem>
                <asp:ListItem Value="patientname">Patient</asp:ListItem>
                <asp:ListItem Value="Date">Date</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label for="TextBox_Keyword">Keyword:</label>
            <asp:TextBox ID="TextBox_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="Button_Search" ClientIDMode="Static" CssClass="btn btn-default" runat="server" Text="Search" OnClick="Button_Search_Click" />
            <asp:HyperLink ID="HlListAll" runat="server" CssClass="btn btn-group-xs btn-default" NavigateUrl="~/prescription.aspx">List all</asp:HyperLink>

        </div>

    </div>
    <div class="table-responsive col-lg-6" style="padding-left: 0px !important;">
        <asp:GridView ID="GVPrescription" CssClass="table table-hover table-striped" runat="server" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataKeyNames="PrescriptionNo" DataSourceID="SDSPrescription" OnRowDeleting="GVPrescription_RowDeleting" OnRowCommand="GVPrescription_RowCommand">
            <Columns>
                <asp:TemplateField HeaderText="PrescriptionNo" SortExpression="PrescriptionNo" Visible="False">                   
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Bind("PrescriptionNo") %>' ID="lbl_PrescriptionNo"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                <asp:HyperLinkField DataTextField="PrescriptionType" HeaderText="Type" SortExpression="PrescriptionType" DataNavigateUrlFields="PrescriptionType,PrescriptionNo" DataNavigateUrlFormatString="{0}.aspx?id={1}" />
                <asp:HyperLinkField DataTextField="patientname" HeaderText="Patient" SortExpression="patientname" DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="patient.aspx?id={0}" />
                  <asp:BoundField DataField="Professional" HeaderText="Professional"  SortExpression="Professional"></asp:BoundField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton_Edit" runat="server" CausesValidation="False"  CommandArgument='<%# Container.DataItemIndex %>' CommandName="Edit" Text="Edit"></asp:LinkButton>
                    </ItemTemplate>
                    <ControlStyle CssClass="btn-success btn btn-xs" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton_Print" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItemIndex %>' CommandName="Print" Text="Print"></asp:LinkButton>
                    </ItemTemplate>
                    <ControlStyle CssClass="btn-info btn btn-xs" />
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs">
                    <ControlStyle CssClass="btn-danger btn btn-xs"></ControlStyle>
                </asp:CommandField>
                <asp:TemplateField>                    
                    <ItemTemplate>
                        <asp:Label runat="server" Visible="false" Text='<%# Bind("PrescriptionType") %>' ID="lbl_PrescriptionType"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>
        <asp:SqlDataSource runat="server" ID="SDSPrescription" OnSelecting="SDSPrescription_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            DeleteCommand="DELETE FROM [dbo].[tbl_Contactlenses] WHERE tbl_Contactlenses.ContactlensesId=@PrescriptionNo"
            SelectCommand=" SELECT tbl_Contactlenses.ContactlensesId as PrescriptionNo, tbl_Contactlenses.Date, tbl_Contactlenses.PrescriptionType
		                      ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,
                              tbl_Contactlenses.Professional  FROM   tbl_Contactlenses INNER JOIN
                              tbl_Patient ON tbl_Contactlenses.PatientId = tbl_Patient.PatientId where tbl_Contactlenses.MasterId=@MasterId
            
                             union all
                              
                            SELECT  tbl_Glass.GlassId as PrescriptionNo, tbl_Glass.Date, tbl_Glass.PrescriptionType
							, tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,
							 tbl_Glass.Professional FROM tbl_Glass INNER JOIN 
							 tbl_Patient ON tbl_Glass.PatientId = tbl_Patient.PatientId where tbl_Glass.MasterId=@MasterId">
            <SelectParameters>
                <asp:Parameter Name="MasterId" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="PrescriptionNo" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

