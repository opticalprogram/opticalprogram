﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageinventory : System.Web.UI.Page
{
    General objGeneral;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Inventory";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        objGeneral = new General();

        if (Session["lblMessage"] != null)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Session["lblMessage"].ToString();
            Session.Remove("lblMessage");
        }

        if (!IsPostBack)
        {
            objGeneral.removeTempEntry();
            if (Request.QueryString["t"] != null)
            {

                if (Request.QueryString["t"].ToString() == "Frame")
                {
                    SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, " 
                                                 +" tbl_Item.ItemBrand, tbl_Item.ItemModel, tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice, "  
                                                 +" tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName "
                                                 + "  FROM tbl_Item INNER JOIN tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId "
                                                 + " where tbl_Item.MasterId=@MasterId and ItemType='Frame' ORDER BY [ItemId]";
                }
                else if (Request.QueryString["t"].ToString() == "Ophtalmic lens")
                {
                    SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, "
                                                 + " tbl_Item.ItemBrand, tbl_Item.ItemModel, tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice, "
                                                 + " tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName "
                                                 + "  FROM tbl_Item INNER JOIN tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId "
                                                 + " where tbl_Item.MasterId=@MasterId and ItemType='Ophtalmic lens' ORDER BY [ItemId]";
                }
                else if (Request.QueryString["t"].ToString() == "Contact lens")
                {
                    SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, "
                                                  + " tbl_Item.ItemBrand, tbl_Item.ItemModel, tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice, "
                                                  + " tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName "
                                                  + "  FROM tbl_Item INNER JOIN tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId "
                                                  + " where tbl_Item.MasterId=@MasterId and ItemType='Contact lens' ORDER BY [ItemId]";
                }

            }
        }
    }

    //grid selectedindexchange
    protected void GVInventory_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GVInventory.SelectedIndex = e.NewSelectedIndex;
        Label ItemType = (Label)GVInventory.Rows[GVInventory.SelectedIndex].FindControl("lbl_Type");
        if (ItemType.Text == "Frame")
        {
            Response.Redirect("inventory.aspx?id=" + GVInventory.SelectedValue.ToString() + "&t=Frame");
        }
        else if (ItemType.Text == "Ophtalmic lens")
        {
            Response.Redirect("inventory.aspx?id=" + GVInventory.SelectedValue.ToString() + "&t=Ophtalmic lens");
        }
        else if (ItemType.Text == "Contact lens")
        {
            Response.Redirect("inventory.aspx?id=" + GVInventory.SelectedValue.ToString() + "&t=Contact lens");
        }
        
    }

    // grid rowdeleted
    protected void GVInventory_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        lblMessage.Visible = true;
        if (e.Exception == null)
        {
            lblMessage.Text = "Item is deleted successfully!";
        }
        else
        {
            lblMessage.CssClass = "alert alert-danger";
            lblMessage.Text = "Error: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
    }

    //Searching
    protected void Button_Search_Click(object sender, EventArgs e)
    {
        if (DDL_SerchBy.SelectedValue=="Quantity")
        {
            SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, tbl_Item.ItemBrand, tbl_Item.ItemModel,"
                           + " tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice,"
                           + " tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName FROM tbl_Item INNER JOIN"
                           + " tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId"
                           + " where tbl_Item.ItemQuantity >'" + TextBox_Keyword.Text + "' and tbl_Item.MasterId='" + MasterId + "' ";
        }
        else if  (Request.QueryString["t"] != null)
        {
            SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, tbl_Item.ItemBrand, tbl_Item.ItemModel,"
                           + " tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice,"
                           + " tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName FROM tbl_Item INNER JOIN"
                           + " tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId"
                           + " where " + DDL_SerchBy.SelectedValue + " like '%" + TextBox_Keyword.Text + "%' and ItemType='" + Request.QueryString["t"].ToString() + "' " 
                           + " and tbl_Item.MasterId='"+ MasterId +"' ";
        }
        else
        {       
        SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, tbl_Item.ItemBrand, tbl_Item.ItemModel,"
                           + " tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice,"
                           + " tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName FROM tbl_Item INNER JOIN"
                           + " tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId"
                           + " where " + DDL_SerchBy.SelectedValue + " like '%" + TextBox_Keyword.Text + "%' and tbl_Item.MasterId='"+ MasterId +"' ";

        }

        //SelectCommand="SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, tbl_Item.ItemBrand, tbl_Item.ItemModel,
        //                   tbl_Item.ItemColor, tbl_Item.ItemFrame,tbl_Item.ItemQuantity, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice, 
        //                   tbl_Item.ItemSalePrice, tbl_Supplier.SupplierName FROM tbl_Item INNER JOIN
        //                   tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId where tbl_Item.MasterId=@MasterId ORDER BY [ItemId]"
     
        TextBox_Keyword.Text = "";
    }
    protected void SDSInventory_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
}