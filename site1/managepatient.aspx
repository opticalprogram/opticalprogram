﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="managepatient.aspx.cs" Inherits="viewpatient" %>

<%--<%@ Register Src="~/UserControl/MasterImage.ascx" TagPrefix="uc1" TagName="MasterImage" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        var maskDateOfBirth = "99-99-9999";
        var maskPhone = "(999) 999-9999";

        function masking() {
            $("#TextBox_Keyword").val("");

            if ($('#DDL_SerchBy').val() == "DateOfBirth") {
                $("#TextBox_Keyword").attr("placeholder", "DD-MM-YYYY");
                $("#TextBox_Keyword").mask(maskDateOfBirth);
            }
            else if ($('#DDL_SerchBy').val() == "CellPhone" || $('#DDL_SerchBy').val() == "HomePhone") {
                $("#TextBox_Keyword").attr("placeholder", "(999) 999-9999");
                $("#TextBox_Keyword").mask(maskPhone);
            }
            else if ($('#DDL_SerchBy').val() != "CellPhone" || $('#DDL_SerchBy').val() != "HomePhone" || $('#DDL_SerchBy').val() != "DateOfBirth") {
                $("#TextBox_Keyword").attr("placeholder", "");
                $("#TextBox_Keyword").unmask(maskPhone);
                $("#TextBox_Keyword").unmask(maskDateOfBirth);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="nav nav-pills PageLink">
        <%--        <li class="active"><a href="managepatient.aspx"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Patients</a></li>--%>
        <li>
            <asp:HyperLink ID="HL_managepatients" NavigateUrl="~/patient.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Add Patient</asp:HyperLink></li>
    </ul>
    <%--  <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <uc1:MasterImage runat="server" ID="MasterImage" />
    </div>--%>
    <div class="form-group col-lg-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-success message"></asp:Label>
    </div>
    <div class="form-inline col-lg-12" style="margin-bottom: 10px; padding-left: 0px !important;">
        <div class="form-group">
            <label for="DDL_SerchBy">Search By:</label>
            <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server" onchange="masking();">
                <asp:ListItem Value="FirstLastName">Last or First Name</asp:ListItem>
                <asp:ListItem Value="FileNo">File No</asp:ListItem>
                <asp:ListItem Value="CellPhone">Cell Phone</asp:ListItem>
                <asp:ListItem Value="HomePhone">Home Phone</asp:ListItem>
                <asp:ListItem Value="DateOfBirth">Date of Birth</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label for="TextBox_Keyword">Keyword:</label>
            <asp:TextBox ID="TextBox_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="Button_Search" ClientIDMode="Static" CssClass="btn btn-default" runat="server" Text="Search" OnClick="Button_Search_Click" />
            <asp:HyperLink ID="HlListAll" runat="server" CssClass="btn btn-group-xs btn-default" NavigateUrl="~/managepatient.aspx">List all</asp:HyperLink>

        </div>

    </div>
    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView GridLines="None" CssClass="table table-hover table-striped" ID="GVPatient" runat="server" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataKeyNames="PatientId" DataSourceID="SDSPatient" OnSelectedIndexChanging="GVPatient_SelectedIndexChanging" OnRowDeleted="GVPatient_RowDeleted" EmptyDataText="No data found!">
            <Columns>
                <asp:BoundField DataField="PatientId" HeaderText="PatientId" SortExpression="PatientId" ReadOnly="True" Visible="false" />
                <asp:BoundField DataField="FileNo" HeaderText="File No" SortExpression="FileNo" />
                <asp:BoundField DataField="OpeningDate" HeaderText="Opening Date" SortExpression="OpeningDate" DataFormatString="{0:dd-MM-yyyy}" />
                <asp:HyperLinkField DataTextField="LastName" HeaderText="Last Name" SortExpression="LastName" DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="patient.aspx?id={0}" />
                <asp:HyperLinkField DataTextField="FirstName" HeaderText="First Name" SortExpression="FirstName" DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="patient.aspx?id={0}" />
                <asp:BoundField DataField="Gender" HeaderText="Gender" SortExpression="Gender" />
                <asp:BoundField DataField="CellPhone" HeaderText="Cell Phone" SortExpression="CellPhone" />
                <asp:BoundField DataField="HomePhone" HeaderText="Home Phone" SortExpression="HomePhone" />
                <%--                <asp:BoundField DataField="CityName" HeaderText="City" SortExpression="City" />--%>
                <asp:BoundField DataField="DateOfBirth" HeaderText="Date Of Birth" SortExpression="DateOfBirth" DataFormatString="{0:dd-MM-yyyy}" />
                <asp:CommandField ShowSelectButton="True" SelectText="Edit" ControlStyle-CssClass="btn-success btn btn-xs" />
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs" />
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>
        <%--<asp:SqlDataSource ID="SDSPatient" runat="server" ConnectionString="<%$ ConnectionStrings:CN-OpticalProgram %>"
            DeleteCommand="DELETE FROM [tbl_Patient] WHERE [PatientId] = @PatientId"
            SelectCommand="SELECT [PatientId], [FileNo], [OpeningDate], [FirstName], [LastName], [Gender], [CellPhone], [HomePhone] ,[CityName], [DateOfBirth] FROM [tbl_Patient],[tbl_City]
                            where [tbl_Patient].CityId=[tbl_City].CityId and [tbl_Patient].MasterId=@MasterId" OnSelecting="SDSPatient_Selecting">--%>
        <asp:SqlDataSource ID="SDSPatient" runat="server" ConnectionString="<%$ ConnectionStrings:CN-OpticalProgram %>"
            DeleteCommand="DELETE FROM [tbl_Patient] WHERE [PatientId] = @PatientId"
            SelectCommand="SELECT [PatientId], [FileNo], [OpeningDate], [FirstName], [LastName], [Gender], [CellPhone], [HomePhone], [DateOfBirth] FROM [tbl_Patient]
                            where [tbl_Patient].MasterId=@MasterId"
            OnSelecting="SDSPatient_Selecting">
            <DeleteParameters>
                <asp:Parameter Name="PatientId" Type="Object" />
            </DeleteParameters>
            <SelectParameters>
                <asp:Parameter Name="MasterId" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

