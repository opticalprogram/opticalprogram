﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;


public partial class contactlenses : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // set title
        string title = "Contact lens";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();
        objdal = new Dal();
        SetPrescriptionNo();
       
        try
        {
            if (!IsPostBack)
            {
                ItemFill();
                if (ddl_Product_OD.Items.Count > 0)
                {
                    ODItemDetails();
                    OSItemDetails();
                }
                Label_Company.Text = objdal.Get_SingleValue("select aspnet_Membership.Comment from aspnet_Membership where aspnet_Membership.UserId='" + MasterId + "'").Split(new char[] { '^' })[0];
                txt_Professional.Text = Membership.GetUser().UserName;
                PatientFill();
                if (Request.QueryString["pid"] != null)
                {
                    DDL_Patient.SelectedValue = Request.QueryString["pid"].ToString();
                }
                if (DDL_Patient.Items.Count > 0)
                {
                    FillExam();
                }
                if (Request.QueryString["id"] != null)
                {
                    txt_Professional.Enabled = false;
                    Button_SAVE.Text = "Update";                  
                    GetPrescriptionData(Request.QueryString["id"].ToString());
                }               
            }
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }

    }

    // Get Prescription Data
    protected void GetPrescriptionData(string EditId)
    {       

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_Contactlenses] where ContactlensesId='" + EditId + "'");
        Txt_PrescriptionNo.Text = dt.Rows[0]["ContactlensesId"].ToString();
        DDL_Patient.SelectedValue = dt.Rows[0]["PatientId"].ToString();
        FillExam();     
        Txt_Date.Text = Convert.ToDateTime(dt.Rows[0]["Date"]).ToString("dd-MM-yyyy");
        ddl_Exam.SelectedValue = dt.Rows[0]["ExamTemplate"].ToString();
        txt_Professional.Text = dt.Rows[0]["Professional"].ToString();     
        txt_Base1_OD.Text = dt.Rows[0]["Base1_OD"].ToString();
        txt_Base2_OD.Text = dt.Rows[0]["Base2_OD"].ToString();
        txt_Horiz_OD.Text = dt.Rows[0]["Horiz_OD"].ToString();
        txt_Vert_OD.Text = dt.Rows[0]["Vert_OD"].ToString();
        txt_Base1_OS.Text = dt.Rows[0]["Base1_OS"].ToString();
        txt_Base2_OS.Text = dt.Rows[0]["Base2_OS"].ToString();
        txt_Horiz_OS.Text = dt.Rows[0]["Horiz_OS"].ToString();
        txt_Vert_OS.Text = dt.Rows[0]["Vert_OS"].ToString();
        Txt_Sphere_OD.Text = dt.Rows[0]["Sphere_OD"].ToString();
        Txt_Cylinder_OD.Text = dt.Rows[0]["Cylinder_OD"].ToString();
        Txt_Axis_OD.Text = dt.Rows[0]["Axis_OD"].ToString();
        Txt_Addition_OD.Text = dt.Rows[0]["Addition_OD"].ToString();
        Txt_Segh_OD.Text = dt.Rows[0]["Segh_OD"].ToString();
        Txt_OZ_OD.Text = dt.Rows[0]["OZ_OD"].ToString();       
        ddl_Trial_OD.SelectedValue = dt.Rows[0]["Trial_OD"].ToString();
        Txt_Sphere_OS.Text = dt.Rows[0]["Sphere_OS"].ToString();
        Txt_Cylinder_OS.Text = dt.Rows[0]["Cylinder_OS"].ToString();
        Txt_Axis_OS.Text = dt.Rows[0]["Axis_OS"].ToString();
        Txt_Addition_OS.Text = dt.Rows[0]["Addition_OS"].ToString();
        Txt_Segh_OS.Text = dt.Rows[0]["Segh_OS"].ToString();
        Txt_OZ_OS.Text = dt.Rows[0]["OZ_OS"].ToString();
        ddl_Trial_OS.SelectedValue = dt.Rows[0]["Trial_OS"].ToString();     
        txt_Contactlenses_OD.Text = dt.Rows[0]["Contactlenses_OD"].ToString();
        txt_Contactlenses_OS.Text = dt.Rows[0]["Contactlenses_OS"].ToString();
        txt_Price_OD.Text = Convert.ToDouble(dt.Rows[0]["Price_OD"]).ToString("n");
        txt_Price_OS.Text = Convert.ToDouble(dt.Rows[0]["Price_OS"]).ToString("n");
        txt_Tint_OD.Text = dt.Rows[0]["Tint_OD"].ToString();
        txt_ReplacementDate_OD.Text = Convert.ToDateTime(dt.Rows[0]["ReplacementDate_OD"]).ToString("dd-MM-yyyy");
        txt_Tint_OS.Text = dt.Rows[0]["Tint_OS"].ToString();
        txt_ReplacementDate_OS.Text = Convert.ToDateTime(dt.Rows[0]["ReplacementDate_OS"]).ToString("dd-MM-yyyy");
        txt_Solution.Text = dt.Rows[0]["Solution"].ToString();
        ddl_Product_OD.SelectedValue = dt.Rows[0]["ItemId_OD"].ToString();        
        txt_Pupilsize_OD.Text = dt.Rows[0]["Pupilsize_OD"].ToString();
        txt_Cornealsize_OD.Text = dt.Rows[0]["Cornealsize_OD"].ToString();
        txt_Typeoftears_OD.Text = dt.Rows[0]["Typeoftears_OD"].ToString();
        txt_But_OD.Text = dt.Rows[0]["But_OD"].ToString();
        txt_Printedon_OD.Text = Convert.ToDateTime(dt.Rows[0]["Printedon_OD"]).ToString("dd-MM-yyyy");
        txt_Orderno_OD.Text = dt.Rows[0]["Orderno_OD"].ToString();
        txt_TrayNo_OD.Text = dt.Rows[0]["TrayNo_OD"].ToString();
        ddl_Product_OS.SelectedValue = dt.Rows[0]["ItemId_OS"].ToString();      
        txt_Pupilsize_OS.Text = dt.Rows[0]["Pupilsize_OS"].ToString();
        txt_Cornealsize_OS.Text = dt.Rows[0]["Cornealsize_OS"].ToString();
        txt_Typeoftears_OS.Text = dt.Rows[0]["Typeoftears_OS"].ToString();
        txt_But_OS.Text = dt.Rows[0]["But_OS"].ToString();
        txt_Printedon_OS.Text = Convert.ToDateTime(dt.Rows[0]["Printedon_OS"]).ToString("dd-MM-yyyy");
        txt_Orderno_OS.Text = dt.Rows[0]["Orderno_OS"].ToString();
        txt_TrayNo_OS.Text = dt.Rows[0]["TrayNo_OS"].ToString();
        Txt_Ready.Text = dt.Rows[0]["Ready"].ToString();
        Txt_Adviceclient.Text = dt.Rows[0]["Adviceclient"].ToString();
        Txt_Followup.Text = dt.Rows[0]["Followup"].ToString();
        Txt_Centrationright.Text = dt.Rows[0]["Centrationright"].ToString();      
        Txt_Centrationleft.Text = dt.Rows[0]["Centrationleft"].ToString();
        Txt_Mouvementlens_OD.Text = dt.Rows[0]["Mouvementlens_OD"].ToString();
        Txt_VisualacuityF_OD.Text = dt.Rows[0]["VisualacuityF_OD"].ToString();
        Txt_VisualacuityC_OD.Text = dt.Rows[0]["VisualacuityC_OD"].ToString();
        Txt_Mouvementlens_OS.Text = dt.Rows[0]["Mouvementlens_OS"].ToString();
        Txt_VisualacuityF_OS.Text = dt.Rows[0]["VisualacuityF_OS"].ToString();
        Txt_VisualacuityC_OS.Text = dt.Rows[0]["VisualacuityC_OS"].ToString();
        Txt_Datecalled.Text = Convert.ToDateTime(dt.Rows[0]["Datecalled"]).ToString("dd-MM-yyyy");
        Txt_Delivery.Text = Convert.ToDateTime(dt.Rows[0]["Delivery"]).ToString("dd-MM-yyyy");
        Txt_Notes.Text = dt.Rows[0]["Notes"].ToString();
        Txt_Professionalfees.Text = Convert.ToDouble(dt.Rows[0]["Professionalfees"]).ToString("n");
        Txt_Total.Text = Convert.ToDouble( dt.Rows[0]["Total"]).ToString("n");
        ODItemDetails();
        OSItemDetails();
    }

    //set Max Prescription no
    protected void SetPrescriptionNo()
    {
        Txt_PrescriptionNo.Text = objdal.Get_SingleValue("select isnull(max(ContactlensesId),0)+1  from tbl_Contactlenses");
    }

    //Fill patient DDl
    protected void PatientFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT [PatientId], [FirstName]+' '+[LastName] +' ('+ SUBSTRING([Gender], 1, 1) +') '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Patient] "
                                 + " FROM [tbl_Patient] where [tbl_Patient].MasterId='" + MasterId + "' ORDER BY [Patient]");
        DDL_Patient.DataSource = dt;
        DDL_Patient.DataValueField = "PatientId";
        DDL_Patient.DataTextField = "Patient";
        DDL_Patient.DataBind();
    }

    // fill Exam on patient selection
    protected void FillExam()
    {
        DataTable dt = new DataTable();
        StringBuilder sb = new StringBuilder();

        // SELECT    CONVERT(varchar, GeneralEyeExamETemplatesId) + ' ' +TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam
        // FROM   tbl_GeneralEyeExamETemplates where PatientId=''
        // union all
        //SELECT    CONVERT(varchar, EmergencyExamtemplatesId) + ' ' +TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam
        //FROM   tbl_EmergencyExamtemplates where PatientId=''
        // union all
        // SELECT    CONVERT(varchar, EyeDilationExamtemplatesId) + ' ' +TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam
        // FROM   tbl_EyeDilationExamtemplates where PatientId=''

        sb.Append("SELECT CONVERT(varchar, GeneralEyeExamETemplatesId) +' '+ TemplateType as Template, TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam");
        sb.Append(" FROM   tbl_GeneralEyeExamETemplates where PatientId='" + DDL_Patient.SelectedValue.ToString() + "'");
        sb.Append(" union all ");
        sb.Append("SELECT CONVERT(varchar, EmergencyExamtemplatesId) +' '+ TemplateType as Template, TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam");
        sb.Append(" FROM   tbl_EmergencyExamtemplates where PatientId='" + DDL_Patient.SelectedValue.ToString() + "'");
        sb.Append(" union all ");
        sb.Append("SELECT CONVERT(varchar, EyeDilationExamtemplatesId) +' '+ TemplateType as Template, TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam");
        sb.Append(" FROM   tbl_EyeDilationExamtemplates where PatientId='" + DDL_Patient.SelectedValue.ToString() + "'");

        dt = objdal.GET_DATATABLE(sb.ToString());
        ddl_Exam.DataSource = dt;
        ddl_Exam.DataValueField = "Template";
        ddl_Exam.DataTextField = "Exam";
        ddl_Exam.DataBind();
        setRXandKeratometry();
    }

    // Set Rx final and Keratometry on exam selection
    protected void setRXandKeratometry()
    {
        if (ddl_Exam.SelectedValue.Contains("General"))
        {
            DataTable dt = new DataTable();
            dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_GeneralEyeExamETemplates] "
                                     + " where [GeneralEyeExamETemplatesId]='" + ddl_Exam.SelectedValue.Split(new char[] { ' ' })[0] + "'");
            Txt_Sphere_OD.Text = dt.Rows[0]["RxScopeOD1"].ToString();
            Txt_Cylinder_OD.Text = dt.Rows[0]["RxScopeOD2"].ToString();
            Txt_Axis_OD.Text = dt.Rows[0]["RxScopeOD3"].ToString();
            Txt_Addition_OD.Text = dt.Rows[0]["RxScopeOD4"].ToString();
            Txt_Sphere_OS.Text = dt.Rows[0]["RxScopeOs1"].ToString();
            Txt_Cylinder_OS.Text = dt.Rows[0]["RxScopeOS2"].ToString();
            Txt_Axis_OS.Text = dt.Rows[0]["RxScopeOS3"].ToString();
            Txt_Addition_OS.Text = dt.Rows[0]["RxScopeOS4"].ToString();

            txt_Base1_OD.Text = dt.Rows[0]["RxFinalOD1"].ToString();
            txt_Base2_OD.Text = dt.Rows[0]["RxFinalOD2"].ToString();
            txt_Horiz_OD.Text = dt.Rows[0]["RxFinalOD3"].ToString();
            txt_Vert_OD.Text = dt.Rows[0]["RxFinalOD4"].ToString();
            txt_Base1_OS.Text = dt.Rows[0]["RxFinalOS1"].ToString();
            txt_Base2_OS.Text = dt.Rows[0]["RxFinalOS2"].ToString();
            txt_Horiz_OS.Text = dt.Rows[0]["RxFinalOS3"].ToString();
            txt_Vert_OS.Text = dt.Rows[0]["RxFinalOS4"].ToString();

            txt_Keratometry_OD1.Text = dt.Rows[0]["Keratometry_OD1"].ToString();
            txt_Keratometry_OD2.Text = dt.Rows[0]["Keratometry_OD2"].ToString();
            txt_Keratometry_OD3.Text = dt.Rows[0]["Keratometry_OD3"].ToString();
            txt_Keratometry_OS1.Text = dt.Rows[0]["Keratometry_OS1"].ToString();
            txt_Keratometry_OS2.Text = dt.Rows[0]["Keratometry_OS2"].ToString();
            txt_Keratometry_OS3.Text = dt.Rows[0]["Keratometry_OS3"].ToString();
        }
        else
        {
            Txt_Sphere_OD.Text = "";
            Txt_Cylinder_OD.Text = "";
            Txt_Axis_OD.Text = "";
            Txt_Addition_OD.Text = "";
            Txt_Sphere_OS.Text = "";
            Txt_Cylinder_OS.Text = "";
            Txt_Axis_OS.Text = "";
            Txt_Addition_OS.Text = "";

            txt_Base1_OD.Text = "";
            txt_Base2_OD.Text = "";
            txt_Horiz_OD.Text = "";
            txt_Vert_OD.Text = "";
            txt_Base1_OS.Text = "";
            txt_Base2_OS.Text = "";
            txt_Horiz_OS.Text = "";
            txt_Vert_OS.Text = "";

            txt_Keratometry_OD1.Text = "";
            txt_Keratometry_OD2.Text = "";
            txt_Keratometry_OD3.Text = "";
            txt_Keratometry_OS1.Text = "";
            txt_Keratometry_OS2.Text = "";
            txt_Keratometry_OS3.Text = "";
        }
    }

    // fill Item 
    protected void ItemFill()
    {
        //DataTable dt = new DataTable();
        //dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
        //                         + " where tbl_Item.ItemType='' and CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName like '%" + TbId.Text + "%' "
        //                         + " and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        //ddlId.DataSource = dt;
        //ddlId.DataValueField = "ItemId";
        //ddlId.DataTextField = "Item";
        //ddlId.DataBind();

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
                                 + " where tbl_Item.ItemType='Contact lens' and CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName like '%" + txt_Keyword_OD.Text + "%' "
                                 + " and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        ddl_Product_OD.DataSource = dt;
        ddl_Product_OD.DataValueField = "ItemId";
        ddl_Product_OD.DataTextField = "Item";
        ddl_Product_OD.DataBind();

        ddl_Product_OS.DataSource = dt;
        ddl_Product_OS.DataValueField = "ItemId";
        ddl_Product_OS.DataTextField = "Item";
        ddl_Product_OS.DataBind();
    }

    // Set Item OD Item Details
    protected void ODItemDetails()
    {
        //select 'lens:'+tbl_Item.ContactType +', Cate.:'+tbl_Item.ContactCategory + ', Sub cate.:' + tbl_Item.ContactSubCategory +', Supplier:'+tbl_Supplier.SupplierName  as [Detail] 
        //from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select tbl_Item.ContactType,tbl_Item.ContactCategory,tbl_Item.ContactSubCategory ,tbl_Supplier.SupplierName"
                                     + " from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId"
                                     +" where tbl_Item.ItemId='"+ ddl_Product_OD.SelectedValue +"'");
        txt_Type.Text = dt.Rows[0]["ContactType"].ToString();
        txt_Category.Text = dt.Rows[0]["ContactCategory"].ToString();
        txt_Subcategory.Text = dt.Rows[0]["ContactSubCategory"].ToString();
        txt_Supplier.Text = dt.Rows[0]["SupplierName"].ToString();


        //txt_Detail_OD.Text = objdal.Get_SingleValue("select 'Lens:'+tbl_Item.ContactType +', Cate.:'+tbl_Item.ContactCategory + ',"
        //                                            + "  Sub cate.:' + tbl_Item.ContactSubCategory +', Supplier:'+tbl_Supplier.SupplierName  as [Detail]"
        //                                            + " from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId"
        //                                            +" where tbl_Item.ItemId='"+ ddl_Product_OD.SelectedValue +"'");
    }

    // Set Item OD Item Details
    protected void OSItemDetails()
    {
        txt_Detail_OS.Text = objdal.Get_SingleValue("select 'Lens:'+tbl_Item.ContactType +', Cate.:'+tbl_Item.ContactCategory + ',"
                                                    + "  Sub cate.:' + tbl_Item.ContactSubCategory +', Supplier:'+tbl_Supplier.SupplierName  as [Detail]"
                                                    + " from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId"
                                                    + " where tbl_Item.ItemId='" + ddl_Product_OS.SelectedValue + "'");
    }

    //patient selection
    protected void DDL_Patient_SelectedIndexChanged(object sender, EventArgs e)
    {       
        FillExam();
    }

    // Save and Update
    protected void Button_SAVE_Click(object sender, EventArgs e)
    {
        try
        {
           
            if (Request.QueryString["id"] != null)
            {
                UpdateContactLenses();
                Session["lblMessage"] = "Prescription is updated successfully.";

            }
            else
            {
                InsertContactLenses();
                Session["lblMessage"] = "Prescription is added successfully.";
            }

            Response.Redirect("~/prescription.aspx", true);
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

      //for Update
    protected void UpdateContactLenses()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("UPDATE [dbo].[tbl_Contactlenses] SET [PatientId] = '" + DDL_Patient.SelectedValue + "'");
        sb.Append(",[Date] = '" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "',[ExamTemplate] = '" + ddl_Exam.SelectedValue + "'");
        sb.Append(",[Professional] = '" + txt_Professional.Text + "',[Base1_OD] = '" + txt_Base1_OD.Text + "'");
        sb.Append(",[Base2_OD] = '" + txt_Base2_OD.Text + "',[Horiz_OD] = '" + txt_Horiz_OD.Text + "',[Vert_OD] = '" + txt_Vert_OD.Text + "'");
        sb.Append(",[Base1_OS] = '" + txt_Base1_OS.Text + "',[Base2_OS] = '" + txt_Base2_OS.Text + "',[Horiz_OS] = '" + txt_Horiz_OS.Text + "'");
        sb.Append(",[Vert_OS] = '" + txt_Vert_OS.Text + "',[Sphere_OD] = '" + Txt_Sphere_OD.Text + "',[Cylinder_OD] = '" + Txt_Cylinder_OD.Text + "'");
        sb.Append(",[Axis_OD] = '" + Txt_Axis_OD.Text + "',[Addition_OD] = '" + Txt_Addition_OD.Text + "',[Segh_OD] = '" + Txt_Segh_OD.Text + "'");
        sb.Append(",[OZ_OD] = '" + Txt_OZ_OD.Text + "',[Trial_OD] = '" + ddl_Trial_OD.SelectedValue + "',[Sphere_OS] = '" + Txt_Sphere_OS.Text + "'");
        sb.Append(",[Cylinder_OS] = '" + Txt_Cylinder_OS.Text + "',[Axis_OS] = '" + Txt_Axis_OS.Text + "',[Addition_OS] = '" + Txt_Addition_OS.Text + "'");
        sb.Append(",[Segh_OS] = '" + Txt_Segh_OS.Text + "',[OZ_OS] = '" + Txt_OZ_OS.Text + "',[Trial_OS] = '" + ddl_Trial_OS.SelectedValue + "'");
        sb.Append(",[Contactlenses_OD] = '" + txt_Contactlenses_OD.Text + "',[Contactlenses_OS] = '" + txt_Contactlenses_OS.Text + "'");
        sb.Append(",[Price_OD]='" + txt_Price_OD.Text + "',[Price_OS]='" + txt_Price_OS.Text + "'");
        sb.Append(",[Tint_OD] = '" + txt_Tint_OD.Text + "',[ReplacementDate_OD] = '" + DateTime.ParseExact(txt_ReplacementDate_OD.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Tint_OS] = '" + txt_Tint_OS.Text + "',[ReplacementDate_OS] = '" + DateTime.ParseExact(txt_ReplacementDate_OS.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Solution] = '" + txt_Solution.Text + "',[ItemId_OD] = '" + ddl_Product_OD.SelectedValue + "'");
        sb.Append(",[Pupilsize_OD] = '" + txt_Pupilsize_OD.Text + "',[Cornealsize_OD] = '" + txt_Cornealsize_OD.Text + "',[Typeoftears_OD] = '" + txt_Typeoftears_OD.Text + "'");
        sb.Append(",[But_OD] = '" + txt_But_OD.Text + "',[Printedon_OD] = '" + DateTime.ParseExact(txt_Printedon_OD.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Orderno_OD]='" + txt_Orderno_OD.Text + "',[TrayNo_OD] = '" + txt_TrayNo_OD.Text + "'");
        sb.Append(",[ItemId_OS] = '" + ddl_Product_OS.SelectedValue + "'");
        sb.Append(",[Pupilsize_OS] = '" + txt_Pupilsize_OS.Text + "',[Cornealsize_OS] = '" + txt_Cornealsize_OS.Text + "',[Typeoftears_OS] = '" + txt_Typeoftears_OS.Text + "'");
        sb.Append(",[But_OS] = '" + txt_But_OS.Text + "',[Printedon_OS] = '" + DateTime.ParseExact(txt_Printedon_OS.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Orderno_OS]='" + txt_Orderno_OS.Text + "',[TrayNo_OS] = '" + txt_TrayNo_OS.Text + "'");
        sb.Append(",[Ready] = '" + Txt_Ready.Text + "',[Centrationright] = '" + Txt_Centrationright.Text + "'");
        sb.Append(",[Adviceclient] = '" + Txt_Adviceclient.Text + "',[Followup] = '" + Txt_Followup.Text + "'");
        sb.Append(",[Centrationleft] = '" + Txt_Centrationleft.Text + "',[Mouvementlens_OD] = '" + Txt_Mouvementlens_OD.Text + "'");
        sb.Append(",[VisualacuityF_OD] = '" + Txt_VisualacuityF_OD.Text + "',[VisualacuityC_OD] = '" + Txt_VisualacuityC_OD.Text + "'");
        sb.Append(",[Mouvementlens_OS] = '" + Txt_Mouvementlens_OS.Text + "',[VisualacuityF_OS] = '" + Txt_VisualacuityF_OS.Text + "'");
        sb.Append(",[VisualacuityC_OS] = '" + Txt_VisualacuityC_OS.Text + "'");
        sb.Append(",[Datecalled] = '" + DateTime.ParseExact(Txt_Datecalled.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Delivery] = '" + DateTime.ParseExact(Txt_Delivery.Text, "dd-MM-yyyy", null) + "',[Notes] = '" + Txt_Notes.Text + "'");
        sb.Append(",[Professionalfees] = '" + Txt_Professionalfees.Text + "',[Total] = '" + Txt_Total.Text + "'");
        sb.Append(" WHERE ContactlensesId='" + Request.QueryString["id"].ToString() + "'");

        objdal.EXECUTE_DML(sb.ToString());
    }

    //for insert
    protected void InsertContactLenses()
    {
        StringBuilder sb = new StringBuilder();
      
        //INSERT INTO [dbo].[tbl_Contactlenses]([ContactlensesId],[PatientId],[Date],[ExamTemplate],[Professional],[Base1_OD],[Base2_OD],[Horiz_OD]
        //,[Vert_OD],[Base1_OS],[Base2_OS],[Horiz_OS],[Vert_OS],[Sphere_OD],[Cylinder_OD],[Axis_OD],[Addition_OD],[Segh_OD],[OZ_OD],[Trial_OD],[Sphere_OS]
        //,[Cylinder_OS],[Axis_OS],[Addition_OS],[Segh_OS],[OZ_OS],[Trial_OS],[Contactlenses_OD],[Contactlenses_OS],[Price_OD],[Price_OS],[Tint_OD]
        //,[ReplacementDate_OD],[Tint_OS],[ReplacementDate_OS],[Solution],[ItemId_OD],[Keratometry_OD],[Pupilsize_OD],[Cornealsize_OD],[Typeoftears_OD]
        //,[But_OD],[Printedon_OD],[Orderno_OD],[TrayNo_OD],[ItemId_OS],[Keratometry_Os],[Pupilsize_OS],[Cornealsize_OS],[Typeoftears_OS],[But_OS]
        //,[Printedon_OS],[Orderno_OS],[TrayNo_OS],[Ready],[Adviceclient],[Followup],[Centrationright],[Centrationleft],[Mouvementlens],[VisualacuityF]
        //,[VisualacuityC],[Datecalled],[Delivery],[Notes],[Professionalfees],[Total],[PrescriptionType],[MasterId])VALUES

        sb.Append("INSERT INTO [dbo].[tbl_Contactlenses]([ContactlensesId],[PatientId],[Date],[ExamTemplate],[Professional],[Base1_OD],[Base2_OD]");
        sb.Append(",[Horiz_OD],[Vert_OD],[Base1_OS],[Base2_OS],[Horiz_OS],[Vert_OS],[Sphere_OD],[Cylinder_OD],[Axis_OD],[Addition_OD],[Segh_OD],[OZ_OD]");
        sb.Append(",[Trial_OD],[Sphere_OS],[Cylinder_OS],[Axis_OS],[Addition_OS],[Segh_OS],[OZ_OS],[Trial_OS],[Contactlenses_OD],[Contactlenses_OS],[Price_OD],[Price_OS]");
        sb.Append(",[Tint_OD],[ReplacementDate_OD],[Tint_OS],[ReplacementDate_OS],[Solution],[ItemId_OD],[Pupilsize_OD],[Cornealsize_OD],[Typeoftears_OD]");
        sb.Append(",[But_OD],[Printedon_OD],[Orderno_OD],[TrayNo_OD],[ItemId_OS],[Pupilsize_OS],[Cornealsize_OS],[Typeoftears_OS],[But_OS]");
        sb.Append(",[Printedon_OS],[Orderno_OS],[TrayNo_OS],[Ready],[Adviceclient],[Followup],[Centrationright],[Centrationleft],[Mouvementlens_OD]");
        sb.Append(",[VisualacuityF_OD],[VisualacuityC_OD],[Mouvementlens_OS],[VisualacuityF_OS],[VisualacuityC_OS],[Datecalled]");
        sb.Append(",[Delivery],[Notes],[Professionalfees],[Total],[PrescriptionType],[MasterId]) ");
        sb.Append("VALUES('" + Txt_PrescriptionNo.Text + "','" + DDL_Patient.SelectedValue + "','" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",'" + ddl_Exam.SelectedValue + "','" + txt_Professional.Text + "','" + txt_Base1_OD.Text + "'");
        sb.Append(",'" + txt_Base2_OD.Text + "','" + txt_Horiz_OD.Text + "','" + txt_Vert_OD.Text + "','" + txt_Base1_OS.Text + "','" + txt_Base2_OS.Text + "'");
        sb.Append(",'" + txt_Horiz_OS.Text + "','" + txt_Vert_OS.Text + "','" + Txt_Sphere_OD.Text + "','" + Txt_Cylinder_OD.Text + "','" + Txt_Axis_OD.Text + "'");
        sb.Append(",'" + Txt_Addition_OD.Text + "','" + Txt_Segh_OD.Text + "','" + Txt_OZ_OD.Text + "','" + ddl_Trial_OD.SelectedValue + "','" + Txt_Sphere_OS.Text + "'");
        sb.Append(",'" + Txt_Cylinder_OS.Text + "','" + Txt_Axis_OS.Text + "','" + Txt_Addition_OS.Text + "','" + Txt_Segh_OS.Text + "','" + Txt_OZ_OS.Text + "'");
        sb.Append(",'" + ddl_Trial_OS.SelectedValue + "','" + txt_Contactlenses_OD.Text + "','" + txt_Contactlenses_OS.Text + "'");
        sb.Append(",'" + txt_Price_OD.Text + "','" + txt_Price_OS.Text + "','" + txt_Tint_OD.Text + "'");
        sb.Append(",'" + DateTime.ParseExact(txt_ReplacementDate_OD.Text, "dd-MM-yyyy", null) + "','" + txt_Tint_OS.Text + "'");
        sb.Append(",'" + DateTime.ParseExact(txt_ReplacementDate_OS.Text, "dd-MM-yyyy", null) + "','" + txt_Solution.Text + "','" + ddl_Product_OD.SelectedValue + "'");
        sb.Append(",'" + txt_Pupilsize_OD.Text + "','" + txt_Cornealsize_OD.Text + "','" + txt_Typeoftears_OD.Text + "'");
        sb.Append(",'" + txt_But_OD.Text + "','" + DateTime.ParseExact(txt_Printedon_OD.Text, "dd-MM-yyyy", null) + "','" + txt_Orderno_OD.Text + "'");
        sb.Append(",'" + txt_TrayNo_OD.Text + "','" + ddl_Product_OS.SelectedValue + "'");
        sb.Append(",'" + txt_Pupilsize_OS.Text + "','" + txt_Cornealsize_OS.Text + "','" + txt_Typeoftears_OS.Text + "','" + txt_But_OS.Text + "'");
        sb.Append(",'" + DateTime.ParseExact(txt_Printedon_OS.Text, "dd-MM-yyyy", null) + "','" + txt_Orderno_OS.Text + "','" + txt_TrayNo_OS.Text + "'");
        sb.Append(",'" + Txt_Ready.Text + "','" + Txt_Adviceclient.Text + "'");
        sb.Append(",'" + Txt_Followup.Text + "','" + Txt_Centrationright.Text + "','" + Txt_Centrationleft.Text + "','" + Txt_Mouvementlens_OD.Text + "'");
        sb.Append(",'" + Txt_VisualacuityF_OD.Text + "','" + Txt_VisualacuityC_OD.Text + "','" + Txt_Mouvementlens_OS.Text + "'");
        sb.Append(",'" + Txt_VisualacuityF_OS.Text + "','" + Txt_VisualacuityC_OS.Text + "','" + DateTime.ParseExact(Txt_Datecalled.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",'" + DateTime.ParseExact(Txt_Delivery.Text, "dd-MM-yyyy", null) + "','" + Txt_Notes.Text + "','" + Txt_Professionalfees.Text + "'");
        sb.Append(",'" + Txt_Total.Text + "','ContactLens','" + MasterId + "')");

        objdal.EXECUTE_DML(sb.ToString());
    }

    //Save
    protected void Button_Invoice_Click(object sender, EventArgs e)
    {
        try
        {
            //InsertContactLenses();
            //InvoiceEntry();
            if (Request.QueryString["id"] != null)
            {
                UpdateContactLenses();
                Response.Redirect("manageinvoice.aspx?Id=" + DDL_Patient.SelectedValue, true);
            }
            else
            {
                InsertContactLenses();
                InsertInvoice();
            }
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
       
    }

    protected void InsertInvoice()
    {
        StringBuilder sb = new StringBuilder();
        string InvoiceMasterId = Guid.NewGuid().ToString();
        string CurrentuserId = Membership.GetUser().ProviderUserKey.ToString();
        string maxInvoice = objdal.Get_SingleValue("select isnull(max(InvoiceNo),0)+1 as MaxInvoiceNumber from tbl_InvoiceMaster");

        //INSERT INTO [dbo].[tbl_InvoiceMaster]([InvoiceMasterId],[InvoiceNo],[PatientId],[InvoiceMasterDate]
        //,[InvoiceMasterGrandTotal],[InvoiceMasterPayment],[InvoiceMasterThirdPartyBalance],[InvoiceMasterPatientBalance]
        //,[InvoiceMasterNote],[InvoiceMasterIsTemporary],[MasterId],[InvoiceMasterEnterydById])VALUES         


        sb.Append("INSERT INTO [dbo].[tbl_InvoiceMaster]([InvoiceMasterId],[InvoiceNo],[PatientId],[InvoiceMasterDate],[InvoiceMasterGrandTotal],[MasterId]");
        sb.Append(",[InvoiceMasterEnterydById]) VALUES('" + InvoiceMasterId + "','" + maxInvoice + "','" + DDL_Patient.SelectedValue + "'");
        sb.Append(",'" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "','" + Txt_Total.Text + "'");
        sb.Append(",'" + MasterId + "','" + CurrentuserId + "')");

        objdal.EXECUTE_DML(sb.ToString());


        // INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceDetailId],[InvoiceMasterId],[ItemId],[ExamTypeId]
        //,[InvoiceDetailDescription],[InvoiceDetailQuantity],[InvoiceDetailDiscount],[InvoiceDetailTax]
        //,[InvoiceDetailUnitprice],[InvoiceDetailDeliveryDate])VALUES
        sb = new StringBuilder();

        double Price = Convert.ToDouble(txt_Price_OD.Text) +  Convert.ToDouble(txt_Price_OS.Text);
       
        sb.Append("INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ItemId],[InvoiceDetailQuantity],[InvoiceDetailDescription],[InvoiceDetailUnitprice])");
        sb.Append("VALUES('" + InvoiceMasterId + "','" + ddl_Product_OD.SelectedValue + "','1','Contact lens','" + Price + "')");
        objdal.EXECUTE_DML(sb.ToString());      

        Response.Redirect("invoice.aspx?Id=" + InvoiceMasterId, true);
    }

    protected void ddl_Product_OD_SelectedIndexChanged(object sender, EventArgs e)
    {
        ODItemDetails();
    }
    protected void ddl_Product_OS_SelectedIndexChanged(object sender, EventArgs e)
    {
        OSItemDetails();
    }
    protected void ddl_Exam_SelectedIndexChanged(object sender, EventArgs e)
    {
        setRXandKeratometry();
    }
    protected void LinkButton_Keyword_OD_Click(object sender, EventArgs e)
    {
        try
        {
            ItemFill();
            ODItemDetails();
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
}