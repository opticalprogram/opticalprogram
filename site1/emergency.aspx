﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="emergency.aspx.cs" Inherits="dilationtemplates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/ProjectHelper.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script>
        $(document).ready(function () {

            var id = getUrlVars()["id"];
            if (id == null) {
                $("#Txt_Date").val(currentdate());
            }

        });
        //Masking       
        jQuery(function ($) {
            $("#Txt_Date").mask("99-99-9999");
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_examtemlate" NavigateUrl="~/examtemplate.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Exam templates</asp:HyperLink></li>
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:ValidationSummary ID="ValidationSummary" CssClass="alert alert-danger summarymessage" runat="server" />
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Examno">Exam no:</label>
            <asp:TextBox ID="Txt_Examno" ClientIDMode="Static" disabled="disabled" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Date">Date:</label>
            <asp:TextBox ID="Txt_Date" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Patient">Patient:</label>&nbsp; 
            <asp:DropDownList ID="DDL_Patient" CssClass="form-control" runat="server"></asp:DropDownList>
        </div>
        <div class="form-group col-lg-12 col-md-12 col-sm-12">
            <label for="txt_CaseHistory">Case history:</label>
            <asp:TextBox ID="txt_CaseHistory" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12 category">

        <table>
            <tr>
                <td colspan="5">
                    <h4>&nbsp;Visual Acuity</h4>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="center">
                    <label>Avl with rx</label></td>
                <td align="center">
                    <label>Avl with vl</label></td>
                <td align="center">
                    <label>TS</label></td>
                <td align="center">
                    <label>VP</label></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD</label></td>
                <td>
                    <asp:TextBox ID="txt_OD_Avlwithrx" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OD_Avlwithvl" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OD_Ts" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OD_VP" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS</label></td>
                <td>
                    <asp:TextBox ID="txt_OS_Avlwithrx" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OS_Avlwithvl" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OS_Ts" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OS_VP" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>
    <div class="form-group col-lg-1 col-md-1 col-sm-12 col-xs-12">
    </div>
    <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 category" style="float: right;">
        <table>
            <tr>
                <td colspan="4">
                    <h4>&nbsp;Reflexes pupillaries</h4>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">
                    <label>MM</label></td>
                <td align="center">
                    <label>LG</label></td>
                <td align="center">
                    <label>MG</label></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD</label></td>
                <td>
                    <asp:TextBox ID="Txt_OD_MM" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OD_LG" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OD_MG" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS</label></td>
                <td>
                    <asp:TextBox ID="Txt_OS_MM" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OS_LG" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OS_MG" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;Notes</label></td>
                <td colspan="3">
                    <asp:TextBox ID="txt_Notes" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>
    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td colspan="2" align="center">
                    <label>Tonometries goldmann</label>
                </td>
                <td align="center">
                    <label>h</label></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD</label></td>
                <td>
                    <asp:TextBox ID="txt_OD_Tonometries" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OD_h" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS</label></td>
                <td>
                    <asp:TextBox ID="txt_OS_Tonometries" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_OS_h" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>
    <div class="form-group col-lg-1 col-md-1 col-sm-12 col-xs-12">
    </div>
    <div class="form-group col-lg-7 col-md-7 col-sm-12 col-xs-12 category" style="float: right;">

        <h4>&nbsp;Agents Diagnostic</h4>

        &nbsp;&nbsp;Proparacaine 0.5%
                    <asp:CheckBox ID="Chk_Proparacaine" runat="server" />
        &nbsp;&nbsp;Benoxinate
                    <asp:CheckBox ID="Chk_Benoxinate" runat="server" />

        &nbsp;&nbsp;Tropicamide 1%
                    <asp:CheckBox ID="Chk_Tropicamide" runat="server" />
        &nbsp;&nbsp;Cyclogyl 1%
                    <asp:CheckBox ID="Chk_Cyclogyl" runat="server" />
        &nbsp;&nbsp;Phenylephrine 2.5%
                    <asp:CheckBox ID="Chk_Phenylephrine" runat="server" />

    </div>
    <div style="clear: both;"></div>
    <div class="form-group col-lg-5 col-md-5 col-sm-12 category">
        <h4>&nbsp;Anterior segment</h4>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Tears">Tears:</label>
            <asp:TextBox ID="txt_Tears" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Eyelids">Eyelids:</label>
            <asp:TextBox ID="txt_Eyelids" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Conjunctiva">Conjunctiva:</label>
            <asp:TextBox ID="txt_Conjunctiva" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_cornea">cornea:</label>
            <asp:TextBox ID="txt_cornea" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_It">It:</label>
            <asp:TextBox ID="txt_It" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Iris">Iris:</label>
            <asp:TextBox ID="txt_Iris" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Crystalline">Crystalline:</label>
            <asp:TextBox ID="txt_Crystalline" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form-group col-lg-1 col-md-1 col-sm-12 col-xs-12">
    </div>
    <div class="form-group col-lg-6 col-md-6 col-sm-12 category">
        <h4>&nbsp;Visual fields</h4>
        <div class="form-group col-lg-6 col-md-6 col-sm-6">
            <br />
            &nbsp;&nbsp;Electro
                    <asp:CheckBox ID="chk_Electro" runat="server" />
            &nbsp;&nbsp;E.T.
                    <asp:CheckBox ID="chk_ET" runat="server" />

            &nbsp;&nbsp;Confront
                    <asp:CheckBox ID="chk_confront" runat="server" />
        </div>

        <div class="form-group col-lg-6 col-md-6 col-sm-6">
            <label for="txt_Target">Target:</label>
            <asp:TextBox ID="txt_Target" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12">
    </div>
    <div class="form-group col-lg-5 col-md-5 col-sm-12 category">
        <h4>&nbsp;Posterior segment</h4>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Environments">Environments:</label>
            <asp:TextBox ID="txt_Environments" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Papilla">Papilla:</label>
            <asp:TextBox ID="txt_Papilla" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Ep">E/p:</label>
            <asp:TextBox ID="txt_Ep" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Pvs">Pvs:</label>
            <asp:TextBox ID="txt_Pvs" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Av">A/v Ral :</label>
            <asp:TextBox ID="txt_Av" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <label for="txt_Macula">Macula:</label>
            <asp:TextBox ID="txt_Macula" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form-group col-lg-1 col-md-1 col-sm-12 col-xs-12">
    </div>
    <div class="form-group col-lg-6 col-md-6 col-sm-12 category">
        <h4>&nbsp;Retinal periphery</h4>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label>No holes:</label>
            <br />
            OD
            <asp:CheckBox ID="chk_OdNoholes" runat="server" />
            OS
            <asp:CheckBox ID="chk_OsNoholes" runat="server" />
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label>Not tearing:</label>
            <br />
            OD
            <asp:CheckBox ID="chk_OdNottearing" runat="server" />
            OS
            <asp:CheckBox ID="chk_OsNottearing" runat="server" />
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label>No detachment:</label>
            <br />
            OD
            <asp:CheckBox ID="chk_OdNodetachment" runat="server" />
            OS
            <asp:CheckBox ID="chk_OsNodetachment" runat="server" />
        </div>
        <%-- <div class="form-group col-lg-3 col-md-3 col-sm-3">
            <asp:Button ID="Button_Submit" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="SAVE" OnClick="Button_Submit_Click" />
        </div>--%>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
    </div>
    <div class="col-lg-5 col-md-5 col-sm-12">
        <asp:Button ID="Button_Submit" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="SAVE" OnClick="Button_Submit_Click" />
    </div>
</asp:Content>

