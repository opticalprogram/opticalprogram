﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="HomePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }

        .carousel-inner {
            width: 100%;
            max-height: 300px !important;
        }

        #login_container > table {
            margin-left: auto;
            margin-right: auto;
        }

        th, td {
            padding: 3px 0px 0px 15px;
        }

        @media screen and (max-width: 768px) {
            #page-content-wrapper > h2 {
                font-size: large;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <asp:Label ID="lblError" CssClass="message alert alert-danger" runat="server" Visible="false"></asp:Label>
    <br />

    <!-- Animation -->
    <link href="animated-bubbles/css/style.css" rel="stylesheet" />
    <div class="bubbles" style="min-height:480px;">

        <script src="animated-bubbles/js/index.js"></script>

        <div id="Slider" runat="server">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" style="max-height: 325px !important;">
                    <div class="item active">
                        <img src="images/img_1.jpg" alt="Chania" width="460" height="200">
                    </div>

                    <div class="item">
                        <img src="images/img_2.jpg" alt="Chania" width="460" height="200">
                    </div>

                    <div class="item">
                        <img src="images/img_3.jpg" alt="Flower" width="460" height="200">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
        <div id="MasterImage" class="form-group col-lg-12 col-md-12 col-sm-12" runat="server" style="display: none;">
            <asp:Image ID="Image_Master" runat="server" CssClass="MasterImage img-responsive img-rounded" />
        </div>
        <br />
        <div class="col-lg-12 col-md-12 col-sm-12" id="login_container">
            <asp:Login ID="Login" runat="server" BorderColor="#BBBBBB" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="10pt" TextLayout="TextOnTop" Width="225px" RememberMeText="&amp;nbsp;Remember me" VisibleWhenLoggedIn="False">
                <LoginButtonStyle CssClass="btn btn-primary" />
                <TitleTextStyle Font-Bold="True" />
            </asp:Login>
        </div>
        <br />
        <br />
        <br />
        <br />
        
    </div>
</asp:Content>

