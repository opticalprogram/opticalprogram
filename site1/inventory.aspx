﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="inventory.aspx.cs" Inherits="Inventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/math.min.js"></script>
    <script src="js/ProjectHelper.js"></script>
    <script>
        $(document).ready(function () {
            //alert(math.eval('12 / (2.3 + 0.7)'));    // 4
            var id = getUrlVars()["id"];
            if (id == null) {
                currentdate();
            }
        });

        function currentdate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '-' + mm + '-' + yyyy;
            $("#Txt_ItemEntryDate").val(today);
        }

        jQuery(function ($) {
            $("#Txt_ItemEntryDate").mask("99-99-9999");
        });

        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head><title></title></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }

        function CalculateSaleprice() {
            $("#Txt_SalePrice").val("0.00");
            var formula = $("#DDL_Formula option:selected").text();
            if (formula.indexOf("|") != -1) {
                var formulaArray = formula.split("|");
                if (formulaArray[1].indexOf("TO") != -1) {
                    $("#Txt_SalePrice").val(formulaArray[2]);
                }
                else {
                    var evaluateString = formulaArray[1].replace("COST", $("#Txt_CostPrice").val());
                    $("#Txt_SalePrice").val(math.eval(evaluateString));
                }
            }
            //$("#HF_SalePrice").val($("#Txt_SalePrice").val());
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li class="active"><a href="#" id="PageHeading" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Item</a></li>
        <li>
            <asp:HyperLink ID="HL_manageinventory" NavigateUrl="~/manageinventory.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Manage Inventory</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_Supplier" NavigateUrl="~/managesupplier.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Supplier</asp:HyperLink></li>
    </ul>

    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:ValidationSummary ID="ValidationSummary" ValidationGroup="SubmitItem" CssClass="alert alert-danger  summarymessage" runat="server" />
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_ItemNumber">Item Number:</label>
            <asp:TextBox ID="Txt_ItemNumber" CssClass="form-control" disabled="disabled" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_ItemName">Item Name:</label>&nbsp;
            <asp:RequiredFieldValidator Display="Static" ValidationGroup="SubmitItem" ControlToValidate="Txt_ItemName" ID="RFV_ItemName" CssClass="label label-danger Errormessage" runat="server" ErrorMessage="Item Name is required!" Text="* Required"></asp:RequiredFieldValidator>
            <asp:TextBox ID="Txt_ItemName" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_ItemEntryDate">Entry Date:</label>
            <asp:TextBox ID="Txt_ItemEntryDate" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Supplier">Supplier:</label>&nbsp;
             <asp:CompareValidator ID="CV_Supplier" ValidationGroup="SubmitItem" ControlToValidate="DDL_Supplier" ValueToCompare="Select" Type="String" CssClass="label label-danger Errormessage" Operator="NotEqual" runat="server" ErrorMessage="Supplier is required!" Text="* Required"></asp:CompareValidator>
            <asp:UpdatePanel ID="UpdatePanelSupplier" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="DDL_Supplier" CssClass="form-control" runat="server" DataSourceID="SDSsupplier" AutoPostBack="true"
                        DataTextField="Supplier" DataValueField="SupplierId" OnSelectedIndexChanged="DDL_Supplier_SelectedIndexChanged" OnDataBound="DDL_Supplier_DataBound">
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:SqlDataSource runat="server" OnSelecting="SDSsupplier_Selecting" ID="SDSsupplier" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                SelectCommand="SELECT [SupplierId], [SupplierName]+' - '+[SupplierAccount] as Supplier  FROM [tbl_Supplier] where [tbl_Supplier].MasterId=@MasterId">
                <SelectParameters>
                    <asp:Parameter Name="MasterId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Brand">Brand:</label>
            <asp:UpdatePanel ID="UpdatePanelBrand" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="DDl_Brand" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="DivInvoiceNumber" runat="server" class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_InvoiceNumber">Invoice number:</label>
            <asp:TextBox ID="Txt_InvoiceNumber" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div id="DivModel" runat="server" class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Model">Model:</label>
            <asp:TextBox ID="Txt_Model" CssClass="form-control" runat="server"></asp:TextBox>
        </div>       
    </div>

     <div id="DivFrame" runat="server" class="form-group col-lg-12 col-md-12 col-sm-12 category">
         <h4>&nbsp;Frame</h4>
          <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Frame">Type:</label>
            <asp:DropDownList ID="DDL_Frame" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Metal</asp:ListItem>
                <asp:ListItem>Plastic</asp:ListItem>
                <asp:ListItem>Nylon</asp:ListItem>
                <asp:ListItem>Rimless</asp:ListItem>
                <asp:ListItem>Optyl</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Glass">Glass:</label>
            <asp:DropDownList ID="DDL_Glass" CssClass="form-control" runat="server">
                 <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Sunglasses</asp:ListItem>
                <asp:ListItem>Clip-on</asp:ListItem>
                <asp:ListItem>Ophtalmic</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Color">Color:</label>
            <asp:TextBox ID="Txt_Color" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_ColorDescription">Color Description:</label>
            <asp:TextBox ID="Txt_ColorDescription" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Quantity">Quantity:</label>
            <asp:TextBox ID="Txt_Quantity" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
         <div class="form-group col-lg-3 col-md-3 col-sm-4">
                <label>Upload new Image:</label>
            <asp:FileUpload ID="FileUpload_Frame" runat="server" accept="image/*" />
            <label style="font-weight: normal; color: gray;">
                Recommended Width: 350 pixel
                <br />
                Recommended Height: 130 pixel</label><br />
            <asp:Button ID="Button_Upload" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" OnClick="Button_Upload_Click" Text="Upload image" />
             </div>
          <div class="form-group col-lg-3 col-md-3 col-sm-4">
                  <label>Frame Image:</label>
            <asp:Image ID="Image_Frame" runat="server" Width="350" Height="130" ImageUrl="~/images/Frame-default.png" CssClass="img-responsive img-rounded" />
              </div>
     </div>

      <div id="DivOphtalmic" runat="server" class="form-group col-lg-12 col-md-12 col-sm-12 category">
         <h4>&nbsp;Ophtalmic lens</h4>
       <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="ddl_OphtalmicType">Type:</label>
           <asp:DropDownList ID="ddl_OphtalmicType" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Single Vision Stock</asp:ListItem>
                <asp:ListItem>Single Vision Surfaced</asp:ListItem>
                <asp:ListItem>Bifocal</asp:ListItem>
                <asp:ListItem>Progressive</asp:ListItem>
                <asp:ListItem>Anti-Fatigue</asp:ListItem>
               <asp:ListItem>Computer</asp:ListItem>
               <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
        </div>      
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="ddl_OphtalmicMaterial">Material:</label>
             <asp:DropDownList ID="ddl_OphtalmicMaterial" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>1.5 CR-39 Organic</asp:ListItem>
                <asp:ListItem>1.56 Plastic</asp:ListItem>
                <asp:ListItem>1.6 Plastic</asp:ListItem>
                <asp:ListItem>1.67 Plastic</asp:ListItem>
                <asp:ListItem>1.74 Plastic</asp:ListItem>
               <asp:ListItem>1.6 Mineral</asp:ListItem>
               <asp:ListItem>1.7 Mineral</asp:ListItem>
               <asp:ListItem>1.8 Mineral</asp:ListItem>
            </asp:DropDownList>
        </div>
       <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_OphtalmicTreatments">Treatments:</label>
            <asp:TextBox ID="Txt_OphtalmicTreatments" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_OphtalmicWarranty">Warranty:</label>
            <asp:TextBox ID="Txt_OphtalmicWarranty" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

     <div id="DivContact" runat="server" class="form-group col-lg-12 col-md-12 col-sm-12 category">
         <h4>&nbsp;Contact lens</h4>
       <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="ddl_ContactType">Type:</label>
           <asp:DropDownList ID="ddl_ContactType" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Soft</asp:ListItem>               
               <asp:ListItem>RGP</asp:ListItem>
               <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
        </div>      
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="ddl_ContactCategory">Category:</label>
             <asp:DropDownList ID="ddl_ContactCategory" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Daily disposable</asp:ListItem>
                <asp:ListItem>Bi-weekly disposable</asp:ListItem>
                <asp:ListItem>Monthly disposable</asp:ListItem>
                <asp:ListItem>Yearly</asp:ListItem>               
            </asp:DropDownList>
        </div>
      <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="ddl_ContactSubCategory">Sub category:</label>
             <asp:DropDownList ID="ddl_ContactSubCategory" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Spherical</asp:ListItem>
                <asp:ListItem>Toric</asp:ListItem>
                <asp:ListItem>Multifocal</asp:ListItem>                       
            </asp:DropDownList>
        </div>
    </div>

    <div id="Divsize" runat="server" class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-1 col-md-3 col-sm-4">
            <h4>Size</h4>
        </div>
        <div class="form-group col-lg-2 col-md-3 col-sm-4">
            <label for="Txt_SizeA">A:</label>
            <asp:TextBox ID="Txt_SizeA" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-2 col-md-3 col-sm-4">
            <label for="Txt_SizeB">B:</label>
            <asp:TextBox ID="Txt_SizeB" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-2 col-md-3 col-sm-4">
            <label for="Txt_SizeSides">Sides:</label>
            <asp:TextBox ID="Txt_SizeSides" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-2 col-md-3 col-sm-4">
            <label for="Txt_SizeH">H:</label>
            <asp:TextBox ID="Txt_SizeH" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-2 col-md-3 col-sm-4">
            <label for="Txt_SizeDBL">DBL:</label>
            <asp:TextBox ID="Txt_SizeDBL" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_CostPrice">Cost Price:</label>&nbsp;
            <asp:CompareValidator ID="CV_CostPrice" ControlToValidate="Txt_CostPrice" ValueToCompare="0" ValidationGroup="SubmitItem" Type="Double" CssClass="label label-danger Errormessage" Operator="NotEqual" runat="server" ErrorMessage="Cost price is required!" Text="* Required"></asp:CompareValidator>
            <asp:TextBox ID="Txt_CostPrice" ClientIDMode="Static" CssClass="form-control" Text="0.00" onkeyup="CalculateSaleprice();" oninput="CalculateSaleprice();" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Formula">Formula:</label>
            <asp:DropDownList ID="DDL_Formula" ClientIDMode="Static" CssClass="form-control" runat="server" AppendDataBoundItems="true" DataSourceID="SDSFormula" DataTextField="formula" DataValueField="FormulaId" onchange="CalculateSaleprice();">
                <asp:ListItem Selected="True" Value="88888888-8888-8888-8888-888888888888">-- Select Formula --</asp:ListItem>
            </asp:DropDownList>
            <asp:SqlDataSource runat="server" OnSelecting="SDSFormula_Selecting" ID="SDSFormula" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                SelectCommand="SELECT [FormulaId], [FormulaName]+' | '+[FormulaString]+' | '+ CONVERT(nvarchar(50),[FormulaFixedPrice]) as formula  
                FROM [tbl_Formula] where [tbl_Formula].MasterId =@MasterId">
                <SelectParameters>
                    <asp:Parameter Name="MasterId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_SalePrice">Sale Price:</label>
            <asp:TextBox ID="Txt_SalePrice" CssClass="form-control" ClientIDMode="Static" Text="0.00" runat="server"></asp:TextBox>
           <%-- <asp:HiddenField ID="HF_SalePrice" ClientIDMode="Static" runat="server" />--%>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <br />
            <asp:Button ID="Button_Submit" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" ValidationGroup="SubmitItem" runat="server" Text="SAVE" OnClick="Button_Submit_Click" />
        </div>
    </div>

    <div id="divlabel" runat="server" style="display: none;" class="form-group col-lg-3 col-md-3 col-sm-4 category">
        <div style="margin-left: 17px;">
            <label for="">Barcode:</label>
            <div id="printablediv">
                <asp:PlaceHolder ID="plBarCode" runat="server" />
                <br />
                Model:
                <asp:Label ID="Lbl_Model" runat="server" Text=""></asp:Label><br />
                Brand:
                <asp:Label ID="Lbl_Brand" runat="server" Text=""></asp:Label><br />
                Color:
                <asp:Label ID="Lbl_Color" runat="server" Text=""></asp:Label><br />
            </div>
            <input type="button" class="btn btn-default btnSubmit" value="Print" onclick="javascript: printDiv('printablediv')" />
        </div>
    </div>

</asp:Content>

