﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

public partial class reports : System.Web.UI.Page
{
    // REF: //http://www.aspsnippets.com/Articles/Create-ASPNet-Chart-Control-from-Database-using-C-and-VBNet-Example.aspx


    Dal objdal;
    string MasterId;
    DateTime StartDate, EndDate;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Report";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        objdal = new Dal();
    }
    protected void Button_Report_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            string Query = string.Empty;

            if (DDL_report.SelectedValue.ToString() == "Bestseller")
            {
                if (Txt_Date.Text.Length >= 15)
                {
                    setStartEndDate();
                    Query = "SELECT  aspnet_Users.UserName,count(tbl_InvoiceMaster.InvoiceMasterId) as cnt FROM tbl_InvoiceMaster "
                             + " INNER JOIN aspnet_Users ON tbl_InvoiceMaster.InvoiceMasterEnterydById = aspnet_Users.UserId "
                             + " where tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' and tbl_InvoiceMaster.MasterId='" + MasterId + "' "
                             + " and (tbl_InvoiceMaster.InvoiceMasterDate>='" + StartDate + "' and tbl_InvoiceMaster.InvoiceMasterDate<='" + EndDate + "') "
                             + " group by aspnet_Users.UserName order by cnt desc";
                }
                else
                {
                    Query = "SELECT  aspnet_Users.UserName,count(tbl_InvoiceMaster.InvoiceMasterId) as cnt FROM tbl_InvoiceMaster "
                              + " INNER JOIN aspnet_Users ON tbl_InvoiceMaster.InvoiceMasterEnterydById = aspnet_Users.UserId "
                              + " where tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' and tbl_InvoiceMaster.MasterId='" + MasterId + "' "
                              + " group by aspnet_Users.UserName order by cnt desc";
                }
            }
            else if (DDL_report.SelectedValue.ToString() == "Glasssold")
            {
                if (Txt_Date.Text.Length >= 15)
                {
                    setStartEndDate();
                    Query = "SELECT  tbl_Item.ItemGlass, count(tbl_InvoiceDetail.InvoiceDetailId) as cnt "
                            + " FROM tbl_Item INNER JOIN tbl_InvoiceDetail ON tbl_Item.ItemId = tbl_InvoiceDetail.ItemId INNER JOIN "
                            + " tbl_InvoiceMaster ON tbl_InvoiceDetail.InvoiceMasterId = tbl_InvoiceMaster.InvoiceMasterId "
                            + " where tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' and tbl_InvoiceMaster.MasterId='" + MasterId + "' "
                            + " and (tbl_InvoiceMaster.InvoiceMasterDate>='" + StartDate + "' and tbl_InvoiceMaster.InvoiceMasterDate<='" + EndDate + "') "
                            + " group by tbl_Item.ItemGlass order by cnt desc";
                }
                else
                {
                    Query = "SELECT  tbl_Item.ItemGlass, count(tbl_InvoiceDetail.InvoiceDetailId) as cnt "
                            + " FROM tbl_Item INNER JOIN tbl_InvoiceDetail ON tbl_Item.ItemId = tbl_InvoiceDetail.ItemId INNER JOIN "
                            + " tbl_InvoiceMaster ON tbl_InvoiceDetail.InvoiceMasterId = tbl_InvoiceMaster.InvoiceMasterId "
                            + " where tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' and tbl_InvoiceMaster.MasterId='" + MasterId + "' "
                            + " group by tbl_Item.ItemGlass order by cnt desc";
                }
            }
            else if (DDL_report.SelectedValue.ToString() == "PatientAgeGroup")
            {

                Query = "select 'Under 16',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())<16 and tbl_Patient.MasterId='" + MasterId + "') t1 " 
		                    +" union all "
                            + " select '16-26',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId  "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())>=16 and DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())<26 and tbl_Patient.MasterId='" + MasterId + "') t1 " 

		                    +" union all "

                            + " select '26-35',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())>=26 and DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())<35 and tbl_Patient.MasterId='" + MasterId + "') t1 " 

		                    +" union all "

                            + " select '35-45',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId  "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())>=35 and DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())<45 and tbl_Patient.MasterId='" + MasterId + "') t1 " 

		                    +" union all "

                            + " select '45-55',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId  "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())>=45 and DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())<55 and tbl_Patient.MasterId='" + MasterId + "') t1 " 

		                    +" union all "

                            + " select '55-65',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId  "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())>=55 and DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())<65 and tbl_Patient.MasterId='" + MasterId + "') t1 " 

		                    +" union all "

                            + " select '65 and Over',count(cnt) as cnt from(select COUNT(tbl_Patient.PatientId) as cnt from tbl_Patient group by tbl_Patient.DateOfBirth,tbl_Patient.MasterId  "
                            + " having DATEDIFF(yy, tbl_Patient.DateOfBirth, getdate())>=65 and tbl_Patient.MasterId='" + MasterId + "') t1";
                
            }
            else if (DDL_report.SelectedValue.ToString() == "PatientSexGroup")
            {
                Query = "select 'Male',COUNT(*) as cnt from tbl_Patient where tbl_Patient.Gender='Male' and tbl_Patient.MasterId='" + MasterId + "'"
			            +" union all "
                        + " select 'Female',COUNT(*) as cnt from tbl_Patient where tbl_Patient.Gender='Female' and tbl_Patient.MasterId='" + MasterId + "'"
			            +" union all "
                        + " select 'Other',COUNT(*) as cnt from tbl_Patient where tbl_Patient.Gender='Other' and tbl_Patient.MasterId='" + MasterId + "'";

            }
            else if (DDL_report.SelectedValue.ToString() == "ExamGroup")
            {
                if (Txt_Date.Text.Length >= 15)
                {
                    setStartEndDate();
                    Query = "select 'General',COUNT(*) as cnt from tbl_GeneralEyeExamETemplates where tbl_GeneralEyeExamETemplates.MasterId='" + MasterId + "'"
                            + " and (tbl_GeneralEyeExamETemplates.ExamDate>='" + StartDate + "' and tbl_GeneralEyeExamETemplates.ExamDate<='" + EndDate + "') "
                            + " union all "
                            + " select 'Emergency',COUNT(*) as cnt from tbl_EmergencyExamtemplates where tbl_EmergencyExamtemplates.MasterId='" + MasterId + "'"
                            + " and (tbl_EmergencyExamtemplates.ExamDate>='" + StartDate + "' and tbl_EmergencyExamtemplates.ExamDate<='" + EndDate + "') "
                            + " union all "
                            + " select 'Dilation',COUNT(*) as cnt from tbl_EyeDilationExamtemplates where tbl_EyeDilationExamtemplates.MasterId='" + MasterId + "'"
                            + " and (tbl_EyeDilationExamtemplates.ExamDate>='" + StartDate + "' and tbl_EyeDilationExamtemplates.ExamDate<='" + EndDate + "') ";
                }
                else
                {
                
                Query = "select 'General',COUNT(*) as cnt from tbl_GeneralEyeExamETemplates where tbl_GeneralEyeExamETemplates.MasterId='" + MasterId + "'"
                        + " union all "
                        + " select 'Emergency',COUNT(*) as cnt from tbl_EmergencyExamtemplates where tbl_EmergencyExamtemplates.MasterId='" + MasterId + "'"
                        + " union all "
                        + " select 'Dilation',COUNT(*) as cnt from tbl_EyeDilationExamtemplates where tbl_EyeDilationExamtemplates.MasterId='" + MasterId + "'";
                }

            }
            else if (DDL_report.SelectedValue.ToString() == "GeneralexamToPrescription")
            {
                if (Txt_Date.Text.Length >= 15)
                {
                    setStartEndDate();
                    Query = "select 'Contactlens',count(cnt) as cnt from(SELECT count(*) as cnt FROM tbl_Contactlenses "
                            + " group by tbl_Contactlenses.ExamTemplate,tbl_Contactlenses.MasterId,tbl_Contactlenses.Date  "
                            + " having tbl_Contactlenses.ExamTemplate like '%General%' and tbl_Contactlenses.MasterId='" + MasterId + "' "
                            + "  and (tbl_Contactlenses.Date>='" + StartDate + "' and tbl_Contactlenses.Date<='" + EndDate + "')) t1 "
                            + " Union all "
                            + " select 'Glass',count(cnt) as cnt from(SELECT count(*) as cnt FROM tbl_Glass "
                            + " group by tbl_Glass.ExamTemplate,tbl_Glass.MasterId,tbl_Glass.Date  "
                            + " having tbl_Glass.ExamTemplate like '%General%' and tbl_Glass.MasterId='" + MasterId + "'"
                            + "  and (tbl_Glass.Date>='" + StartDate + "' and tbl_Glass.Date<='" + EndDate + "')) t1 ";
                }
                else
                {

                    Query = "select 'Contactlens',count(cnt) as cnt from(SELECT count(*) as cnt FROM tbl_Contactlenses "
                           + " group by tbl_Contactlenses.ExamTemplate,tbl_Contactlenses.MasterId  "
                           + " having tbl_Contactlenses.ExamTemplate like '%General%' and tbl_Contactlenses.MasterId='" + MasterId + "') t1 "
                           + " Union all "
                           + " select 'Glass',count(cnt) as cnt from(SELECT count(*) as cnt FROM tbl_Glass "
                           + " group by tbl_Glass.ExamTemplate,tbl_Glass.MasterId  "
                           + " having tbl_Glass.ExamTemplate like '%General%' and tbl_Glass.MasterId='" + MasterId + "') t1";
                }

            }


            if (DDL_report.SelectedValue.ToString() == "ItemDelivery")
            {
                Chart1.Visible = false;
                GVSoldFrame.Visible = false;
                GVItemDelivery.Visible = true;
                if (Txt_Date.Text.Length >= 15)
                {
                    setStartEndDate();
                    SDSItemDelivery.SelectCommand = "SELECT tbl_Item.ItemId,tbl_Item.ItemName,tbl_InvoiceDetail.InvoiceDetailDescription,tbl_InvoiceMaster.InvoiceNo,tbl_InvoiceMaster.InvoiceMasterDate, "
                         + "  DATEDIFF(DAY,tbl_InvoiceMaster.InvoiceMasterDate,tbl_InvoiceDetail.InvoiceDetailDeliveryDate) [Days] "
                           + "  FROM tbl_Item INNER JOIN tbl_InvoiceDetail ON tbl_Item.ItemId = tbl_InvoiceDetail.ItemId "
                          + "   Inner JOIN tbl_InvoiceMaster ON tbl_InvoiceDetail.InvoiceMasterId = tbl_InvoiceMaster.InvoiceMasterId "
                          + "   where tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' and tbl_InvoiceDetail.InvoiceDetailDeliveryDate is not null "
                          + " and tbl_Item.MasterId='" + MasterId + "'"
                          + " and (tbl_InvoiceMaster.InvoiceMasterDate>='" + StartDate + "' and tbl_InvoiceMaster.InvoiceMasterDate<='" + EndDate + "') order by [Days] desc";
                }
            }
            else if (DDL_report.SelectedValue.ToString() == "SoldFrame")
            {
                Chart1.Visible = false;
                GVItemDelivery.Visible = false;
                GVSoldFrame.Visible = true;
                if (Txt_Date.Text.Length >= 15)
                {
                    setStartEndDate();
                    SDSFameSold.SelectCommand = "SELECT  tbl_InvoiceDetail.ItemId, tbl_Item.ItemName, tbl_InvoiceMaster.InvoiceMasterDate "
					                                 + " FROM  tbl_InvoiceMaster INNER JOIN tbl_InvoiceDetail "
					                                 + " ON tbl_InvoiceMaster.InvoiceMasterId = tbl_InvoiceDetail.InvoiceMasterId INNER JOIN "
                                                     + " tbl_Item ON tbl_InvoiceDetail.ItemId = tbl_Item.ItemId where tbl_Item.ItemType='Frame' and tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' "
                                                     + " and tbl_Item.MasterId='" + MasterId + "'"
                                                     + " and (tbl_InvoiceMaster.InvoiceMasterDate>='" + StartDate + "' and tbl_InvoiceMaster.InvoiceMasterDate<='" + EndDate + "') "
                                                     + " order by tbl_InvoiceMaster.InvoiceMasterDate desc";
                }
            }          
            else
            {
                Chart1.Visible = true;
                GVItemDelivery.Visible = false;
                GVSoldFrame.Visible = false;
                Generatechart(Query);
            }
           

        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    protected void setStartEndDate()
    {
        string[] Date = Txt_Date.Text.Split(',');
        StartDate = DateTime.ParseExact(Date[0].Trim(), "dd-MM-yyyy", null);
        EndDate = DateTime.ParseExact(Date[1].Trim(), "dd-MM-yyyy", null);
    }

    protected void Generatechart(string Query)
    {
        DataTable dt = objdal.GET_DATATABLE(Query);
        if (dt.Rows.Count > 0)
        {
            string[] x = new string[dt.Rows.Count];
            int[] y = new int[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                x[i] = dt.Rows[i][0].ToString();
                y[i] = Convert.ToInt32(dt.Rows[i][1]);
            }
            Chart1.Series[0].Points.DataBindXY(x, y);
            Chart1.Series[0].ChartType = SeriesChartType.Column;
            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            //Chart1.Legends[0].Enabled = true;
        }
        else
        {
            lblMessage.Visible = true;
            lblMessage.Text = "No data found!";
        }

    }
    protected void SDSItemDelivery_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void SDSFameSold_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
}