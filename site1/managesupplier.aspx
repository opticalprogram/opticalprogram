﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="managesupplier.aspx.cs" Inherits="supplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">    
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_Supplier" NavigateUrl="~/supplier.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Add Supplier</asp:HyperLink></li>
       <li>
            <asp:HyperLink ID="HL_inventoryFrame" NavigateUrl="~/inventory.aspx?t=Frame" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Frame</asp:HyperLink></li>
         <li>
            <asp:HyperLink ID="HL_inventoryOphtalmic" NavigateUrl="~/inventory.aspx?t=Ophtalmic lens" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Ophtalmic lens</asp:HyperLink></li>
         <li>
            <asp:HyperLink ID="HL_inventoryContact" NavigateUrl="~/inventory.aspx?t=Contact lens" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Contact lens</asp:HyperLink></li>

    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" Width="500" runat="server" Visible="false" CssClass="alert alert-success message"></asp:Label>
    </div>
    <div class="form-inline col-lg-12" style="margin-bottom: 10px; padding-left: 0px !important;">
        <div class="form-group">
            <label for="DDL_SerchBy">Search By:</label>
            <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server">
                <asp:ListItem Value="SupplierName">Name</asp:ListItem>
                <asp:ListItem Value="SupplierAccount">Account</asp:ListItem>
                <asp:ListItem Value="SupplierBrand">Brand</asp:ListItem>
                <asp:ListItem Value="Type">Type</asp:ListItem>               
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label for="TextBox_Keyword">Keyword:</label>
            <asp:TextBox ID="TextBox_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="Button_Search" ClientIDMode="Static" CssClass="btn btn-default" runat="server" Text="Search" OnClick="Button_Search_Click"/>
            <asp:HyperLink ID="HlListAll" runat="server" CssClass="btn btn-group-xs btn-default" NavigateUrl="~/managesupplier.aspx">List all</asp:HyperLink>
        </div>
    </div>
    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView ID="GVSupplier" CssClass="table table-hover table-striped" runat="server" Width="900" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataKeyNames="SupplierId" DataSourceID="SDSsupplier" OnRowDeleted="GVSupplier_RowDeleted" OnSelectedIndexChanging="GVSupplier_SelectedIndexChanging">
            <Columns>
                <asp:BoundField DataField="SupplierId" Visible="false" HeaderText="SupplierId" ReadOnly="True" SortExpression="SupplierId"></asp:BoundField>
                <asp:BoundField DataField="SupplierName" HeaderText="Name" SortExpression="SupplierName"></asp:BoundField>
                <asp:BoundField DataField="SupplierAccount" HeaderText="Account" SortExpression="SupplierAccount"></asp:BoundField>
                <asp:BoundField DataField="SupplierBrand" HeaderText="Brand" SortExpression="SupplierBrand"></asp:BoundField>
                <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type"></asp:BoundField>
                <asp:BoundField DataField="SupplierAddress" HeaderText="Address" SortExpression="SupplierAddress"></asp:BoundField>
                <asp:CommandField ShowSelectButton="true" SelectText="Edit" ControlStyle-CssClass="btn-success btn btn-xs"></asp:CommandField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs"></asp:CommandField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>

        <asp:SqlDataSource runat="server" ID="SDSsupplier" OnSelecting="SDSsupplier_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            SelectCommand="SELECT [SupplierId], [SupplierName], [SupplierAddress], [SupplierAccount],[SupplierBrand],[Type] FROM [tbl_Supplier] 
                           where [tbl_Supplier].MasterId=@MasterId ORDER BY [SupplierName]"
            DeleteCommand="DELETE FROM [dbo].[tbl_Supplier]WHERE SupplierId=@SupplierId">
            <SelectParameters>
                <asp:Parameter Name="MasterId" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="SupplierId" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>   

</asp:Content>

