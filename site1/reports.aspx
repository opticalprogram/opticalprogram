﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="reports.aspx.cs" Inherits="reports" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/cupertino/jquery-ui.css" rel="stylesheet" />
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery-ui.multidatespicker.js"></script>
    <script>
        $(function () {
            $('#Txt_Date').multiDatesPicker({
                maxPicks: 2,
                dateFormat: 'dd-mm-yy'
            });
            hideDatepicker();            
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_report">Report:</label>
            <asp:DropDownList ID="DDL_report" CssClass="form-control" runat="server">
                <asp:ListItem Value="Bestseller">Best seller</asp:ListItem>
                <asp:ListItem Value="Glasssold">Glass sold</asp:ListItem>
                <asp:ListItem Value="ItemDelivery">Item Delivery</asp:ListItem>
                <asp:ListItem Value="PatientAgeGroup">Patient age group</asp:ListItem>
                <asp:ListItem Value="PatientSexGroup">Patient sex group</asp:ListItem>
                <asp:ListItem Value="ExamGroup">Exam group</asp:ListItem> 
                <asp:ListItem Value="GeneralexamToPrescription">General exam to prescription</asp:ListItem>   
                <asp:ListItem Value="SoldFrame">Sold frame</asp:ListItem>              
            </asp:DropDownList>
        </div>

        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Date">Date:</label>
            <asp:DropDownList EnableViewState="false" ID="DDL_Date" ClientIDMode="Static" runat="server" onchange="hideDatepicker();">
                <asp:ListItem>Custom</asp:ListItem>
                <asp:ListItem>Last 7 days</asp:ListItem>
                <asp:ListItem>Last 30 days</asp:ListItem>
                <asp:ListItem>Last 180 days</asp:ListItem>
                <asp:ListItem>Last 365 days</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="Txt_Date" ClientIDMode="Static" CssClass="form-control" placeholder="Pick date range" runat="server"></asp:TextBox>
            <script>
                var today, tomorrow;

                function hideDatepicker() {

                    $('#Txt_Date').multiDatesPicker('resetDates');

                    if ($("#DDL_Date option:selected").val() != "Custom") {

                      //  $("#Txt_Date").attr("disabled", true);

                        if ($("#DDL_Date option:selected").val() == "Last 7 days") {
                            today = new Date();
                            tomorrow = (new Date()).setDate(today.getDate() - 7);
                        } else if ($("#DDL_Date option:selected").val() == "Last 30 days") {
                            today = new Date();
                            tomorrow = (new Date()).setDate(today.getDate() - 30);
                        }
                        else if ($("#DDL_Date option:selected").val() == "Last 180 days") {
                            today = new Date();
                            tomorrow = (new Date()).setDate(today.getDate() - 180);
                        }
                        else if ($("#DDL_Date option:selected").val() == "Last 365 days") {
                            today = new Date();
                            tomorrow = (new Date()).setDate(today.getDate() - 365);
                        }
                        $('#Txt_Date').multiDatesPicker('addDates', [today, tomorrow]);

                    } else {
                        $("#Txt_Date").attr("disabled", false);
                    }

                }
            </script>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <br />
            <asp:Button ID="Button_Report" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="Generate report" OnClick="Button_Report_Click" />
        </div>
    </div>
    <div style="padding-left: 0px !important;">
        <asp:Chart ID="Chart1" ClientIDMode="Static" runat="server" CssClass="col-lg-12">
            <Titles>
                <asp:Title ShadowOffset="3" Name="Items" />
            </Titles>
            <%--<Legends>
                    <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                </Legends>--%>
            <Series>
                <asp:Series Name="Default" />
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" />
            </ChartAreas>
        </asp:Chart>
    </div>

    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView ID="GVItemDelivery" CssClass="table table-hover table-striped" runat="server" Width="500px" PageSize="8" AutoGenerateColumns="False" Visible="false" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataKeyNames="ItemId" DataSourceID="SDSItemDelivery">
            <Columns>
                <%--<asp:BoundField DataField="ItemId" HeaderText="ItemId" ReadOnly="True" InsertVisible="False" SortExpression="ItemId"></asp:BoundField>--%>
                <asp:BoundField DataField="ItemName" HeaderText="Name" SortExpression="ItemName"></asp:BoundField>
                <asp:BoundField DataField="InvoiceDetailDescription" HeaderText="Description" SortExpression="InvoiceDetailDescription"></asp:BoundField>
                <%--<asp:BoundField DataField="InvoiceNo" HeaderText="InvoiceNo" SortExpression="InvoiceNo"></asp:BoundField>--%>
                <asp:BoundField DataField="InvoiceMasterDate" HeaderText="Invoice date" DataFormatString="{0:dd-MM-yyyy}" SortExpression="InvoiceMasterDate"></asp:BoundField>
                <asp:BoundField DataField="Days" HeaderText="Days for delivery" ReadOnly="True" SortExpression="Days"></asp:BoundField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>
        <asp:SqlDataSource runat="server" ID="SDSItemDelivery" OnSelecting="SDSItemDelivery_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            SelectCommand="SELECT tbl_Item.ItemId,tbl_Item.ItemName,tbl_InvoiceDetail.InvoiceDetailDescription,tbl_InvoiceMaster.InvoiceNo,tbl_InvoiceMaster.InvoiceMasterDate, 
                           DATEDIFF(DAY,tbl_InvoiceMaster.InvoiceMasterDate,tbl_InvoiceDetail.InvoiceDetailDeliveryDate) [Days]
                           FROM tbl_Item INNER JOIN tbl_InvoiceDetail ON tbl_Item.ItemId = tbl_InvoiceDetail.ItemId 
                           Inner JOIN tbl_InvoiceMaster ON tbl_InvoiceDetail.InvoiceMasterId = tbl_InvoiceMaster.InvoiceMasterId
                           where tbl_InvoiceMaster.InvoiceMasterIsTemporary='0' and tbl_InvoiceDetail.InvoiceDetailDeliveryDate is not null and tbl_Item.MasterId=@MasterId order by [Days] desc">
              <SelectParameters>
                <asp:Parameter Name="MasterId"/>
            </SelectParameters>
        </asp:SqlDataSource>
    </div>

    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView ID="GVSoldFrame" CssClass="table table-hover table-striped" runat="server" Width="500px" PageSize="8" AutoGenerateColumns="False" Visible="false" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataSourceID="SDSFameSold">
            <Columns>
                <asp:BoundField DataField="ItemId" HeaderText="Item no." SortExpression="ItemId"></asp:BoundField>
                <asp:BoundField DataField="ItemName" HeaderText="Name" SortExpression="ItemName"></asp:BoundField>
                <asp:BoundField DataField="InvoiceMasterDate" HeaderText="Invoice Date" DataFormatString="{0:dd-MM-yyyy}" SortExpression="InvoiceMasterDate"></asp:BoundField>
            </Columns>           
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>

        <asp:SqlDataSource runat="server" ID="SDSFameSold" OnSelecting="SDSFameSold_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>' 
            SelectCommand="SELECT  tbl_InvoiceDetail.ItemId, tbl_Item.ItemName, tbl_InvoiceMaster.InvoiceMasterDate
					  FROM  tbl_InvoiceMaster INNER JOIN tbl_InvoiceDetail 
					  ON tbl_InvoiceMaster.InvoiceMasterId = tbl_InvoiceDetail.InvoiceMasterId INNER JOIN
                      tbl_Item ON tbl_InvoiceDetail.ItemId = tbl_Item.ItemId where tbl_Item.ItemType='Frame' and tbl_InvoiceMaster.InvoiceMasterIsTemporary='0'
                     and tbl_Item.MasterId=@MasterId order by tbl_InvoiceMaster.InvoiceMasterDate desc">
             <SelectParameters>
                <asp:Parameter Name="MasterId"/>
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

