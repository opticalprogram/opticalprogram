﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class examtemplate : System.Web.UI.Page
{
    string MasterId;
    Dal objdal;
    protected void Page_Load(object sender, EventArgs e)
    {
        // set title
        string title = "Exam templates";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();
        objdal = new Dal();
        if (Session["lblMessage"] != null)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Session["lblMessage"].ToString();
            Session.Remove("lblMessage");
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {

                SDSExamTemplate.SelectCommand = "SELECT tbl_EmergencyExamtemplates.EmergencyExamtemplatesId as ExamNo, tbl_EmergencyExamtemplates.ExamDate "
                                                + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,"
                                                + " tbl_EmergencyExamtemplates.TemplateType FROM  tbl_EmergencyExamtemplates INNER JOIN"
                                                + " tbl_Patient ON tbl_EmergencyExamtemplates.PatientId = tbl_Patient.PatientId "
                                                + " where tbl_EmergencyExamtemplates.PatientId='" + Request.QueryString["id"].ToString() + "' and "
                                                + " tbl_EmergencyExamtemplates.MasterId='" + MasterId + "'"

                                                + "  union all"

                                                + " SELECT tbl_EyeDilationExamtemplates.EyeDilationExamtemplatesId as ExamNo, tbl_EyeDilationExamtemplates.ExamDate, "
                                                + " tbl_Patient.PatientId,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                                + " tbl_EyeDilationExamtemplates.TemplateType FROM  tbl_EyeDilationExamtemplates INNER JOIN "
                                                + " tbl_Patient ON tbl_EyeDilationExamtemplates.PatientId = tbl_Patient.PatientId "
                                                + " where tbl_EyeDilationExamtemplates.PatientId='" + Request.QueryString["id"].ToString() + "' and "
                                                + " tbl_EyeDilationExamtemplates.MasterId='" + MasterId + "'"

                                                + " union all"

                                                + " SELECT tbl_GeneralEyeExamETemplates.GeneralEyeExamETemplatesId as ExamNo, tbl_GeneralEyeExamETemplates.ExamDate, "
                                                + " tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                                + " tbl_GeneralEyeExamETemplates.TemplateType FROM tbl_GeneralEyeExamETemplates INNER JOIN "
                                                + " tbl_Patient ON tbl_GeneralEyeExamETemplates.PatientId = tbl_Patient.PatientId "
                                                + " where tbl_GeneralEyeExamETemplates.PatientId='" + Request.QueryString["id"].ToString() + "' and "
                                                + " tbl_GeneralEyeExamETemplates.MasterId='" + MasterId + "'";
            } 

        }
    }
    protected void GVExamtemplate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GVExamtemplate.SelectedIndex = e.NewSelectedIndex;

        Label TemplateType = (Label)GVExamtemplate.Rows[GVExamtemplate.SelectedIndex].FindControl("lbl_TemplateType");

        if (TemplateType.Text=="Dilation")
        {
            Response.Redirect("dilatation.aspx?id=" + GVExamtemplate.SelectedValue, true);
        }
        else if (TemplateType.Text == "Emergency")
        {
            Response.Redirect("emergency.aspx?id=" + GVExamtemplate.SelectedValue, true);
        }
        else if (TemplateType.Text == "General")
        {
            Response.Redirect("general.aspx?id=" + GVExamtemplate.SelectedValue, true);
        }
    }
    protected void SDSExamTemplate_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    //for Deleting
    protected void GVExamtemplate_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string str = GVExamtemplate.Rows[e.RowIndex].Cells[0].Text;

        Label DeleteId = (Label)GVExamtemplate.Rows[e.RowIndex].FindControl("lbl_ExamNo");
        Label TemplateType = (Label)GVExamtemplate.Rows[e.RowIndex].FindControl("lbl_TemplateType");

        if (TemplateType.Text == "Dilation")
        {
            objdal.EXECUTE_DML("DELETE FROM [dbo].[tbl_EyeDilationExamtemplates] WHERE EyeDilationExamtemplatesId='" + DeleteId.Text + "'");
        }
        else if (TemplateType.Text == "Emergency")
        {
            objdal.EXECUTE_DML("DELETE FROM [dbo].[tbl_EmergencyExamtemplates] WHERE EmergencyExamtemplatesId='" + DeleteId.Text + "'");
        }
        else if (TemplateType.Text == "General")
        {
            objdal.EXECUTE_DML("DELETE FROM [dbo].[tbl_GeneralEyeExamETemplates] WHERE GeneralEyeExamETemplatesId='" + DeleteId.Text + "'");
        }
    }
    //For searching
    protected void Button_Search_Click(object sender, EventArgs e)
    {
        if (DDL_SerchBy.SelectedValue == "ExamDate")
        {
            SDSExamTemplate.SelectCommand = "select q1.ExamNo,q1.ExamDate,q1.PatientId,q1.TemplateType,q1.patientname from "
                                                + " (SELECT tbl_EmergencyExamtemplates.EmergencyExamtemplatesId as ExamNo, tbl_EmergencyExamtemplates.ExamDate "
                                                + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,"
                                                + " tbl_EmergencyExamtemplates.TemplateType FROM  tbl_EmergencyExamtemplates INNER JOIN"
                                                + " tbl_Patient ON tbl_EmergencyExamtemplates.PatientId = tbl_Patient.PatientId "
                                                + " where tbl_EmergencyExamtemplates.MasterId='" + MasterId + "'"
                                                
                                                + "  union all"

                                                + " SELECT tbl_EyeDilationExamtemplates.EyeDilationExamtemplatesId as ExamNo, tbl_EyeDilationExamtemplates.ExamDate, "
                                                + " tbl_Patient.PatientId,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                                + " tbl_EyeDilationExamtemplates.TemplateType FROM  tbl_EyeDilationExamtemplates INNER JOIN "
                                                + " tbl_Patient ON tbl_EyeDilationExamtemplates.PatientId = tbl_Patient.PatientId "
                                                + " where tbl_EyeDilationExamtemplates.MasterId='" + MasterId + "'"

                                                + " union all"

                                                + " SELECT tbl_GeneralEyeExamETemplates.GeneralEyeExamETemplatesId as ExamNo, tbl_GeneralEyeExamETemplates.ExamDate, "
                                                + " tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                                + " tbl_GeneralEyeExamETemplates.TemplateType FROM tbl_GeneralEyeExamETemplates INNER JOIN "
                                                + " tbl_Patient ON tbl_GeneralEyeExamETemplates.PatientId = tbl_Patient.PatientId "
                                                + " where tbl_GeneralEyeExamETemplates.MasterId='" + MasterId + "'"
                                                + " ) as q1 where q1.ExamDate='" + DateTime.ParseExact(TextBox_Keyword.Text, "dd-MM-yyyy", null) + "'";
        }
        else if (DDL_SerchBy.SelectedValue == "patientname")
        {
            SDSExamTemplate.SelectCommand = "select q1.ExamNo,q1.ExamDate,q1.PatientId,q1.TemplateType,q1.patientname from "
                                    + " (SELECT tbl_EmergencyExamtemplates.EmergencyExamtemplatesId as ExamNo, tbl_EmergencyExamtemplates.ExamDate "
                                    + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,"
                                    + " tbl_EmergencyExamtemplates.TemplateType FROM  tbl_EmergencyExamtemplates INNER JOIN"
                                    + " tbl_Patient ON tbl_EmergencyExamtemplates.PatientId = tbl_Patient.PatientId "
                                    + " where tbl_EmergencyExamtemplates.MasterId='" + MasterId + "'"

                                    + "  union all"

                                    + " SELECT tbl_EyeDilationExamtemplates.EyeDilationExamtemplatesId as ExamNo, tbl_EyeDilationExamtemplates.ExamDate, "
                                    + " tbl_Patient.PatientId,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                    + " tbl_EyeDilationExamtemplates.TemplateType FROM  tbl_EyeDilationExamtemplates INNER JOIN "
                                    + " tbl_Patient ON tbl_EyeDilationExamtemplates.PatientId = tbl_Patient.PatientId "
                                    + " where tbl_EyeDilationExamtemplates.MasterId='" + MasterId + "'"

                                    + " union all"

                                    + " SELECT tbl_GeneralEyeExamETemplates.GeneralEyeExamETemplatesId as ExamNo, tbl_GeneralEyeExamETemplates.ExamDate, "
                                    + " tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                    + " tbl_GeneralEyeExamETemplates.TemplateType FROM tbl_GeneralEyeExamETemplates INNER JOIN "
                                    + " tbl_Patient ON tbl_GeneralEyeExamETemplates.PatientId = tbl_Patient.PatientId "
                                    + " where tbl_GeneralEyeExamETemplates.MasterId='" + MasterId + "'"
                                    + " ) as q1 where q1.patientname like '%" + TextBox_Keyword.Text + "%'";
        }
        else
        {
            SDSExamTemplate.SelectCommand = "select q1.ExamNo,q1.ExamDate,q1.PatientId,q1.TemplateType,q1.patientname from "
                                    + " (SELECT tbl_EmergencyExamtemplates.EmergencyExamtemplatesId as ExamNo, tbl_EmergencyExamtemplates.ExamDate "
                                    + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,"
                                    + " tbl_EmergencyExamtemplates.TemplateType FROM  tbl_EmergencyExamtemplates INNER JOIN"
                                    + " tbl_Patient ON tbl_EmergencyExamtemplates.PatientId = tbl_Patient.PatientId "
                                    + " where tbl_EmergencyExamtemplates.MasterId='" + MasterId + "'"

                                    + "  union all"

                                    + " SELECT tbl_EyeDilationExamtemplates.EyeDilationExamtemplatesId as ExamNo, tbl_EyeDilationExamtemplates.ExamDate, "
                                    + " tbl_Patient.PatientId,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                    + " tbl_EyeDilationExamtemplates.TemplateType FROM  tbl_EyeDilationExamtemplates INNER JOIN "
                                    + " tbl_Patient ON tbl_EyeDilationExamtemplates.PatientId = tbl_Patient.PatientId "
                                    + " where tbl_EyeDilationExamtemplates.MasterId='" + MasterId + "'"

                                    + " union all"

                                    + " SELECT tbl_GeneralEyeExamETemplates.GeneralEyeExamETemplatesId as ExamNo, tbl_GeneralEyeExamETemplates.ExamDate, "
                                    + " tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                    + " tbl_GeneralEyeExamETemplates.TemplateType FROM tbl_GeneralEyeExamETemplates INNER JOIN "
                                    + " tbl_Patient ON tbl_GeneralEyeExamETemplates.PatientId = tbl_Patient.PatientId "
                                    + " where tbl_GeneralEyeExamETemplates.MasterId='" + MasterId + "'"
                                    + " ) as q1 where q1.TemplateType like '%" + TextBox_Keyword.Text + "%'";
        }
    }
}