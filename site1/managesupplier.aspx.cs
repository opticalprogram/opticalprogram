﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class supplier : System.Web.UI.Page
{
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Supplier";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        if (Session["lblMessage"] != null)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Session["lblMessage"].ToString();
            Session.Remove("lblMessage");
        }
    }


    protected void GVSupplier_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        lblMessage.Visible = true;

        if (e.Exception == null)
        {
            lblMessage.Text = "Supplier is deleted successfully!";
        }
        else
        {
            lblMessage.CssClass = "alert alert-danger";
            lblMessage.Text = "Error: " + e.Exception;
            e.ExceptionHandled = true;
        }

    }
    protected void GVSupplier_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GVSupplier.SelectedIndex = e.NewSelectedIndex;
            Response.Redirect("supplier.aspx?id=" + GVSupplier.SelectedValue.ToString(), true);
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }

    }
    protected void SDSsupplier_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void Button_Search_Click(object sender, EventArgs e)
    {
        //SELECT [SupplierId], [SupplierName], [SupplierAddress], [SupplierAccount],[SupplierBrand],[Type] FROM [tbl_Supplier] 
        //                   where [tbl_Supplier].MasterId=@MasterId ORDER BY [SupplierName]"
        SDSsupplier.SelectCommand = "SELECT [SupplierId], [SupplierName], [SupplierAddress], [SupplierAccount],[SupplierBrand],[Type] FROM [tbl_Supplier]  "
                                + " where " + DDL_SerchBy.SelectedValue + " like '%" + TextBox_Keyword.Text + "%' and tbl_Supplier.MasterId='" + MasterId + "' ";
    }
}