﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageinvoice : System.Web.UI.Page
{
    General objGeneral;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        Dal objdal = new Dal();
        objGeneral = new General();

        // Set title
        string title = "Invoice";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        if (Session["lblMessage"] != null)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Session["lblMessage"].ToString();
            Session.Remove("lblMessage");
        }
        if (!IsPostBack)
        {           
            string userName = Membership.GetUser().UserName;
            if (!User.IsInRole("Admin") && !User.IsInRole("Master"))
            {
                if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionInvoiceDelete = 1") == "0")
                {
                    GVManageInvoice.Columns[GVManageInvoice.Columns.Count - 1].ControlStyle.CssClass = "btn btn-danger btn-xs disabled";
                }
            }
            if (Request.QueryString["id"] != null)
            {
                SDSManageInvoice.SelectCommand = " SELECT tbl_InvoiceMaster.InvoiceMasterId, tbl_InvoiceMaster.InvoiceNo, tbl_InvoiceMaster.InvoiceMasterDate, "
                          + "  tbl_InvoiceMaster.InvoiceMasterGrandTotal, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                          + " tbl_Patient.Optometrist,aspnet_Users.UserName FROM tbl_InvoiceMaster INNER JOIN "
                          + " tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId INNER join aspnet_Users "
                          + " ON tbl_InvoiceMaster.InvoiceMasterEnterydById = aspnet_Users.UserId where InvoiceMasterIsTemporary='0'"
                          + "  and tbl_InvoiceMaster.PatientId='" + Request.QueryString["id"].ToString() + "' order by tbl_InvoiceMaster.InvoiceNo";
            }
            objGeneral.removeTempEntry();           
        }
    }
    protected void SDSManageInvoice_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void GVManageInvoice_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        lblMessage.Visible = true;
        if (e.Exception == null)
        {
            lblMessage.Text = "Invoice is deleted successfully!";
        }
        else
        {
            lblMessage.CssClass = "alert alert-danger";
            lblMessage.Text = "Error: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
    }
    protected void Button_Search_Click(object sender, EventArgs e)
    {
        if (DDL_SerchBy.SelectedValue != "patientname")
        {
            SDSManageInvoice.SelectCommand = "SELECT tbl_InvoiceMaster.InvoiceMasterId, tbl_InvoiceMaster.InvoiceNo, tbl_InvoiceMaster.InvoiceMasterDate,"
                                         + "  tbl_InvoiceMaster.InvoiceMasterGrandTotal, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                         + "   tbl_Patient.Optometrist,aspnet_Users.UserName FROM tbl_InvoiceMaster INNER JOIN "
                                         + "  tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId  "
                                         + " INNER join aspnet_Users ON tbl_InvoiceMaster.InvoiceMasterEnterydById = aspnet_Users.UserId"
                                         + "   where InvoiceMasterIsTemporary='0' and " + DDL_SerchBy.SelectedValue + " like '%" + TextBox_Keyword.Text + "%' and [tbl_InvoiceMaster].MasterId='" + MasterId + "'";
        }
        else
        {
            SDSManageInvoice.SelectCommand = "SELECT tbl_InvoiceMaster.InvoiceMasterId, tbl_InvoiceMaster.InvoiceNo, tbl_InvoiceMaster.InvoiceMasterDate,"
                                       + "  tbl_InvoiceMaster.InvoiceMasterGrandTotal, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                       + "   tbl_Patient.Optometrist,aspnet_Users.UserName FROM tbl_InvoiceMaster INNER JOIN "
                                       + "  tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId  "
                                       + " INNER join aspnet_Users ON tbl_InvoiceMaster.InvoiceMasterEnterydById = aspnet_Users.UserId"
                                       + "   where InvoiceMasterIsTemporary='0' and  (FirstName like '%" + TextBox_Keyword.Text + "%' or LastName like '%" + TextBox_Keyword.Text + "%') and [tbl_InvoiceMaster].MasterId='" + MasterId + "'";
        }
        TextBox_Keyword.Text = "";
    }
    protected void GVManageInvoice_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GVManageInvoice.SelectedIndex = e.NewSelectedIndex;
        Response.Redirect("~/invoice.aspx?id=" + GVManageInvoice.SelectedValue.ToString(), true);
    }
}