﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HomePage : System.Web.UI.Page
{
    Dal objdal;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["lblError"] != null)
        {
            lblError.Visible = true;
            lblError.Text = Session["lblError"].ToString();
            Session.Remove("lblError");
        }

        // Set title
        string title = ConfigurationManager.AppSettings["ApplicationName"];
        Page.Title = title;
        ((Site)Page.Master).heading = title;

        objdal = new Dal();
        // Hide login control when logged in
        if (User.Identity.IsAuthenticated)
        {

            Login.Visible = false;
            if (User.IsInRole("Admin") || (User.IsInRole("Master")))
            {
                Image_Master.ImageUrl = "~/images/" + Membership.GetUser().Comment.Split(new char[] { '^' })[1];
            }
            else if (User.IsInRole("Other"))
            {
                // find master comment from child                
                StringBuilder sb = new StringBuilder();
                sb.Append("select  aspnet_Membership.Comment from aspnet_Membership where aspnet_Membership.UserId in");
                sb.Append("(select tbl_MasterChildAccount.MasterId from tbl_MasterChildAccount where tbl_MasterChildAccount.UserId='" + Membership.GetUser().ProviderUserKey.ToString() + "')");

                string comment = objdal.Get_SingleValue(sb.ToString());
                Image_Master.ImageUrl = "~/images/" + comment.Split(new char[] { '^' })[1];

            }

            Slider.Style.Add("display", "none");
            MasterImage.Style.Add("display", "inline");
        }

    }
}