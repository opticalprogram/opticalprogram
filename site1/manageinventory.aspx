﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="manageinventory.aspx.cs" Inherits="manageinventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="nav nav-pills PageLink">
<%--        <li class="active"><a href="manageinventory.aspx"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Manage Inventory</a></li>--%>
        <li>
            <asp:HyperLink ID="HL_inventoryFrame" NavigateUrl="~/inventory.aspx?t=Frame" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Frame</asp:HyperLink></li>
         <li>
            <asp:HyperLink ID="HL_inventoryOphtalmic" NavigateUrl="~/inventory.aspx?t=Ophtalmic lens" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Ophtalmic lens</asp:HyperLink></li>
         <li>
            <asp:HyperLink ID="HL_inventoryContact" NavigateUrl="~/inventory.aspx?t=Contact lens" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Contact lens</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_Supplier" NavigateUrl="~/managesupplier.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Supplier</asp:HyperLink></li>
    
    </ul>
    <div class="form-group col-lg-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-success message"></asp:Label>
    </div>
    <div class="form-inline col-lg-12" style="margin-bottom: 10px; padding-left: 0px !important;">
        <div class="form-group">
            <label for="DDL_SerchBy">Search By:</label>
            <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server">
                <asp:ListItem Value="ItemId">Item No</asp:ListItem>
                <asp:ListItem Value="ItemName">Item Name</asp:ListItem>
                <asp:ListItem Value="ItemInvoiceNumber">Invoice No</asp:ListItem>
                <asp:ListItem Value="ItemBrand">Brand</asp:ListItem>
                <asp:ListItem Value="ItemModel">Model</asp:ListItem>
                <asp:ListItem Value="ItemType">Type</asp:ListItem>
                <asp:ListItem Value="ItemColor">Color</asp:ListItem>
                 <asp:ListItem Value="Quantity">Quantity ></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label for="TextBox_Keyword">Keyword:</label>
            <asp:TextBox ID="TextBox_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="Button_Search" ClientIDMode="Static" CssClass="btn btn-default" runat="server" Text="Search" OnClick="Button_Search_Click" />
            <asp:HyperLink ID="HlListAll" runat="server" CssClass="btn btn-group-xs btn-default" NavigateUrl="~/manageinventory.aspx">List all</asp:HyperLink>
        </div>
    </div>
    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView ID="GVInventory" CssClass="table table-hover table-striped" runat="server" PageSize="8" AutoGenerateColumns="False" DataKeyNames="ItemId" DataSourceID="SDSInventory" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" OnRowDeleted="GVInventory_RowDeleted" OnSelectedIndexChanging="GVInventory_SelectedIndexChanging">
            <Columns>
                <asp:BoundField DataField="ItemId" HeaderText="Item No" ReadOnly="True" InsertVisible="False" SortExpression="ItemId"></asp:BoundField>
                <asp:HyperLinkField DataTextField="ItemName" HeaderText="Item Name" SortExpression="ItemName" DataNavigateUrlFields="ItemId,ItemType" DataNavigateUrlFormatString="inventory.aspx?id={0}&t={1}"></asp:HyperLinkField>
                <asp:BoundField DataField="ItemEntryDate" HeaderText="Entry Date" SortExpression="ItemEntryDate" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="ItemInvoiceNumber" HeaderText="Invoice No" SortExpression="ItemInvoiceNumber"></asp:BoundField>
                 <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"></asp:BoundField>
                <asp:BoundField DataField="ItemBrand" HeaderText="Brand" SortExpression="ItemBrand"></asp:BoundField>               
                <asp:BoundField DataField="ItemModel" HeaderText="Model" SortExpression="ItemModel"></asp:BoundField>
                <asp:TemplateField HeaderText="Type" SortExpression="ItemType">                   
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Bind("ItemType") %>' ID="lbl_Type"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="ItemCostPrice" HeaderText="Cost Price" SortExpression="ItemCostPrice" DataFormatString="{0:0.00}"></asp:BoundField>
                <asp:BoundField DataField="ItemSalePrice" HeaderText="Sale Price" SortExpression="ItemSalePrice" DataFormatString="{0:0.00}"></asp:BoundField>
                 <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" SortExpression="ItemQuantity"></asp:BoundField>
                 <asp:BoundField DataField="ItemColor" HeaderText="Color" SortExpression="ItemColor"></asp:BoundField>
                <asp:CommandField ShowSelectButton="true" SelectText="Edit" ControlStyle-CssClass="btn btn-success btn-xs"></asp:CommandField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn btn-danger btn-xs"></asp:CommandField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>
        <asp:SqlDataSource runat="server" ID="SDSInventory" OnSelecting="SDSInventory_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            SelectCommand="SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemEntryDate, tbl_Item.ItemInvoiceNumber, tbl_Item.ItemBrand, tbl_Item.ItemModel,
                           tbl_Item.ItemType, tbl_Item.ItemGlass, tbl_Item.ItemCostPrice, 
                           tbl_Item.ItemSalePrice, tbl_Item.ItemQuantity, tbl_Item.ItemColor, tbl_Supplier.SupplierName FROM tbl_Item INNER JOIN
                           tbl_Supplier ON tbl_Item.ItemSupplierId = tbl_Supplier.SupplierId where tbl_Item.MasterId=@MasterId ORDER BY [ItemId]"
            DeleteCommand="DELETE FROM [tbl_Item] WHERE [ItemId] = @ItemId">
            <SelectParameters>
                <asp:Parameter Name="MasterId"/>
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="ItemId" Type="Int32"></asp:Parameter>
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

