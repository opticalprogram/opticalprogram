﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="invoice.aspx.cs" Inherits="invoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/ProjectHelper.js"></script>
    <script>

        $(document).ready(function () {

            var id = getUrlVars()["id"];
            if (id == null) {
                $("#Txt_InvoiceDate").val(currentdate());                
            }
           

            doMasking();
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(doMasking);

        });

        function doMasking() {
            //Masking
            jQuery(function ($) {
                $("#TextBox_DateOfBirth").mask("99-99-9999");
                $("#Txt_InvoiceDate").mask("99-99-9999");
                $("#TextBox_HomePhone").mask("(999) 999-9999");
                $("#Txt_DeliveryDate").mask("99-99-9999");
            });
        }

        function openAddProductModel() {
            $('#AddProductModal').modal('show');
        }

        //function SetCurrentDateToDelivary() {
        //    $("#Txt_DeliveryDate").val(currentdate());
        //}

        

        function distroymodal() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_manageinvoice" NavigateUrl="~/manageinvoice.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Manage Invoice</asp:HyperLink></li>
    </ul>

    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_InvoiceNumber">Invoice number:</label>
            <asp:TextBox ID="Txt_InvoiceNumber" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_InvoiceDate">Date:</label>
            <asp:TextBox ID="Txt_InvoiceDate" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Patient">Patient:</label>&nbsp;
             <span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" data-target="#PatientModal"></span>
            <asp:UpdatePanel runat="server" ID="UpdatePaneladdpatient">
                <ContentTemplate>
                    <asp:DropDownList ID="DDL_Patient" CssClass="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DDL_Patient_SelectedIndexChanged"></asp:DropDownList>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="DDL_Patient" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Optometrist">Optometrist:</label>
            <asp:UpdatePanel runat="server" ID="UpdatePanelOptometrist">
                <ContentTemplate>
                    <asp:TextBox ID="Txt_Optometrist" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="form-group btn-group col-lg-12 col-md-12 col-sm-12">
        <span id="AddProduct" runat="server" class="glyphicon glyphicon-user btn btn-sm btn-default" data-toggle="modal" style="cursor: pointer;" data-target="#AddProductModal"><span style="font-family: Arial;">&nbsp;Add Product</span></span>
        <span id="AddService" runat="server" class="glyphicon glyphicon-user btn btn-sm btn-default" data-toggle="modal" style="cursor: pointer;" data-target="#ModalAddService"><span style="font-family: Arial;">&nbsp;Add Service</span></span>
    </div>

    <div class="form-group btn-group col-lg-12 col-md-12 col-sm-12">
        <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
            <asp:UpdatePanel ID="UpdatePanelGVInvoice" runat="server">
                <ContentTemplate>
                    <asp:GridView Caption="Note: Please update delivery date of a product when the product delivers." CaptionAlign="Bottom" ID="GVInvoice" CssClass="table table-hover table-striped" runat="server" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No product or service added!" GridLines="None" DataKeyNames="InvoiceDetailId" OnRowUpdating="GVInvoice_RowUpdating" DataSourceID="SDSInvoice">
                        <Columns>
                            <asp:BoundField DataField="InvoiceDetailId" ReadOnly="true" HeaderText="InvoiceDetailId" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="InvoiceDetailDescription" ReadOnly="true" HeaderText="Description" SortExpression="InvoiceDetailDescription"></asp:BoundField>
                            <asp:BoundField DataField="InvoiceDetailQuantity" ReadOnly="true" HeaderText="Quantity" SortExpression="InvoiceDetailQuantity"></asp:BoundField>
                            <asp:BoundField DataField="InvoiceDetailUnitprice" ReadOnly="true" HeaderText="Unit price" DataFormatString="{0:0.00}" SortExpression="InvoiceDetailUnitprice"></asp:BoundField>
                            <asp:BoundField DataField="SubTotal" ReadOnly="true" HeaderText="Sub Total" DataFormatString="{0:0.00}" SortExpression="SubTotal"></asp:BoundField>
                            <asp:BoundField DataField="InvoiceDetailDiscount" ReadOnly="true" HeaderText="Discount" DataFormatString="{0:0.00}" SortExpression="InvoiceDetailDiscount"></asp:BoundField>
                            <asp:BoundField DataField="InvoiceDetailTax" ReadOnly="true" HeaderText="Tax" DataFormatString="{0:0.00}" SortExpression="InvoiceDetailTax"></asp:BoundField>
                            <asp:BoundField DataField="Total" ReadOnly="true" HeaderText="Total" DataFormatString="{0:0.00}" SortExpression="Total"></asp:BoundField>
                            <asp:BoundField DataField="Optometrist" ReadOnly="true" HeaderText="Optometrist" SortExpression="Optometrist"></asp:BoundField>
                            <asp:BoundField DataField="patientname" ReadOnly="true" HeaderText="Patient" SortExpression="patientname"></asp:BoundField>
                            <asp:TemplateField HeaderText="Delivery" SortExpression="InvoiceDetailDeliveryDate">
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" Text='<%# Bind("InvoiceDetailDeliveryDate", "{0:dd-MM-yyyy}") %>' ClientIDMode="Static" MaxLength="10" Width="100" placeholder="DD-MM-YYYY" ID="Txt_DeliveryDate"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("InvoiceDetailDeliveryDate", "{0:dd-MM-yyyy}") %>' ID="Label1"></asp:Label>                                   
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:CommandField ShowEditButton="True" ControlStyle-CssClass="btn btn-success btn-xs"></asp:CommandField>
                            <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn btn-danger btn-xs"></asp:CommandField>
                        </Columns>
                        <PagerStyle CssClass="GridPager" />
                        <SortedAscendingHeaderStyle CssClass="asc" />
                        <SortedDescendingHeaderStyle CssClass="desc" />
                    </asp:GridView>
                    <asp:SqlDataSource OnDeleted="SDSInvoice_Deleted" runat="server" ID="SDSInvoice" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                        DeleteCommand="DELETE FROM [dbo].[tbl_InvoiceDetail] WHERE tbl_InvoiceDetail.InvoiceDetailId=@InvoiceDetailId"
                        SelectCommand="SELECT tbl_InvoiceDetail.InvoiceDetailId,tbl_InvoiceDetail.InvoiceDetailDescription, tbl_InvoiceDetail.InvoiceDetailQuantity,
                                       tbl_InvoiceDetail.InvoiceDetailUnitprice, tbl_InvoiceDetail.InvoiceDetailQuantity * tbl_InvoiceDetail.InvoiceDetailUnitprice AS SubTotal,
                                       tbl_InvoiceDetail.InvoiceDetailDiscount, tbl_InvoiceDetail.InvoiceDetailTax,
                                       ((tbl_InvoiceDetail.InvoiceDetailQuantity * tbl_InvoiceDetail.InvoiceDetailUnitprice) - tbl_InvoiceDetail.InvoiceDetailDiscount)
                                       + (tbl_InvoiceDetail.InvoiceDetailTax) AS Total,tbl_Patient.Optometrist,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,
                                       tbl_InvoiceMaster.InvoiceMasterId, tbl_InvoiceDetail.InvoiceDetailDeliveryDate FROM tbl_InvoiceMaster INNER JOIN tbl_InvoiceDetail 
                                       ON tbl_InvoiceMaster.InvoiceMasterId = tbl_InvoiceDetail.InvoiceMasterId
                                       INNER JOIN tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId
                                       where tbl_InvoiceMaster.InvoiceMasterId=@InvoiceMasterId"
                         UpdateCommand="Update [dbo].[tbl_InvoiceDetail] set InvoiceDetailDescription='ff' where 1=2">
                        <SelectParameters>
                            <asp:SessionParameter Name="InvoiceMasterId" SessionField="InvoiceMasterId" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <asp:UpdatePanel ID="UpdatePanelTotal" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                    <label for="Txt_Total">Total:</label>&nbsp;
                    <asp:CompareValidator CssClass="label label-danger Errormessage" ID="cvTotal" ControlToValidate="Txt_Total" ValueToCompare="0" Operator="GreaterThan" runat="server" ErrorMessage="Required!"></asp:CompareValidator>
                    <asp:TextBox ID="Txt_Total" CssClass="form-control" Text="0" Enabled="false" runat="server"></asp:TextBox>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Payment">Payment:</label>
            <asp:TextBox ID="Txt_Payment" Text="0.00" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_ThirdpartyBalance">Third-party Balance:</label>
            <asp:TextBox ID="Txt_ThirdpartyBalance" Text="0.00" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_PatientBalance">Patient Balance:</label>
            <asp:TextBox ID="Txt_PatientBalance" Text="0.00" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Note">Note:</label>
            <asp:TextBox ID="Txt_Note" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <br />
            <asp:Button ID="Button_Submit" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" OnClick="Button_Submit_Click" Text="SAVE" />
        </div>
    </div>

    <!-- Modal Add Product -->
    <asp:UpdatePanel ID="UpdatePanelAddProductModel" runat="server">
        <ContentTemplate>
            <div id="ModalAddProduct" runat="server">
                <div class="modal modal-open" id="AddProductModal" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal Add Product content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" onclick="distroymodal();" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Product</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label for="txt_ItemQuantity">Quantity:</label>
                                    <asp:TextBox ID="txt_ItemQuantity" CssClass="form-control" Text="1" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label for="txt_ItemDiscount">Discount:</label>
                                    <asp:TextBox ID="txt_ItemDiscount" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label for="txt_ItemTax">Tax:</label>
                                    <asp:TextBox ID="txt_ItemTax" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label for="txt_ItemDescription">Description:</label>
                                    <asp:TextBox ID="txt_ItemDescription" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label for="DDL_SerchBy">Search By:</label>
                                    <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server">
                                        <asp:ListItem Value="ItemId">Item No</asp:ListItem>
                                        <asp:ListItem Value="ItemName">Item Name</asp:ListItem>
                                        <asp:ListItem Value="ItemBrand">Brand</asp:ListItem>
                                        <asp:ListItem Value="ItemModel">Model</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label for="TextBox_Item_Keyword">Keyword:</label>
                                    <asp:TextBox ID="TextBox_Item_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_Keyword" Display="Dynamic" runat="server" ValidationGroup="Button_ItemSerach_Group" ControlToValidate="TextBox_Item_Keyword" ErrorMessage="Keyword is required!"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                    <label>&nbsp;</label>
                                    <br />
                                    <asp:Button ID="Button_ItemSearch" ClientIDMode="Static" ValidationGroup="Button_ItemSerach_Group" OnClick="Button_ItemSearch_Click" CssClass="btn btn-default" runat="server" Text="Search" />
                                </div>
                                <div class="table table-responsive">
                                    <asp:GridView ID="GVInventory" CssClass="table table-hover table-striped" runat="server" PageSize="3" AutoGenerateColumns="False" DataKeyNames="ItemId" DataSourceID="SDSInventory" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" OnSelectedIndexChanging="GVInventory_SelectedIndexChanging">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="true" SelectText="Add" ControlStyle-CssClass="btn btn-success btn-xs"></asp:CommandField>
                                            <asp:BoundField DataField="ItemId" HeaderText="Number" ReadOnly="True" InsertVisible="False" SortExpression="ItemId"></asp:BoundField>
                                            <asp:BoundField DataField="ItemName" HeaderText="Name" SortExpression="ItemName"></asp:BoundField>                                            
                                            <asp:BoundField DataField="ItemBrand" HeaderText="Brand" SortExpression="ItemBrand"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Quantity" SortExpression="ItemQuantity">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%# Bind("ItemQuantity") %>' ID="Lbl_Quantity"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="ItemModel" HeaderText="Model" SortExpression="ItemModel"></asp:BoundField>
                                            <asp:BoundField HeaderText="Unit Price" DataField="ItemSalePrice" SortExpression="ItemSalePrice" DataFormatString="{0:0.00}"></asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridPager" />
                                        <SortedAscendingHeaderStyle CssClass="asc" />
                                        <SortedDescendingHeaderStyle CssClass="desc" />
                                    </asp:GridView>
                                    <asp:SqlDataSource runat="server" ID="SDSInventory" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                                        SelectCommand="SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemBrand, tbl_Item.ItemQuantity, tbl_Item.ItemModel FROM tbl_Item where 1=2"></asp:SqlDataSource>
                                </div>

                            </div>
                            <div class="modal-footer">
                               <%-- <asp:Button ID="Button2" runat="server" Text="Cancel" OnClientClick="distroymodal();" />--%>
                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="distroymodal();">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal Add Service -->
    <asp:UpdatePanel ID="UpdatePanelAddService" runat="server">
        <ContentTemplate>
            <div id="ServiceModal" runat="server">
                <div class="modal modal-open" id="ModalAddService" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal Add Service content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" onclick="distroymodal();" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Service</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="txt_ServiceQuantity">Quantity:</label>
                                    <asp:TextBox ID="txt_ServiceQuantity" CssClass="form-control" Text="1" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="txt_ServiceDiscount">Discount:</label>
                                    <asp:TextBox ID="txt_ServiceDiscount" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="txt_ServiceTax">Tax:</label>
                                    <asp:TextBox ID="txt_ServiceTax" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="txt_ServiceDescription">Description:</label>
                                    <asp:TextBox ID="txt_ServiceDescription" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="DDL_ExamType">Exam Type:</label>
                                    <asp:DropDownList CssClass="form-control" ID="DDL_ExamType" ClientIDMode="Static" runat="server" DataSourceID="SDSExamType" DataTextField="ExamType" DataValueField="ExamTypeId"></asp:DropDownList>
                                    <asp:SqlDataSource runat="server" ID="SDSExamType" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>' SelectCommand="SELECT [ExamTypeId], [ExamType] FROM [tbl_ExamType] ORDER BY [ExamType]"></asp:SqlDataSource>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="txt_UnitPrice">Unit Price:</label>
                                    <asp:TextBox ID="txt_UnitPrice" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txt_UnitPrice" ValidationGroup="Button_Service_Group" ID="RFV_UnitPrice" runat="server" ErrorMessage="Unit Price is required!"></asp:RequiredFieldValidator>
                                </div>
                                <div class="table table-responsive">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Btn_Service" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Service_Group" OnClientClick="distroymodal();" OnClick="Btn_Service_Click" Text="Add" />
                                <button type="button" class="btn btn-default" onclick="distroymodal();" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal Patient -->
    <asp:UpdatePanel ID="UpdatePanelPatientModel" runat="server">
        <ContentTemplate>
            <div id="ModalPatient" runat="server">
                <div class="modal fade modal-open" id="PatientModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal Patient content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add new patient</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <label for="TextBox_LastName">Last Name:</label>
                                    <asp:TextBox ID="TextBox_LastName" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="RFV_LastName" runat="server" ValidationGroup="Button_Patient_Group" ControlToValidate="TextBox_LastName" ErrorMessage="Last name is required!"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <label for="TextBox_FirstName">First Name:</label>
                                    <asp:TextBox ID="TextBox_FirstName" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="TextBox_FirstName" ValidationGroup="Button_Patient_Group" ID="RFV_FirstName" runat="server" ErrorMessage="First Name is required!"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <label for="TextBox_DateOfBirth">Date of Birth:</label>
                                    <asp:TextBox ID="TextBox_DateOfBirth" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="TextBox_DateOfBirth" ValidationGroup="Button_Patient_Group" ID="RFV_TextBox_DateOfBirth" runat="server" ErrorMessage="Date Of Birth is required!"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <label for="TextBox_HomePhone">Home Phone:</label>&nbsp;
                                    <asp:TextBox ID="TextBox_HomePhone" ClientIDMode="Static" MaxLength="14" Placeholder="(XXX)-XXX-XXXX" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="TextBox_HomePhone" ID="RFV_HomePhone" ValidationGroup="Button_Patient_Group" runat="server" ErrorMessage="Home Phone is required!"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <label for="TextBox_Optometrist">Optometrist:</label>
                                    <asp:TextBox ID="TextBox_Optometrist" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="TextBox_Optometrist" ValidationGroup="Button_Patient_Group" ID="RFV_TextBox_Optometrist" runat="server" ErrorMessage="Optometrist is required!"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_Patient" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Patient_Group" OnClientClick="distroymodal();" OnClick="Button_Patient_Click" Text="Save" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

