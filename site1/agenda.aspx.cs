﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;


public partial class agenda : System.Web.UI.Page
{
    public string MasterId;
    Dal objdal;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Agenda";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        objdal = new Dal();       
        MasterId = Session["MasterId"].ToString();       
        EventDAO.MasterId = Session["MasterId"].ToString();
        if (!IsPostBack)
        {
            patientfill(txt_Keyword_Patient.Text); 
        }
    }

    //this method only updates title and description
    //this is called when a event is clicked on the calendar
    [System.Web.Services.WebMethod(true)]
    public static string UpdateEvent(CalendarEvent cevent)
    {
        try
        {
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(cevent.id))
            {
                //if (CheckAlphaNumeric(cevent.title) && CheckAlphaNumeric(cevent.description))
                //{
                EventDAO.updateEvent(cevent.id, cevent.ProfessionalId, cevent.PatientId, cevent.description, cevent.ExamTypeId,
                    cevent.Status,Convert.ToDateTime(cevent.start).ToUniversalTime(),Convert.ToDateTime(cevent.end).ToUniversalTime());

                //return "updated event with id:" + cevent.id + " update title to: " + cevent.title +
                //    " update description to: " + cevent.description;

                return "Appointment updated successfully!";
                //}

            }

            //return "unable to update event with id:" + cevent.id + " title : " + cevent.title +
            //    " description : " + cevent.description;

            return "Error: Unable to update the appointment!";
        }
        catch (Exception ex)
        {
            return "Error: " + ex.Message;

        }

    }

    //this method only updates start and end time
    //this is called when a event is dragged or resized in the calendar
    [System.Web.Services.WebMethod(true)]
    public static string UpdateEventTime(ImproperCalendarEvent improperEvent)
    {
        try
        {
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(improperEvent.id))
            {
                // FullCalendar 1.x
                //EventDAO.updateEventTime(improperEvent.id,
                //    DateTime.ParseExact(improperEvent.start, "dd-MM-yyyy hh:mm:ss tt", null),
                //    DateTime.ParseExact(improperEvent.end, "dd-MM-yyyy hh:mm:ss tt", null));

                // FullCalendar 2.x
                EventDAO.updateEventTime(improperEvent.id,
                                         Convert.ToDateTime(improperEvent.start).ToUniversalTime(),
                                         Convert.ToDateTime(improperEvent.end).ToUniversalTime(),
                                         improperEvent.allDay);  //allDay parameter added for FullCalendar 2.x

                return "updated event with id:" + improperEvent.id + " update start to: " + improperEvent.start +
                    " update end to: " + improperEvent.end;
            }

            return "unable to update event with id: " + improperEvent.id;
        }
        catch (Exception ex)
        {
            return "Error: " + ex.Message;
        }

    }

    // add exam type
    [System.Web.Services.WebMethod]
    public static String addexamtype(string examtype)
    {
        try
        {
            if (EventDAO.addexamtype(examtype) == 1)
            {
                return "Exam type added successfully!";
            }
            return "Error: Unable to add the exam type!";
        }
        catch (Exception ex)
        {
            return "Error: " + ex.Message;
        }
    }

    // Get Selected Patient Data
    [System.Web.Services.WebMethod]
    public static String GetPatientData(string PatinetId)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = EventDAO.GetPatientData(PatinetId);
            return dt.Rows[0]["LastName"].ToString() + "^" + dt.Rows[0]["FirstName"].ToString() + "^" + dt.Rows[0]["DateOfBirth"].ToString() + "^" + dt.Rows[0]["HomePhone"].ToString() + "^" + dt.Rows[0]["CellPhone"].ToString();
        }
        catch (Exception ex)
        {
            return "Error: " + ex.Message;
        }
    }

    // Get Setting Data
    [System.Web.Services.WebMethod]
    public static String GetSettingData()
    {
        string dt;
        try
        {
            dt = EventDAO.GetSettingData();
            return dt;
        }
        catch (Exception ex)
        {
            return "Error: " + ex.Message;
        }
    }


    //called when delete button is pressed
    [System.Web.Services.WebMethod(true)]
    public static String deleteEvent(string[] eventToDelete)
    {
        //idList is stored in Session by JsonResponse.ashx for security reasons
        //whenever any event is update or deleted, the event id is checked
        //whether it is present in the idList, if it is not present in the idList
        //then it may be a malicious user trying to delete someone elses events
        //thus this checking prevents misuse
        try
        {
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(Convert.ToInt32(eventToDelete[0])))
            {
                EventDAO.deleteEvent(Convert.ToInt32(eventToDelete[0]), eventToDelete[1].ToString());
                // return "deleted event with id:" + id;
                return "Appointment deleted successfully!";
            }

            return "Error: Unable to delete the appointment!";
        }
        catch (Exception ex)
        {

            return "Error: " + ex.Message;
        }

    }

    //called when Add button is clicked
    //this is called when a mouse is clicked on open space of any day or dragged 
    //over mutliple days
    [System.Web.Services.WebMethod]
    public static string addEvent(ImproperCalendarEvent[] improperEvents)
    {
        // FullCalendar 1.x
        //CalendarEvent cevent = new CalendarEvent()
        //{
        //    title = improperEvent.title,
        //    description = improperEvent.description,
        //    start = DateTime.ParseExact(improperEvent.start, "dd-MM-yyyy hh:mm:ss tt", null),
        //    end = DateTime.ParseExact(improperEvent.end, "dd-MM-yyyy hh:mm:ss tt", null)
        //};
        string key = "0";
        HttpContext.Current.Session.Remove("NewPatientId");
        try
        {
            // FullCalendar 2.x
            // List<int> keys = new List<int>();

            foreach (ImproperCalendarEvent improperEvent in improperEvents)
            {
                CalendarEvent cevent = new CalendarEvent()
                {
                    ProfessionalId = improperEvent.ProfessionalId,
                    PatientId = improperEvent.PatientId,
                    description = improperEvent.description,
                    ExamTypeId = improperEvent.ExamTypeId,
                    start = Convert.ToDateTime(improperEvent.start).ToUniversalTime(),
                    end = Convert.ToDateTime(improperEvent.end).ToUniversalTime(),
                    allDay = improperEvent.allDay,
                    LastName = improperEvent.LastName,
                    FirstName = improperEvent.FirstName,
                    DateOfBirth = improperEvent.DateOfBirth,
                    HomePhone = improperEvent.HomePhone,
                    CellPhone = improperEvent.CellPhone
                };
                if ( HttpContext.Current.Session["NewPatientId"]!=null &&  HttpContext.Current.Session["NewPatientId"]!="")
                {
                    cevent.PatientId = Guid.Parse(HttpContext.Current.Session["NewPatientId"].ToString());
                }
                if (CheckAlphaNumeric(cevent.description))
                {
                    int CheckOverlap = EventDAO.CheckScheduleExist(cevent);
                    if (CheckOverlap == 0)
                    {
                        key = EventDAO.addEvent(cevent).ToString();

                        //List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];

                        //if (idList != null)
                        //{
                        //    idList.Add(key);
                        //}

                        //keys.Add(key); //return the primary key of the added cevent object
                    }
                    else
                    {
                        return key = "xx," + cevent.start;
                    }
                }
            }

            //if (keys.Count >= 1)
            //{
            //    return keys.ToArray();
            //}

            // return new int[] { -1 }; //return a negative number just to signify nothing has been added
            return key;
        }
        catch (Exception ex)
        {
            return "Error: " + ex.Message;
        }

    }

    private static bool CheckAlphaNumeric(string str)
    {
        return Regex.IsMatch(str, @"^[a-zA-Z0-9 ]*$");
    }

    // Setting Set
    protected void Btn_Setting_Click(object sender, EventArgs e)
    {
        try
        {
            string TimeData = DDL_MinHour.SelectedValue + ":" + DDL_MinMinute.SelectedValue + "^" + DDL_MaxHour.SelectedValue + ":" + DDL_MaxMinute.SelectedValue + "^" + DDL_Slot.SelectedValue;
            EventDAO.SetSettingData(TimeData);

            List<string> SelectedProfessionals = new List<string>();
            List<string> MondayProfessionals = new List<string>();
            List<string> TuesdayProfessionals = new List<string>();
            List<string> WednesdayProfessionals = new List<string>();
            List<string> ThursdayProfessionals = new List<string>();
            List<string> FridayProfessionals = new List<string>();
            List<string> SaturdayProfessionals = new List<string>();
            List<string> SundayProfessionals = new List<string>();




            foreach (ListItem Professional in Lst_Monday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    MondayProfessionals.Add(Professional.Value);
                }
            }
            foreach (ListItem Professional in Lst_Tuesday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    TuesdayProfessionals.Add(Professional.Value);
                }
            }
            foreach (ListItem Professional in Lst_Wednesday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    WednesdayProfessionals.Add(Professional.Value);
                }
            }
            foreach (ListItem Professional in Lst_Thursday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    ThursdayProfessionals.Add(Professional.Value);
                }
            }
            foreach (ListItem Professional in Lst_Friday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    FridayProfessionals.Add(Professional.Value);
                }
            }
            foreach (ListItem Professional in Lst_Saturday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    SaturdayProfessionals.Add(Professional.Value);
                }
            }
            foreach (ListItem Professional in Lst_Sunday.Items)
            {
                if (Professional.Selected)
                {
                    SelectedProfessionals.Add(Professional.Value);
                    SundayProfessionals.Add(Professional.Value);
                }
            }



            foreach (string selectedProfessional in SelectedProfessionals)
            {
                string usrComment = "PROFESSIONAL";
                if (MondayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Mon";
                }
                if (TuesdayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Tue";
                }
                if (WednesdayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Wed";
                }
                if (ThursdayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Thu";
                }
                if (FridayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Fri";
                }
                if (SaturdayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Sat";
                }
                if (SundayProfessionals.Contains(selectedProfessional))
                {
                    usrComment += "^Sun";
                }
                MembershipUser usr = Membership.GetUser(new Guid(selectedProfessional));
                usr.Comment = usrComment;
                Membership.UpdateUser(usr);
            }



        }
        catch (Exception)
        {

        }

    }

    protected void Lst_Sunday_PreRender(object sender, EventArgs e)
    {

        string dt = EventDAO.GetSettingData();
        DDL_MinHour.SelectedValue = dt.Split(new char[] { '^' })[3].Substring(0, 2);
        DDL_MinMinute.SelectedValue = dt.Split(new char[] { '^' })[3].Substring(2, 2);
        DDL_MaxHour.SelectedValue = dt.Split(new char[] { '^' })[4].Substring(0, 2);
        DDL_MaxMinute.SelectedValue = dt.Split(new char[] { '^' })[4].Substring(2, 2);
        DDL_Slot.SelectedValue = dt.Split(new char[] { '^' })[5].ToString();



        foreach (ListItem Professional in Lst_Sunday.Items)
        {
            MembershipUser usr = Membership.GetUser(new Guid(Professional.Value));
            if (usr.Comment.Contains("Mon"))
            {
                Lst_Monday.Items.FindByValue(Professional.Value).Selected = true;
            }
            if (usr.Comment.Contains("Tue"))
            {
                Lst_Tuesday.Items.FindByValue(Professional.Value).Selected = true;
            }
            if (usr.Comment.Contains("Wed"))
            {
                Lst_Wednesday.Items.FindByValue(Professional.Value).Selected = true;
            }
            if (usr.Comment.Contains("Thu"))
            {
                Lst_Thursday.Items.FindByValue(Professional.Value).Selected = true;
            }
            if (usr.Comment.Contains("Fri"))
            {
                Lst_Friday.Items.FindByValue(Professional.Value).Selected = true;
            }
            if (usr.Comment.Contains("Sat"))
            {
                Lst_Saturday.Items.FindByValue(Professional.Value).Selected = true;
            }
            if (usr.Comment.Contains("Sun"))
            {
                Lst_Sunday.Items.FindByValue(Professional.Value).Selected = true;
            }
        }
    }
    protected void SDSPatient_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void SDSProfession_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void SDSprofessional_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void SDSStatus_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void LinkButton_patient_Click(object sender, EventArgs e)
    {
        try
        {           
            patientfill(Hdn_keyword.Value);         
            
        }
        catch (Exception ex)
        {
            
           
        }
    }

    protected void patientfill(string keyword)
    {
        txt_Keyword_Patient.Text = keyword;
        DataTable dt = objdal.GET_DATATABLE("SELECT [PatientId], [FirstName]+' '+[LastName]+' '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Title] FROM [tbl_Patient]"
                                        + "where (FirstName like '%" + keyword + "%' or LastName like '%" + keyword + "%') and [tbl_Patient].MasterId='" + MasterId + "'");
        addPatient.DataSource = dt;
        //addPatient.DataTextField = "Title";
        //addPatient.DataValueField = "PatientId";
        addPatient.DataBind();        
    }
    
}