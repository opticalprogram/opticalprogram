﻿var currentUpdateEvent;
var addStartDate;
var addEndDate;
var globalAllDay;

//var IsFirstEventAdded = false;

// page script start
function gotomydate() {
    var mydate = $('#TextGotoDate').val().replace("_", "");
    if (mydate.length == 10) {
        var date_regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        if ((date_regex.test(mydate))) {
            var pieces = mydate.split('-');
            pieces.reverse();
            mydate = pieces.join('-');
            var calendar = $('#calendar').fullCalendar('getCalendar');
            var m = calendar.moment(mydate);
            $('#calendar').fullCalendar('gotoDate', m);
        }
    }
}

function currentdate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = dd + '-' + mm + '-' + yyyy;
    $("#TextGotoDate").val(today);
}

jQuery(function ($) {
    $("#TextGotoDate").mask("99-99-9999");
    $("#Txt_DateofBirth").mask("99-99-9999");
    $("#Txt_HomePhone").mask("(999) 999-9999");
    $("#Txt_CellPhone").mask("(999) 999-9999");
});

function closedialog() {
    $('#addDialog').dialog("close");
}

function addexamtype() {
    var examtype = $("#TextBox_Examtype").val();
    if (examtype != "") {
        PageMethods.addexamtype(examtype, OnSuccess);
    }
    $('#ExamtypeModal').modal('toggle');
}


function OnSuccess(result) {
    $('#lblMessage').css('display', 'block');
    $("#lblMessage").text(result);
    $("#lblMessage").delay(3000).fadeOut();
}

function OnPatientSelection() {
    var SelectedPatient = $("#addPatient option:selected").val();
    if (SelectedPatient != "55555555-5555-5555-5555-555555555555") {
        PageMethods.GetPatientData(SelectedPatient, OnGetPatientData);
        $('#Txt_LastName').attr('disabled', true);
        $('#Txt_FirstName').attr('disabled', true);
        $('#Txt_DateofBirth').attr('disabled', true);
        $('#Txt_HomePhone').attr('disabled', true);
        $('#Txt_CellPhone').attr('disabled', true);
    }
    else {
        clearTextBox();
        enableTextbox();
    }
}
function enableTextbox() {
    $('#Txt_LastName').attr('disabled', false);
    $('#Txt_FirstName').attr('disabled', false);
    $('#Txt_DateofBirth').attr('disabled', false);
    $('#Txt_HomePhone').attr('disabled', false);
    $('#Txt_CellPhone').attr('disabled', false);
}
function clearTextBox() {
    $('#Txt_LastName').val("");
    $('#Txt_FirstName').val("");
    $('#Txt_DateofBirth').val("");
    $('#Txt_HomePhone').val("");
    $('#Txt_CellPhone').val("");

}
function OnGetPatientData(result) {
    var patientdata = result.split('^');
    $('#Txt_LastName').val(patientdata[0]);
    $('#Txt_FirstName').val(patientdata[1]);
    $('#Txt_DateofBirth').val(patientdata[2]);
    $('#Txt_HomePhone').val(patientdata[3]);
    $('#Txt_CellPhone').val(patientdata[4]);
}

//function UpdateTime()
//{   
//    //$("#addExamType option:selected").text()
//    var min = $("#DDL_MinHour option:selected").val() + ":" + $("#DDL_MinMinute option:selected").val() + ":00";
//    var max = $("#DDL_MaxHour option:selected").val() + ":" + $("#DDL_MaxMinute option:selected").val() + ":00";

//    var SettingsData = min+"^"+max;   
//    PageMethods.SetSettingData(SettingsData);
//    //alert('heee');
//}
// page script end

function updateEvent(event, element) {
    //alert(event.description);


    if ($(this).data("qtip")) $(this).qtip("destroy");

    currentUpdateEvent = event;

    $('#updatedialog').dialog('open');
    $("#eventPatient").val(event.PatientId);
    $("#eventDesc").val(event.description);
    $("#eventType").val(event.ExamTypeId);
    $("#eventProfessional").val(event.ProfessionalId);
    $("#eventStatus").val(event.Status);
    $("#eventId").val(event.id);   
    $("#eventStart").val(event.start.toString().substr(3,18));
    $("#eventEnd").val(event.end.toString().substr(3, 18));  
  
    return false;
}

function updateSuccess(updateResult) {
    // alert(updateResult);   
    //$("#lblMessage").attr("Visible", "true");
    addpatientddl();
    eventpatientddl();
    $('#calendar').fullCalendar('refetchEvents');
    $('#lblMessage').css('display', 'block');
    $("#lblMessage").text(updateResult);
    $("#lblMessage").delay(3000).fadeOut();
}

function deleteSuccess(deleteResult) {
    //alert(deleteResult);
    addpatientddl();
    eventpatientddl();
    $('#calendar').fullCalendar('refetchEvents');
    $('#lblMessage').css('display', 'block');
    $("#lblMessage").text(deleteResult);
    $("#lblMessage").delay(3000).fadeOut();
}

function addSuccess(addResult) {
    // if addresult is -1, means event was not added
    //    alert("added key: " + addResult);   

    //if (addResult[0] != -1) {
    //alert(addStartDate.length);
    //if (IsFirstEventAdded == true) {
    //for (var i = 0; i < addResult.length ; i++) {
    //    $('#calendar').fullCalendar('renderEvent',
    //                {
    //                    title: $("#addPatient option:selected").text(),
    //                    PatientId: $("#addPatient option:selected").val(),
    //                    start: addStartDate[i],
    //                    end: addEndDate[i],
    //                    id: addResult[i],
    //                    description: $("#addEventDesc").val(),
    //                    ExamTypeId: $("#addExamType option:selected").val(),
    //                    ExamType: $("#addExamType option:selected").text(),
    //                    allDay: globalAllDay                                
    //                },
    //                true // make the event "stick"                               
    //        );

    //}
    //alert(addStartDate[i] + '-' + addEndDate[i]);           
    //$('#calendar').fullCalendar('unselect');

    //$('#calendar').fullCalendar('next');
    //}

    //else {
    //$('#calendar').fullCalendar('changeView', 'month');
    //$('#calendar').fullCalendar('changeView', 'agendaWeek');
    //IsFirstEventAdded = true;
    //}
    // $('#calendar').fullCalendar('render');

    if (addResult == "1") {
        $('#calendar').fullCalendar('refetchEvents');
        clearTextBox();
        addpatientddl();
        eventpatientddl();
    }
    else if (addResult.indexOf("xx") > -1) {
        $('#calendar').fullCalendar('refetchEvents');
        //alert('The professional is not available at this time (' + addResult.split(",")[1] + ').');    
        alert('The professional is not available at this time (' + moment(new Date(addResult.split(",")[1])).format("DD/MM/YYYY hh:mm") + ').');
    }
    else if (addResult.indexOf("Error") > -1) {
        $('#lblMessage').css('display', 'block');
        $("#lblMessage").text(addResult);
        $("#lblMessage").delay(3000).fadeOut();
    }
}

function UpdateTimeSuccess(updateResult) {
    // alert(updateResult);
    // location.reload();
    // alert('Time is updated successfully.');
    //$('#calendar').fullCalendar('unselect');

}

function removeOtherProfessionals(Lst_Day) {
    $("#addProfessional option").remove();
    var $CurrentOptions = $("#" + Lst_Day + " > option:selected").clone();
    $('#addProfessional').append($CurrentOptions);
}

function selectDate(start, end, allDay) {
    var currentDay = start.toString().substring(0, 3);
    switch (currentDay) {
        case "Sun":
            removeOtherProfessionals("Lst_Sunday");
            break;
        case "Mon":
            removeOtherProfessionals("Lst_Monday");
            break;
        case "Tue":
            removeOtherProfessionals("Lst_Tuesday");
            break;
        case "Wed":
            removeOtherProfessionals("Lst_Wednesday");
            break;
        case "Thu":
            removeOtherProfessionals("Lst_Thursday");
            break;
        case "Fri":
            removeOtherProfessionals("Lst_Friday");
            break;
        case "Sat":
            removeOtherProfessionals("Lst_Saturday");
            break;
    }

    enableTextbox();
    $('#addDialog').dialog('open');
    addStartDate = start;
    addEndDate = end;
    globalAllDay = allDay;
    $(':radio').prop('checked', false);


}

function updateEventOnDropResize(event, allDay) {

    var eventToUpdate = {
        id: event.id,
        start: event.start
    };

    // FullCalendar 1.x
    //if (allDay) {
    //    eventToUpdate.start.setHours(0, 0, 0);
    //}

    if (event.end === null) {
        eventToUpdate.end = eventToUpdate.start;
    }
    else {
        eventToUpdate.end = event.end;

        // FullCalendar 1.x
        //if (allDay) {
        //    eventToUpdate.end.setHours(0, 0, 0);
        //}
    }

    // FullCalendar 1.x
    //eventToUpdate.start = eventToUpdate.start.format("dd-MM-yyyy hh:mm:ss tt");
    //eventToUpdate.end = eventToUpdate.end.format("dd-MM-yyyy hh:mm:ss tt");

    // FullCalendar 2.x
    var endDate;
    if (!event.allDay) {
        endDate = new Date(eventToUpdate.end + 60 * 60000);
        endDate = endDate.toJSON();
    }
    else {
        endDate = eventToUpdate.end.toJSON();
    }
   
    eventToUpdate.start = eventToUpdate.start.toJSON();
    eventToUpdate.end = eventToUpdate.end.toJSON(); //endDate;
    eventToUpdate.allDay = event.allDay;

    PageMethods.UpdateEventTime(eventToUpdate, UpdateTimeSuccess);
}

function eventDropped(event, dayDelta, minuteDelta, allDay, revertFunc) {
    if ($(this).data("qtip")) $(this).qtip("destroy");

    // FullCalendar 1.x
    //updateEventOnDropResize(event, allDay);

    // FullCalendar 2.x
    updateEventOnDropResize(event);
}

function eventResized(event, dayDelta, minuteDelta, revertFunc) {

    if ($(this).data("qtip")) $(this).qtip("destroy");

    updateEventOnDropResize(event);

}

function checkForSpecialChars(stringToCheck) {
    var pattern = /[^A-Za-z0-9 ]/;
    return pattern.test(stringToCheck);
}

function isAllDay(startDate, endDate) {
    var allDay;

    if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
        allDay = true;
        globalAllDay = true;
    }
    else {
        allDay = false;
        globalAllDay = false;
    }

    return allDay;
}

function qTipText(start, end, description, ExamType, Professional, Status) {

    var text;


    if (end !== null)
        text = "<strong>Professional:</strong> " + Professional + "<br/><strong>Start:</strong> " + start.format("DD/MM/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("DD/MM/YYYY hh:mm T") + "<br/><b>Exam:</b> " + ExamType + "<br/><strong>Status:</strong> " + Status + "<br/><strong>Description:</strong> " + description;
    else
        text = "<strong>Professional:</strong> " + Professional + "<br/><strong>Start:</strong> " + start.format("DD/MM/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><b>Exam:</b> " + ExamType + "<br/><strong>Status:</strong> " + Status + "<br/><strong>Description:</strong> " + description;

    return text;
}

$(document).ready(function () {
    var minimumTime;
    var maximumTime;
    var slotTime;
    PageMethods.GetSettingData(GetMinMaxTime);
    function GetMinMaxTime(result) {
        var settingarray = result.split('^');
        minimumTime = settingarray[3];
        maximumTime = settingarray[4];
        slotTime = "00:" + settingarray[5]; // slotDuration: "00:60",
        currentdate();
        $('[data-toggle="tooltip"]').tooltip();

        // update Dialog    
        $('#updatedialog').dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: '85%',
            buttons: {
                "Update": function () {
                    //alert(currentUpdateEvent.title);
                    var eventToUpdate = {
                        id: currentUpdateEvent.id,
                        ProfessionalId: $("#eventProfessional").val(),
                        PatientId: $("#eventPatient").val(),
                        description: $("#eventDesc").val(),
                        ExamTypeId: $("#eventType").val(),
                        Status: $("#eventStatus").val(),                       
                        start: moment(new Date($("#eventStart").val().toString() + ' ' + "GMT+0000")).toJSON(),
                        end: moment(new Date($("#eventEnd").val().toString() + ' ' + "GMT+0000")).toJSON()
                    };

                   
                    //Mon Feb 01 2016 00:30:00 GMT+0000
                    //var eventTime = {
                    //    id: currentUpdateEvent.id,
                    //    start: $("#eventStart").val(),
                    //    end: $("#eventEnd").val()
                    //    start: moment(new Date(addStartDate)).toJSON(),
                    //    end: moment(new Date(addEndDate)).toJSON(),
                    //};


                    if (checkForSpecialChars(eventToUpdate.description)) {
                        alert("please enter characters: A to Z, a to z, 0 to 9, spaces");
                    }
                    else {
                        PageMethods.UpdateEvent(eventToUpdate, updateSuccess);                       
                        $(this).dialog("close");

                        currentUpdateEvent.title = $("#eventPatient option:selected").text();
                        currentUpdateEvent.ProfessionalId = $("#eventProfessional").val();
                        currentUpdateEvent.Professional = $("#eventProfessional option:selected").text();
                        currentUpdateEvent.PatientId = $("#eventPatient").val();
                        currentUpdateEvent.description = $("#eventDesc").val();
                        currentUpdateEvent.ExamTypeId = $("#eventType").val();
                        currentUpdateEvent.ExamType = $("#eventType option:selected").text();
                        currentUpdateEvent.Status = $("#eventStatus").val();
                        // currentUpdateEvent.Status = $("#eventStatus option:selected").text();
                        $('#calendar').fullCalendar('updateEvent', currentUpdateEvent);
                    }

                },
                "Delete": function () {

                    if (confirm("do you really want to delete this event?")) {
                        var eventToDelete = [];
                        eventToDelete[0] = $("#eventId").val();
                        eventToDelete[1] = $("#eventPatient").val();
                        PageMethods.deleteEvent(eventToDelete, deleteSuccess);
                        $(this).dialog("close");
                        $('#calendar').fullCalendar('removeEvents', $("#eventId").val());
                    }
                }
            }
        });

        //add dialog
        $('#addDialog').dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: '85%',
            buttons: {
                "Add": function () {
                    if ($("#addProfessional option:selected").val() == undefined || $("#Txt_FirstName").val() == "" || $("#Txt_DateofBirth").val() == "" || $("#Txt_HomePhone").val() == "") {
                        alert('Professional, First Name, Date of Birth, and Home Phone are required!');
                    }
                    else {
                        var eventToAdd = [];
                        eventToAdd[0] = {
                            ProfessionalId: $("#addProfessional option:selected").val(),
                            PatientId: $("#addPatient option:selected").val(),
                            description: $("#addEventDesc").val(),
                            ExamTypeId: $("#addExamType option:selected").val(),
                            // FullCalendar 1.x
                            //start: addStartDate.format("dd-MM-yyyy hh:mm:ss tt"),
                            //end: addEndDate.format("dd-MM-yyyy hh:mm:ss tt")
                            title: $("#addPatient option:selected").text(),
                            ExamType: $("#addExamType option:selected").text(),
                            LastName: $("#Txt_LastName").val(),
                            FirstName: $("#Txt_FirstName").val(),
                            DateOfBirth: $("#Txt_DateofBirth").val(),
                            HomePhone: $("#Txt_HomePhone").val(),
                            CellPhone: $("#Txt_CellPhone").val(),

                            // FullCalendar 2.x
                            start: moment(new Date(addStartDate)).toJSON(),
                            end: moment(new Date(addEndDate)).toJSON(),

                            allDay: isAllDay(addStartDate, addEndDate)
                        };

                        if (checkForSpecialChars(eventToAdd.description)) {
                            alert("please enter characters: A to Z, a to z, 0 to 9, spaces");
                        }
                        else {

                            // add days to date if moment not support
                            //eventToAdd.start = new Date(new Date(addStartDate).getTime() + 7 * 24 * 60 * 60 * 1000); 
                            // eventToAdd.end = new Date(new Date(addEndDate).getTime() + 7 * 24 * 60 * 60 * 1000);

                            var RepeatNumber = $("#addRepeatNumber").val();

                            if ($("#RadioWeek").is(":checked"))  //add days to date
                            {
                                for (var i = 1; i < parseInt(RepeatNumber) + 1; i++) {

                                    eventToAdd[i] = {
                                        ProfessionalId: $("#addProfessional option:selected").val(),
                                        PatientId: $("#addPatient option:selected").val(),
                                        description: $("#addEventDesc").val(),
                                        ExamTypeId: $("#addExamType option:selected").val(),
                                        title: $("#addPatient option:selected").text(),
                                        ExamType: $("#addExamType option:selected").text(),
                                        LastName: $("#Txt_LastName").val(),
                                        FirstName: $("#Txt_FirstName").val(),
                                        DateOfBirth: $("#Txt_DateofBirth").val(),
                                        HomePhone: $("#Txt_HomePhone").val(),
                                        CellPhone: $("#Txt_CellPhone").val(),
                                        start: new Date(moment(new Date(addStartDate)).add(i * 7, 'days')).toJSON(),
                                        end: new Date(moment(new Date(addEndDate)).add(i * 7, 'days')).toJSON(),
                                        allDay: isAllDay(addStartDate, addEndDate)
                                    };
                                }
                                PageMethods.addEvent(eventToAdd, addSuccess);
                            }
                            else if ($("#RadioMonth").is(":checked")) //add months to date
                            {

                                for (var i = 1; i < parseInt(RepeatNumber) + 1; i++) {
                                    eventToAdd[i] = {
                                        ProfessionalId: $("#addProfessional option:selected").val(),
                                        PatientId: $("#addPatient option:selected").val(),
                                        description: $("#addEventDesc").val(),
                                        ExamTypeId: $("#addExamType option:selected").val(),
                                        title: $("#addPatient option:selected").text(),
                                        ExamType: $("#addExamType option:selected").text(),
                                        LastName: $("#Txt_LastName").val(),
                                        FirstName: $("#Txt_FirstName").val(),
                                        DateOfBirth: $("#Txt_DateofBirth").val(),
                                        HomePhone: $("#Txt_HomePhone").val(),
                                        CellPhone: $("#Txt_CellPhone").val(),
                                        start: new Date(moment(new Date(addStartDate)).add(i, 'months')).toJSON(),
                                        end: new Date(moment(new Date(addEndDate)).add(i, 'months')).toJSON(),
                                        allDay: isAllDay(addStartDate, addEndDate)
                                    };
                                }
                                PageMethods.addEvent(eventToAdd, addSuccess);
                            }
                            else if ($("#RadioYear").is(":checked"))  //add year to date
                            {
                                for (var i = 1; i < parseInt(RepeatNumber) + 1; i++) {
                                    eventToAdd[i] = {
                                        ProfessionalId: $("#addProfessional option:selected").val(),
                                        PatientId: $("#addPatient option:selected").val(),
                                        description: $("#addEventDesc").val(),
                                        ExamTypeId: $("#addExamType option:selected").val(),
                                        title: $("#addPatient option:selected").text(),
                                        ExamType: $("#addExamType option:selected").text(),
                                        LastName: $("#Txt_LastName").val(),
                                        FirstName: $("#Txt_FirstName").val(),
                                        DateOfBirth: $("#Txt_DateofBirth").val(),
                                        HomePhone: $("#Txt_HomePhone").val(),
                                        CellPhone: $("#Txt_CellPhone").val(),
                                        start: new Date(moment(new Date(addStartDate)).add(i, 'years')).toJSON(),
                                        end: new Date(moment(new Date(addEndDate)).add(i, 'years')).toJSON(),
                                        allDay: isAllDay(addStartDate, addEndDate)
                                    };
                                }
                                PageMethods.addEvent(eventToAdd, addSuccess);
                            }
                            else {
                                PageMethods.addEvent(eventToAdd, addSuccess);
                            }

                            $(this).dialog("close"); //close dialog
                        }
                    }
                }
            }
        });




        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var options = {
            weekday: "long", year: "numeric", month: "short",
            day: "numeric", hour: "2-digit", minute: "2-digit"
        };

        function getProfessionals(Lst_Day) {
            var professionals = '';
            $("#" + Lst_Day + " option:selected").each(function () {
                professionals += $(this).text() + '\n';
            });
            return professionals;
        }

        $('#calendar').fullCalendar({
            theme: true,
            buttonText: {
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day'
            },
            header: {
                left: 'title',
                center: 'month,agendaWeek,agendaDay',
                right: 'today,prev,next'
            },
            views: {
                month: {
                    //titleFormat: 'MMMM YYYY'
                    // options apply to agendaWeek and agendaDay views
                },
                week: {
                    columnFormat: 'ddd D/M',
                    displayEventTime: false
                    //titleFormat: 'MMM D YYYY'
                    // options apply to basicWeek and agendaWeek views
                },
                day: {
                    displayEventTime: false
                    //titleFormat: 'MMMM D YYYY'
                    // options apply to basicDay and agendaDay views
                }
            },
            defaultView: 'agendaWeek',
            eventClick: updateEvent,
            selectable: true,
            selectHelper: true,
            select: selectDate,
            editable: true,
            events: "JsonResponse.ashx",
            eventDrop: eventDropped,
            eventResize: eventResized,
            minTime: minimumTime,
            maxTime: maximumTime,
            allDaySlot: false,
            height: "auto",
            //contentHeight: "auto",
            dayNames: [getProfessionals('Lst_Sunday') + 'Sunday', getProfessionals('Lst_Monday') + 'Monday', getProfessionals('Lst_Tuesday') + 'Tuesday',
                getProfessionals('Lst_Wednesday') + 'Wednesday', getProfessionals('Lst_Thursday') + 'Thursday', getProfessionals('Lst_Friday') + 'Friday', getProfessionals('Lst_Saturday') + 'Saturday'],
            dayNamesShort: [getProfessionals('Lst_Sunday') + 'Sun', getProfessionals('Lst_Monday') + 'Mon', getProfessionals('Lst_Tuesday') + 'Tue',
                getProfessionals('Lst_Wednesday') + 'Wed', getProfessionals('Lst_Thursday') + 'Thu', getProfessionals('Lst_Friday') + 'Fri', getProfessionals('Lst_Saturday') + 'Sat'],
            //scrollTime: "09:00:00",
            slotDuration: slotTime,
            slotLabelInterval: slotTime,
            firstDay: 1,
            eventRender: function (event, element) {

                element.qtip({
                    content: {
                        text: qTipText(event.start, event.end, event.description, event.ExamType, event.Professional, event.Status),
                        title: '<strong>' + event.title + '</strong>', button: true
                    },
                    position: {
                        target: 'mouse',
                        adjust: { mouse: false },
                        viewport: $(window),
                    },
                    show: {
                        solo: true
                    },
                    hide: {
                        fixed: true,
                        delay: 300
                    },
                    style: {
                        classes: 'qtip-shadow qtip-rounded Cupertino',
                        widget: true
                    }
                });

                // default
                $(element).css("color", "gray");
                $(element).css("border-color", "gray");
                $(element).css("backgroundColor", "white");
                $(element).css("border-left", "7px solid gray");


                //exam type
                if (event.ExamType == "Emergency")
                    $(element).css("border-left", "7px solid red");
                else if (event.ExamType == "Contact lenses Exam")
                    $(element).css("border-left", "7px solid blue");
                else if (event.ExamType == "Cycloplegia")
                    $(element).css("border-left", "7px solid Magenta");
                else if (event.ExamType == "Dilation")
                    $(element).css("border-left", "7px solid Orange");
                else if (event.ExamType == "Follow-up")
                    $(element).css("border-left", "7px solid DarkSalmon");
                else if (event.ExamType == "General Eye Exam")
                    $(element).css("border-left", "7px solid green");
                else if (event.ExamType == "Kids Eye Exam")
                    $(element).css("border-left", "7px solid PaleVioletRed");
                else if (event.ExamType == "OCT")
                    $(element).css("border-left", "7px solid CadetBlue");

                //status
                if (event.Status == "Arriving")
                    $(element).css("color", "green");
                else if (event.Status == "Did not come")
                    $(element).css("color", "red");
                else if (event.Status == "Will be late")
                    $(element).css("color", "#cc7000");

            }
        });
    }
});
