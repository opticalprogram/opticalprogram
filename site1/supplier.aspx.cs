﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class supplier : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Supplier";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        objdal = new Dal();

        if (!IsPostBack)
        {
             ProvinceFill();
             CityFill();
             if (Request.QueryString["id"] != null)
             {
                 Button_Submit.Text = "Update";
                 GetSupplierData(Request.QueryString["id"].ToString());
             }
        }
    }

    //get supplier data
    protected void GetSupplierData(string EditId)
    {
        //SELECT [SupplierName],[SupplierAddress],[SupplierCityId],[SupplierProvinceId],[SupplierCountry],[SupplierAccount]
        //,[SupplierBrand]  FROM [OpticalProgram].[dbo].[tbl_Supplier]

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_Supplier] where SupplierId='" + EditId + "'");
        Txt_Supplier.Text = dt.Rows[0]["SupplierName"].ToString();
        Txt_Address.Text = dt.Rows[0]["SupplierAddress"].ToString();
        DropDownList_City.SelectedValue = dt.Rows[0]["SupplierCityId"].ToString();
        DropDownList_Province.SelectedValue = dt.Rows[0]["SupplierProvinceId"].ToString();
        DropDownList_Country.SelectedValue = dt.Rows[0]["SupplierCountry"].ToString();
        Txt_Account.Text = dt.Rows[0]["SupplierAccount"].ToString();
        Txt_Brand.Text = dt.Rows[0]["SupplierBrand"].ToString();
        ddl_Type.SelectedValue = dt.Rows[0]["Type"].ToString();
    }
   

    //for city fill
    protected void CityFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from tbl_SupplierCity where MasterId='" + MasterId + "' order by SupplierCityName");
        DropDownList_City.DataSource = dt;
        DropDownList_City.DataValueField = "SupplierCityId";
        DropDownList_City.DataTextField = "SupplierCityName";
        DropDownList_City.DataBind();
    }

    //for Province fill
    protected void ProvinceFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from tbl_SupplierProvince where MasterId='" + MasterId + "' order by SupplierProvinceName");
        DropDownList_Province.DataSource = dt;
        DropDownList_Province.DataValueField = "SupplierProvinceId";
        DropDownList_Province.DataTextField = "SupplierProvinceName";
        DropDownList_Province.DataBind();
    }

    protected void Button_Province_Click(object sender, EventArgs e)
    {
        try
        {
            ModalProvince.Style.Add("display", "none");
            //INSERT INTO [dbo].[tbl_SupplierProvince]([SupplierProvinceId],[SupplierProvinceName],[MasterId])VALUES            
            objdal.EXECUTE_DML("INSERT INTO [tbl_SupplierProvince]([SupplierProvinceName],[MasterId])VALUES('" + TextBox_Province.Text + "','" + MasterId + "')");
            ProvinceFill();
            ModalProvince.Style.Add("display", "inline");
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
    protected void Button_City_Click(object sender, EventArgs e)
    {
        try
        {
            ModalCity.Style.Add("display", "none");
            //INSERT INTO [dbo].[tbl_SupplierCity]([SupplierCityId],[SupplierCityName],[MasterId]) VALUES           
            objdal.EXECUTE_DML("INSERT INTO [tbl_SupplierCity]([SupplierCityName],[MasterId])VALUES('" + TextBox_City.Text + "','" + MasterId + "')");
            CityFill();
            ModalCity.Style.Add("display", "inline");
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    //Save and update
    protected void Button_Submit_Click(object sender, EventArgs e)
    {
        try
        {
             StringBuilder sb = new StringBuilder();
             if (Request.QueryString["id"] != null)
             { 
                // UPDATE [dbo].[tbl_Supplier] SET [SupplierName] = '',[SupplierAddress] = '',[SupplierCityId] = ''
                 //,[SupplierProvinceId] = '',[SupplierCountry] = '',[SupplierAccount] = '',[SupplierBrand] = '',[Type]=''
                //,[MasterId] = '' WHERE 

                 sb.Append("UPDATE [dbo].[tbl_Supplier] SET [SupplierName] = '" + Txt_Supplier.Text.Replace("'", "''") + "',[SupplierAddress] = '" + Txt_Address.Text + "'");
                 sb.Append(",[SupplierCityId] = '" + DropDownList_City.SelectedValue + "',[SupplierProvinceId] = '" + DropDownList_Province.SelectedValue + "'");
                 sb.Append(",[SupplierCountry] = '" + DropDownList_Country.SelectedValue + "',[SupplierAccount] = '" + Txt_Account.Text + "'");
                 sb.Append(",[SupplierBrand] = '" + Txt_Brand.Text + "',[Type]='"+ ddl_Type.SelectedValue +"',[MasterId] = '" + MasterId + "' ");
                 sb.Append(" WHERE [SupplierId]='" + Request.QueryString["id"].ToString() + "'");

                 objdal.EXECUTE_DML(sb.ToString());
                 Session["lblMessage"] = "Supplier is updated successfully.";
             }
             else
             {
                 //INSERT INTO [dbo].[tbl_Supplier]([SupplierId],[SupplierName],[SupplierAddress],[SupplierCityId],[SupplierProvinceId]
                 //,[SupplierCountry],[SupplierAccount],[SupplierBrand],[Type],[MasterId])VALUES()

                 sb.Append("INSERT INTO [dbo].[tbl_Supplier]([SupplierName],[SupplierAddress],[SupplierCityId],[SupplierProvinceId]");
                 sb.Append(",[SupplierCountry],[SupplierAccount],[SupplierBrand],[Type],[MasterId])");
                 sb.Append("VALUES('" + Txt_Supplier.Text.Replace("'", "''") + "','" + Txt_Address.Text + "','" + DropDownList_City.SelectedValue + "'");
                 sb.Append(",'" + DropDownList_Province.SelectedValue +"','" + DropDownList_Country.SelectedValue +"'");
                 sb.Append(",'" + Txt_Account.Text + "','" + Txt_Brand.Text + "','" + ddl_Type.SelectedValue + "','" + MasterId + "')");

                 objdal.EXECUTE_DML(sb.ToString());

                 Session["lblMessage"] = "Supplier is added successfully.";
             }
             Response.Redirect("managesupplier.aspx", true);
        }
        catch (Exception ex)
        {
            
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
}