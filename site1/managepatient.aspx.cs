﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class viewpatient : System.Web.UI.Page
{
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // set title
        string title = "Patient";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
           Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();


        if (Session["lblMessage"] != null)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Session["lblMessage"].ToString();
            Session.Remove("lblMessage");
        }
    }


    protected void GVPatient_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GVPatient.SelectedIndex = e.NewSelectedIndex;
        Response.Redirect("patient.aspx?id=" + GVPatient.SelectedValue.ToString(), true);
    }

    protected void GVPatient_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        lblMessage.Visible = true;
        if (e.Exception == null)
        {
            lblMessage.Text = "Patient is deleted successfully!";
        }
        else
        {
            lblMessage.CssClass = "alert alert-danger";
            lblMessage.Text = "Error: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
    }

    protected void Button_Search_Click(object sender, EventArgs e)
    {

        if (DDL_SerchBy.SelectedValue == "DateOfBirth")
        {
            SDSPatient.SelectCommand = "SELECT [PatientId], [FileNo], [OpeningDate], [FirstName], [LastName], [Gender], [CellPhone], [HomePhone], [DateOfBirth] FROM [tbl_Patient] "
                                         + "where DateOfBirth='" + DateTime.ParseExact(TextBox_Keyword.Text, "dd-MM-yyyy", null) + "' and [tbl_Patient].MasterId='"+ MasterId +"'";
        }
        else if (DDL_SerchBy.SelectedValue != "FirstLastName")
        {
            SDSPatient.SelectCommand = "SELECT [PatientId], [FileNo], [OpeningDate], [FirstName], [LastName], [Gender], [CellPhone], [HomePhone], [DateOfBirth] FROM [tbl_Patient]"
                                       + "where " + DDL_SerchBy.SelectedValue + " like '%" + TextBox_Keyword.Text + "%' and [tbl_Patient].MasterId='" + MasterId + "'";
        }
        else
        {
            SDSPatient.SelectCommand = "SELECT [PatientId], [FileNo], [OpeningDate], [FirstName], [LastName], [Gender], [CellPhone], [HomePhone], [DateOfBirth] FROM [tbl_Patient]"
                                       + "where (FirstName like '%" + TextBox_Keyword.Text + "%' or LastName like '%" + TextBox_Keyword.Text + "%') and [tbl_Patient].MasterId='" + MasterId + "'";
        }
        DDL_SerchBy.SelectedValue = "FirstLastName";
        TextBox_Keyword.Text = "";
    }
    protected void SDSPatient_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;   
    }
}