﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class prescription : System.Web.UI.Page
{
    string MasterId;
    Dal objdal;
    protected void Page_Load(object sender, EventArgs e)
    {
        // set title
        string title = "Prescription";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();
        objdal = new Dal();
        if (Session["lblMessage"] != null)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Session["lblMessage"].ToString();
            Session.Remove("lblMessage");
        }
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                SDSPrescription.SelectCommand = "  select q1.PrescriptionNo,q1.Date,q1.PrescriptionType,q1.PatientId,q1.patientname,q1.Professional from "
                                          + " (SELECT tbl_Contactlenses.ContactlensesId as PrescriptionNo, tbl_Contactlenses.Date, tbl_Contactlenses.PrescriptionType "
                                          + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                          + "   tbl_Contactlenses.Professional  FROM   tbl_Contactlenses INNER JOIN "
                                          + "   tbl_Patient ON tbl_Contactlenses.PatientId = tbl_Patient.PatientId  "
                                          + " where tbl_Contactlenses.MasterId='" + MasterId + "'"

                                          + "   union all "

                                          + "   SELECT  tbl_Glass.GlassId as PrescriptionNo, tbl_Glass.Date, tbl_Glass.PrescriptionType "
                                          + "  , tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                          + "   tbl_Glass.Professional FROM tbl_Glass INNER JOIN  "
                                          + "  tbl_Patient ON tbl_Glass.PatientId = tbl_Patient.PatientId "
                                          + " where tbl_Glass.MasterId='" + MasterId + "') as q1 "
                                          + "   where q1.PatientId='" + Request.QueryString["id"].ToString() + "'";
                
            }

        }
    }

    //protected void GVPrescription_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{           
    //    GVPrescription.SelectedIndex = e.NewSelectedIndex;
        
    //    Label PrescriptionType = (Label)GVPrescription.Rows[GVPrescription.SelectedIndex].FindControl("lbl_PrescriptionType");

    //    if (PrescriptionType.Text == "ContactLens")
    //    {
    //        Response.Redirect("contactlens.aspx?id=" + GVPrescription.SelectedValue + "");
    //    }
    //    else if (PrescriptionType.Text == "Glass")
    //    {
    //        Response.Redirect("glass.aspx?id=" + GVPrescription.SelectedValue, true);
    //    }       
    //}

    protected void SDSPrescription_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }

    //For searching
    protected void Button_Search_Click(object sender, EventArgs e)
    {
        if (DDL_SerchBy.SelectedValue=="Date")
        {
            SDSPrescription.SelectCommand = "  select q1.PrescriptionNo,q1.Date,q1.PrescriptionType,q1.PatientId,q1.patientname,q1.Professional from "
                                           + " (SELECT tbl_Contactlenses.ContactlensesId as PrescriptionNo, tbl_Contactlenses.Date, tbl_Contactlenses.PrescriptionType "
		                                   + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                           + "   tbl_Contactlenses.Professional  FROM   tbl_Contactlenses INNER JOIN "
                                           + "   tbl_Patient ON tbl_Contactlenses.PatientId = tbl_Patient.PatientId  "
                                           + " where tbl_Contactlenses.MasterId='" + MasterId + "'"
            
                                           + "   union all "
                              
                                           + "   SELECT  tbl_Glass.GlassId as PrescriptionNo, tbl_Glass.Date, tbl_Glass.PrescriptionType "
							               + "  , tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                           + "   tbl_Glass.Professional FROM tbl_Glass INNER JOIN  "
							               + "  tbl_Patient ON tbl_Glass.PatientId = tbl_Patient.PatientId "
                                           + " where tbl_Glass.MasterId='" + MasterId + "') as q1 "
                                           + "   where q1.Date='" + DateTime.ParseExact(TextBox_Keyword.Text, "dd-MM-yyyy", null) + "'";
        }
        else if (DDL_SerchBy.SelectedValue == "patientname")
        {
            SDSPrescription.SelectCommand = "  select q1.PrescriptionNo,q1.Date,q1.PrescriptionType,q1.PatientId,q1.patientname,q1.Professional from "
                                           + " (SELECT tbl_Contactlenses.ContactlensesId as PrescriptionNo, tbl_Contactlenses.Date, tbl_Contactlenses.PrescriptionType "
                                           + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                           + "   tbl_Contactlenses.Professional  FROM   tbl_Contactlenses INNER JOIN "
                                           + "   tbl_Patient ON tbl_Contactlenses.PatientId = tbl_Patient.PatientId  "
                                           + " where tbl_Contactlenses.MasterId='" + MasterId + "'"

                                           + "   union all "

                                           + "   SELECT  tbl_Glass.GlassId as PrescriptionNo, tbl_Glass.Date, tbl_Glass.PrescriptionType "
                                           + "  , tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                           + "   tbl_Glass.Professional FROM tbl_Glass INNER JOIN  "
                                           + "  tbl_Patient ON tbl_Glass.PatientId = tbl_Patient.PatientId "
                                           + " where tbl_Glass.MasterId='" + MasterId + "') as q1 "
                                           + "   where q1.patientname like '%" + TextBox_Keyword.Text + "%'";
        }
        else
        {
            SDSPrescription.SelectCommand = "  select q1.PrescriptionNo,q1.Date,q1.PrescriptionType,q1.PatientId,q1.patientname,q1.Professional from "
                                          + " (SELECT tbl_Contactlenses.ContactlensesId as PrescriptionNo, tbl_Contactlenses.Date, tbl_Contactlenses.PrescriptionType "
                                          + " ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                          + "   tbl_Contactlenses.Professional  FROM   tbl_Contactlenses INNER JOIN "
                                          + "   tbl_Patient ON tbl_Contactlenses.PatientId = tbl_Patient.PatientId  "
                                          + " where tbl_Contactlenses.MasterId='" + MasterId + "'"

                                          + "   union all "

                                          + "   SELECT  tbl_Glass.GlassId as PrescriptionNo, tbl_Glass.Date, tbl_Glass.PrescriptionType "
                                          + "  , tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, "
                                          + "   tbl_Glass.Professional FROM tbl_Glass INNER JOIN  "
                                          + "  tbl_Patient ON tbl_Glass.PatientId = tbl_Patient.PatientId "
                                          + " where tbl_Glass.MasterId='" + MasterId + "') as q1 "
                                          + "   where q1.PrescriptionType like '%" + TextBox_Keyword.Text + "%'";
        }
    }

    //for Deleting
    protected void GVPrescription_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Label DeleteId = (Label)GVPrescription.Rows[e.RowIndex].FindControl("lbl_PrescriptionNo");
        Label PrescriptionType = (Label)GVPrescription.Rows[e.RowIndex].FindControl("lbl_PrescriptionType");

        if (PrescriptionType.Text == "Contactlens")
        {
            objdal.EXECUTE_DML("DELETE FROM [dbo].[tbl_Contactlenses] WHERE ContactlensesId='" + DeleteId.Text + "'");
        }
        else if (PrescriptionType.Text == "Glass")
        {
            objdal.EXECUTE_DML("DELETE FROM [dbo].[tbl_Glass] WHERE GlassId='" + DeleteId.Text + "'");
        }
      
    }
   
    protected void GVPrescription_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        Label PrescriptionType = (Label)GVPrescription.Rows[index].FindControl("lbl_PrescriptionType");
        Label Id = (Label)GVPrescription.Rows[index].FindControl("lbl_PrescriptionNo");
        if (e.CommandName=="Edit")
        {
            if (PrescriptionType.Text == "ContactLens")
            {
                Response.Redirect("contactlens.aspx?id=" + Id.Text,true);
            }
            else if (PrescriptionType.Text == "Glass")
            {
                Response.Redirect("glass.aspx?id=" + Id.Text, true);
            }   
        }
        else if (e.CommandName == "Print")
        {
            if (PrescriptionType.Text == "ContactLens")
            {
                Response.Redirect("printcontanctlens.aspx?id=" + Id.Text, true);
            }
            else if (PrescriptionType.Text == "Glass")
            {
                Response.Redirect("printglass.aspx?id=" + Id.Text, true);
            }   
        }
    }
}