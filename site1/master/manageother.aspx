﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="manageother.aspx.cs" Inherits="master_addother" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        td, th {
            padding: 2px 10px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(".topToolTip").tooltip({
                placement: "bottom"
            });
        });

        function distroymodal() {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <span id="CreateUserButton" title="This button will be automatically disabled when you reach your total logins limit!" runat="server" class="glyphicon glyphicon-user btn btn-sm btn-default LinkButton topToolTip" data-toggle="modal" style="cursor: pointer;" data-target="#CreateUserModal"><span style="font-family: Arial;">&nbsp;Create user</span></span>
        </li>
        <li>
            <asp:HyperLink ID="HL_manageformula" NavigateUrl="~/master/manageformula.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-baby-formula"></span>&nbsp;Manage Formula</asp:HyperLink></li>
    </ul>

    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 category">
        <div class="form-group col-lg-6 col-md-6 col-sm-6">
            <label>Home Image:</label>
            <asp:Image ID="Image_Master" runat="server" Width="350" Height="130" CssClass="img-responsive img-rounded" />
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-6">
            <label>Upload new Image:</label>
            <asp:FileUpload ID="FileUpload_MasterHome" runat="server" accept="image/*" />
            <label style="font-weight: normal; color: gray;">
                Recommended Width: 1278 pixel
                <br />
                Recommended Height: 444 pixel</label><br />
            <asp:Button ID="Button_Upload" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" OnClick="Button_Upload_Click" Text="Upload image" />
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-6">
            <label for="DDL_Status">Status:</label>
            &nbsp;
            <span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" data-target="#StatusModal"></span>
            <asp:UpdatePanel ID="UpdatePanelStatus" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="DDL_Status" runat="server" CssClass="form-control"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
            
        </div>
    </div>



    <div class="table-responsive col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 0px !important; margin-top: 10px;">
        <asp:GridView ID="GVOtherUser" DataKeyNames="UserName" OnRowDeleting="GVOtherUser_RowDeleting" GridLines="None" CssClass="table table-hover table-striped" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" runat="server" EmptyDataText="No data found!" DataSourceID="SDSOtherUser">
            <Columns>
                <asp:CommandField ShowSelectButton="True" SelectText="Permissions" ControlStyle-CssClass="btn-success btn btn-xs"></asp:CommandField>
                <asp:TemplateField HeaderText="UserId" SortExpression="UserId" Visible="False">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Bind("UserId") %>' ID="lbl_UserId"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserName" HeaderText="User" SortExpression="UserName"></asp:BoundField>
                <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity" DataFormatString="{0:dd-MM-yyyy}" SortExpression="LastActivityDate"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"></asp:BoundField>
                <asp:BoundField DataField="cmt" HeaderText="Position" SortExpression="cmt"></asp:BoundField>
                <asp:BoundField DataField="CreateDate" HeaderText="Created" DataFormatString="{0:dd-MM-yyyy}" SortExpression="CreateDate"></asp:BoundField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs"></asp:CommandField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
            <SelectedRowStyle BackColor="#FFEF8F" />
        </asp:GridView>
    </div>

    <div class="table-responsive col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 0px !important; margin-top: 10px; border: none;">
        <asp:DetailsView ID="dvPermission" HeaderText="User module permissions" runat="server" GridLines="None" CssClass="table table-hover table-striped" AutoGenerateRows="False" DataKeyNames="PermissionId" DataSourceID="sdsGvPermission" DefaultMode="Edit">
            <Fields>
                <asp:TemplateField HeaderText="Patient module" SortExpression="PermissionPatientAdd">
                    <EditItemTemplate>
                        <asp:CheckBox ID="cbPatientAdd" ClientIDMode="Static" runat="server" Checked='<%# Bind("PermissionPatientAdd") %>' />
                        <label for="cbPatientAdd">Full</label>
                        &nbsp;
                        <asp:CheckBox ID="cbPatientEdit" ClientIDMode="Static" Visible="false" runat="server" Checked='<%# Bind("PermissionPatientEdit") %>' />
                        <label for="cbPatientEdit" style="display: none;">Edit</label>
                        &nbsp;                      
                        <asp:CheckBox ID="cbPatientDelete" ClientIDMode="Static" Visible="false" runat="server" Checked='<%# Bind("PermissionPatientDelete") %>' />
                        <label for="cbPatientDelete" style="display: none;">Delete</label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Agenda module" SortExpression="PermissionAgendaAdd">
                    <EditItemTemplate>
                        <asp:CheckBox ID="cbAgendaAdd" ClientIDMode="Static" runat="server" Checked='<%# Bind("PermissionAgendaAdd") %>' />
                        <label for="cbAgendaAdd">Full</label>
                        &nbsp;<asp:CheckBox ID="cbAgendaEdit" ClientIDMode="Static" Visible="false" runat="server" Checked='<%# Bind("PermissionAgendaEdit") %>' />
                        <label for="cbAgendaEdit" style="display: none;">Edit</label>
                        &nbsp;
                         <asp:CheckBox ID="cbAgendaDelete" ClientIDMode="Static" Visible="false" runat="server" Checked='<%# Bind("PermissionAgendaDelete") %>' />
                        <label for="cbAgendaDelete" style="display: none;">Delete</label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Inventory module" SortExpression="PermissioninventoryAdd">
                    <EditItemTemplate>
                        <asp:CheckBox runat="server" ClientIDMode="Static" Checked='<%# Bind("PermissioninventoryAdd") %>' ID="cbInventoryAdd"></asp:CheckBox>
                        <label for="cbInventoryAdd">Full</label>
                        &nbsp;<asp:CheckBox runat="server" ClientIDMode="Static" Visible="false" Checked='<%# Bind("PermissioninventoryEdit") %>' ID="cbInventoryEdit"></asp:CheckBox>
                        <label for="cbInventoryEdit" style="display: none;">Edit</label>
                        &nbsp;
                         <asp:CheckBox runat="server" ClientIDMode="Static" Visible="false" Checked='<%# Bind("PermissioninventoryDelete") %>' ID="cbInventoryDelete"></asp:CheckBox>
                        <label for="cbInventoryDelete" style="display: none;">Delete</label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Invoice module" SortExpression="PermissionInvoiceAdd">
                    <EditItemTemplate>
                        <asp:CheckBox runat="server" ClientIDMode="Static" Checked='<%# Bind("PermissionInvoiceAdd") %>' ID="cbInvoiceAdd"></asp:CheckBox>&nbsp;<label for="cbInvoiceAdd">Add</label><br />
                        <asp:CheckBox runat="server" ClientIDMode="Static" Checked='<%# Bind("PermissionInvoiceEdit") %>' ID="cbInvoiceEdit"></asp:CheckBox>&nbsp;<label for="cbInvoiceEdit">Edit</label><br />
                        <asp:CheckBox runat="server" ClientIDMode="Static" Checked='<%# Bind("PermissionInvoiceDelete") %>' ID="cbInvoiceDelete"></asp:CheckBox>&nbsp;<label for="cbInvoiceDelete">Delete</label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Exam Template module" SortExpression="PermissionExamTemplateAdd">
                    <EditItemTemplate>
                        <asp:CheckBox runat="server" ClientIDMode="Static" Checked='<%# Bind("PermissionExamTemplateAdd") %>' ID="cbExamTemplateAdd"></asp:CheckBox>
                        <label for="cbExamTemplateAdd">Full</label>
                        &nbsp; 
                         <asp:CheckBox runat="server" ClientIDMode="Static" Visible="false" Checked='<%# Bind("PermissionExamTemplateEdit") %>' ID="cbExamTemplateEdit"></asp:CheckBox>
                        <label for="cbExamTemplateEdit" style="display: none;">Edit</label>
                        &nbsp;
                         <asp:CheckBox runat="server" ClientIDMode="Static" Visible="false" Checked='<%# Bind("PermissionExamTemplateDelete") %>' ID="cbExamTemplateDelete"></asp:CheckBox>
                        <label for="cbExamTemplateDelete" style="display: none;">Delete</label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Prescription module" SortExpression="PermissionPrescriptionAdd">
                    <EditItemTemplate>
                        <asp:CheckBox runat="server" ClientIDMode="Static" Checked='<%# Bind("PermissionPrescriptionAdd") %>' ID="cbPrescriptionAdd"></asp:CheckBox>
                        <label for="cbPrescriptionAdd">Full</label>
                        &nbsp;
                         <asp:CheckBox runat="server" ClientIDMode="Static" Visible="false" Checked='<%# Bind("PermissionPrescriptionEdit") %>' ID="cbPrescriptionEdit"></asp:CheckBox>
                        <label for="cbPrescriptionEdit" style="display: none;">Edit</label>
                        &nbsp;
                         <asp:CheckBox runat="server" ClientIDMode="Static" Visible="false" Checked='<%# Bind("PermissionPrescriptionDelete") %>' ID="cbPrescriptionDelete"></asp:CheckBox>
                        <label for="cbPrescriptionDelete" style="display: none;">Delete</label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Statistics module" SortExpression="PermissionStatistics">
                    <EditItemTemplate>
                        <asp:CheckBox runat="server" Checked='<%# Bind("PermissionStatistics") %>' ClientIDMode="Static" ID="cbStatisticsAdd"></asp:CheckBox>
                        <label for="cbStatisticsAdd">Full</label>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:CommandField ShowEditButton="True" ControlStyle-CssClass="btn btn-success btn-xs" />
            </Fields>
            <HeaderStyle BackColor="#FFEF8F" Font-Bold="true" />
            <FieldHeaderStyle ForeColor="#337AB7" Font-Bold="True" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="sdsGvPermission" runat="server" ConnectionString="<%$ ConnectionStrings:CN-OpticalProgram %>"
            SelectCommand="SELECT * FROM [tbl_Permission] WHERE ([UserName] = @UserName)"
            UpdateCommand="UPDATE [tbl_Permission] SET [PermissionPatientAdd] = @PermissionPatientAdd, [PermissionPatientEdit] = @PermissionPatientEdit,
                           [PermissionPatientDelete] = @PermissionPatientDelete, [PermissionAgendaAdd] = @PermissionAgendaAdd, 
                           [PermissionAgendaEdit] = @PermissionAgendaEdit, [PermissionAgendaDelete] = @PermissionAgendaDelete,
                           [PermissioninventoryAdd] = @PermissioninventoryAdd, [PermissioninventoryEdit] = @PermissioninventoryEdit,
                           [PermissioninventoryDelete] = @PermissioninventoryDelete, [PermissionInvoiceAdd] = @PermissionInvoiceAdd, 
                           [PermissionInvoiceEdit] = @PermissionInvoiceEdit, [PermissionInvoiceDelete] = @PermissionInvoiceDelete,
                           [PermissionExamTemplateAdd] = @PermissionExamTemplateAdd, [PermissionExamTemplateEdit] = @PermissionExamTemplateEdit,
                           [PermissionExamTemplateDelete] = @PermissionExamTemplateDelete, [PermissionPrescriptionAdd] = @PermissionPrescriptionAdd, 
                           [PermissionPrescriptionEdit] = @PermissionPrescriptionEdit, [PermissionPrescriptionDelete] = @PermissionPrescriptionDelete, 
                           [PermissionStatistics] = @PermissionStatistics WHERE [PermissionId] = @PermissionId">



            <SelectParameters>
                <asp:ControlParameter ControlID="GVOtherUser" Name="UserName" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>

            <UpdateParameters>
                <asp:Parameter Name="UserId" Type="String"></asp:Parameter>
                <asp:Parameter Name="PermissionPatientAdd" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionPatientEdit" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionPatientDelete" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionAgendaAdd" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionAgendaEdit" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionAgendaDelete" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissioninventoryAdd" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissioninventoryEdit" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissioninventoryDelete" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionInvoiceAdd" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionInvoiceEdit" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionInvoiceDelete" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionExamTemplateAdd" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionExamTemplateEdit" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionExamTemplateDelete" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionPrescriptionAdd" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionPrescriptionEdit" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionPrescriptionDelete" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="PermissionStatistics" Type="Boolean"></asp:Parameter>
                <asp:Parameter Name="UserName" Type="String"></asp:Parameter>
                <asp:Parameter Name="PermissionId" Type="Object"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>

    <asp:SqlDataSource runat="server" OnSelecting="SDSOtherUser_Selecting" ID="SDSOtherUser" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
        SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Users.LastActivityDate, aspnet_Membership.Email, aspnet_Membership.CreateDate
                       ,aspnet_Membership.Comment as cmt
                       FROM  aspnet_Membership INNER JOIN tbl_MasterChildAccount ON aspnet_Membership.UserId = tbl_MasterChildAccount.UserId INNER JOIN
                       aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE aspnet_Membership.Comment not like '%^%'
                       and tbl_MasterChildAccount.MasterId=@MasterId
        
                         union all

                        SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Users.LastActivityDate, aspnet_Membership.Email, aspnet_Membership.CreateDate
                       , cmt='Optometrist'
                       FROM  aspnet_Membership INNER JOIN tbl_MasterChildAccount ON aspnet_Membership.UserId = tbl_MasterChildAccount.UserId INNER JOIN
                       aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId WHERE aspnet_Membership.Comment like '%^%'
                       and tbl_MasterChildAccount.MasterId=@MasterId"
        DeleteCommand="DELETE FROM aspnet_Users WHERE 1=2">
        <SelectParameters>
            <asp:Parameter Name="MasterId" />
        </SelectParameters>
    </asp:SqlDataSource>


    <!-- Modal CreateUser -->
    <div id="ModalCreateUser" runat="server">
        <div class="modal fade modal-open" id="CreateUserModal" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal Exam type content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create new user</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" CompleteSuccessText="User has been successfully created." Width="250" ContinueDestinationPageUrl="~/home.aspx" LoginCreatedUser="False" OnCreatedUser="CreateUserWizard1_CreatedUser" UnknownErrorMessage="User was not created. Please try again.">
                                <CreateUserButtonStyle CssClass="btn btn-primary" />
                                <WizardSteps>
                                    <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="UserName" Width="200" runat="server"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" Display="Dynamic" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="Password" Width="200" runat="server" TextMode="Password"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="PasswordRequired" Display="Dynamic" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                       <%-- <asp:RegularExpressionValidator ID="Regex2" runat="server" ControlToValidate="Password" Display="Dynamic"
                                                            ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
                                                            ErrorMessage="Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="ConfirmPassword" Width="200" runat="server" TextMode="Password"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" Display="Dynamic" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="Email" Width="200" runat="server"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="EmailRequired" Display="Dynamic" runat="server" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="Label1" runat="server" AssociatedControlID="RblProfessional">Professional:</asp:Label>
                                                        <asp:UpdatePanel ID="UpdatePanelProfessional" runat="server">
                                                            <ContentTemplate>
                                                                <asp:RadioButtonList ID="RblProfessional" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" RepeatLayout="Flow">
                                                                    <asp:ListItem Selected="True">Yes</asp:ListItem>
                                                                    <asp:ListItem style="margin-left: 5px;">No</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                 <br />
                                                                <asp:CheckBoxList ID="CblDays" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                                    <asp:ListItem>Mon</asp:ListItem>
                                                                    <asp:ListItem>Tue</asp:ListItem>
                                                                    <asp:ListItem>Wed</asp:ListItem>
                                                                    <asp:ListItem>Thu</asp:ListItem>
                                                                    <asp:ListItem>Fri</asp:ListItem>
                                                                    <asp:ListItem>Sat</asp:ListItem>
                                                                    <asp:ListItem>Sun</asp:ListItem>
                                                                </asp:CheckBoxList>                                                                  
                                                                 <asp:Label ID="lbl_Position" runat="server" AssociatedControlID="txt_Position">Position:</asp:Label>
                                                                   <br />
                                                                 <asp:TextBox ID="txt_Position" Width="200" runat="server"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2" style="color: Red;">
                                                        <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:CreateUserWizardStep>
                                    <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                                    </asp:CompleteWizardStep>
                                </WizardSteps>
                            </asp:CreateUserWizard>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--<asp:Button ID="Button_Examtype" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Examtype_Group" Text="Save" OnClientClick="addexamtype();" />--%>
                        <%--<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal Status -->
    <asp:UpdatePanel ID="UpdatePanelStatusModel" runat="server">
        <ContentTemplate>
            <div id="ModalStatus" runat="server">
                <div class="modal fade modal-open" id="StatusModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal Status content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Status</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <label for="TextBox_Status">Status:</label>
                                    <asp:TextBox ID="TextBox_Status" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_Status" runat="server" ValidationGroup="Button_Status_Group" ControlToValidate="TextBox_Status" ErrorMessage="Status is required!"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_Status" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Status_Group" OnClick="Button_Status_Click" OnClientClick="distroymodal();" Text="Save" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

