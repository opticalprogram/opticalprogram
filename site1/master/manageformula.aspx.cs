﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageformula : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Formula";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        objdal = new Dal();
        FormulaTypeSelection();

        if (!IsPostBack)
        {
            ViewState["EditId"] = "";
        }
    }

    protected void FormulaTypeSelection()
    {
        if (RBL_Formula.SelectedValue.ToString() == "Custom")
        {
            Txt_FixedPrice.Visible = false;
            lblFixedPrice.Visible = false;
            Lbl_formulaHint.Text = "Sample: (COST*2)+15";
        }
        else
        {
            Txt_FixedPrice.Visible = true;
            lblFixedPrice.Visible = true;
            Lbl_formulaHint.Text = "Sample: 20 TO 100";
        }
    }

    protected void Button_Formula_Click(object sender, EventArgs e)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (Txt_Formula.Text.Trim().ToUpper().Contains("COST") || Txt_Formula.Text.Trim().ToUpper().Contains("TO"))
            {

                if (ViewState["EditId"].ToString() != "")
                {
                    //UPDATE [dbo].[tbl_Formula] SET [FormulaName] = '',[FormulaString] = '',[FormulaFixedPrice] = ''  WHERE [FormulaId]=''
                    sb.Append("UPDATE [dbo].[tbl_Formula] SET [FormulaName] ='" + Txt_FormulaName.Text + "',[FormulaString] ='" + Txt_Formula.Text.Trim().ToUpper() + "'");
                    sb.Append(",[FormulaFixedPrice] ='" + Txt_FixedPrice.Text + "',[MasterId]='" + MasterId + "'  WHERE [FormulaId] ='" + ViewState["EditId"].ToString() + "' ");
                    objdal.EXECUTE_DML(sb.ToString());
                }
                else
                {

                    //INSERT INTO [dbo].[tbl_Formula]([FormulaName],[FormulaString],[FormulaFixedPrice],[MasterId])VALUES(,,,)
                    sb.Append("INSERT INTO [dbo].[tbl_Formula]([FormulaName],[FormulaString],[FormulaFixedPrice],[MasterId])");
                    sb.Append("VALUES('" + Txt_FormulaName.Text + "','" + Txt_Formula.Text.Trim().ToUpper() + "','" + Txt_FixedPrice.Text + "','" + MasterId + "')");

                    objdal.EXECUTE_DML(sb.ToString());
                }

                Response.Redirect("~/master/manageformula.aspx", true);
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Formula string is incorrect! Missing a keyword: TO or COST";
            }
        }
        catch (Exception ex)
        {
            
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
    protected void GVFormula_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        lblMessage.Visible = true;

        if (e.Exception == null)
        {
            lblMessage.Text = "Formula is deleted successfully!";
        }
        else
        {
            lblMessage.CssClass = "alert alert-danger";
            lblMessage.Text = "Error: " + e.Exception;
            e.ExceptionHandled = true;
        }
    }
    protected void GVFormula_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GVFormula.SelectedIndex = e.NewSelectedIndex;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            string editid = GVFormula.SelectedValue.ToString();
            ViewState["EditId"] = editid;

            DataTable dt = new DataTable();
            dt = objdal.GET_DATATABLE("SELECT [FormulaName],[FormulaString],CONVERT(DECIMAL(10,2),([FormulaFixedPrice])) as FormulaFixedPrice FROM [dbo].[tbl_Formula] where [FormulaId]='" + editid + "'");
            Txt_FormulaName.Text = dt.Rows[0]["FormulaName"].ToString();
            Txt_Formula.Text = dt.Rows[0]["FormulaString"].ToString();
            Txt_FixedPrice.Text = dt.Rows[0]["FormulaFixedPrice"].ToString();

            // Edit time select Formula type
            if (Txt_Formula.Text.Trim().ToUpper().Contains("COST"))
            {
                RBL_Formula.SelectedValue = "Custom";
            }
            else
            {
                RBL_Formula.SelectedValue = "Fixed";
            }
            FormulaTypeSelection();
        }
        catch (Exception ex)
        {
            
             lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    protected void SDSFormula_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
}