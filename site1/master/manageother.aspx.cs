﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class master_addother : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;

    protected void Page_Init(object sender, EventArgs e)
    {// Stop Caching in IE
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

        // Stop Caching in Firefox
        Response.Cache.SetNoStore();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        

        // Set title        
        string title = "Master";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();


        objdal = new Dal();

        HideCreateUserButton(Membership.GetUser().ProviderUserKey.ToString());


        if (((RadioButtonList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("RblProfessional")).SelectedValue == "No")
        {
            ((CheckBoxList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("CblDays")).Visible = false;
            lbl_Position.Visible = true;
            txt_Position.Visible = true;
        }
        else
        {
            lbl_Position.Visible = false;
            txt_Position.Visible = false;
            ((CheckBoxList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("CblDays")).Visible = true;
        }

        Image_Master.ImageUrl = "~/images/" + Membership.GetUser().Comment.Split(new char[] { '^' })[1];

        if(!IsPostBack)
        {
            statusFill();
        }


    }
    private void HideCreateUserButton(string userID)
    {
        //if (objdal.GetFromSP("SpIsTotalLoginUsed",userID ) == "True")
        //{
        //    CreateUserButton.Disabled=true;
        //    CreateUserButton.Style.Add("background-color","gray");
        //    CreateUserButton.Attributes.Remove("data-target");
        //}

        string TotalLogins = Membership.GetUser().Comment.Split(new char[] { '^' })[2];
        string UsedLogins = objdal.Get_SingleValue("SELECT count(UserId) FROM tbl_MasterChildAccount where tbl_MasterChildAccount.MasterId='" + userID + "'");
        if (Convert.ToInt16(UsedLogins) >= Convert.ToInt16(TotalLogins))
        {
            CreateUserButton.Disabled = true;
            CreateUserButton.Style.Add("background-color", "gray");
            CreateUserButton.Attributes.Remove("data-target");
        }
       
    }
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        try
        {
            string cmt = "";
            string CurrentUserId,CurrentUserName;
            if (((RadioButtonList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("RblProfessional")).SelectedValue == "Yes") // For Professional User
            {
                 cmt = "PROFESSIONAL"; 

                foreach (ListItem day in ((CheckBoxList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("CblDays")).Items)
                {
                    if (day.Selected)
                    {
                        cmt += "^" + day.Value;
                    }
                }

                MembershipUser usr = Membership.GetUser(CreateUserWizard1.UserName);
                CurrentUserName = usr.UserName;
                usr.Comment = cmt;
                CurrentUserId = usr.ProviderUserKey.ToString();
                Membership.UpdateUser(usr);

            }
            else // other User
            {
                cmt = txt_Position.Text; 

                MembershipUser usr = Membership.GetUser(CreateUserWizard1.UserName);
                CurrentUserName = usr.UserName;
                usr.Comment = cmt;
                CurrentUserId = usr.ProviderUserKey.ToString();
                Membership.UpdateUser(usr);
            }

            Roles.AddUserToRole(CreateUserWizard1.UserName, "Other");

            //master-child Entry
           
            objdal.EXECUTE_DML("INSERT INTO [dbo].[tbl_MasterChildAccount]([MasterId],[UserId])VALUES('" + MasterId + "','" + CurrentUserId + "')");

            // Permission Default Entry for new User
            objdal.EXECUTE_DML("INSERT INTO [dbo].[tbl_Permission]([UserId],[UserName]) VALUES ('" + CurrentUserId + "','" + CurrentUserName.Replace("'", "''") + "')");
            Response.Redirect("~/master/manageother.aspx", true);
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }

    }

    protected void GVOtherUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            //User id 
            Label DeleteId = (Label)GVOtherUser.Rows[e.RowIndex].FindControl("lbl_UserId");
            Guid userIdToDelete = new Guid(DeleteId.Text);
            //Delete User with its id
            Membership.DeleteUser(Membership.GetUser(userIdToDelete).UserName, true);
            //Membership.DeleteUser(GVOtherUser.Rows[e.RowIndex].Cells[2].Text, true);
            Response.Redirect("~/master/manageother.aspx",false);
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
       
    }

    protected void SDSOtherUser_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void Button_Upload_Click(object sender, EventArgs e)
    {
        if (FileUpload_MasterHome.HasFile)
        {
            string OldImage = Membership.GetUser().Comment.Split(new char[] { '^' })[1];
            string imageExt = FileUpload_MasterHome.FileName.Split('.')[1];
            string newimage = MasterId + "." + imageExt;
            MembershipUser usr = Membership.GetUser();
            usr.Comment = usr.Comment.Replace(OldImage, newimage);
            Membership.UpdateUser(usr);
            if (OldImage!="default.jpg")
            {
                File.Delete(Server.MapPath("~/images/" + OldImage)); 
            }           
            FileUpload_MasterHome.SaveAs(Server.MapPath("~/images/" + newimage));
            Response.Redirect("~/master/manageother.aspx");
        }
        
    }

    protected void Button_Status_Click(object sender, EventArgs e)
    {
        objdal.EXECUTE_DML("INSERT INTO [dbo].[tbl_status]([status],[MasterId])VALUES('" + TextBox_Status.Text + "','" + MasterId + "')");
        statusFill();
    }

    //for status fill
    protected void statusFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT [statusId],[status] FROM [dbo].[tbl_status] where MasterId='" + MasterId + "'");
        DDL_Status.DataSource = dt;
        DDL_Status.DataValueField = "statusId";
        DDL_Status.DataTextField = "status";
        DDL_Status.DataBind();
    }

    protected void SDSStatus_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
}