﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="manageformula.aspx.cs" Inherits="manageformula" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function openModal() {
            $('#AddFormulaModal').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_addinventory" NavigateUrl="~/master/manageother.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage user</asp:HyperLink></li>
        <li>
            <span class="btn btn-sm btn-default LinkButton glyphicon glyphicon-baby-formula" data-toggle="modal" style="cursor: pointer;" data-target="#AddFormulaModal"><span style="font-family: Arial;">&nbsp;Add Formula</span></span>
        </li>
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" Width="500" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>

    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView ID="GVFormula" CssClass="table table-hover table-striped" runat="server" Width="500px" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataKeyNames="FormulaId" DataSourceID="SDSFormula" OnRowDeleted="GVFormula_RowDeleted" OnSelectedIndexChanging="GVFormula_SelectedIndexChanging">
            <Columns>
                <asp:BoundField DataField="FormulaId" Visible="false" ReadOnly="True" SortExpression="FormulaId" />
                <asp:BoundField DataField="FormulaName" HeaderText="Name" SortExpression="FormulaName" />
                <asp:BoundField DataField="FormulaString" HeaderText="Formula" SortExpression="FormulaString" />
                <asp:BoundField DataField="FormulaFixedPrice" HeaderText="Fixed Price" SortExpression="FormulaFixedPrice" DataFormatString="{0:0.00}" />
                <asp:CommandField ShowSelectButton="true" SelectText="Edit" ControlStyle-CssClass="btn-success btn btn-xs"></asp:CommandField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs"></asp:CommandField>
            </Columns>

            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>

        <asp:SqlDataSource ID="SDSFormula" OnSelecting="SDSFormula_Selecting" runat="server" ConnectionString="<%$ ConnectionStrings:CN-OpticalProgram %>"
            SelectCommand="SELECT [FormulaName], [FormulaString], [FormulaFixedPrice], [FormulaId] FROM [tbl_Formula] 
                           where [tbl_Formula].MasterId =@MasterId ORDER BY [FormulaName]"
            DeleteCommand="DELETE FROM [dbo].[tbl_Formula] WHERE FormulaId=@FormulaId">
            <SelectParameters>
                <asp:Parameter Name="MasterId"/>
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="FormulaId" />
            </DeleteParameters>
        </asp:SqlDataSource>

    </div>

    <!-- Modal Formula -->

    <div id="ModalFormula" runat="server">
        <div class="modal fade modal-open" id="AddFormulaModal" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal city content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add new Formula</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <asp:UpdatePanel ID="UpdatePanelFormulaType" runat="server">
                                <ContentTemplate>
                            <label for="RBL_Formula">Formula Type:</label>                            
                                    <asp:RadioButtonList ID="RBL_Formula" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                                        <asp:ListItem>Fixed</asp:ListItem>
                                        <asp:ListItem Selected="True" style="margin-left: 5px;">Custom</asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div>
                            <label for="Txt_FormulaName">Formula Name:</label>
                            <asp:TextBox ID="Txt_FormulaName" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_FormulaName" runat="server" ControlToValidate="Txt_FormulaName" ErrorMessage="Formula name is required!"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <label for="Txt_Formula">Formula:</label>
                            <asp:TextBox ID="Txt_Formula" CssClass="form-control" runat="server" style="text-transform:uppercase;"></asp:TextBox>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <asp:Label ID="Lbl_formulaHint" Text="" runat="server"></asp:Label>
                                     </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:RequiredFieldValidator ID="RFV_Formula" runat="server" ControlToValidate="Txt_Formula" ErrorMessage="Formula string is required!"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <label id="lblFixedPrice" runat="server" for="Txt_FixedPrice">Fixed Price:</label>
                                    <asp:TextBox ID="Txt_FixedPrice" ClientIDMode="Static" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Button_Formula" runat="server" CssClass="btn btn-default" Text="Save" OnClick="Button_Formula_Click" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

