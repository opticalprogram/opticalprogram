﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="agenda.aspx.cs" Inherits="agenda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <link href="css/fullcalendar.css" rel="stylesheet" />
    <link href="css/fullcalendar.print.css" rel="stylesheet" media='print' />
    <script src="lib/moment.min.js"></script>

    <%--<script src="lib/jquery.min.js"></script>--%>
    <script src="js/fullcalendar.js"></script>
    <link href="css/cupertino/jquery-ui.css" rel="stylesheet" />
    <link href="css/jquery.qtip-2.2.0.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.qtip-2.2.0.js" type="text/javascript"></script>
    <script src="js/calendarscript.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">

        var UpdatePaneladdpatient = '<%=UpdatePaneladdpatient.ClientID%>';

        function addpatientddl() {
            if (UpdatePaneladdpatient != null) {
                __doPostBack(UpdatePaneladdpatient, '');
            }
        }

        var UpdatepaneleventPatient = '<%=UpdatepaneleventPatient.ClientID%>';

        function eventpatientddl() {
            if (UpdatepaneleventPatient != null) {
                __doPostBack(UpdatepaneleventPatient, '');
            }
        }

    </script>
    <link href="css/multiselect.css" rel="stylesheet" />
    <script src="js/multiselect.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=Lst_Monday]').multiselect({
                includeSelectAllOption: true
            });

            $('[id*=Lst_Tuesday]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=Lst_Wednesday]').multiselect({
                includeSelectAllOption: true
            });

            $('[id*=Lst_Thursday]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=Lst_Friday]').multiselect({
                includeSelectAllOption: true
            });

            $('[id*=Lst_Saturday]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=Lst_Sunday]').multiselect({
                includeSelectAllOption: true
            });

        });
    </script>

    <script>
        function setKeyword() {
            document.getElementById('<%=Hdn_keyword.ClientID %>').value = document.getElementById('<%=txt_Keyword_Patient.ClientID %>').value
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>

    <input id="TextGotoDate" data-toggle="tooltip" title="Go to Date function: Just enter the date in the textbox and the calendar will automatically show that date." type="text" onkeyup="gotomydate();" oninput="gotomydate();" placeholder="dd-mm-yyyy" />
    <span id="SettigLink" class="glyphicon glyphicon-cog btn btn-sm btn-default" data-toggle="modal" style="cursor: pointer;" data-target="#SettingModal"><span style="font-family: Arial,sans-serif;">&nbsp;Settings</span></span>
    <asp:HyperLink ID="HL_managepatients" ClientIDMode="Static" NavigateUrl="~/patient.aspx" CssClass="btn btn-sm btn-default LinkButton" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Patient</asp:HyperLink>

    <asp:Label ID="lblMessage" ClientIDMode="Static" runat="server" Style="display: none;" CssClass="alert alert-info message"></asp:Label>

    <div id="calendar" class="fc-clear" style="border-bottom: 1px solid #3BAAE3;">
    </div>
    <asp:HiddenField ID="Hdn_keyword" runat="server" />

    <div id="updatedialog" style="font: 85% Arial,sans-serif; display: none;" title="Update Appointment">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">Patient :</label>
            <asp:UpdatePanel runat="server" ID="UpdatepaneleventPatient">
                <ContentTemplate>
                    <asp:DropDownList CssClass="form-control" EnableViewState="false" ID="eventPatient" ClientIDMode="Static" runat="server" DataSourceID="SDSPatient" DataTextField="Title" DataValueField="PatientId"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">Professional :</label>
            <asp:DropDownList CssClass="form-control" ID="eventProfessional" EnableViewState="false" ClientIDMode="Static" runat="server" DataSourceID="SDSprofessional" DataTextField="UserName" DataValueField="UserId"></asp:DropDownList>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">Exam type :</label>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:DropDownList CssClass="form-control" EnableViewState="false" ID="eventType" ClientIDMode="Static" runat="server" DataSourceID="SDSExamType" DataTextField="ExamType" DataValueField="ExamTypeId"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">Status :</label>
            <asp:DropDownList ClientIDMode="Static" ID="eventStatus" CssClass="form-control" runat="server" DataSourceID="SDSStatus" DataTextField="status" DataValueField="status">
            </asp:DropDownList>
            <asp:SqlDataSource runat="server" OnSelecting="SDSStatus_Selecting" ID="SDSStatus" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                SelectCommand="SELECT [status] FROM [tbl_status] where [tbl_status].MasterId=@MasterId">
                <SelectParameters>
                    <asp:Parameter Name="MasterId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">Description :</label>
            <textarea id="eventDesc" class="form-control" rows="2"></textarea>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">Start :</label>
            <input id="eventStart" type="text" class="form-control" />
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <label class="labelspace">End :</label>
            <input id="eventEnd" type="text" class="form-control" />
        </div>
        <input type="hidden" id="eventId" />
    </div>


    <div id="addDialog" style="font: 85% Arial,sans-serif; display: none;" title="Add Appointment">
        <asp:UpdatePanel runat="server" ID="UpdatePaneladdpatient">
            <ContentTemplate>
                <div class="col-lg-3 col-md-3 col-sm-4">
                  <label for="txt_Keyword_Patient" class="labelspace">Keyword:</label>&nbsp;<asp:LinkButton ID="LinkButton_patient" runat="server" OnClick="LinkButton_patient_Click"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                  <asp:TextBox ID="txt_Keyword_Patient" MaxLength="10" ClientIDMode="Static" CssClass="form-control" runat="server" onkeyup="setKeyword();" oninput="setKeyword();"></asp:TextBox>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <label class="labelspace" for="addPatient">Add new or Select a patient :</label>
                    <asp:DropDownList CssClass="form-control" ID="addPatient" EnableViewState="false" ClientIDMode="Static" runat="server" DataTextField="Title" DataValueField="PatientId"  AppendDataBoundItems="True" onchange="OnPatientSelection()">
                        <asp:ListItem Text="New patient" Value="55555555-5555-5555-5555-555555555555" />
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="SDSPatient" OnSelecting="SDSPatient_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                        SelectCommand="SELECT [PatientId], [FirstName]+' '+[LastName]+' '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Title] FROM [tbl_Patient] where [tbl_Patient].MasterId=@MasterId ORDER BY [Title]">
                        <SelectParameters>
                            <asp:Parameter Name="MasterId" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_LastName" class="labelspace">Last Name :</label>
            <input id="Txt_LastName" type="text" class="form-control" />
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">First Name :</label>
            <input id="Txt_FirstName" type="text" class="form-control" />
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">Date of Birth :</label>
            <input id="Txt_DateofBirth" type="text" class="form-control" placeholder="DD-MM-YYYY" />
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_HomePhone" class="labelspace">Home Phone :</label>
            <input id="Txt_HomePhone" type="text" class="form-control" placeholder="(999)-999-9999" />
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">Cell Phone :</label>
            <input id="Txt_CellPhone" type="text" class="form-control" placeholder="(999)-999-9999" />
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">Professional :</label>

            <asp:DropDownList CssClass="form-control" ID="addProfessional" EnableViewState="false" ClientIDMode="Static" runat="server" DataSourceID="SDSprofessional" DataTextField="UserName" DataValueField="UserId"></asp:DropDownList>
            <asp:SqlDataSource runat="server" OnSelecting="SDSprofessional_Selecting" ID="SDSprofessional" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
                SelectCommand="SELECT  aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Comment, tbl_MasterChildAccount.MasterId
                               FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId INNER JOIN
                               tbl_MasterChildAccount ON aspnet_Users.UserId = tbl_MasterChildAccount.UserId 
                               where tbl_MasterChildAccount.MasterId=@MasterId 
                               and aspnet_Membership.Comment like '%PROFESSIONAL%'">
                <SelectParameters>
                    <asp:Parameter Name="MasterId" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">Exam type :</label>&nbsp;<span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" onclick="closedialog();" data-target="#ExamtypeModal"></span>&nbsp; 
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DropDownList CssClass="form-control" ID="addExamType" EnableViewState="false" ClientIDMode="Static" runat="server" DataSourceID="SDSExamType" DataTextField="ExamType" DataValueField="ExamTypeId"></asp:DropDownList>
                            <asp:SqlDataSource runat="server" ID="SDSExamType" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>' SelectCommand="SELECT [ExamTypeId], [ExamType] FROM [tbl_ExamType] ORDER BY [ExamType]"></asp:SqlDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">Description :</label>
            <textarea id="addEventDesc" class="form-control" rows="2"></textarea>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <label class="labelspace">Repeat :</label>
            <label>Next</label>
            <input style="width: 25px;" id="addRepeatNumber" type="text" value="01" />
            <br />
            <input id="RadioWeek" name="Repeat" type="radio" />
            <label for="RadioWeek">Week</label>
            <input name="Repeat" id="RadioMonth" type="radio" />
            <label for="RadioMonth">Month</label>
            <input name="Repeat" id="RadioYear" type="radio" />
            <label for="RadioYear">Year</label>
        </div>
    </div>
    <div runat="server" id="jsonDiv" />
    <input type="hidden" id="hdClient" runat="server" />

    <!-- Modal Examtype -->
    <asp:UpdatePanel ID="UpdatePanelExamtypeModel" runat="server">
        <ContentTemplate>
            <div id="ModalExamtype" runat="server">
                <div class="modal fade modal-open" id="ExamtypeModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal Exam type content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Exam type</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <label for="TextBox_Examtype">Exam type:</label>
                                    <asp:TextBox ID="TextBox_Examtype" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_Examtype" runat="server" ValidationGroup="Button_Examtype_Group" ControlToValidate="TextBox_Examtype" ErrorMessage="Examtype is required!"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_Examtype" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Examtype_Group" Text="Save" OnClientClick="addexamtype();" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal Setting -->
    <div id="ModalSetting" runat="server">
        <div class="modal fade modal-open" id="SettingModal" role="dialog">
            <div class="modal-dialog modal-md">

                <!-- Modal Exam type content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Set agenda time and professional</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label>Agenda Start Time: </label>
                            <asp:DropDownList ClientIDMode="Static" ID="DDL_MinHour" runat="server">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ClientIDMode="Static" ID="DDL_MinMinute" runat="server">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div>
                            <label class="labelspace">Agenda End Time:&nbsp;</label>
                            <asp:DropDownList ClientIDMode="Static" ID="DDL_MaxHour" runat="server">
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ClientIDMode="Static" ID="DDL_MaxMinute" runat="server">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div>
                            <label for="DDL_Slot" class="labelspace">Agenda Slot Time: </label>
                            <asp:DropDownList ClientIDMode="Static" ID="DDL_Slot" runat="server">
                                <asp:ListItem Value="60">1 Hour</asp:ListItem>
                                <%--                                <asp:ListItem Value="45">45 Minutes</asp:ListItem>--%>
                                <asp:ListItem Value="30">30 Minutes</asp:ListItem>
                                <asp:ListItem Value="15">15 Minutes</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <label for="Lst_Monday" class="labelspace">Monday Professionals:</label>
                            <asp:ListBox ID="Lst_Monday" runat="server" ClientIDMode="Static" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId" Rows="4"></asp:ListBox>
                        </div>
                        <div>
                            <label for="Lst_Tuesday" class="labelspace">Tuesday Professionals:</label>
                            <asp:ListBox ID="Lst_Tuesday" runat="server" ClientIDMode="Static" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId"></asp:ListBox>
                        </div>
                        <div>
                            <label for="Lst_Wednesday" class="labelspace">Wednesday Professionals:</label>
                            <asp:ListBox ID="Lst_Wednesday" runat="server" ClientIDMode="Static" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId"></asp:ListBox>
                        </div>
                        <div>
                            <label for="Lst_Thursday" class="labelspace">Thursday Professionals:</label>
                            <asp:ListBox ID="Lst_Thursday" runat="server" ClientIDMode="Static" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId"></asp:ListBox>
                        </div>
                        <div>
                            <label for="Lst_Friday" class="labelspace">Friday Professionals:</label>
                            <asp:ListBox ID="Lst_Friday" runat="server" ClientIDMode="Static" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId"></asp:ListBox>
                        </div>
                        <div>
                            <label for="Lst_Saturday" class="labelspace">Saturday Professionals:</label>
                            <asp:ListBox ID="Lst_Saturday" runat="server" ClientIDMode="Static" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId"></asp:ListBox>
                        </div>
                        <div>
                            <label for="Lst_Sunday" class="labelspace">Sunday Professionals:</label>
                            <asp:ListBox ID="Lst_Sunday" OnPreRender="Lst_Sunday_PreRender" ClientIDMode="Static" runat="server" SelectionMode="Multiple" DataSourceID="SDSProfession" DataTextField="UserName" DataValueField="UserId"></asp:ListBox>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Btn_Setting" runat="server" CssClass="btn btn-default" Text="Save" OnClick="Btn_Setting_Click" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <asp:SqlDataSource runat="server" ID="SDSProfession" OnSelecting="SDSProfession_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
        SelectCommand="SELECT  aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Comment, tbl_MasterChildAccount.MasterId
                               FROM aspnet_Membership INNER JOIN aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId INNER JOIN
                               tbl_MasterChildAccount ON aspnet_Users.UserId = tbl_MasterChildAccount.UserId 
                               where tbl_MasterChildAccount.MasterId=@MasterId
                               and aspnet_Membership.Comment like '%PROFESSIONAL%'">
        <SelectParameters>
            <asp:Parameter Name="MasterId" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

