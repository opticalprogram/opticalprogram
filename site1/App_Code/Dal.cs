﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for Dal
/// </summary>
public class Dal
{
    public SqlConnection cn;
    public SqlCommand cmd;
    public SqlDataAdapter da;
    public DataTable dt;
    public Dal()
    {
        // for database connection 
        cn = new SqlConnection();        
        cn.ConnectionString = ConfigurationManager.ConnectionStrings["CN-OpticalProgram"].ConnectionString;
    }

    public string GetFromSP(string spName, string spParameter)
    {
        cmd = new SqlCommand();
        cmd.Connection = cn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText=spName;
        cmd.Parameters.AddWithValue("@UserId", spParameter);
        cmd.Parameters.Add("@result", SqlDbType.Bit);
        cmd.Parameters["@result"].Direction = ParameterDirection.Output;
        cn.Open();
        cmd.ExecuteNonQuery();
        cn.Close();
        return cmd.Parameters["@result"].Value.ToString();
    }

    // for execute dml statement
    public void EXECUTE_DML(string SQLQuery)
    {
        cmd = new SqlCommand();
        cmd.Connection = cn;
        if (cn.State == ConnectionState.Closed)
        {
            cn.Open();
        }
        cmd.CommandText = SQLQuery;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        cn.Close();
    }

    // for Get single value
    public string Get_SingleValue(string SQLQuery)
    {
        string retval;
        cmd = new SqlCommand();
        cmd.Connection = cn;
        if (cn.State == ConnectionState.Closed)
        {
            cn.Open();
        }
        cmd.CommandText = SQLQuery;
        retval = cmd.ExecuteScalar().ToString();
        cmd.Dispose();
        cn.Close();
        return retval;
    }

    // for execute ddl statement
    public DataTable GET_DATATABLE(string SQLQuery)
    {
        dt = new DataTable();
        cmd = new SqlCommand(SQLQuery, cn);
        da = new SqlDataAdapter(cmd);
        if (cn.State == ConnectionState.Closed)
        {
            cn.Open();
        }
        da.Fill(dt);
        da.Dispose();
        cn.Close();
        return dt;
    }

    // for execute storeprocedure
    public DataTable GET_STOREPROCEDUREDATA(string StoreProcedureName, string parameter)
    {
        dt = new DataTable();
        dt = GET_DATATABLE("exec " + StoreProcedureName + " '" + parameter +"'");
        return dt;
    }
}