﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Do not use this object, it is used just as a go between between javascript and asp.net
public class ImproperCalendarEvent
{
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string start { get; set; }
    public string end { get; set; }
    public bool allDay { get; set; }
    public int ExamTypeId { get; set; }
    public Guid PatientId { get; set; }
    public string ExamType { get; set; }
    public Guid ProfessionalId { get; set; }
    public string Professional { get; set; }
    public string Status { get; set; }
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string DateOfBirth { get; set; }
    public string HomePhone { get; set; }
    public string CellPhone { get; set; }
}
