﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for General
/// </summary>
public class General
{
    Dal objdal;
	public General()
	{
        objdal = new Dal();
	}

    //remove temp Entry
    public void removeTempEntry()
    {
        //delete from [tbl_InvoiceMaster] WHERE [tbl_InvoiceMaster].InvoiceMasterIsTemporary='1' 
        // and [tbl_InvoiceMaster].InvoiceMasterDate<CONVERT(VARCHAR(10),DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), -1),120)
        string query = "  delete from [tbl_InvoiceMaster] WHERE [tbl_InvoiceMaster].InvoiceMasterIsTemporary='1'"
                       + " and [tbl_InvoiceMaster].InvoiceMasterDate<=CONVERT(VARCHAR(10),DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), -1),120)";
        objdal.EXECUTE_DML(query);
    }
}