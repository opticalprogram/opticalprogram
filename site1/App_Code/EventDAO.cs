﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Web.Security;

/// <summary>
/// EventDAO class is the main class which interacts with the database. SQL Server express edition
/// has been used.
/// the event information is stored in a table named 'event' in the database.
///
/// Here is the table format:
/// event(event_id int, title varchar(100), description varchar(200),event_start datetime, event_end datetime)
/// event_id is the primary key
/// </summary>
public class EventDAO
{
	//change the connection string as per your database connection.
    private static string connectionString = ConfigurationManager.ConnectionStrings["CN-OpticalProgram"].ConnectionString;
    public static string MasterId;
    //private static string OtherId = "d8746b76-20b9-4b21-acb7-5cfc866098aa";

	//this method retrieves all events within range start-end
    public static List<CalendarEvent> getEvents(DateTime start, DateTime end)
    {   
        List<CalendarEvent> events = new List<CalendarEvent>();
        SqlConnection con = new SqlConnection(connectionString);
        StringBuilder sb = new StringBuilder();
        sb.Append("SELECT event_id, [description],FirstName+' '+LastName+ ' ('+CONVERT(nvarchar, DATEDIFF(hour,DateOfBirth,GETDATE())/8766)+') '+ExamType as title, [tbl_Appointment].ProfessionalId,[aspnet_Users].UserName as Professional");
        sb.Append(", [tbl_Patient].[PatientId],event_start, event_end, all_day, tbl_Appointment.ExamTypeId");
        sb.Append(",ExamType,[Status] FROM tbl_Appointment,tbl_ExamType,tbl_Patient,aspnet_Users  where ");
        sb.Append("tbl_Appointment.ExamTypeId=tbl_ExamType.ExamTypeId and tbl_Appointment.PatientId=tbl_Patient.PatientId and ");
        sb.Append("event_start>=@start AND event_end<=@end and aspnet_Users.UserId=tbl_Appointment.ProfessionalId and tbl_Appointment.MasterId='" + MasterId + "'");
        SqlCommand cmd = new SqlCommand(sb.ToString(), con);
        cmd.Parameters.Add("@start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@end", SqlDbType.DateTime).Value = end;
        
        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }
           
            SqlDataReader reader = cmd.ExecuteReader();           
            while (reader.Read())
            {
                CalendarEvent cevent = new CalendarEvent();
                cevent.id = (int)reader["event_id"];
                cevent.title = (string)reader["title"];
                cevent.description = (string)reader["description"];
                cevent.start = (DateTime)reader["event_start"];
                cevent.end = (DateTime)reader["event_end"];
                cevent.allDay = (bool)reader["all_day"];
                cevent.PatientId = (Guid)reader["PatientId"];
                cevent.ExamTypeId = (int)reader["ExamTypeId"];
                cevent.ExamType = (string)reader["ExamType"];
                cevent.ProfessionalId = (Guid)reader["ProfessionalId"];
                cevent.Professional = (string)reader["Professional"];
                cevent.Status = reader["Status"] == DBNull.Value ? "" : (string)reader["Status"];
                events.Add(cevent);
            }
        }
        con.Close();
        return events;
        //side note: if you want to show events only related to particular users,
        //if user id of that user is stored in session as Session["userid"]
        //the event table also contains an extra field named 'user_id' to mark the event for that particular user
        //then you can modify the SQL as:
        //SELECT event_id, description, title, event_start, event_end FROM event where user_id=@user_id AND event_start>=@start AND event_end<=@end
        //then add paramter as:cmd.Parameters.AddWithValue("@user_id", HttpContext.Current.Session["userid"]);
    }

	//this method updates the event title and description
    public static void updateEvent(int id, Guid ProfessionalId, Guid PatientId, String description, int ExamTypeId, String Status, DateTime start, DateTime end)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE tbl_Appointment SET ProfessionalId=@ProfessionalId, PatientId=@PatientId, description=@description, "
        + " ExamTypeId=@ExamTypeId, Status=@Status, event_start=@event_start, event_end=@event_end WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@ProfessionalId", SqlDbType.UniqueIdentifier).Value = ProfessionalId;
        cmd.Parameters.Add("@PatientId", SqlDbType.UniqueIdentifier).Value = PatientId;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value= description;
        cmd.Parameters.Add("@ExamTypeId", SqlDbType.Int).Value = ExamTypeId;
        cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
        cmd.Parameters.Add("@event_id", SqlDbType.NVarChar).Value = id;
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = end;

        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }          
            cmd.ExecuteNonQuery();
            con.Close();
        }

        if (Status == "did not come")
        {
            con = new SqlConnection(connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select COUNT(*) as cnt from tbl_Appointment where PatientId='"+ PatientId +"'", con);
            da.Fill(dt);
            if (dt.Rows[0]["cnt"].ToString() == "1")
            {
                cmd = new SqlCommand("delete from tbl_Patient where PatientId='" + PatientId + "'",con);
                using (con)
                {
                    if (ConnectionState.Closed == con.State)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            
        }
    }

	//this method updates the event start and end time ... allDay parameter added for FullCalendar 2.x
    public static void updateEventTime(int id, DateTime start, DateTime end, bool allDay)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE tbl_Appointment SET event_start=@event_start, event_end=@event_end, all_day=@all_day WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = allDay;

        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }

	//this mehtod deletes event with the id passed in.
    public static void deleteEvent(int id, string PatientId)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("DELETE FROM tbl_Appointment WHERE (event_id = @event_id)", con);
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }
            cmd.ExecuteNonQuery();
            con.Close();
        }
       
            con = new SqlConnection(connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select COUNT(*) as cnt from tbl_Appointment where PatientId='" + PatientId + "'", con);
            da.Fill(dt);
            if (dt.Rows[0]["cnt"].ToString() == "0")
            {
                cmd = new SqlCommand("delete from tbl_Patient where PatientId='" + PatientId + "'", con);
                using (con)
                {
                    if (ConnectionState.Closed == con.State)
                    {
                        con.Open();
                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

       
    }

    //this method prevent the overlap event for same professional
    public static int CheckScheduleExist(CalendarEvent cevent)
    {
        int retval = 0;
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("SELECT COUNT(*) as CheckExist FROM tbl_Appointment WHERE '" + cevent.end + "' > event_start AND event_end > '" + cevent.start + "' and ProfessionalId='" + cevent.ProfessionalId + "'", con);
        cmd.Parameters.Add("@ProfessionalId", SqlDbType.UniqueIdentifier).Value = cevent.ProfessionalId;
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }
            retval=(int)cmd.ExecuteScalar();
            con.Close();
        }
        return retval;
    }

	//this method adds events to the database
    public static int addEvent(CalendarEvent cevent)
    {
        //add event to the database and return the primary key of the added event row

        //insert
        SqlCommand cmd;
        SqlConnection con = new SqlConnection(connectionString);
        if (cevent.PatientId.ToString() == "55555555-5555-5555-5555-555555555555")
        { 
          //INSERT INTO [dbo].[tbl_Patient] ([PatientId],[FirstName],[LastName],[DateOfBirth],[HomePhone],[Cel,lPhone])
          //VALUES(,,,,,,,)
            Guid NewPatientId = Guid.NewGuid();
            cevent.PatientId = NewPatientId;
            HttpContext.Current.Session["NewPatientId"] = NewPatientId;
            DataTable dt = new DataTable();           
            SqlDataAdapter da = new SqlDataAdapter("select isnull(max(fileno),0)+1 as fileno from tbl_Patient", con);
            da.Fill(dt);
            int Fileno = int.Parse(dt.Rows[0]["fileno"].ToString());

            cmd = new SqlCommand("INSERT INTO [dbo].[tbl_Patient] ([PatientId],[FileNo],[OpeningDate],[FirstName],[LastName],[DateOfBirth],[HomePhone],[CellPhone],[MasterId]) VALUES(@PatientId,@FileNo,@OpeningDate,@FirstName,@LastName,@DateOfBirth,@HomePhone,@CellPhone,@MasterId)", con);
            cmd.Parameters.Add("@PatientId", SqlDbType.UniqueIdentifier).Value = cevent.PatientId;
            cmd.Parameters.Add("@FileNo",SqlDbType.Int).Value=Fileno;
            cmd.Parameters.Add("@OpeningDate", SqlDbType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@FirstName",SqlDbType.NVarChar).Value= cevent.FirstName;
            cmd.Parameters.Add("@LastName",SqlDbType.NVarChar).Value=cevent.LastName;
            cmd.Parameters.Add("@DateOfBirth", SqlDbType.DateTime).Value = DateTime.ParseExact(cevent.DateOfBirth, "dd-MM-yyyy", null);
            //cmd.Parameters.Add("@CityId", SqlDbType.UniqueIdentifier).Value = Guid.Parse("B42EA173-E6D7-46E8-BAE5-013331CA05B9");
            cmd.Parameters.Add("@HomePhone",SqlDbType.NVarChar).Value=cevent.HomePhone;
            cmd.Parameters.Add("@CellPhone", SqlDbType.NVarChar).Value = cevent.CellPhone;
            cmd.Parameters.Add("@MasterId", SqlDbType.UniqueIdentifier).Value = Guid.Parse(MasterId);
          
            
                if (ConnectionState.Closed == con.State)
                {
                    con.Open();
                }
                cmd.ExecuteNonQuery();                    
          
        }
        
        cmd = new SqlCommand("INSERT INTO tbl_Appointment(MasterId,ProfessionalId,PatientId, description, event_start, event_end, all_day, ExamTypeId,Status) VALUES(@MasterId,@ProfessionalId,@PatientId, @description, @event_start, @event_end, @all_day, @ExamTypeId,@Status)", con);
        cmd.Parameters.Add("@MasterId", SqlDbType.NVarChar).Value = MasterId;
        cmd.Parameters.Add("@ProfessionalId", SqlDbType.UniqueIdentifier).Value = cevent.ProfessionalId;
        cmd.Parameters.Add("@PatientId", SqlDbType.UniqueIdentifier).Value = cevent.PatientId;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;
        cmd.Parameters.Add("@ExamTypeId", SqlDbType.Int).Value = cevent.ExamTypeId;
        cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = "Arriving";

        int key = 0;        
        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }
            cmd.ExecuteNonQuery();
            con.Close();

            //get primary key of inserted row
           // cmd = new SqlCommand("SELECT max(event_id) FROM tbl_Appointment", con);
            //cmd.Parameters.Add("@PatientId", SqlDbType.UniqueIdentifier).Value = cevent.PatientId;
            //cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
            //cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
            //cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
            //cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

            key = 1;
        }

        return key;
    }

    //this method prevent the overlap event for same professional

    public static int addexamtype(string examtype)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("INSERT INTO tbl_ExamType(ExamType) VALUES(@ExamType)", con);
        cmd.Parameters.Add("@ExamType", SqlDbType.NVarChar).Value = examtype;
       
        int retval = 0;
        using (con)
        {
            if (ConnectionState.Closed == con.State)
            {
                con.Open();
            }
            cmd.ExecuteNonQuery();
            con.Close();

            //get primary key of inserted row
            // cmd = new SqlCommand("SELECT max(event_id) FROM tbl_Appointment", con);
            //cmd.Parameters.Add("@PatientId", SqlDbType.UniqueIdentifier).Value = cevent.PatientId;
            //cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
            //cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
            //cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
            //cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

            retval = 1;
        }

        return retval;
    }

    // Get Selected Patient Data
    public static DataTable GetPatientData(string PatinetId)
    {
        SqlConnection con;
        con = new SqlConnection(connectionString);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter("select LastName,FirstName,CONVERT(VARCHAR(11),DateOfBirth,105) as DateOfBirth,HomePhone,CellPhone from tbl_Patient where PatientId='" + PatinetId + "'", con);
        da.Fill(dt);
        return dt;
    }
    
    // Get Settings
    public static string GetSettingData()
    {
        //string userId = Membership.GetUser().ProviderUserKey.ToString();    
        SqlConnection con;
        con = new SqlConnection(connectionString);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter("select Comment from aspnet_Membership where UserId='" + MasterId + "'", con);
        da.Fill(dt);
        return dt.Rows[0]["Comment"].ToString();
    }

    // Set Settings
    public static void SetSettingData(string TimeData)
    {        
        //string userId = Membership.GetUser().ProviderUserKey.ToString();     
        SqlConnection con = new SqlConnection(connectionString);
        con = new SqlConnection(connectionString);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter("select Comment from aspnet_Membership where UserId='" + MasterId + "'", con);
        da.Fill(dt);
        string[] TimeArray = (dt.Rows[0]["Comment"].ToString()).Split('^');
        MembershipUser usr = Membership.GetUser(new Guid(MasterId));
        usr.Comment = TimeArray[0] + "^" + TimeArray[1] + "^" + TimeArray[2] + "^" + TimeData;
        Membership.UpdateUser(usr);
    }

   
}
