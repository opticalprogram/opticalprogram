﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Text;
using System.Web.Security;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Text;
using System.Web.UI.WebControls;

public partial class Inventory : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    //string ItemType="Frame";
    protected void Page_Load(object sender, EventArgs e)
    {

        // String to math calculation
        //DataTable dt = new DataTable();
        //string costprice = "3";
        //var v = dt.Compute("" + costprice + "*(2+4)", "");
        //Response.Write(v);
        //return;

        // Set title
        string title = "Inventory";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        objdal = new Dal();
        SetItemNumber();

       

        if (!IsPostBack)
        {
            if (Request.QueryString["t"] != null)
            {
                string linkTitle = "<span class='glyphicon glyphicon-shopping-cart'></span>&nbsp;Add ";
                if (Request.QueryString["t"].ToString() == "Ophtalmic lens")
                {                    
                    DivFrame.Visible = false;
                    DivContact.Visible = false;
                    Divsize.Visible = false;
                    DivModel.Visible = false;
                    DivInvoiceNumber.Visible = false;
                    PageHeading.InnerHtml = linkTitle +"ophtalmic lens";
                }
                else if (Request.QueryString["t"].ToString() == "Contact lens")
                {                   
                    DivFrame.Visible = false;
                    DivOphtalmic.Visible = false;
                    Divsize.Visible = false;
                    DivInvoiceNumber.Visible = false;
                    PageHeading.InnerHtml = linkTitle + "contact lens";
                }
                else
                {
                    DivContact.Visible = false;
                    DivOphtalmic.Visible = false;
                    PageHeading.InnerHtml = linkTitle + "frame";
                    Txt_Quantity.Text = "1";
                }
               
            }
            else
            {
               // Request.QueryString["t"] = "Frame";
                DivContact.Visible = false;
                DivOphtalmic.Visible = false;                
            }

            ViewState["NewImage"] = "";         
            if (Request.QueryString["id"] != null)
            {
                PageHeading.InnerHtml = "<span class='glyphicon glyphicon-shopping-cart'></span>&nbsp;Edit Item";
                getItemData(Request.QueryString["id"].ToString());
                divlabel.Style.Add("display", "block");
                GenerateBarcode(Request.QueryString["id"].ToString());
                Button_Submit.Text = "Update";
            }
        }
    }

    // Max Item Number Set
    protected void SetItemNumber()
    {
        try
        {
            DataTable dt = new DataTable();
            if (Request.QueryString["t"].ToString() == "Ophtalmic lens")
            {
                dt = objdal.GET_DATATABLE("select isnull(max(ItemId),20000)+1 as MaxItemNumber from tbl_Item where ItemType='Ophtalmic lens'");
            }
            else if (Request.QueryString["t"].ToString() == "Contact lens")
            {
                dt = objdal.GET_DATATABLE("select isnull(max(ItemId),30000)+1 as MaxItemNumber from tbl_Item where ItemType='Contact lens'");
            }
            else
            {
                dt = objdal.GET_DATATABLE("select isnull(max(ItemId),10000)+1 as MaxItemNumber from tbl_Item where ItemType='Frame'");
            }           
            Txt_ItemNumber.Text = dt.Rows[0]["MaxItemNumber"].ToString();
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    //Submit Form
    protected void Button_Submit_Click(object sender, EventArgs e)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            

            if (Request.QueryString["id"] != null)
            {              
               
                //UPDATE [dbo].[tbl_Item] SET [ItemName] = '',[ItemEntryDate] = '',[ItemSupplierId] ='',[ItemInvoiceNumber] = '',[ItemBrand] = ''
                //,[ItemModel] = '',[ItemColor] = '',[ItemColorDescription] = '',[ItemQuantity]='',[ItemFrame] = '',[ItemFrameImage]='',[ItemGlass] = ''
                //,[ItemSizeSides] = '',[ItemSizeH] = '',[ItemSizeDBL] = '',[ItemFormulaId] = '',[ItemCostPrice] = '',[ItemSalePrice] = ''
                // ,[ItemSizeA] ='',[ItemSizeB] = '',[OphtalmicType]='',[OphtalmicMaterial]='',[OphtalmicTreatments]='',[OphtalmicWarranty]=''
                // ,[ContactType]='',[ContactCategory]='',[ContactSubCategory]='',[MasterId] = '' WHERE 

                sb.Append("UPDATE [dbo].[tbl_Item] SET [ItemName] ='" + Txt_ItemName.Text + "',[ItemEntryDate] = '" + DateTime.ParseExact(Txt_ItemEntryDate.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",[ItemSupplierId] = '" + DDL_Supplier.SelectedValue + "',[ItemInvoiceNumber] = '" + Txt_InvoiceNumber.Text + "'");
                sb.Append(",[ItemBrand] = '" + DDl_Brand.SelectedValue + "',[ItemModel] = '" + Txt_Model.Text + "'");
                sb.Append(",[ItemColor] = '" + Txt_Color.Text + "',[ItemColorDescription] = '" + Txt_ColorDescription.Text + "'");
                sb.Append(",[ItemQuantity]='" + Txt_Quantity.Text + "',[ItemSizeA] ='" + Txt_SizeA.Text + "',[ItemSizeB] = '" + Txt_SizeB.Text + "'");
                sb.Append(",[ItemSizeSides] = '" + Txt_SizeSides.Text + "',[ItemSizeH] = '" + Txt_SizeH.Text + "',[ItemSizeDBL] = '" + Txt_SizeDBL.Text + "'");
                sb.Append(",[ItemFrame] = '" + DDL_Frame.SelectedValue + "',[ItemFrameImage]='" + ViewState["NewImage"].ToString() + "',[ItemGlass] ='" + DDL_Glass.SelectedValue + "'");
                sb.Append(",[ItemFormulaId] = '" + DDL_Formula.SelectedValue + "',[ItemCostPrice] = '" + Txt_CostPrice.Text + "',[ItemSalePrice] = '" + Txt_SalePrice.Text + "'");
                sb.Append(",[OphtalmicType]='" + ddl_OphtalmicType.SelectedValue + "',[OphtalmicMaterial]='" + ddl_OphtalmicMaterial.SelectedValue + "'");
                sb.Append(",[OphtalmicTreatments]='" + Txt_OphtalmicTreatments.Text + "',[OphtalmicWarranty]='" + Txt_OphtalmicWarranty.Text + "'");
                sb.Append(",[ContactType]='" + ddl_ContactType.SelectedValue + "',[ContactCategory]='" + ddl_ContactCategory.SelectedValue + "'");
                sb.Append(",[ContactSubCategory]='" + ddl_ContactSubCategory.SelectedValue + "',[ItemType]='" + Request.QueryString["t"].ToString() + "',[MasterId] ='" + MasterId + "'");
                sb.Append("  WHERE [ItemId] = " + Request.QueryString["id"].ToString() + "");

                objdal.EXECUTE_DML(sb.ToString());
                Session["lblMessage"] = "Item is updated successfully.";
            }
            else
            {
                //  INSERT INTO [dbo].[tbl_Item]([ItemName],[ItemEntryDate],[ItemSupplierId],[ItemInvoiceNumber],[ItemBrand],[ItemModel],[ItemColor]
                //,[ItemColorDescription],[ItemQuantity],[ItemFrame],[ItemFrameImage],[ItemGlass],[ItemSizeA],[ItemSizeB],[ItemSizeSides],[ItemSizeH],[ItemSizeDBL],[ItemFormulaId]
                //,[ItemCostPrice],[ItemSalePrice],[OphtalmicType],[OphtalmicMaterial],[OphtalmicTreatments],[OphtalmicWarranty],[ContactType]
                //,[ContactCategory],[ContactSubCategory],[ItemType],[MasterId])

                string Inventorytype = "Frame";
                if (Request.QueryString["t"] != null)
                {
                    Inventorytype = Request.QueryString["t"].ToString();
                }

                if (Inventorytype=="Frame")
                {
                    int cnt = int.Parse((objdal.Get_SingleValue("select count(*) from [tbl_Item] where [ItemModel]='" + Txt_Model.Text.Trim() + "'")));
                    if (cnt>0)
                    {
                         lblMessage.Visible = true;
                         lblMessage.Text = "Error: Model number is already exist. Please enter unique number.";
                         return;
                    }
                }
               

                sb.Append("INSERT INTO [dbo].[tbl_Item]([ItemId],[ItemName],[ItemEntryDate],[ItemSupplierId],[ItemInvoiceNumber],[ItemBrand],[ItemModel]");
                sb.Append(",[ItemColor],[ItemColorDescription],[ItemQuantity],[ItemFrame],[ItemFrameImage],[ItemGlass],[ItemSizeA],[ItemSizeB],[ItemSizeSides]");
                sb.Append(",[ItemSizeH],[ItemSizeDBL],[ItemFormulaId],[ItemCostPrice],[ItemSalePrice],[OphtalmicType],[OphtalmicMaterial]");
                sb.Append(",[OphtalmicTreatments],[OphtalmicWarranty],[ContactType],[ContactCategory],[ContactSubCategory],[ItemType],[MasterId])");
                sb.Append("VALUES('" + Txt_ItemNumber.Text + "','" + Txt_ItemName.Text + "','" + DateTime.ParseExact(Txt_ItemEntryDate.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",'" + DDL_Supplier.SelectedValue + "','" + Txt_InvoiceNumber.Text + "','" + DDl_Brand.SelectedValue + "','" + Txt_Model.Text + "'");
                sb.Append(",'" + Txt_Color.Text + "','" + Txt_ColorDescription.Text + "','" + Txt_Quantity.Text + "','" + DDL_Frame.SelectedValue + "'");
                sb.Append(",'" + ViewState["NewImage"].ToString() + "','" + DDL_Glass.SelectedValue + "','" + Txt_SizeA.Text + "','" + Txt_SizeB.Text + "','" + Txt_SizeSides.Text + "'");
                sb.Append(",'" + Txt_SizeH.Text + "','" + Txt_SizeDBL.Text + "','" + DDL_Formula.SelectedValue + "','" + Txt_CostPrice.Text + "'");
                sb.Append(",'" + Txt_SalePrice.Text + "','" + ddl_OphtalmicType.SelectedValue + "','" + ddl_OphtalmicMaterial.SelectedValue + "'");
                sb.Append(",'" + Txt_OphtalmicTreatments.Text + "','" + Txt_OphtalmicWarranty.Text + "','" + ddl_ContactType.SelectedValue + "'");
                sb.Append(",'" + ddl_ContactCategory.SelectedValue + "','" + ddl_ContactSubCategory.SelectedValue + "','" + Inventorytype + "','" + MasterId + "')");



                objdal.EXECUTE_DML(sb.ToString());

                Session["lblMessage"] = "Item is added successfully.";
            }

            Response.Redirect("manageinventory.aspx", true);
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }



    //get selected item data
    protected void getItemData(string id)
    {
        //  INSERT INTO [dbo].[tbl_Item]([ItemName],[ItemEntryDate],[ItemSupplierId],[ItemInvoiceNumber],[ItemBrand],[ItemModel],[ItemColor]
        //,[ItemColorDescription],[ItemQuantity],[ItemFrame],[ItemGlass],[ItemSizeA],[ItemSizeB],[ItemSizeSides],[ItemSizeH],[ItemSizeDBL],[ItemFormulaId]
        //,[ItemCostPrice],[ItemSalePrice],[OphtalmicType],[OphtalmicMaterial],[OphtalmicTreatments],[OphtalmicWarranty],[ContactType]
        //,[ContactCategory],[ContactSubCategory],[MasterId])

        DataTable dtitem = new DataTable();
        dtitem = objdal.GET_DATATABLE("select * from tbl_Item where ItemId=" + id + "");
        Txt_ItemNumber.Text = dtitem.Rows[0]["ItemId"].ToString();
        Txt_ItemName.Text = dtitem.Rows[0]["ItemName"].ToString();
        Txt_ItemEntryDate.Text = Convert.ToDateTime(dtitem.Rows[0]["ItemEntryDate"]).ToString("dd-MM-yyyy");
        Txt_InvoiceNumber.Text = dtitem.Rows[0]["ItemInvoiceNumber"].ToString();
        Txt_Color.Text = dtitem.Rows[0]["ItemColor"].ToString();
        Txt_ColorDescription.Text = dtitem.Rows[0]["ItemColorDescription"].ToString();
        Txt_Quantity.Text = dtitem.Rows[0]["ItemQuantity"].ToString();
        Lbl_Color.Text = dtitem.Rows[0]["ItemColor"].ToString();
        Txt_Model.Text = dtitem.Rows[0]["ItemModel"].ToString();
        Lbl_Model.Text = dtitem.Rows[0]["ItemModel"].ToString();
        //Lbl_Size.Text = dtitem.Rows[0]["ItemSize"].ToString();
        DDL_Frame.SelectedValue = dtitem.Rows[0]["ItemFrame"].ToString();
        if (dtitem.Rows[0]["ItemFrameImage"].ToString() != null && dtitem.Rows[0]["ItemFrameImage"].ToString() != "")
        {
            Image_Frame.ImageUrl = "~/images/" + dtitem.Rows[0]["ItemFrameImage"].ToString();
        }       
        DDL_Glass.SelectedValue = dtitem.Rows[0]["ItemGlass"].ToString();
        Txt_SizeA.Text = dtitem.Rows[0]["ItemSizeA"].ToString();
        Txt_SizeB.Text = dtitem.Rows[0]["ItemSizeB"].ToString();
        Txt_SizeSides.Text = dtitem.Rows[0]["ItemSizeSides"].ToString();
        Txt_SizeH.Text = dtitem.Rows[0]["ItemSizeH"].ToString();
        Txt_SizeDBL.Text = dtitem.Rows[0]["ItemSizeDBL"].ToString();
        DDL_Formula.SelectedValue = dtitem.Rows[0]["ItemFormulaId"].ToString();
        Txt_CostPrice.Text = Convert.ToDouble(dtitem.Rows[0]["ItemCostPrice"]).ToString("n");
        Txt_SalePrice.Text = Convert.ToDouble(dtitem.Rows[0]["ItemSalePrice"]).ToString("n");
       // HF_SalePrice.Value = Txt_SalePrice.Text;
        DDL_Supplier.SelectedValue = dtitem.Rows[0]["ItemSupplierId"].ToString();
        DDl_Brand.Items.Add(dtitem.Rows[0]["ItemBrand"].ToString());
        Lbl_Brand.Text = dtitem.Rows[0]["ItemBrand"].ToString();
        ddl_OphtalmicType.SelectedValue = dtitem.Rows[0]["OphtalmicType"].ToString();
        ddl_OphtalmicMaterial.SelectedValue = dtitem.Rows[0]["OphtalmicMaterial"].ToString();
        Txt_OphtalmicTreatments.Text = dtitem.Rows[0]["OphtalmicTreatments"].ToString();
        Txt_OphtalmicWarranty.Text = dtitem.Rows[0]["OphtalmicWarranty"].ToString();
        ddl_ContactType.SelectedValue = dtitem.Rows[0]["ContactType"].ToString();
        ddl_ContactCategory.SelectedValue = dtitem.Rows[0]["ContactCategory"].ToString();
        ddl_ContactSubCategory.SelectedValue = dtitem.Rows[0]["ContactSubCategory"].ToString();
        
    }

    //generated barcode
    protected void GenerateBarcode(string barCode)
    {
        System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
        using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
        {
            using (Graphics graphics = Graphics.FromImage(bitMap))
            {
                PrivateFontCollection pfc = new PrivateFontCollection();
                pfc.AddFontFile(Server.MapPath("~/fonts/IDAutomationHC39M_FREE.otf"));
                Font oFont = new Font(pfc.Families[0], 16, FontStyle.Regular);
                PointF point = new PointF(2f, 2f);
                SolidBrush blackBrush = new SolidBrush(Color.Black);
                SolidBrush whiteBrush = new SolidBrush(Color.White);
                graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] byteImage = ms.ToArray();

                Convert.ToBase64String(byteImage);
                imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
            }
            plBarCode.Controls.Add(imgBarCode);
        }
    }
    protected void SDSFormula_Selecting(object sender, System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void SDSsupplier_Selecting(object sender, System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }
    protected void DDL_Supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillBrand();
    }

    protected void fillBrand()
    {
        DDl_Brand.Items.Clear();
        if (DDL_Supplier.SelectedIndex != 0)
        {
            string brands = objdal.Get_SingleValue("SELECT SupplierBrand FROM tbl_Supplier where SupplierId='" + DDL_Supplier.SelectedValue + "'");
            string[] brandArray = brands.Split(',');
            foreach (string brand in brandArray)
            {
                DDl_Brand.Items.Add(brand);
            }
        }

    }
    protected void DDL_Supplier_DataBound(object sender, EventArgs e)
    {
        DDL_Supplier.Items.Insert(0, "Select");
    }
    protected void Button_Upload_Click(object sender, EventArgs e)
    {
        if (FileUpload_Frame.HasFile)
        {          
            string imageExt = FileUpload_Frame.FileName.Split('.')[1];
            ViewState["NewImage"] = "frame" + Txt_ItemNumber.Text + "." + imageExt;        
            //if (OldImage!=null)
            //{
            //    File.Delete(Server.MapPath("~/images/" + OldImage));
            //}
            FileUpload_Frame.SaveAs(Server.MapPath("~/images/" + ViewState["NewImage"].ToString()));
            Image_Frame.ImageUrl = "~/images/" + ViewState["NewImage"].ToString();
        }
    }
}