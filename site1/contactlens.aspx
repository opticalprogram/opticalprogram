﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="contactlens.aspx.cs" Inherits="contactlenses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/ProjectHelper.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script>
        $(document).ready(function () {

            var id = getUrlVars()["id"];
            if (id == null) {
                $("#Txt_Date").val(currentdate());
                $("#txt_ReplacementDate_OD").val(currentdate());
                $("#txt_ReplacementDate_OS").val(currentdate());
                $("#Txt_Datecalled").val(currentdate());
                $("#Txt_Delivery").val(currentdate());
                $("#txt_Printedon_OD").val(currentdate());
                $("#txt_Printedon_OS").val(currentdate());
            }

        });
        //Masking       
        jQuery(function ($) {
            $("#Txt_Date").mask("99-99-9999");
            $("#txt_ReplacementDate_OD").mask("99-99-9999");
            $("#txt_ReplacementDate_OS").mask("99-99-9999");
            $("#Txt_Datecalled").mask("99-99-9999");
            $("#Txt_Delivery").mask("99-99-9999");
            $("#txt_Printedon_OD").mask("99-99-9999");
            $("#txt_Printedon_OS").mask("99-99-9999");
            $("#Txt_Axis_OD").mask("999");
            $("#Txt_Axis_OS").mask("999");
        });

        function CalTotal() {
            if ($("#txt_Price_OD").val() != "" && $("#txt_Price_OS").val() != "" && $("#Txt_Professionalfees").val() != "") {
                var Price_OD = parseFloat($("#txt_Price_OD").val());
                var Price_OS = parseFloat($("#txt_Price_OS").val());
                var Professionalfees = parseFloat($("#Txt_Professionalfees").val())
                $("#Txt_Total").val(Price_OD + Price_OS + Professionalfees);
            }
        }

        function AxisRange() {
            if ($("#Txt_Axis_OD").val() > 180) {
                $("#Txt_Axis_OD").val('180');
            }
            if ($("#Txt_Axis_OS").val() > 180) {
                $("#Txt_Axis_OS").val('180');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_Prescription" NavigateUrl="~/prescription.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Prescription</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_manageinventory" NavigateUrl="~/manageinventory.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Manage Inventory</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_Supplier" NavigateUrl="~/managesupplier.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Supplier</asp:HyperLink></li>
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Label_Company">Company:</label>
            <asp:Label ID="Label_Company" runat="server" CssClass="form-control" disabled="disabled"></asp:Label>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_PrescriptionNo">Prescription no:</label>
            <asp:TextBox ID="Txt_PrescriptionNo" ClientIDMode="Static" disabled="disabled" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Date">Date:</label>
            <asp:TextBox ID="Txt_Date" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="txt_Professional">Professional:</label>
            <asp:TextBox ID="txt_Professional" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <asp:UpdatePanel ID="UpdatePanelPatient" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                    <label for="DDL_Patient">Patient:</label>&nbsp; 
                  <asp:DropDownList ID="DDL_Patient" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDL_Patient_SelectedIndexChanged" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                    <label for="ddl_Exam">Exam:</label>
                    <asp:DropDownList ID="ddl_Exam" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddl_Exam_SelectedIndexChanged" runat="server">
                    </asp:DropDownList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="txt_Orderno_OD">Lab invoice no.:</label>
            <asp:TextBox ID="txt_Orderno_OD" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="txt_TrayNo_OD">Tray Number:</label>
            <asp:TextBox ID="txt_TrayNo_OD" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="table-responsive form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Rx</h4>
        <asp:UpdatePanel ID="UpdatePanel_RxOd" runat="server">
            <ContentTemplate>
                <table style="min-width: 400px;">
                    <tr>
                        <td colspan="11">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="center">Sphere</td>
                        <td align="center">Cylinder</td>
                        <td align="center">Axis</td>
                        <td>&nbsp;</td>
                        <td align="center">Addition</td>
                        <td align="center">Segh</td>
                        <td>&nbsp;</td>
                        <td align="center">O.Z.</td>
                        <td>&nbsp;</td>
                        <td align="center">Trial</td>
                    </tr>
                    <tr>
                        <td>
                            <label>&nbsp;OD:</label></td>
                        <td>
                            <asp:TextBox ID="Txt_Sphere_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Cylinder_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Axis_OD" CssClass="form-control" onkeyup="AxisRange();" oninput="AxisRange();" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_Addition_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>

                            <asp:TextBox ID="Txt_Segh_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_OZ_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:DropDownList ID="ddl_Trial_OD" runat="server">
                                <asp:ListItem>No</asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            <label>&nbsp;OS:</label></td>
                        <td>
                            <asp:TextBox ID="Txt_Sphere_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Cylinder_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Axis_OS" CssClass="form-control" onkeyup="AxisRange();" oninput="AxisRange();" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_Addition_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Segh_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_OZ_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:DropDownList ID="ddl_Trial_OS" runat="server">
                                <asp:ListItem>No</asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                            </asp:DropDownList></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Rx</h4>
        <asp:UpdatePanel ID="UpdatePanelRxFinal" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td colspan="2" align="center">
                            <label>Base curve</label></td>
                        <td colspan="2">&nbsp;
                        </td>
                        <td colspan="2" align="center">
                            <label>Diameter</label></td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="center">Base1</td>
                        <td align="center">Base2</td>
                        <td>&nbsp;</td>
                        <td align="center">Horiz.</td>
                        <td align="center">Vert.</td>
                    </tr>
                    <tr>
                        <td>
                            <label>&nbsp;OD:</label></td>
                        <td>
                            <asp:TextBox ID="txt_Base1_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txt_Base2_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_Horiz_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txt_Vert_OD" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>&nbsp;OS:</label></td>
                        <td>
                            <asp:TextBox ID="txt_Base1_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txt_Base2_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_Horiz_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txt_Vert_OS" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Keratometry</h4>
        <asp:UpdatePanel ID="UpdatePanelKeratometryOD" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <label>&nbsp;OD:</label></td>
                        <td>
                            <asp:TextBox ID="txt_Keratometry_OD1" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;/&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_Keratometry_OD2" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;X&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_Keratometry_OD3" CssClass="form-control" runat="server"></asp:TextBox></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Keratometry</h4>
        <asp:UpdatePanel ID="UpdatePanelKeratometryOS" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <label>&nbsp;OS:</label></td>
                        <td>
                            <asp:TextBox ID="txt_Keratometry_OS1" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;/&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_Keratometry_OS2" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;X&nbsp;</td>
                        <td>
                            <asp:TextBox ID="txt_Keratometry_OS3" CssClass="form-control" runat="server"></asp:TextBox></td>

                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 category">
        <asp:UpdatePanel ID="UpdatePanel_Product_OD" runat="server">
            <ContentTemplate>
                  <div class="form-group col-lg-2 col-md-4 col-sm-4">
                  <label for="txt_Keyword_OD">Keyword:</label>&nbsp;<asp:LinkButton ID="LinkButton_Keyword_OD" runat="server" OnClick="LinkButton_Keyword_OD_Click"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                  <asp:TextBox ID="txt_Keyword_OD" MaxLength="10" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="ddl_Product_OD">Product:</label>
                    <asp:DropDownList ID="ddl_Product_OD" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddl_Product_OD_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_Type">Type:</label>
                    <asp:TextBox ID="txt_Type" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_Category">Category:</label>
                    <asp:TextBox ID="txt_Category" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_Subcategory">Sub category:</label>
                    <asp:TextBox ID="txt_Subcategory" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_Supplier">Supplier:</label>
                    <asp:TextBox ID="txt_Supplier" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <%--   <div class="form-group col-lg-4 col-md-4 col-sm-6">
                    <label for="txt_Detail_OD">Detail:</label>
                    <asp:TextBox ID="txt_Detail_OD" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;OD</h4>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_Pupilsize_OD">Pupil size:</label>
            <asp:TextBox ID="txt_Pupilsize_OD" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_Cornealsize_OD">Corneal size:</label>
            <asp:TextBox ID="txt_Cornealsize_OD" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_Typeoftears_OD">Type of tears:</label>
            <asp:TextBox ID="txt_Typeoftears_OD" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_But_OD">B.U.T.:</label>
            <asp:TextBox ID="txt_But_OD" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_Centrationright">Centration:</label>
            <asp:TextBox ID="Txt_Centrationright" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_Mouvementlens_OD">Mouvement of the lens:</label>
            <asp:TextBox ID="Txt_Mouvementlens_OD" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_VisualacuityF_OD">Visual acuity far:</label>
            <asp:TextBox ID="Txt_VisualacuityF_OD" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_VisualacuityC_OD">Visual acuity close:</label>
            <asp:TextBox ID="Txt_VisualacuityC_OD" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <label for="txt_Printedon_OD">Printed on:</label>
            <asp:TextBox ID="txt_Printedon_OD" CssClass="form-control" ClientIDMode="Static" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>

    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;OS</h4>
        <asp:UpdatePanel ID="UpdatePanel_Product_OS" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
                    <label for="ddl_Product_OS">Product:</label>
                    <asp:DropDownList ID="ddl_Product_OS" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddl_Product_OS_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-6" style="display: none;">
                    <label for="txt_Detail_OS">Detail:</label>
                    <asp:TextBox ID="txt_Detail_OS" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_Pupilsize_OS">Pupil size:</label>
            <asp:TextBox ID="txt_Pupilsize_OS" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_Cornealsize_OS">Corneal size:</label>
            <asp:TextBox ID="txt_Cornealsize_OS" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_Typeoftears_OS">Type of tears:</label>
            <asp:TextBox ID="txt_Typeoftears_OS" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="txt_But_OS">B.U.T.:</label>
            <asp:TextBox ID="txt_But_OS" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_Centrationleft">Centration:</label>
            <asp:TextBox ID="Txt_Centrationleft" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_Mouvementlens_OS">Mouvement of the lens:</label>
            <asp:TextBox ID="Txt_Mouvementlens_OS" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_VisualacuityF_OS">Visual acuity far:</label>
            <asp:TextBox ID="Txt_VisualacuityF_OS" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4">
            <label for="Txt_VisualacuityC_OS">Visual acuity close:</label>
            <asp:TextBox ID="Txt_VisualacuityC_OS" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <label for="txt_Printedon_OS">Printed on:</label>
            <asp:TextBox ID="txt_Printedon_OS" CssClass="form-control" ClientIDMode="Static" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <label for="txt_Orderno_OS">Order no.:</label>
            <asp:TextBox ID="txt_Orderno_OS" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <label for="txt_TrayNo_OS">Tray Number:</label>
            <asp:TextBox ID="txt_TrayNo_OS" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div style="clear: both;"></div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td colspan="3">
                    <h4>&nbsp;&nbsp;Contact lenses</h4>
                </td>
                <td align="center">
                    <label>Price</label></td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Contactlenses_OD" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Price_OD" ClientIDMode="Static" CssClass="form-control" onkeyup="CalTotal();" oninput="CalTotal();" Text="0.00" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Contactlenses_OS" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Price_OS" ClientIDMode="Static" CssClass="form-control" onkeyup="CalTotal();" oninput="CalTotal();" Text="0.00" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">Tint</td>
                <td>&nbsp;</td>
                <td colspan="2">Replacement Date</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Tint_OD" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_ReplacementDate_OD" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;Os:</label></td>
                <td>
                    <asp:TextBox ID="txt_Tint_OS" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_ReplacementDate_OS" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;Solution:</label></td>
                <td colspan="3">
                    <asp:TextBox ID="txt_Solution" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div style="clear: both;"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Ready">Ready:</label>
            <asp:TextBox ID="Txt_Ready" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Adviceclient">Advice client:</label>
            <asp:TextBox ID="Txt_Adviceclient" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Followup">Follow-up:</label>
            <asp:TextBox ID="Txt_Followup" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Datecalled">Date called:</label>
            <asp:TextBox ID="Txt_Datecalled" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Delivery">Delivery date:</label>
            <asp:TextBox ID="Txt_Delivery" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Notes">Notes:</label>
            <asp:TextBox ID="Txt_Notes" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Professionalfees">Professional fees:</label>
            <asp:TextBox ID="Txt_Professionalfees" ClientIDMode="Static" Text="0.00" onkeyup="CalTotal();" oninput="CalTotal();" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Total">Total:</label>
            <asp:TextBox ID="Txt_Total" ClientIDMode="Static" Text="0.00" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <br />
            <asp:Button ID="Button_SAVE" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="Save" OnClick="Button_SAVE_Click" />
            <asp:Button ID="Button_Invoice" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="Invoice" OnClick="Button_Invoice_Click" />
        </div>
    </div>
</asp:Content>

