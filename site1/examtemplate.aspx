﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="examtemplate.aspx.cs" Inherits="examtemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        var maskDate = "99-99-9999";

        function masking() {
            $("#TextBox_Keyword").val("");

            if ($('#DDL_SerchBy').val() == "ExamDate") {
                $("#TextBox_Keyword").attr("placeholder", "DD-MM-YYYY");
                $("#TextBox_Keyword").mask(maskDate);
            }
            else if ($('#DDL_SerchBy').val() != "ExamDate") {
                $("#TextBox_Keyword").attr("placeholder", "");
                $("#TextBox_Keyword").unmask(maskDate);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_General" NavigateUrl="~/general.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;General</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_Emergency" NavigateUrl="~/emergency.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Emergency</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_Dilation" NavigateUrl="~/dilation.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Dilation</asp:HyperLink></li>
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-success message"></asp:Label>
    </div>
    <div class="form-inline col-lg-12" style="margin-bottom: 10px; padding-left: 0px !important;">
        <div class="form-group">
            <label for="DDL_SerchBy">Search By:</label>
            <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server" onchange="masking();">
                <asp:ListItem Value="TemplateType">Exam</asp:ListItem>
                <asp:ListItem Value="patientname">Patient</asp:ListItem>
                <asp:ListItem Value="ExamDate">Date</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label for="TextBox_Keyword">Keyword:</label>
            <asp:TextBox ID="TextBox_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="Button_Search" ClientIDMode="Static" CssClass="btn btn-default" runat="server" Text="Search" OnClick="Button_Search_Click" />
            <asp:HyperLink ID="HlListAll" runat="server" CssClass="btn btn-group-xs btn-default" NavigateUrl="~/examtemplate.aspx">List all</asp:HyperLink>

        </div>

    </div>
    <div class="table-responsive col-lg-6" style="padding-left: 0px !important;">
        <asp:GridView ID="GVExamtemplate" CssClass="table table-hover table-striped" runat="server" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataKeyNames="ExamNo" DataSourceID="SDSExamTemplate" OnSelectedIndexChanging="GVExamtemplate_SelectedIndexChanging" OnRowDeleting="GVExamtemplate_RowDeleting">
            <Columns>

                <asp:TemplateField HeaderText="ExamNo" SortExpression="ExamNo" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lbl_ExamNo" runat="server" Text='<%# Bind("ExamNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ExamDate" HeaderText="Date" SortExpression="ExamDate" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                <asp:HyperLinkField DataTextField="TemplateType" HeaderText="Exam" SortExpression="TemplateType" DataNavigateUrlFields="TemplateType,ExamNo" DataNavigateUrlFormatString="{0}.aspx?id={1}" />
                <asp:HyperLinkField DataTextField="patientname" HeaderText="Patient" SortExpression="patientname" DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="patient.aspx?id={0}" />
                <asp:CommandField ShowSelectButton="true" SelectText="Edit" ControlStyle-CssClass="btn-success btn btn-xs">
                    <ControlStyle CssClass="btn-success btn btn-xs"></ControlStyle>
                </asp:CommandField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs">
                    <ControlStyle CssClass="btn-danger btn btn-xs"></ControlStyle>
                </asp:CommandField>
                <asp:TemplateField SortExpression="TemplateType">
                    <ItemTemplate>
                        <asp:Label ID="lbl_TemplateType" Visible="false" runat="server" Text='<%# Bind("TemplateType") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>
        <asp:SqlDataSource runat="server" ID="SDSExamTemplate" OnSelecting="SDSExamTemplate_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            SelectCommand="SELECT tbl_EmergencyExamtemplates.EmergencyExamtemplatesId as ExamNo, tbl_EmergencyExamtemplates.ExamDate
                            ,tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, 
                            tbl_EmergencyExamtemplates.TemplateType FROM  tbl_EmergencyExamtemplates INNER JOIN
                            tbl_Patient ON tbl_EmergencyExamtemplates.PatientId = tbl_Patient.PatientId where tbl_EmergencyExamtemplates.MasterId=@MasterId
                            
                            union all
                            
                            SELECT tbl_EyeDilationExamtemplates.EyeDilationExamtemplatesId as ExamNo, tbl_EyeDilationExamtemplates.ExamDate, 
                            tbl_Patient.PatientId,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, 
                            tbl_EyeDilationExamtemplates.TemplateType FROM  tbl_EyeDilationExamtemplates INNER JOIN
                            tbl_Patient ON tbl_EyeDilationExamtemplates.PatientId = tbl_Patient.PatientId where tbl_EyeDilationExamtemplates.MasterId=@MasterId
            
                             union all
                            
                            SELECT tbl_GeneralEyeExamETemplates.GeneralEyeExamETemplatesId as ExamNo, tbl_GeneralEyeExamETemplates.ExamDate, 
							tbl_Patient.PatientId, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname,
							tbl_GeneralEyeExamETemplates.TemplateType FROM tbl_GeneralEyeExamETemplates INNER JOIN 
							tbl_Patient ON tbl_GeneralEyeExamETemplates.PatientId = tbl_Patient.PatientId where tbl_GeneralEyeExamETemplates.MasterId=@MasterId"
            DeleteCommand="DELETE FROM [dbo].[tbl_EmergencyExamtemplates] WHERE 1=2">
            <SelectParameters>
                <asp:Parameter Name="MasterId" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="ExamNo" />
            </DeleteParameters>
        </asp:SqlDataSource>
        <%--DeleteCommand="DELETE FROM [dbo].[tbl_EmergencyExamtemplates] WHERE EmergencyExamtemplatesId=@ExamNo"--%>
    </div>
</asp:Content>

