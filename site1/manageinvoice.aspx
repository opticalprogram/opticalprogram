﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="manageinvoice.aspx.cs" Inherits="manageinvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_invoice" NavigateUrl="~/invoice.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add Invoice</asp:HyperLink></li>
    </ul>
    <div class="form-group col-lg-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-success message"></asp:Label>
    </div>
    <div class="form-inline col-lg-12" style="margin-bottom: 10px; padding-left: 0px !important;">
        <div class="form-group">
            <label for="DDL_SerchBy">Search By:</label>
            <asp:DropDownList ID="DDL_SerchBy" CssClass="form-control" ClientIDMode="Static" runat="server">
                <asp:ListItem Value="InvoiceNo">Invoice No</asp:ListItem>               
                <asp:ListItem Value="patientname">Patient</asp:ListItem>
                <asp:ListItem Value="Optometrist">Optometrist</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label for="TextBox_Keyword">Keyword:</label>
            <asp:TextBox ID="TextBox_Keyword" MaxLength="20" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="Button_Search" ClientIDMode="Static" CssClass="btn btn-default" runat="server" OnClick="Button_Search_Click" Text="Search" />
<asp:HyperLink ID="HlListAll" runat="server" CssClass="btn btn-group-xs btn-default" NavigateUrl="~/manageinvoice.aspx">List all</asp:HyperLink>

        </div>
    </div>

    <div class="table-responsive col-lg-12" style="padding-left: 0px !important;">
        <asp:GridView ID="GVManageInvoice" CssClass="table table-hover table-striped" Width="800" runat="server" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" GridLines="None" DataKeyNames="InvoiceMasterId" OnRowDeleted="GVManageInvoice_RowDeleted" DataSourceID="SDSManageInvoice" OnSelectedIndexChanging="GVManageInvoice_SelectedIndexChanging">
            <Columns>
                <asp:BoundField DataField="InvoiceMasterId" HeaderText="InvoiceMasterId" Visible="false" ReadOnly="True" SortExpression="InvoiceMasterId"></asp:BoundField>
                <asp:HyperLinkField DataTextField="InvoiceNo" HeaderText="Invoice No" SortExpression="InvoiceNo" DataNavigateUrlFields="InvoiceMasterId" DataNavigateUrlFormatString="invoice.aspx?id={0}" />
                <asp:BoundField DataField="InvoiceMasterDate" HeaderText="Date" SortExpression="InvoiceMasterDate" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="InvoiceMasterGrandTotal" HeaderText="Grand Total" DataFormatString="{0:0.00}" SortExpression="InvoiceMasterGrandTotal"></asp:BoundField>
                <asp:BoundField DataField="patientname" HeaderText="Patient" ReadOnly="True" SortExpression="patientname"></asp:BoundField>
                <asp:BoundField DataField="Optometrist" HeaderText="Optometrist" SortExpression="Optometrist"></asp:BoundField>
                <asp:BoundField DataField="UserName" HeaderText="Invoiced by" SortExpression="UserName"></asp:BoundField>
                <asp:CommandField ShowSelectButton="true" SelectText="Edit" ControlStyle-CssClass="btn btn-success btn-xs"></asp:CommandField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn btn-danger btn-xs"></asp:CommandField>
            </Columns>
            <PagerStyle CssClass="GridPager" />
            <SortedAscendingHeaderStyle CssClass="asc" />
            <SortedDescendingHeaderStyle CssClass="desc" />
        </asp:GridView>

        <asp:SqlDataSource runat="server" ID="SDSManageInvoice" OnSelecting="SDSManageInvoice_Selecting" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            DeleteCommand=" delete FROM tbl_InvoiceMaster where InvoiceMasterId=@InvoiceMasterId"
            SelectCommand=" SELECT tbl_InvoiceMaster.InvoiceMasterId, tbl_InvoiceMaster.InvoiceNo, tbl_InvoiceMaster.InvoiceMasterDate,
                            tbl_InvoiceMaster.InvoiceMasterGrandTotal, tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, 
                            tbl_Patient.Optometrist,aspnet_Users.UserName FROM tbl_InvoiceMaster INNER JOIN
                            tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId  
                            INNER join aspnet_Users ON tbl_InvoiceMaster.InvoiceMasterEnterydById = aspnet_Users.UserId 
                            where InvoiceMasterIsTemporary='0' and tbl_InvoiceMaster.MasterId=@MasterId order by tbl_InvoiceMaster.InvoiceNo">
            <SelectParameters>
                <asp:Parameter Name="MasterId" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="InvoiceMasterId"></asp:Parameter>
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>

</asp:Content>

