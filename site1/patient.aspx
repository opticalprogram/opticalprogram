﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="patient.aspx.cs" Inherits="patient" %>

<%--<%@ Register Src="~/UserControl/MasterImage.ascx" TagPrefix="uc1" TagName="MasterImage" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/ProjectHelper.js"></script>
    <script type="text/javascript">

        // Calculate Age

        function calculateAge() {
            var datetext = document.getElementById('<%=TextBox_DateOfBirth.ClientID %>').value;

            if (datetext.length == 10) {
                var splitarr = datetext.split("-");
                if (splitarr[0] != null && splitarr[0] != "") {
                    var dob = new Date(splitarr[2], splitarr[1] - 1, splitarr[0], 00, 00, 00, 00);

                    var now = new Date();


                    function isLeap(year) {
                        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
                    }

                    var days = Math.floor((now.getTime() - dob.getTime()) / 1000 / 60 / 60 / 24);
                    var age = 0;

                    for (var y = dob.getFullYear() ; y <= now.getFullYear() ; y++) {
                        var daysInYear = isLeap(y) ? 366 : 365;

                        if (days >= daysInYear) {
                            days -= daysInYear;
                            age++;

                        }
                    }
                    document.getElementById('<%=TextBox_Age.ClientID %>').value = age;
                }
            }
        }

        function calculateDOBGender() {

            var Medicare = document.getElementById('<%=TextBox_Medicare.ClientID %>').value
            if (Medicare != null && Medicare != "") {
                var day = Medicare.substring(10, 12);
                var month = Medicare.substring(7, 9);
                var year = Medicare.substring(5, 7);
                if (month >= 50) {
                    $("#<%=DropDownList_Gender.ClientID%>").val('Female');
                    month = month - 50;
                }
                else {
                    $("#<%=DropDownList_Gender.ClientID%>").val('Male');
                }
                if (year >= 25) {
                    year = "19" + year;
                }
                else {
                    year = "20" + year;
                }
                $("#<%=TextBox_DateOfBirth.ClientID%>").val(day + '-' + month + '-' + year);
                calculateAge();
            }
        }



        function CheckPreference() {
            var SelectedValue = document.getElementById('<%=DropDownList_Preference.ClientID%>').value;
            if (SelectedValue == "CellPhone") {
                if ((document.getElementById('<%=TextBox_CellPhone.ClientID%>').value).length == 0) {
                    alert('Cell Phone is required!');
                }
            }
            else if (SelectedValue == "Email") {
                if ((document.getElementById('<%=TextBox_Email.ClientID%>').value).length == 0) {
                    alert('Email is required!');
                }
            }
            else if (SelectedValue == "HomePhone") {
                if ((document.getElementById('<%=TextBox_HomePhone.ClientID%>').value).length == 0) {
                    alert('Home Phone is required!');
                }
            }
}

function distroymodal() {
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}
    </script>
    <script>
        $(document).ready(function () {
            // $('[data-toggle="tooltip"]').tooltip();

            var id = getUrlVars()["id"];
            if (id == null) {
                $("#TextBox_OpeningDate").val(currentdate());
            }
        });

        //Masking
        jQuery(function ($) {
            //$("#TextBox_OpeningDate").mask("99-99-9999", { placeholder: "DD-MM-YYYY" });
            //$("#TextBox_DateOfBirth").mask("99-99-9999", { placeholder: "DD-MM-YYYY" });
            $("#TextBox_HomePhone").mask("(999) 999-9999");
            $("#TextBox_CellPhone").mask("(999) 999-9999");
            $("#TextBox_WorkPhone").mask("(999) 999-9999");
            $("#TextBox_PostalCode").mask("a9a 9a9");
            $("#TextBox_DateOfBirth").mask("99-99-9999");
            $("#TextBox_OpeningDate").mask("99-99-9999");

            //$("#tin").mask("99-9999999");
            //$("#ssn").mask("999-99-9999");
        });
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li class="active"><a href="#" id="PageHeading" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Add Patient</a></li>
        <li>
            <asp:HyperLink ID="HL_managepatients" NavigateUrl="~/managepatient.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Patients</asp:HyperLink></li>
        <%--<li>
            <asp:HyperLink ID="Hl_invoice" Visible="false" NavigateUrl="#" CssClass="btn btn-sm btn-success historyLink" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Invoices</asp:HyperLink></li>--%>
      <%--  <li>
            <asp:HyperLink ID="Hl_examtemplate" Visible="false" NavigateUrl="#" CssClass="btn btn-sm btn-success historyLink" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Exams history</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="Hl_Prescription" Visible="false" NavigateUrl="#" CssClass="btn btn-sm btn-success historyLink" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Prescription</asp:HyperLink></li>--%>
        <li>
            <div id="DivInvoice" runat="server" class="dropdown" style="display:none;">
                <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;
                    Invoices
                 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a runat="server"  id="Hl_Manageinvoice" href="#">History</a></li>
                    <li><a id="Hl_Invoice" runat="server" href="#">Add new</a></li>
                </ul>
            </div>
        </li>
        <li>
            <div id="DivExams" runat="server" class="dropdown" style="display:none;">
                <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-file"></span>&nbsp;
                    Exams
                 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a runat="server"  id="Hl_ManageExamtemplate" href="#">History</a></li>
                    <li><a runat="server"  id="Hl_AddGeneral" href="#">Add general</a></li>
                     <li><a runat="server"  id="Hl_AddEmergency" href="#">Add emergency</a></li>
                     <li><a runat="server"  id="Hl_AddDilation" href="#">Add dilation</a></li>
                </ul>
            </div>
        </li>
        <li>
             <div id="DivPrescription" runat="server" class="dropdown" style="display:none;">
                <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-file"></span>&nbsp;
                    Prescription
                 <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a runat="server"  id="Hl_ManagePrescription" href="#">History</a></li>
                    <li><a runat="server"  id="Hl_AddGlass" href="#">Add glass</a></li>
                     <li><a runat="server"  id="Hl_AddContact" href="#">Add Contact lens</a></li>
                </ul>
            </div>
        </li>
    </ul>



    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:ValidationSummary ID="ValidationSummary" CssClass="alert alert-danger summarymessage" runat="server" />
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Label_Company">Company:</label>
            <asp:Label ID="Label_Company" runat="server" CssClass="form-control" disabled="disabled"></asp:Label>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_FileNo">File:</label>
            <asp:TextBox ID="TextBox_FileNo" CssClass="form-control" disabled="disabled" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_OpeningDate">Opening Date:</label>
            <asp:TextBox ID="TextBox_OpeningDate" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Title">Title:</label>&nbsp;
           <span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" data-target="#TitleModal"></span>
            <asp:UpdatePanel ID="UpdatePanel_Title" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="ddl_Title" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_LastName">Last Name:</label>
            <asp:TextBox ID="TextBox_LastName" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_FirstName">First Name:</label>&nbsp;
        <asp:RequiredFieldValidator Display="Static" ControlToValidate="TextBox_FirstName" ID="RFV_FirstName" CssClass="label label-danger Errormessage" runat="server" ErrorMessage="First Name is required!" Text="* Required"></asp:RequiredFieldValidator>
            <asp:TextBox ID="TextBox_FirstName" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Medicare">Medicare:</label>&nbsp;
         <%--<span onclick="calculateDOBGender();" data-toggle="tooltip" title="First, enter the Medicare number, then click this button to set the Date of Birth, Age, and Gender." data-placement="top" class="CalButton" style="cursor: pointer;">Set DOB</span>--%>
            <asp:TextBox ID="TextBox_Medicare" onkeyup="calculateDOBGender();" oninput="calculateDOBGender();" CssClass="form-control Capital" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_DateOfBirth">Date of Birth:</label>&nbsp;        
         <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="TextBox_DateOfBirth" ID="RFV_TextBox_DateOfBirth" CssClass="form-control label label-danger Errormessage" runat="server" ErrorMessage="Date Of Birth is required!" Text="* Required"></asp:RequiredFieldValidator>
            <%--<span onclick="calculateAge();" data-toggle="tooltip" title="First, enter the Date of Birth, then click this button." data-placement="top" class="CalButton" style="cursor: pointer;">Set Age</span>--%>
            <asp:TextBox ID="TextBox_DateOfBirth" ClientIDMode="Static" onkeyup="calculateAge();" oninput="calculateAge();" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Age">Age:</label>
            <asp:TextBox ID="TextBox_Age" CssClass="form-control" runat="server" disabled="disabled"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_Gender">Gender:</label>
            <asp:DropDownList ID="DropDownList_Gender" CssClass="form-control" runat="server">
                <asp:ListItem Selected="True">Male</asp:ListItem>
                <asp:ListItem>Female</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_spokenlanguage">Language:</label>
            <asp:DropDownList ID="DropDownList_spokenlanguage" CssClass="form-control" runat="server">
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Address">Address:</label>&nbsp;
        <asp:RequiredFieldValidator Display="Static" ControlToValidate="TextBox_Address" ID="RFV_Address" CssClass="label label-danger Errormessage" runat="server" ErrorMessage="Address is required!" Text="* Required"></asp:RequiredFieldValidator>
            <asp:TextBox ID="TextBox_Address" CssClass="form-control" runat="server"></asp:TextBox>
        </div>

        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_City">City:</label>&nbsp;      
        <span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" data-target="#CityModal"></span>
            <asp:UpdatePanel ID="UpdatePanelCity" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="DropDownList_City" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>


        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_Province">Province:</label>&nbsp;
            <span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" data-target="#ProvinceModal"></span>
            <asp:UpdatePanel ID="UpdatePanelProvince" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="DropDownList_Province" CssClass="form-control" runat="server"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_Country">Country:</label>
            <asp:DropDownList ID="DropDownList_Country" CssClass="form-control" runat="server">
                <asp:ListItem Value="AF">Afghanistan</asp:ListItem>
                <asp:ListItem Value="AL">Albania</asp:ListItem>
                <asp:ListItem Value="DZ">Algeria</asp:ListItem>
                <asp:ListItem Value="AS">American Samoa</asp:ListItem>
                <asp:ListItem Value="AD">Andorra</asp:ListItem>
                <asp:ListItem Value="AO">Angola</asp:ListItem>
                <asp:ListItem Value="AI">Anguilla</asp:ListItem>
                <asp:ListItem Value="AQ">Antarctica</asp:ListItem>
                <asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>
                <asp:ListItem Value="AR">Argentina</asp:ListItem>
                <asp:ListItem Value="AM">Armenia</asp:ListItem>
                <asp:ListItem Value="AW">Aruba</asp:ListItem>
                <asp:ListItem Value="AU">Australia</asp:ListItem>
                <asp:ListItem Value="AT">Austria</asp:ListItem>
                <asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
                <asp:ListItem Value="BS">Bahamas</asp:ListItem>
                <asp:ListItem Value="BH">Bahrain</asp:ListItem>
                <asp:ListItem Value="BD">Bangladesh</asp:ListItem>
                <asp:ListItem Value="BB">Barbados</asp:ListItem>
                <asp:ListItem Value="BY">Belarus</asp:ListItem>
                <asp:ListItem Value="BE">Belgium</asp:ListItem>
                <asp:ListItem Value="BZ">Belize</asp:ListItem>
                <asp:ListItem Value="BJ">Benin</asp:ListItem>
                <asp:ListItem Value="BM">Bermuda</asp:ListItem>
                <asp:ListItem Value="BT">Bhutan</asp:ListItem>
                <asp:ListItem Value="BO">Bolivia</asp:ListItem>
                <asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>
                <asp:ListItem Value="BW">Botswana</asp:ListItem>
                <asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
                <asp:ListItem Value="BR">Brazil</asp:ListItem>
                <asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
                <asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
                <asp:ListItem Value="BG">Bulgaria</asp:ListItem>
                <asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
                <asp:ListItem Value="BI">Burundi</asp:ListItem>
                <asp:ListItem Value="KH">Cambodia</asp:ListItem>
                <asp:ListItem Value="CM">Cameroon</asp:ListItem>
                <asp:ListItem Value="CA" Selected="True">Canada</asp:ListItem>
                <asp:ListItem Value="CV">Cape Verde</asp:ListItem>
                <asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
                <asp:ListItem Value="CF">Central African Republic</asp:ListItem>
                <asp:ListItem Value="TD">Chad</asp:ListItem>
                <asp:ListItem Value="CL">Chile</asp:ListItem>
                <asp:ListItem Value="CN">China</asp:ListItem>
                <asp:ListItem Value="CX">Christmas Island</asp:ListItem>
                <asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
                <asp:ListItem Value="CO">Colombia</asp:ListItem>
                <asp:ListItem Value="KM">Comoros</asp:ListItem>
                <asp:ListItem Value="CG">Congo</asp:ListItem>
                <asp:ListItem Value="CK">Cook Islands</asp:ListItem>
                <asp:ListItem Value="CR">Costa Rica</asp:ListItem>
                <asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>
                <asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>
                <asp:ListItem Value="CU">Cuba</asp:ListItem>
                <asp:ListItem Value="CY">Cyprus</asp:ListItem>
                <asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
                <asp:ListItem Value="DK">Denmark</asp:ListItem>
                <asp:ListItem Value="DJ">Djibouti</asp:ListItem>
                <asp:ListItem Value="DM">Dominica</asp:ListItem>
                <asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
                <asp:ListItem Value="TP">East Timor</asp:ListItem>
                <asp:ListItem Value="EC">Ecuador</asp:ListItem>
                <asp:ListItem Value="EG">Egypt</asp:ListItem>
                <asp:ListItem Value="SV">El Salvador</asp:ListItem>
                <asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
                <asp:ListItem Value="ER">Eritrea</asp:ListItem>
                <asp:ListItem Value="EE">Estonia</asp:ListItem>
                <asp:ListItem Value="ET">Ethiopia</asp:ListItem>
                <asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
                <asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
                <asp:ListItem Value="FJ">Fiji</asp:ListItem>
                <asp:ListItem Value="FI">Finland</asp:ListItem>
                <asp:ListItem Value="FR">France</asp:ListItem>
                <asp:ListItem Value="GF">French Guiana</asp:ListItem>
                <asp:ListItem Value="PF">French Polynesia</asp:ListItem>
                <asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
                <asp:ListItem Value="GA">Gabon</asp:ListItem>
                <asp:ListItem Value="GM">Gambia</asp:ListItem>
                <asp:ListItem Value="GE">Georgia</asp:ListItem>
                <asp:ListItem Value="DE">Germany</asp:ListItem>
                <asp:ListItem Value="GH">Ghana</asp:ListItem>
                <asp:ListItem Value="GI">Gibraltar</asp:ListItem>
                <asp:ListItem Value="GR">Greece</asp:ListItem>
                <asp:ListItem Value="GL">Greenland</asp:ListItem>
                <asp:ListItem Value="GD">Grenada</asp:ListItem>
                <asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
                <asp:ListItem Value="GU">Guam</asp:ListItem>
                <asp:ListItem Value="GT">Guatemala</asp:ListItem>
                <asp:ListItem Value="GN">Guinea</asp:ListItem>
                <asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
                <asp:ListItem Value="GY">Guyana</asp:ListItem>
                <asp:ListItem Value="HT">Haiti</asp:ListItem>
                <asp:ListItem Value="HM">Heard And Mc Donald Islands</asp:ListItem>
                <asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
                <asp:ListItem Value="HN">Honduras</asp:ListItem>
                <asp:ListItem Value="HK">Hong Kong</asp:ListItem>
                <asp:ListItem Value="HU">Hungary</asp:ListItem>
                <asp:ListItem Value="IS">Icel And</asp:ListItem>
                <asp:ListItem Value="IN">India</asp:ListItem>
                <asp:ListItem Value="ID">Indonesia</asp:ListItem>
                <asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>
                <asp:ListItem Value="IQ">Iraq</asp:ListItem>
                <asp:ListItem Value="IE">Ireland</asp:ListItem>
                <asp:ListItem Value="IL">Israel</asp:ListItem>
                <asp:ListItem Value="IT">Italy</asp:ListItem>
                <asp:ListItem Value="JM">Jamaica</asp:ListItem>
                <asp:ListItem Value="JP">Japan</asp:ListItem>
                <asp:ListItem Value="JO">Jordan</asp:ListItem>
                <asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
                <asp:ListItem Value="KE">Kenya</asp:ListItem>
                <asp:ListItem Value="KI">Kiribati</asp:ListItem>
                <asp:ListItem Value="KP">Korea, Dem People'S Republic</asp:ListItem>
                <asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>
                <asp:ListItem Value="KW">Kuwait</asp:ListItem>
                <asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
                <asp:ListItem Value="LA">Lao People'S Dem Republic</asp:ListItem>
                <asp:ListItem Value="LV">Latvia</asp:ListItem>
                <asp:ListItem Value="LB">Lebanon</asp:ListItem>
                <asp:ListItem Value="LS">Lesotho</asp:ListItem>
                <asp:ListItem Value="LR">Liberia</asp:ListItem>
                <asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
                <asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
                <asp:ListItem Value="LT">Lithuania</asp:ListItem>
                <asp:ListItem Value="LU">Luxembourg</asp:ListItem>
                <asp:ListItem Value="MO">Macau</asp:ListItem>
                <asp:ListItem Value="MK">Macedonia</asp:ListItem>
                <asp:ListItem Value="MG">Madagascar</asp:ListItem>
                <asp:ListItem Value="MW">Malawi</asp:ListItem>
                <asp:ListItem Value="MY">Malaysia</asp:ListItem>
                <asp:ListItem Value="MV">Maldives</asp:ListItem>
                <asp:ListItem Value="ML">Mali</asp:ListItem>
                <asp:ListItem Value="MT">Malta</asp:ListItem>
                <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                <asp:ListItem Value="MQ">Martinique</asp:ListItem>
                <asp:ListItem Value="MR">Mauritania</asp:ListItem>
                <asp:ListItem Value="MU">Mauritius</asp:ListItem>
                <asp:ListItem Value="YT">Mayotte</asp:ListItem>
                <asp:ListItem Value="MX">Mexico</asp:ListItem>
                <asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>
                <asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>
                <asp:ListItem Value="MC">Monaco</asp:ListItem>
                <asp:ListItem Value="MN">Mongolia</asp:ListItem>
                <asp:ListItem Value="MS">Montserrat</asp:ListItem>
                <asp:ListItem Value="MA">Morocco</asp:ListItem>
                <asp:ListItem Value="MZ">Mozambique</asp:ListItem>
                <asp:ListItem Value="MM">Myanmar</asp:ListItem>
                <asp:ListItem Value="NA">Namibia</asp:ListItem>
                <asp:ListItem Value="NR">Nauru</asp:ListItem>
                <asp:ListItem Value="NP">Nepal</asp:ListItem>
                <asp:ListItem Value="NL">Netherlands</asp:ListItem>
                <asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>
                <asp:ListItem Value="NC">New Caledonia</asp:ListItem>
                <asp:ListItem Value="NZ">New Zealand</asp:ListItem>
                <asp:ListItem Value="NI">Nicaragua</asp:ListItem>
                <asp:ListItem Value="NE">Niger</asp:ListItem>
                <asp:ListItem Value="NG">Nigeria</asp:ListItem>
                <asp:ListItem Value="NU">Niue</asp:ListItem>
                <asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
                <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                <asp:ListItem Value="NO">Norway</asp:ListItem>
                <asp:ListItem Value="OM">Oman</asp:ListItem>
                <asp:ListItem Value="PK">Pakistan</asp:ListItem>
                <asp:ListItem Value="PW">Palau</asp:ListItem>
                <asp:ListItem Value="PA">Panama</asp:ListItem>
                <asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
                <asp:ListItem Value="PY">Paraguay</asp:ListItem>
                <asp:ListItem Value="PE">Peru</asp:ListItem>
                <asp:ListItem Value="PH">Philippines</asp:ListItem>
                <asp:ListItem Value="PN">Pitcairn</asp:ListItem>
                <asp:ListItem Value="PL">Poland</asp:ListItem>
                <asp:ListItem Value="PT">Portugal</asp:ListItem>
                <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                <asp:ListItem Value="QA">Qatar</asp:ListItem>
                <asp:ListItem Value="RE">Reunion</asp:ListItem>
                <asp:ListItem Value="RO">Romania</asp:ListItem>
                <asp:ListItem Value="RU">Russian Federation</asp:ListItem>
                <asp:ListItem Value="RW">Rwanda</asp:ListItem>
                <asp:ListItem Value="KN">Saint K Itts And Nevis</asp:ListItem>
                <asp:ListItem Value="LC">Saint Lucia</asp:ListItem>
                <asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>
                <asp:ListItem Value="WS">Samoa</asp:ListItem>
                <asp:ListItem Value="SM">San Marino</asp:ListItem>
                <asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>
                <asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
                <asp:ListItem Value="SN">Senegal</asp:ListItem>
                <asp:ListItem Value="SC">Seychelles</asp:ListItem>
                <asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
                <asp:ListItem Value="SG">Singapore</asp:ListItem>
                <asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
                <asp:ListItem Value="SI">Slovenia</asp:ListItem>
                <asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
                <asp:ListItem Value="SO">Somalia</asp:ListItem>
                <asp:ListItem Value="ZA">South Africa</asp:ListItem>
                <asp:ListItem Value="GS">South Georgia , S Sandwich Is.</asp:ListItem>
                <asp:ListItem Value="ES">Spain</asp:ListItem>
                <asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
                <asp:ListItem Value="SH">St. Helena</asp:ListItem>
                <asp:ListItem Value="PM">St. Pierre And Miquelon</asp:ListItem>
                <asp:ListItem Value="SD">Sudan</asp:ListItem>
                <asp:ListItem Value="SR">Suriname</asp:ListItem>
                <asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>
                <asp:ListItem Value="SZ">Sw Aziland</asp:ListItem>
                <asp:ListItem Value="SE">Sweden</asp:ListItem>
                <asp:ListItem Value="CH">Switzerland</asp:ListItem>
                <asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
                <asp:ListItem Value="TW">Taiwan</asp:ListItem>
                <asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
                <asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>
                <asp:ListItem Value="TH">Thailand</asp:ListItem>
                <asp:ListItem Value="TG">Togo</asp:ListItem>
                <asp:ListItem Value="TK">Tokelau</asp:ListItem>
                <asp:ListItem Value="TO">Tonga</asp:ListItem>
                <asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>
                <asp:ListItem Value="TN">Tunisia</asp:ListItem>
                <asp:ListItem Value="TR">Turkey</asp:ListItem>
                <asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
                <asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>
                <asp:ListItem Value="TV">Tuvalu</asp:ListItem>
                <asp:ListItem Value="UG">Uganda</asp:ListItem>
                <asp:ListItem Value="UA">Ukraine</asp:ListItem>
                <asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
                <asp:ListItem Value="GB">United Kingdom</asp:ListItem>
                <asp:ListItem Value="US">United States</asp:ListItem>
                <asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>
                <asp:ListItem Value="UY">Uruguay</asp:ListItem>
                <asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
                <asp:ListItem Value="VU">Vanuatu</asp:ListItem>
                <asp:ListItem Value="VE">Venezuela</asp:ListItem>
                <asp:ListItem Value="VN">Viet Nam</asp:ListItem>
                <asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
                <asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
                <asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>
                <asp:ListItem Value="EH">Western Sahara</asp:ListItem>
                <asp:ListItem Value="YE">Yemen</asp:ListItem>
                <asp:ListItem Value="ZR">Zaire</asp:ListItem>
                <asp:ListItem Value="ZM">Zambia</asp:ListItem>
                <asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_PostalCode">Postal Code:</label>
            <asp:TextBox ID="TextBox_PostalCode" Placeholder="A9A 9A9" ClientIDMode="Static" CssClass="form-control Capital" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_PrimaryContact">Primary Contact:</label>&nbsp;
        <%--<asp:RequiredFieldValidator Display="Static" ControlToValidate="TextBox_PrimaryContact" ID="RFV_PrimaryContact" CssClass="label label-danger Errormessage" runat="server" ErrorMessage="Primary Contact is required!" Text="* Required"></asp:RequiredFieldValidator>--%>
            <asp:TextBox ID="TextBox_PrimaryContact" CssClass="form-control" runat="server"></asp:TextBox>

        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_HomePhone">Home Phone:</label>&nbsp;
            <asp:RequiredFieldValidator Display="Static" ControlToValidate="TextBox_HomePhone" ID="RFV_HomePhone" CssClass="label label-danger Errormessage" runat="server" ErrorMessage="Home Phone is required!" Text="* Required"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="REV_HomePhone" ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" ControlToValidate="TextBox_HomePhone" runat="server" CssClass="label label-danger Errormessage" ErrorMessage="Home Phone is invalid!" Text="* Invalid"></asp:RegularExpressionValidator>
            <asp:TextBox ID="TextBox_HomePhone" ClientIDMode="Static" MaxLength="14" Placeholder="(XXX)-XXX-XXXX" CssClass="form-control" runat="server"></asp:TextBox>
            <%--http://www.harshbaid.in/2013/08/03/regular-expression-for-us-and-canada-phone-number/--%>
        </div>

        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_CellPhone">Cell Phone:</label>&nbsp;
         <asp:RegularExpressionValidator ID="REV_CellPhone" ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" ControlToValidate="TextBox_CellPhone" runat="server" CssClass="label label-danger Errormessage" ErrorMessage="Cell Phone is invalid!" Text="* Invalid"></asp:RegularExpressionValidator>
            <asp:TextBox ID="TextBox_CellPhone" ClientIDMode="Static" Placeholder="(XXX)-XXX-XXXX" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_WorkPhone">Work Phone:</label>&nbsp;
         <asp:RegularExpressionValidator ID="REV_WorkPhone" ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" ControlToValidate="TextBox_WorkPhone" runat="server" CssClass="label label-danger Errormessage" ErrorMessage="Work Phone is invalid!" Text="* Invalid"></asp:RegularExpressionValidator>
            <asp:TextBox ID="TextBox_WorkPhone" ClientIDMode="Static" Placeholder="(XXX)-XXX-XXXX" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Extension">Extension:</label>
            <asp:TextBox ID="TextBox_Extension" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Email">Email:</label>
            <asp:TextBox ID="TextBox_Email" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_Preference">Preference:</label>
            <asp:DropDownList ID="DropDownList_Preference" CssClass="form-control" onchange="CheckPreference();" runat="server">
                <asp:ListItem Selected="True" Value="HomePhone">Contact by Home Phone</asp:ListItem>
                <asp:ListItem Value="CellPhone">Contact by Cell Phone</asp:ListItem>
                <asp:ListItem Value="Email">Contact by Email</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Optometrist">Optometrist:</label>
            <asp:TextBox ID="TextBox_Optometrist" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Ophthalmologist">Ophthalmologist:</label>
            <asp:TextBox ID="TextBox_Ophthalmologist" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Optician">Optician:</label>
            <asp:TextBox ID="TextBox_Optician" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Assistant">Assistant:</label>
            <asp:TextBox ID="TextBox_Assistant" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_ThirdPartyBalance">Third-party Balance:</label>
            <asp:TextBox ID="TextBox_ThirdPartyBalance" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_PatientBalance">Patient Balance:</label>
            <asp:TextBox ID="TextBox_PatientBalance" CssClass="form-control" Text="0.00" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_Insurer">Insurer:</label>
            <asp:TextBox ID="TextBox_Insurer" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_PlanNumber">Plan Number:</label>
            <asp:TextBox ID="TextBox_PlanNumber" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_DoNotRecall">Do Not Recall:</label>
            <asp:TextBox ID="TextBox_DoNotRecall" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DropDownList_Occupation">Occupation:</label>&nbsp;
             <span class="glyphicon glyphicon-plus" data-toggle="modal" style="cursor: pointer;" data-target="#OccupationModal"></span>
            <asp:UpdatePanel ID="UpdatePanelOccupation" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="DropDownList_Occupation" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="TextBox_OtherDetail">Other Detail:</label>
            <asp:TextBox ID="TextBox_OtherDetail" CssClass="form-control" runat="server"></asp:TextBox>
        </div>

        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <br />
            <asp:Button ID="Button_Submit" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="SAVE" OnClientClick="CheckPreference();" OnClick="Button_Submit_Click" />
        </div>
    </div>
    <!-- Modal city -->
    <asp:UpdatePanel ID="UpdatePanelCityModel" runat="server">
        <ContentTemplate>
            <div id="ModalCity" runat="server">
                <div class="modal fade modal-open" id="CityModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal city content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add City</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <label for="TextBox_City">City:</label>
                                    <asp:TextBox ID="TextBox_City" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_City" runat="server" ValidationGroup="Button_City_Group" ControlToValidate="TextBox_City" ErrorMessage="City is required!"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_City" runat="server" CssClass="btn btn-default" ValidationGroup="Button_City_Group" Text="Save" OnClientClick="distroymodal();" OnClick="Button_City_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal Province -->
    <asp:UpdatePanel ID="UpdatePanelProvinceModel" runat="server">
        <ContentTemplate>
            <div id="ModalProvince" runat="server">
                <div class="modal fade modal-open" id="ProvinceModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal Province content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Province</h4>
                            </div>
                            <div class="modal-body">
                                <label for="TextBox_Province">Province:</label>
                                <asp:TextBox ID="TextBox_Province" CssClass="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_Province" runat="server" ValidationGroup="Button_Province_Group" ControlToValidate="TextBox_Province" ErrorMessage="Province is required!"></asp:RequiredFieldValidator>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_Province" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Province_Group" Text="Save" OnClientClick="distroymodal();" OnClick="Button_Province_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal Occupation -->
    <asp:UpdatePanel ID="UpdatePanelOccupationModel" runat="server">
        <ContentTemplate>
            <div id="ModalOccupation" runat="server">
                <div class="modal fade modal-open" id="OccupationModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal Occupation content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Occupation</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <label for="TextBox_Occupation">Occupation:</label>
                                    <asp:TextBox ID="TextBox_Occupation" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_Occupation" runat="server" ValidationGroup="Button_Occupation_Group" ControlToValidate="TextBox_Occupation" ErrorMessage="Occupation is required!"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_Occupation" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Occupation_Group" Text="Save" OnClientClick="distroymodal();" OnClick="Button_Occupation_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Modal Title -->
    <asp:UpdatePanel ID="UpdatePanelTitle" runat="server">
        <ContentTemplate>
            <div id="ModalTitle" runat="server">
                <div class="modal fade modal-open" id="TitleModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal Title content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Title</h4>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <label for="TextBox_Title">Title:</label>
                                    <asp:TextBox ID="TextBox_Title" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" ValidationGroup="Button_Title_Group" ControlToValidate="TextBox_Title" ErrorMessage="Title is required!"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="Button_Title" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Title_Group" Text="Save" OnClientClick="distroymodal();" OnClick="Button_Title_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

