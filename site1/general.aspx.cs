﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class general : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            // set title
            string title = "General";
            Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
            ((Site)Page.Master).heading = title;
            if (Session["MasterId"] == null)
            {
                Response.Redirect("~/home.aspx", true);
            }
            MasterId = Session["MasterId"].ToString();
            objdal = new Dal();
            SetExamNo();

            if (!IsPostBack)
            {
                PatientFill();
               // SetPatientDetails();
                if (Request.QueryString["id"] != null)
                {
                    Button_Submit.Text = "Update";
                    GetTemplateData(Request.QueryString["id"].ToString());
                }
                if (Request.QueryString["pid"] != null)
                {
                    DDL_Patient.SelectedValue = Request.QueryString["pid"].ToString();
                }
            }
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }

    }

    //Fill patient DDl
    protected void PatientFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT [PatientId], [FirstName]+' '+[LastName] +' ('+ SUBSTRING([Gender], 1, 1) +') '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Patient] "
                                 + " FROM [tbl_Patient] where [tbl_Patient].MasterId='" + MasterId + "' ORDER BY [Patient]");
        DDL_Patient.DataSource = dt;
        DDL_Patient.DataValueField = "PatientId";
        DDL_Patient.DataTextField = "Patient";
        DDL_Patient.DataBind();
    }

    //set Max Exam no
    protected void SetExamNo()
    {
        Txt_Examno.Text = objdal.Get_SingleValue("select isnull(max(GeneralEyeExamETemplatesId),0)+1 as fileno from tbl_GeneralEyeExamETemplates");
    }

    // Set Address And Case History
    //protected void SetPatientDetails()
    //{
    //    string patientid = DDL_Patient.SelectedValue.ToString();
    //    DataTable dt = new DataTable();
    //    dt = objdal.GET_STOREPROCEDUREDATA("SpGetPatientDetails", patientid);       
    //    string Item = "Last invoice item: " + dt.Rows[0]["Item"].ToString();
    //    string Service = "Last service: " + dt.Rows[0]["Service"].ToString();
    //    string ThirdPartyBalance = "Third party balance: " + Convert.ToDouble(dt.Rows[0]["ThirdPartyBalance"]).ToString("n");
    //    string PatientBalance = "Patient balance: " + Convert.ToDouble(dt.Rows[0]["PatientBalance"]).ToString("n");
    //    string FeatureEvent = "Next appointment: " + "-";
    //    if (dt.Rows[0]["FeatureEvent"].ToString() != "")
    //    {
    //        FeatureEvent = "Next appointment: " + Convert.ToDateTime(dt.Rows[0]["FeatureEvent"]).ToString("dd-MM-yyyy");
    //    }
    //    txt_CaseHistory.Text = FeatureEvent + "\n" + Item + "\n" + Service + "\n" + ThirdPartyBalance + "\n" + PatientBalance;
    //}
  

    //GetData
    protected void GetTemplateData(string Editid)
    {
        //SELECT [GeneralEyeExamETemplatesId],[PatientId],[ExamDate],[CaseHistory],[RxScope],[RxScopeOD1],[RxScopeOD2],[RxScopeOD3],,[RxScopeOD4]
        //,[RxScopeOs1],[RxScopeOS2],[RxScopeOS3],,[RxScopeOS4],[RxFinal],[RxFinalOD1],[RxFinalOD2],[RxFinalOD3],[RxFinalOD4],[RxFinalOS1]
        //,[RxFinalOS2],[RxFinalOS3],[RxFinalOS4],[Riv],[Sop],[Sgp],[Sogf],[Meds],[All],[VisualAcuity],[VisualAcuity_VL1]
        //,[VisualAcuity_VP1],[VisualAcuity_VLVP],[VisualAcuity_VL2],[VisualAcuity_VP2],[AA],[AAOD],[AAOS],[Ishihara_OD]
        //,[Ishihara_OS],[StereoscopyAnimals],[StereoscopyCircles],[OcularDominance],[ScreentestVL],[ScreentestVP],[Wards1]
        //,[Wards2],[Wards3],[Wards4],[Eyemotilites],[Phoria],[Phoria_VL],[Phoria_VP],[Fusional_VL],[Fusional_VP],[Fusional_VL_BE]
        //,[Fusional_VL_BH],[Fusional_VP_BE],[Fusional_VP_BH],[Fusional_VL_BI],[Fusional_VL_BB],[Fusional_VP_BI],[Fusional_VP_BB]
        //,[PRC1],[PRC2],[PRC_Nose],[Retinoscopy],[Retinoscopy_OD1],[Retinoscopy_OD2],[Retinoscopy_OD3],[Retinoscopy_OD4]
        //,[Retinoscopy_OS1],[Retinoscopy_OS2],[Retinoscopy_OS3],[Retinoscopy_OS4],[Retinoscopy_MAV_OD1],[Retinoscopy_MAV_OD2]
        //,[Retinoscopy_MAV_OD3],[Retinoscopy_MAV_OD4],[Retinoscopy_MAV_ODOS],[Retinoscopy_MAV_OS1],[Retinoscopy_MAV_OS2]
        //,[Retinoscopy_MAV_OS3],[Retinoscopy_MAV_OS4],[Retinoscopy_ACC],[Retinoscopy_Ordiarp],[Retinoscopy_Addfinal]
        //,[Retinoscopy_Vertex],[Keratometry_OD1],[Keratometry_OD2],[Keratometry_OD3],[Keratometry_OS1],[Keratometry_OS2]
        //,[Keratometry_OS3],[Keratometry_Recall],[Keratometry_Fees],[Centralvisualfield],[Peripheralvisualfield]
        //,[Biomicroscopy],[Fundus],[Fundus_OD_RF1],[Fundus_OD_RF2],[Fundus_OS_RF1],[Fundus_OS_RF2],[Periphery],[Tonometry_OD]
        //,[Tonometry_OS],[Tonometry_A1],[Tonometry_A2],[Tonometry],[Drug_benoxinate],[Drug_proparacaine],[Drug_tropicamide]
        //,[Drug_phenylephrine],[Drug_cyclopentolate],[Drug_A1],[Drug_A2],[DiagnosticsTxt],[DiagnosticsChk],[PlanTxt]
        //,[PlanChk],[TemplateType],[MasterId]  FROM [OpticalProgram].[dbo].[tbl_GeneralEyeExamETemplates]

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_GeneralEyeExamETemplates] where [GeneralEyeExamETemplatesId]='" + Editid + "'");
        Txt_Examno.Text = dt.Rows[0]["GeneralEyeExamETemplatesId"].ToString();
        DDL_Patient.SelectedValue = dt.Rows[0]["PatientId"].ToString();        
        Txt_Date.Text = Convert.ToDateTime(dt.Rows[0]["ExamDate"]).ToString("dd-MM-yyyy");
        txt_CaseHistory.Text = dt.Rows[0]["CaseHistory"].ToString();
        txt_RxScopeOD1.Text = dt.Rows[0]["RxScopeOD1"].ToString();
        txt_RxScopeOD2.Text = dt.Rows[0]["RxScopeOD2"].ToString();
        txt_RxScopeOD3.Text = dt.Rows[0]["RxScopeOD3"].ToString();
        txt_RxScopeOD4.Text = dt.Rows[0]["RxScopeOD4"].ToString();
        txt_RxScopeOs1.Text = dt.Rows[0]["RxScopeOs1"].ToString();
        txt_RxScopeOS2.Text = dt.Rows[0]["RxScopeOS2"].ToString();
        txt_RxScopeOS3.Text = dt.Rows[0]["RxScopeOS3"].ToString();
        txt_RxScopeOS4.Text = dt.Rows[0]["RxScopeOS4"].ToString(); 
        RxFinalOD1.Text = dt.Rows[0]["RxFinalOD1"].ToString();
        RxFinalOD2.Text = dt.Rows[0]["RxFinalOD2"].ToString();
        RxFinalOD3.Text = dt.Rows[0]["RxFinalOD3"].ToString();
        RxFinalOD4.Text = dt.Rows[0]["RxFinalOD4"].ToString();
        RxFinalOS1.Text = dt.Rows[0]["RxFinalOS1"].ToString();
        RxFinalOS2.Text = dt.Rows[0]["RxFinalOS2"].ToString();
        RxFinalOS3.Text = dt.Rows[0]["RxFinalOS3"].ToString();
        RxFinalOS4.Text = dt.Rows[0]["RxFinalOS4"].ToString();
        txt_Prism1_OD.Text = dt.Rows[0]["Prism1_OD"].ToString();
        txt_Orientation1_OD.Text = dt.Rows[0]["Orientation1_OD"].ToString();
        txt_Decentration11_OD.Text = dt.Rows[0]["Decentration11_OD"].ToString();
        txt_Decentration12_OD.Text = dt.Rows[0]["Decentration12_OD"].ToString();
        txt_Prism1_OS.Text = dt.Rows[0]["Prism1_OS"].ToString();
        txt_Orientation1_OS.Text = dt.Rows[0]["Orientation1_OS"].ToString();
        txt_Decentration11_OS.Text = dt.Rows[0]["Decentration11_OS"].ToString();
        txt_Decentration12_OS.Text = dt.Rows[0]["Decentration12_OS"].ToString();
        txt_Prism2_OD.Text = dt.Rows[0]["Prism2_OD"].ToString();
        txt_Orientation2_OD.Text = dt.Rows[0]["Orientation2_OD"].ToString();
        txt_Decentration21_OD.Text = dt.Rows[0]["Decentration21_OD"].ToString();
        txt_Decentration22_OD.Text = dt.Rows[0]["Decentration22_OD"].ToString();
        txt_Prism2_OS.Text = dt.Rows[0]["Prism2_OS"].ToString();
        txt_Orientation2_OS.Text = dt.Rows[0]["Orientation2_OS"].ToString();
        txt_Decentration21_OS.Text = dt.Rows[0]["Decentration21_OS"].ToString();
        txt_Decentration22_OS.Text = dt.Rows[0]["Decentration22_OS"].ToString();
        txt_Riv.Text = dt.Rows[0]["Riv"].ToString();
        ddl_Sop.SelectedValue = dt.Rows[0]["Sop"].ToString();
        ddl_Sgp.SelectedValue = dt.Rows[0]["Sgp"].ToString();
        ddl_Sogf.SelectedValue = dt.Rows[0]["Sogf"].ToString();
        txt_Meds.Text = dt.Rows[0]["Meds"].ToString();
        txt_All.Text = dt.Rows[0]["All"].ToString();
        txt_VisualAcuity_VL1.Text = dt.Rows[0]["VisualAcuity_VL1"].ToString();
        txt_VisualAcuity_VP1.Text = dt.Rows[0]["VisualAcuity_VP1"].ToString();
        txt_VisualAcuity_VLVP.Text = dt.Rows[0]["VisualAcuity_VLVP"].ToString();
        txt_VisualAcuity_VL2.Text = dt.Rows[0]["VisualAcuity_VL2"].ToString();
        txt_VisualAcuity_VP2.Text = dt.Rows[0]["VisualAcuity_VP2"].ToString();
        txt_AAOD.Text = dt.Rows[0]["AAOD"].ToString();
        txt_AAOS.Text = dt.Rows[0]["AAOS"].ToString();
        txt_IshiharaOD.Text = dt.Rows[0]["Ishihara_OD"].ToString();
        txt_IshiharaOS.Text = dt.Rows[0]["Ishihara_OS"].ToString();
        txt_StereoscopyAnimals.Text = dt.Rows[0]["StereoscopyAnimals"].ToString();
        txt_StereoscopyCircles.Text = dt.Rows[0]["StereoscopyCircles"].ToString();
        txt_OcularDominance.Text = dt.Rows[0]["OcularDominance"].ToString();
        txt_ScreentestVL.Text = dt.Rows[0]["ScreentestVL"].ToString();
        txt_ScreentestVP.Text = dt.Rows[0]["ScreentestVP"].ToString();     
        txt_Wards1.Text = dt.Rows[0]["Wards1"].ToString();
        txt_Wards2.Text = dt.Rows[0]["Wards2"].ToString();
        txt_Wards3.Text = dt.Rows[0]["Wards3"].ToString();
        txt_Wards4.Text = dt.Rows[0]["Wards4"].ToString();
        txt_PhoriaVL.Text = dt.Rows[0]["Phoria_VL"].ToString();
        txt_PhoriaVP.Text = dt.Rows[0]["Phoria_VP"].ToString();
        txt_Fusional_VL.Text = dt.Rows[0]["Fusional_VL"].ToString();
        txt_Fusional_VP.Text = dt.Rows[0]["Fusional_VP"].ToString();
        txt_Fusional_VL_BE.Text = dt.Rows[0]["Fusional_VL_BE"].ToString();     
        txt_Fusional_VL_BH.Text = dt.Rows[0]["Fusional_VL_BH"].ToString();
        txt_Fusional_VP_BE.Text = dt.Rows[0]["Fusional_VP_BE"].ToString();
        txt_Fusional_VP_BH.Text = dt.Rows[0]["Fusional_VP_BH"].ToString();
        txt_Fusional_VL_BI.Text = dt.Rows[0]["Fusional_VL_BI"].ToString();
        txt_Fusional_VL_BB.Text = dt.Rows[0]["Fusional_VL_BB"].ToString();
        txt_Fusional_VP_BI.Text = dt.Rows[0]["Fusional_VP_BI"].ToString();
        txt_Fusional_VP_BB.Text = dt.Rows[0]["Fusional_VP_BB"].ToString();
        txt_PRC1.Text = dt.Rows[0]["PRC1"].ToString();
        txt_PRC2.Text = dt.Rows[0]["PRC2"].ToString();
        txt_Retinoscopy_OD1.Text = dt.Rows[0]["Retinoscopy_OD1"].ToString();
        txt_Retinoscopy_OD2.Text = dt.Rows[0]["Retinoscopy_OD2"].ToString();
        txt_Retinoscopy_OD3.Text = dt.Rows[0]["Retinoscopy_OD3"].ToString();
        txt_Retinoscopy_OD4.Text = dt.Rows[0]["Retinoscopy_OD4"].ToString();
        txt_Retinoscopy_OS1.Text = dt.Rows[0]["Retinoscopy_OS1"].ToString();
        txt_Retinoscopy_OS2.Text = dt.Rows[0]["Retinoscopy_OS2"].ToString();
        txt_Retinoscopy_OS3.Text = dt.Rows[0]["Retinoscopy_OS3"].ToString();
        txt_Retinoscopy_OS4.Text = dt.Rows[0]["Retinoscopy_OS4"].ToString();
        txt_Retinoscopy_MAV_OD1.Text = dt.Rows[0]["Retinoscopy_MAV_OD1"].ToString();
        txt_Retinoscopy_MAV_OD2.Text = dt.Rows[0]["Retinoscopy_MAV_OD2"].ToString();
        txt_Retinoscopy_MAV_OD3.Text = dt.Rows[0]["Retinoscopy_MAV_OD3"].ToString();
        txt_Retinoscopy_MAV_OD4.Text = dt.Rows[0]["Retinoscopy_MAV_OD4"].ToString();
        txt_Retinoscopy_MAV_ODOS.Text = dt.Rows[0]["Retinoscopy_MAV_ODOS"].ToString();
        txt_Retinoscopy_MAV_OS1.Text = dt.Rows[0]["Retinoscopy_MAV_OS1"].ToString();
        txt_Retinoscopy_MAV_OS2.Text = dt.Rows[0]["Retinoscopy_MAV_OS2"].ToString();
        txt_Retinoscopy_MAV_OS3.Text = dt.Rows[0]["Retinoscopy_MAV_OS3"].ToString();
        txt_Retinoscopy_MAV_OS4.Text = dt.Rows[0]["Retinoscopy_MAV_OS4"].ToString();
        txt_Retinoscopy_ACC.Text = dt.Rows[0]["Retinoscopy_ACC"].ToString();
        txt_Retinoscopy_Ordiarp.Text = dt.Rows[0]["Retinoscopy_Ordiarp"].ToString();
        txt_Retinoscopy_Addfinal.Text = dt.Rows[0]["Retinoscopy_Addfinal"].ToString();       
        txt_Retinoscopy_Vertex.Text = dt.Rows[0]["Retinoscopy_Vertex"].ToString();
        txt_Keratometry_OD1.Text = dt.Rows[0]["Keratometry_OD1"].ToString();
        txt_Keratometry_OD2.Text = dt.Rows[0]["Keratometry_OD2"].ToString();
        txt_Keratometry_OD3.Text = dt.Rows[0]["Keratometry_OD3"].ToString();
        txt_Keratometry_OS1.Text = dt.Rows[0]["Keratometry_OS1"].ToString();
        txt_Keratometry_OS2.Text = dt.Rows[0]["Keratometry_OS2"].ToString();
        txt_Keratometry_OS3.Text = dt.Rows[0]["Keratometry_OS3"].ToString();
        txt_Keratometry_Recall.Text = dt.Rows[0]["Keratometry_Recall"].ToString();
        txt_Keratometry_Fees.Text = dt.Rows[0]["Keratometry_Fees"].ToString();
        txt_Fundus_OD_RF1.Text = dt.Rows[0]["Fundus_OD_RF1"].ToString();
        txt_Fundus_OD_RF2.Text = dt.Rows[0]["Fundus_OD_RF2"].ToString();
        txt_Fundus_OS_RF1.Text = dt.Rows[0]["Fundus_OS_RF1"].ToString();
        txt_Fundus_OS_RF2.Text = dt.Rows[0]["Fundus_OS_RF2"].ToString();
        txt_Tonometry_OD.Text = dt.Rows[0]["Tonometry_OD"].ToString();
        txt_Tonometry_OS.Text = dt.Rows[0]["Tonometry_OS"].ToString();
        txt_Tonometry_A1.Text = dt.Rows[0]["Tonometry_A1"].ToString();
        txt_Tonometry_A2.Text = dt.Rows[0]["Tonometry_A2"].ToString();
        txt_Drug_benoxinate.Text = dt.Rows[0]["Drug_benoxinate"].ToString();
        txt_Drug_proparacaine.Text = dt.Rows[0]["Drug_proparacaine"].ToString();
        txt_Drug_tropicamide.Text = dt.Rows[0]["Drug_tropicamide"].ToString();
        txt_Drug_phenylephrine.Text = dt.Rows[0]["Drug_phenylephrine"].ToString();
        txt_Drug_cyclopentolate.Text = dt.Rows[0]["Drug_cyclopentolate"].ToString();
        txt_Drug_A1.Text = dt.Rows[0]["Drug_A1"].ToString();
        txt_Drug_A2.Text = dt.Rows[0]["Drug_A2"].ToString();
        txt_Diagnostics.Text = dt.Rows[0]["DiagnosticsTxt"].ToString();
        txt_Plan.Text = dt.Rows[0]["PlanTxt"].ToString();

        string RxScope = dt.Rows[0]["RxScope"].ToString();       
        if (RxScope.Contains("chk_simple"))
        {
            chk_simple.Checked = true;
        }
        if (RxScope.Contains("chk_progressive"))
        {
            chk_progressive.Checked = true;
        }
        if (RxScope.Contains("chk_bifocal"))
        {
            chk_bifocal.Checked = true;
        }

        string RxFinal = dt.Rows[0]["RxFinal"].ToString();
        if (RxFinal.Contains("True"))
        {
            chk_Rxfinal.Checked = true;
        }

        string VisualAcuity = dt.Rows[0]["VisualAcuity"].ToString();       
        if (VisualAcuity.Contains("chk_VLSc"))
        {
            chk_VLSc.Checked = true;
        }
        if (VisualAcuity.Contains("chk_VPSc"))
        {
            chk_VPSc.Checked = true;
        }
         if (VisualAcuity.Contains("chk_VLCc"))
        {
            chk_VLCc.Checked = true;
        }
          if (VisualAcuity.Contains("chk_VPCc"))
        {
            chk_VPCc.Checked = true;
        }

        
        string AA = dt.Rows[0]["AA"].ToString();     
        if (AA.Contains("chk_AADonders"))
        {
            chk_AADonders.Checked = true;
        }
        if (AA.Contains("chk_AASheard"))
        {
            chk_AASheard.Checked = true;
        }

        string Eyemotilites = dt.Rows[0]["Eyemotilites"].ToString();
        if (Eyemotilites.Contains("True"))
        {
            chk_Flexibleandcomplete.Checked = true;
        }

        string Phoria = dt.Rows[0]["Phoria"].ToString();       
        if (Phoria.Contains("chk_PhoriaTm"))
        {
            chk_PhoriaTm.Checked = true;
        }
        if (Phoria.Contains("chk_PhoriaVongraefe"))
        {
            chk_PhoriaVongraefe.Checked = true;
        }

        string PRC_Nose = dt.Rows[0]["PRC_Nose"].ToString();
        if (PRC_Nose.Contains("True"))
        {
            chk_PRC_Nose.Checked = true;
        }

        string Retinoscopy = dt.Rows[0]["Retinoscopy"].ToString();
        if (Retinoscopy.Contains("chk_Retinoscopy_Static"))
        {
            chk_Retinoscopy_Static.Checked = true;
        }
        if (Retinoscopy.Contains("chk_Retinoscopy_LM"))
        {
            chk_Retinoscopy_LM.Checked = true;
        }
        if (Retinoscopy.Contains("chk_Retinoscopy_Auto"))
        {
            chk_Retinoscopy_Auto.Checked = true;
        }

        string Centralvisualfield = dt.Rows[0]["Centralvisualfield"].ToString();
        if (Centralvisualfield.Contains("True"))
        {
            Chk_Centralvisualfield.Checked = true;
        }

        string Peripheralvisualfield = dt.Rows[0]["Peripheralvisualfield"].ToString();
        if (Peripheralvisualfield.Contains("True"))
        {
            chk_Peripheralvisualfield.Checked = true;
        }

        string Biomicroscopy = dt.Rows[0]["Biomicroscopy"].ToString();        
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Eyelids"))
        {
            chk_Biomicroscopy_OD_Eyelids.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Connective"))
        {
            chk_Biomicroscopy_OD_Connective.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Cornea"))
        {
            chk_Biomicroscopy_OD_Cornea.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Tearfilm"))
        {
            chk_Biomicroscopy_OD_Tearfilm.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Chambreant"))
        {
            chk_Biomicroscopy_OD_Chambreant.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Iris"))
        {
            chk_Biomicroscopy_OD_Iris.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OD_Crystalline"))
        {
            chk_Biomicroscopy_OD_Crystalline.Checked = true;
        }      
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Eyelids"))
        {
            chk_Biomicroscopy_OS_Eyelids.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Connective"))
        {
            chk_Biomicroscopy_OS_Connective.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Cornea"))
        {
            chk_Biomicroscopy_OS_Cornea.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Tearfilm"))
        {
            chk_Biomicroscopy_OS_Tearfilm.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Chambreant"))
        {
            chk_Biomicroscopy_OS_Chambreant.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Iris"))
        {
            chk_Biomicroscopy_OS_Iris.Checked = true;
        }
        if (Biomicroscopy.Contains("chk_Biomicroscopy_OS_Crystalline"))
        {
            chk_Biomicroscopy_OS_Crystalline.Checked = true;
        }


        string Fundus =  dt.Rows[0]["Fundus"].ToString();        
        
        if (Fundus.Contains("chk_Fundus_Ophthalmoscope"))
        {
            chk_Fundus_Ophthalmoscope.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_Volk"))
        {
            chk_Fundus_Volk.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OD_Papilla"))
        {
            chk_Fundus_OD_Papilla.Checked = true;
        }       
        if (Fundus.Contains("chk_Fundus_OS_Papilla"))
        {
            chk_Fundus_OS_Papilla.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OD_Anr"))
        {
            chk_Fundus_OD_Anr.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OS_Anr"))
        {
            chk_Fundus_OS_Anr.Checked = true;
        }      
        if (Fundus.Contains("chk_Fundus_OD_Macula"))
        {
            chk_Fundus_OD_Macula.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OS_Macula"))
        {
            chk_Fundus_OS_Macula.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OD_Vessels"))
        {
            chk_Fundus_OD_Vessels.Checked = true;
        }       
        if (Fundus.Contains("chk_Fundus_OS_Vessels"))
        {
            chk_Fundus_OS_Vessels.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OD_Window"))
        {
            chk_Fundus_OD_Window.Checked = true;
        }
        if (Fundus.Contains("chk_Fundus_OS_Window"))
        {
            chk_Fundus_OS_Window.Checked = true;
        }

        string Periphery = dt.Rows[0]["Periphery"].ToString();        
        if (Periphery.Contains("chk_Periphery_Bio"))
        {
            chk_Periphery_Bio.Checked = true;
        }
        if (Periphery.Contains("chk_Periphery_Mirrors"))
        {
            chk_Periphery_Mirrors.Checked = true;
        }
        if (Periphery.Contains("chk_Periphery_OU"))
        {
            chk_Periphery_OU.Checked = true;
        }


        string Tonometry = dt.Rows[0]["Tonometry"].ToString();     
        if (Tonometry.Contains("chk_Tonometry_Goldmann"))
        {
            chk_Tonometry_Goldmann.Checked = true;
        }
        if (Tonometry.Contains("chk_Tonometry_Nct"))
        {
            chk_Tonometry_Nct.Checked = true;
        }


        string DiagnosticsChk = dt.Rows[0]["DiagnosticsChk"].ToString();       
        if (DiagnosticsChk.Contains("chk_Farsightedness"))
        {
            chk_Farsightedness.Checked = true;
        }
        if (DiagnosticsChk.Contains("chk_Myopia"))
        {
            chk_Myopia.Checked = true;
        }
        if (DiagnosticsChk.Contains("chk_Astigmatism"))
        {
            chk_Astigmatism.Checked = true;
        }
        if (DiagnosticsChk.Contains("chk_Cataract"))
        {
            chk_Cataract.Checked = true;
        }
        if (DiagnosticsChk.Contains("chk_Presbyopia"))
        {
            chk_Presbyopia.Checked = true;
        }
        if (DiagnosticsChk.Contains("chk_Blepharitis"))
        {
            chk_Blepharitis.Checked = true;
        }
        if (DiagnosticsChk.Contains("chk_Dryeye"))
        {
            chk_Dryeye.Checked = true;
        }
        if (DiagnosticsChk.Contains("Chk_Macular"))
        {
            Chk_Macular.Checked = true;
        }
        if (DiagnosticsChk.Contains("Chk_Glaucoma"))
        {
            Chk_Glaucoma.Checked = true;
        }

        string PlanChk = dt.Rows[0]["PlanChk"].ToString();   
        if (PlanChk.Contains("chk_Changerx"))
        {
            chk_Changerx.Checked = true;
        }
        if (PlanChk.Contains("chk_Artificialtears"))
        {
            chk_Artificialtears.Checked = true;
        }
        if (PlanChk.Contains("chk_Coldcompresses"))
        {
            chk_Coldcompresses.Checked = true;
        }
        if (PlanChk.Contains("chk_Warmcompresses"))
        {
            chk_Warmcompresses.Checked = true;
        }
        if (PlanChk.Contains("chk_Eyelidhygiene"))
        {
            chk_Eyelidhygiene.Checked = true;
        }
        if (PlanChk.Contains("chk_Preventiveexpansion"))
        {
            chk_Preventiveexpansion.Checked = true;
        }
        if (PlanChk.Contains("chk_Ptadvised"))
        {
            chk_Ptadvised.Checked = true;
        }
        if (PlanChk.Contains("chk_Reyoiremergency"))
        {
            chk_Reyoiremergency.Checked = true;
        }
    }

    // Save and update template
    protected void Button_Submit_Click(object sender, EventArgs e)
    {
        try
        {
            StringBuilder sb = new StringBuilder();


            // for RxScope
            CheckBox[] checkboxes = new CheckBox[] { chk_simple, chk_progressive, chk_bifocal };
            List<string> check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string RxScope = String.Join(",", check);

            // for RxFinal
            string RxFinal = "0";
            if (chk_Rxfinal.Checked)
            {
                RxFinal = "1";
            }

            // for VisualAcuity
            checkboxes = new CheckBox[] { chk_VLSc, chk_VPSc, chk_VLCc, chk_VPCc };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string VisualAcuity = String.Join(",", check);


            // for AA
            checkboxes = new CheckBox[] { chk_AADonders, chk_AASheard };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string AA = String.Join(",", check);

            // for Eyemotilites
            string Eyemotilites = "0";
            if (chk_Flexibleandcomplete.Checked)
            {
                Eyemotilites = "1";
            }

            // for Phoria
            checkboxes = new CheckBox[] { chk_PhoriaTm, chk_PhoriaVongraefe };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Phoria = String.Join(",", check);


            // for PRC_Nose
            string PRC_Nose = "0";
            if (chk_PRC_Nose.Checked)
            {
                PRC_Nose = "1";
            }

            // for Retinoscopy
            checkboxes = new CheckBox[] { chk_Retinoscopy_Static, chk_Retinoscopy_LM, chk_Retinoscopy_Auto };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Retinoscopy = String.Join(",", check);

            // for Centralvisualfield
            string Centralvisualfield = "0";
            if (Chk_Centralvisualfield.Checked)
            {
                Centralvisualfield = "1";
            }

            // for Peripheralvisualfield
            string Peripheralvisualfield = "0";
            if (chk_Peripheralvisualfield.Checked)
            {
                Peripheralvisualfield = "1";
            }

            // for Biomicroscopy
            checkboxes = new CheckBox[] { chk_Biomicroscopy_OD_Eyelids, chk_Biomicroscopy_OD_Connective, chk_Biomicroscopy_OD_Cornea, 
                                           chk_Biomicroscopy_OD_Tearfilm, chk_Biomicroscopy_OD_Chambreant, chk_Biomicroscopy_OD_Iris,
                                           chk_Biomicroscopy_OD_Crystalline,chk_Biomicroscopy_OS_Eyelids, chk_Biomicroscopy_OS_Connective,
                                           chk_Biomicroscopy_OS_Cornea, chk_Biomicroscopy_OS_Tearfilm, chk_Biomicroscopy_OS_Chambreant,
                                           chk_Biomicroscopy_OS_Iris, chk_Biomicroscopy_OS_Crystalline };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Biomicroscopy = String.Join(",", check);


            // for Fundus
            checkboxes = new CheckBox[] { chk_Fundus_Ophthalmoscope, chk_Fundus_Volk, chk_Fundus_OD_Papilla, chk_Fundus_OS_Papilla,
                                           chk_Fundus_OD_Anr,chk_Fundus_OS_Anr,chk_Fundus_OD_Macula,chk_Fundus_OS_Macula,chk_Fundus_OD_Vessels,
                                           chk_Fundus_OS_Vessels,chk_Fundus_OD_Window,chk_Fundus_OS_Window};
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Fundus = String.Join(",", check);

            // for Periphery
            checkboxes = new CheckBox[] { chk_Periphery_Bio, chk_Periphery_Mirrors, chk_Periphery_OU };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Periphery = String.Join(",", check);


            // for Tonometry
            checkboxes = new CheckBox[] { chk_Tonometry_Goldmann, chk_Tonometry_Nct };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Tonometry = String.Join(",", check);


            // for DiagnosticsChk
            checkboxes = new CheckBox[] { chk_Farsightedness, chk_Myopia, chk_Astigmatism, chk_Cataract, chk_Presbyopia, chk_Blepharitis, 
                                           chk_Dryeye, Chk_Macular, Chk_Glaucoma };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string DiagnosticsChk = String.Join(",", check);

            // for PlanChk
            checkboxes = new CheckBox[] { chk_Changerx, chk_Artificialtears, chk_Coldcompresses, chk_Warmcompresses, chk_Eyelidhygiene, 
                                           chk_Preventiveexpansion, chk_Ptadvised,chk_Reyoiremergency };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string PlanChk = String.Join(",", check);

            if (Request.QueryString["id"] != null)
            {
                //UPDATE [dbo].[tbl_GeneralEyeExamETemplates]  SET [PatientId] = '',[ExamDate] = '',[CaseHistory] = '',[RxScope] = ''
                //,[RxScopeOD1] = '',[RxScopeOD2] = '',[RxScopeOD3] = '',[RxScopeOD4]='',[RxScopeOs1] = '',[RxScopeOS2] = '',[RxScopeOS3] = '',[RxScopeOS4]=''
                // ,[RxFinal] = '',[RxFinalOD1] = '',[RxFinalOD2] = '',[RxFinalOD3] = '',[RxFinalOD4] = '',[RxFinalOS1] = ''
                // ,[RxFinalOS2] = '',[RxFinalOS3] = '',[RxFinalOS4] = '',[Prism1_OD] = '',[Orientation1_OD] = '',[Decentration11_OD] = '',[Decentration12_OD] = '',[Prism1_OS] = ''
                //     ,[Orientation1_OS] = '',[Decentration11_OS] = '',[Decentration12_OS] = '',[Prism2_OD] = '',[Orientation2_OD] = ''
                //    ,[Decentration21_OD] = '',[Decentration22_OD] = '',[Prism2_OS] = '',[Orientation2_OS] = '',[Decentration21_OS] = ''
                //     ,[Decentration22_OS] = '',[Riv] = '',[Sop] = '',[Sgp] = '',[Sogf] = '',[Meds] = ''
                // ,[All] = '',[VisualAcuity] = '',[VisualAcuity_VL1] = '',[VisualAcuity_VP1] = '',[VisualAcuity_VLVP] = ''
                // ,[VisualAcuity_VL2] = '',[VisualAcuity_VP2] = '',[AA] = '',[AAOD] = '',[AAOS] = '',[Ishihara_OD] = '',[Ishihara_OS] = ''
                // ,[StereoscopyAnimals] = '',[StereoscopyCircles] = '',[OcularDominance] = '',[ScreentestVL] = '',[ScreentestVP] = ''
                // ,[Wards1] = '',[Wards2] = '',[Wards3] = '',[Wards4] = '',[Eyemotilites] = '',[Phoria] = '',[Phoria_VL] = ''
                // ,[Phoria_VP] = '',[Fusional_VL] = '',[Fusional_VP] = '',[Fusional_VL_BE] = '',[Fusional_VL_BH] = ''
                // ,[Fusional_VP_BE] = '',[Fusional_VP_BH] = '',[Fusional_VL_BI] = '',[Fusional_VL_BB] = '',[Fusional_VP_BI] = ''
                // ,[Fusional_VP_BB] = '',[PRC1] = '',[PRC2] = '',[PRC_Nose] = '',[Retinoscopy] = '',[Retinoscopy_OD1] = ''
                //  ,[Retinoscopy_OD2] = '',[Retinoscopy_OD3] = '',[Retinoscopy_OD4] = '',[Retinoscopy_OS1] = '',[Retinoscopy_OS2] =''
                //  ,[Retinoscopy_OS3] = '',[Retinoscopy_OS4] = '',[Retinoscopy_MAV_OD1] = '',[Retinoscopy_MAV_OD2] = ''
                //  ,[Retinoscopy_MAV_OD3] = '',[Retinoscopy_MAV_OD4] = '',[Retinoscopy_MAV_ODOS] = '',[Retinoscopy_MAV_OS1] = ''
                //  ,[Retinoscopy_MAV_OS2] = '',[Retinoscopy_MAV_OS3] = '',[Retinoscopy_MAV_OS4] = '',[Retinoscopy_ACC] = ''
                //  ,[Retinoscopy_Ordiarp] = '',[Retinoscopy_Addfinal] = '',[Retinoscopy_Vertex] = '',[Keratometry_OD1] = ''
                //  ,[Keratometry_OD2] = '',[Keratometry_OD3] = '',[Keratometry_OS1] = '',[Keratometry_OS2] = '',[Keratometry_OS3] = ''
                //  ,[Keratometry_Recall] = '',[Keratometry_Fees] = '',[Centralvisualfield] = '',[Peripheralvisualfield] = ''
                //   ,[Biomicroscopy] = '',[Fundus] = '',[Fundus_OD_RF1] = '',[Fundus_OD_RF2] = '',[Fundus_OS_RF1] = ''
                //   ,[Fundus_OS_RF2] = '',[Periphery] = '',[Tonometry_OD] = '',[Tonometry_OS] = '',[Tonometry_A1] = '',[Tonometry_A2] = ''
                //   ,[Tonometry] = '',[Drug_benoxinate] = '',[Drug_proparacaine] = '',[Drug_tropicamide] = ''
                //  ,[Drug_phenylephrine] = '',[Drug_cyclopentolate] = '',[Drug_A1] = '',[Drug_A2] = '',[DiagnosticsTxt] = ''
                //  ,[DiagnosticsChk] = '',[PlanTxt] = '',[PlanChk] = ''     
                //    WHERE 


                sb.Append("UPDATE [dbo].[tbl_GeneralEyeExamETemplates]  SET [PatientId] = '" + DDL_Patient.SelectedValue + "'");
                sb.Append(",[ExamDate] = '" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "',[CaseHistory] = '" + txt_CaseHistory.Text + "'");
                sb.Append(",[RxScope] = '" + RxScope + "',[RxScopeOD1] = '" + txt_RxScopeOD1.Text + "',[RxScopeOD2] = '" + txt_RxScopeOD2.Text + "'");
                sb.Append(",[RxScopeOD3] = '" + txt_RxScopeOD3.Text + "',[RxScopeOD4]='"+ txt_RxScopeOD4.Text +"',[RxScopeOs1] = '" + txt_RxScopeOs1.Text + "'");
                sb.Append(",[RxScopeOS2] = '" + txt_RxScopeOS2.Text + "',[RxScopeOS3] = '" + txt_RxScopeOS3.Text + "'");
                sb.Append(",[RxScopeOS4]='"+ txt_RxScopeOS4.Text +"',[RxFinal] = '" + RxFinal + "',[RxFinalOD1] = '" + RxFinalOD1.Text + "'");
                sb.Append(",[RxFinalOD2] = '" + RxFinalOD2.Text + "',[RxFinalOD3] = '" + RxFinalOD3.Text + "',[RxFinalOD4] = '" + RxFinalOD4.Text + "'");
                sb.Append(",[RxFinalOS1] = '" + RxFinalOS1.Text + "',[RxFinalOS2] = '" + RxFinalOS2.Text + "',[RxFinalOS3] = '" + RxFinalOS3.Text + "'");
                sb.Append(",[RxFinalOS4] = '" + RxFinalOS4.Text + "'");                
                sb.Append(",[Prism1_OD] = '" + txt_Prism1_OD.Text + "',[Orientation1_OD] = '" + txt_Orientation1_OD.Text + "'");
                sb.Append(",[Decentration11_OD] = '" + txt_Decentration11_OD.Text + "',[Decentration12_OD] = '" + txt_Decentration12_OD.Text + "'");
                sb.Append(",[Prism1_OS] = '" + txt_Prism1_OS.Text + "',[Orientation1_OS] = '" + txt_Orientation1_OS.Text + "'");
                sb.Append(",[Decentration11_OS] = '" + txt_Decentration11_OS.Text + "',[Decentration12_OS] = '" + txt_Decentration12_OS.Text + "'");
                sb.Append(",[Prism2_OD] = '" + txt_Prism2_OD.Text + "',[Orientation2_OD] = '" + txt_Orientation2_OD.Text + "'");
                sb.Append(",[Decentration21_OD] = '" + txt_Decentration21_OD.Text + "',[Decentration22_OD] = '" + txt_Decentration22_OD.Text + "'");
                sb.Append(",[Prism2_OS] = '" + txt_Prism2_OS.Text + "',[Orientation2_OS] = '" + txt_Orientation2_OS.Text + "'");
                sb.Append(",[Decentration21_OS] = '" + txt_Decentration21_OS.Text + "',[Decentration22_OS] = '" + txt_Decentration22_OS.Text + "'");
                sb.Append(",[Riv] = '" + txt_Riv.Text + "',[Sop] = '" + ddl_Sop.SelectedValue + "'");
                sb.Append(",[Sgp] = '" + ddl_Sgp.SelectedValue + "',[Sogf] = 'ddl_Sogf.SelectedValue',[Meds] = '" + txt_Meds.Text + "'");
                sb.Append(",[All] = '" + txt_All.Text + "',[VisualAcuity] = '" + VisualAcuity + "',[VisualAcuity_VL1] = '" + txt_VisualAcuity_VL1.Text + "'");
                sb.Append(",[VisualAcuity_VP1] = '" + txt_VisualAcuity_VP1.Text + "',[VisualAcuity_VLVP] = '" + txt_VisualAcuity_VLVP.Text + "'");
                sb.Append(",[VisualAcuity_VL2] = '" + txt_VisualAcuity_VL2.Text + "',[VisualAcuity_VP2] = '" + txt_VisualAcuity_VP2.Text + "'");
                sb.Append(",[AA] = '" + AA + "',[AAOD] = '" + txt_AAOD.Text + "',[AAOS] = '" + txt_AAOS.Text + "',[Ishihara_OD] = '" + txt_IshiharaOD.Text + "'");
                sb.Append(",[Ishihara_OS] = '" + txt_IshiharaOS.Text + "',[StereoscopyAnimals] = '" + txt_StereoscopyAnimals.Text + "'");
                sb.Append(",[StereoscopyCircles] = '" + txt_StereoscopyCircles.Text + "',[OcularDominance] = '" + txt_OcularDominance.Text + "'");
                sb.Append(",[ScreentestVL] = '" + txt_ScreentestVL.Text + "',[ScreentestVP] = '" + txt_ScreentestVP.Text + "'");
                sb.Append(",[Wards1] = '" + txt_Wards1.Text + "',[Wards2] = '" + txt_Wards2.Text + "',[Wards3] = '" + txt_Wards3.Text + "'");
                sb.Append(",[Wards4] = '" + txt_Wards4.Text + "',[Eyemotilites] = '" + Eyemotilites + "',[Phoria] = '" + Phoria + "'");
                sb.Append(",[Phoria_VL] = '" + txt_PhoriaVL.Text + "',[Phoria_VP] = '" + txt_PhoriaVP.Text + "',[Fusional_VL] = '" + txt_Fusional_VL.Text + "'");
                sb.Append(",[Fusional_VP] = '" + txt_Fusional_VP.Text + "',[Fusional_VL_BE] = '" + txt_Fusional_VL_BE.Text + "'");
                sb.Append(",[Fusional_VL_BH] = '" + txt_Fusional_VL_BH.Text + "',[Fusional_VP_BE] = '" + txt_Fusional_VP_BE.Text + "'");
                sb.Append(",[Fusional_VP_BH] = '" + txt_Fusional_VP_BH.Text + "',[Fusional_VL_BI] = '" + txt_Fusional_VL_BI.Text + "'");
                sb.Append(",[Fusional_VL_BB] = '" + txt_Fusional_VL_BB.Text + "',[Fusional_VP_BI] = '" + txt_Fusional_VP_BI.Text + "'");
                sb.Append(",[Fusional_VP_BB] = '" + txt_Fusional_VP_BB.Text + "',[PRC1] = '" + txt_PRC1.Text + "',[PRC2] = '" + txt_PRC2.Text + "'");
                sb.Append(",[PRC_Nose] = '" + PRC_Nose + "',[Retinoscopy] = '" + Retinoscopy + "',[Retinoscopy_OD1] = '" + txt_Retinoscopy_OD1.Text + "'");
                sb.Append(",[Retinoscopy_OD2] = '" + txt_Retinoscopy_OD2.Text + "',[Retinoscopy_OD3] = '" + txt_Retinoscopy_OD3.Text + "'");
                sb.Append(",[Retinoscopy_OD4] = '" + txt_Retinoscopy_OD4.Text + "',[Retinoscopy_OS1] = '" + txt_Retinoscopy_OS1.Text + "'");
                sb.Append(",[Retinoscopy_OS2] ='" + txt_Retinoscopy_OS2.Text + "',[Retinoscopy_OS3] = '" + txt_Retinoscopy_OS3.Text + "'");
                sb.Append(",[Retinoscopy_OS4] = '" + txt_Retinoscopy_OS4.Text + "',[Retinoscopy_MAV_OD1] = '" + txt_Retinoscopy_MAV_OD1.Text + "'");
                sb.Append(",[Retinoscopy_MAV_OD2] = '" + txt_Retinoscopy_MAV_OD2.Text + "',[Retinoscopy_MAV_OD3] = '" + txt_Retinoscopy_MAV_OD3.Text + "'");
                sb.Append(",[Retinoscopy_MAV_OD4] = '" + txt_Retinoscopy_MAV_OD4.Text + "',[Retinoscopy_MAV_ODOS] = '" + txt_Retinoscopy_MAV_ODOS.Text + "'");
                sb.Append(",[Retinoscopy_MAV_OS1] = '" + txt_Retinoscopy_MAV_OS1.Text + "',[Retinoscopy_MAV_OS2] = '" + txt_Retinoscopy_MAV_OS2.Text + "'");
                sb.Append(",[Retinoscopy_MAV_OS3] = '" + txt_Retinoscopy_MAV_OS3.Text + "',[Retinoscopy_MAV_OS4] = '" + txt_Retinoscopy_MAV_OS4.Text + "'");
                sb.Append(",[Retinoscopy_ACC] = '" + txt_Retinoscopy_ACC.Text + "',[Retinoscopy_Ordiarp] = '" + txt_Retinoscopy_Ordiarp.Text + "'");
                sb.Append(",[Retinoscopy_Addfinal] = '" + txt_Retinoscopy_Addfinal.Text + "',[Retinoscopy_Vertex] = '" + txt_Retinoscopy_Vertex.Text + "'");
                sb.Append(",[Keratometry_OD1] = '" + txt_Keratometry_OD1.Text + "',[Keratometry_OD2] = '" + txt_Keratometry_OD2.Text + "'");
                sb.Append(",[Keratometry_OD3] = '" + txt_Keratometry_OS3.Text + "',[Keratometry_OS1] = '" + txt_Keratometry_OS1.Text + "'");
                sb.Append(",[Keratometry_OS2] = '" + txt_Keratometry_OS2.Text + "',[Keratometry_OS3] = '" + txt_Keratometry_OS3.Text + "'");
                sb.Append(",[Keratometry_Recall] = '" + txt_Keratometry_Recall.Text + "',[Keratometry_Fees] = '" + txt_Keratometry_Fees.Text + "'");
                sb.Append(",[Centralvisualfield] = '" + Centralvisualfield + "',[Peripheralvisualfield] = '" + Peripheralvisualfield + "'");
                sb.Append(",[Biomicroscopy] = '" + Biomicroscopy + "',[Fundus] = '" + Fundus + "',[Fundus_OD_RF1] = '" + txt_Fundus_OD_RF1.Text + "'");
                sb.Append(",[Fundus_OD_RF2] = '" + txt_Fundus_OD_RF2.Text + "',[Fundus_OS_RF1] = '" + txt_Fundus_OS_RF1.Text + "'");
                sb.Append(",[Fundus_OS_RF2] = '" + txt_Fundus_OS_RF2.Text + "',[Periphery] = '" + Periphery + "',[Tonometry_OD] = '" + txt_Tonometry_OD.Text + "'");
                sb.Append(",[Tonometry_OS] = '" + txt_Tonometry_OS.Text + "',[Tonometry_A1] = '" + txt_Tonometry_A1.Text + "'");
                sb.Append(",[Tonometry_A2] = '" + txt_Tonometry_A2.Text + "',[Tonometry] = '" + Tonometry + "'");
                sb.Append(",[Drug_benoxinate] = '" + txt_Drug_benoxinate.Text + "',[Drug_proparacaine] = '" + txt_Drug_proparacaine.Text + "'");
                sb.Append(",[Drug_tropicamide] = '" + txt_Drug_tropicamide.Text + "',[Drug_phenylephrine] = '" + txt_Drug_phenylephrine.Text + "'");
                sb.Append(",[Drug_cyclopentolate] = '" + txt_Drug_cyclopentolate.Text + "',[Drug_A1] = '" + txt_Drug_A1.Text + "',[Drug_A2] = '" + txt_Drug_A2.Text + "'");
                sb.Append(",[DiagnosticsTxt] = '" + txt_Diagnostics.Text + "',[DiagnosticsChk] = '" + DiagnosticsChk + "',[PlanTxt] = '" + txt_Plan.Text + "'");
                sb.Append(",[PlanChk] = '" + PlanChk + "' WHERE GeneralEyeExamETemplatesId='" + Request.QueryString["id"].ToString() + "'");

                objdal.EXECUTE_DML(sb.ToString());
                Session["lblMessage"] = "Exam template is updated successfully.";
            }
            else
            {
                // INSERT INTO [dbo].[tbl_GeneralEyeExamETemplates]([GeneralEyeExamETemplatesId],[PatientId]
                //,[ExamDate],[CaseHistory],[RxScope],[RxScopeOD1],[RxScopeOD2],[RxScopeOD3]
                //,[RxScopeOs1],[RxScopeOS2],[RxScopeOS3],[RxFinal],[RxFinalOD1],[RxFinalOD2]
                //,[RxFinalOD3],[RxFinalOD4],[RxFinalOS1],[RxFinalOS2],[RxFinalOS3],[RxFinalOS4]
                //,[Riv],[Sop],[Sgp],[Sogf],[Meds],[All],[VisualAcuity],[VisualAcuity_VL1]
                //,[VisualAcuity_VP1],[VisualAcuity_VLVP],[VisualAcuity_VL2],[VisualAcuity_VP2]
                //,[AA],[AAOD],[AAOS],[Ishihara_OD],[Ishihara_OS],[StereoscopyAnimals]
                //,[StereoscopyCircles],[OcularDominance],[ScreentestVL],[ScreentestVP]
                //,[Wards1],[Wards2],[Wards3],[Wards4],[Eyemotilites],[Phoria],[Phoria_VL]
                //,[Phoria_VP],[Fusional_VL],[Fusional_VP],[Fusional_VL_BE],[Fusional_VL_BH]
                //,[Fusional_VP_BE],[Fusional_VP_BH],[Fusional_VL_BI],[Fusional_VL_BB]
                //,[Fusional_VP_BI],[Fusional_VP_BB],[PRC1],[PRC2],[PRC_Nose],[Retinoscopy]
                //,[Retinoscopy_OD1],[Retinoscopy_OD2],[Retinoscopy_OD3],[Retinoscopy_OD4]
                //,[Retinoscopy_OS1],[Retinoscopy_OS2],[Retinoscopy_OS3],[Retinoscopy_OS4]
                //,[Retinoscopy_MAV_OD1],[Retinoscopy_MAV_OD2],[Retinoscopy_MAV_OD3],[Retinoscopy_MAV_OD4]
                //,[Retinoscopy_MAV_ODOS],[Retinoscopy_MAV_OS1],[Retinoscopy_MAV_OS2],[Retinoscopy_MAV_OS3]
                //,[Retinoscopy_MAV_OS4],[Retinoscopy_ACC],[Retinoscopy_Ordiarp],[Retinoscopy_Addfinal]
                //,[Retinoscopy_Vertex],[Keratometry_OD1],[Keratometry_OD2],[Keratometry_OD3],[Keratometry_OS1]
                //,[Keratometry_OS2],[Keratometry_OS3],[Keratometry_Recall],[Keratometry_Fees],[Centralvisualfield]
                //,[Peripheralvisualfield],[Biomicroscopy],[Fundus],[Fundus_OD_RF1],[Fundus_OD_RF2]
                //,[Fundus_OS_RF1],[Fundus_OS_RF2],[Periphery],[Tonometry_OD],[Tonometry_OS],[Tonometry_A1]
                //,[Tonometry_A2],[Tonometry],[Drug_benoxinate],[Drug_proparacaine],[Drug_tropicamide]
                //,[Drug_phenylephrine],[Drug_cyclopentolate],[Drug_A1],[Drug_A2],[DiagnosticsTxt],[DiagnosticsChk]
                //,[PlanTxt],[PlanChk],[TemplateType],[MasterId]) VALUES()

                sb.Append("INSERT INTO [dbo].[tbl_GeneralEyeExamETemplates]([GeneralEyeExamETemplatesId],[PatientId],[ExamDate],[CaseHistory]");
                sb.Append(",[RxScope],[RxScopeOD1],[RxScopeOD2],[RxScopeOD3],[RxScopeOD4],[RxScopeOs1],[RxScopeOS2],[RxScopeOS3],[RxScopeOS4],[RxFinal]");
                sb.Append(",[RxFinalOD1],[RxFinalOD2],[RxFinalOD3],[RxFinalOD4],[RxFinalOS1],[RxFinalOS2],[RxFinalOS3],[RxFinalOS4]");
                sb.Append(",[Prism1_OD],[Orientation1_OD],[Decentration11_OD],[Decentration12_OD],[Prism1_OS],[Orientation1_OS],[Decentration11_OS]");
                sb.Append(",[Decentration12_OS],[Prism2_OD],[Orientation2_OD],[Decentration21_OD],[Decentration22_OD],[Prism2_OS],[Orientation2_OS]");
                sb.Append(",[Decentration21_OS],[Decentration22_OS],[Riv],[Sop],[Sgp],[Sogf],[Meds]");               
                sb.Append(",[All],[VisualAcuity],[VisualAcuity_VL1],[VisualAcuity_VP1],[VisualAcuity_VLVP],[VisualAcuity_VL2],[VisualAcuity_VP2]");
                sb.Append(",[AA],[AAOD],[AAOS],[Ishihara_OD],[Ishihara_OS],[StereoscopyAnimals],[StereoscopyCircles],[OcularDominance],[ScreentestVL],[ScreentestVP]");
                sb.Append(",[Wards1],[Wards2],[Wards3],[Wards4],[Eyemotilites],[Phoria],[Phoria_VL],[Phoria_VP],[Fusional_VL],[Fusional_VP],[Fusional_VL_BE]");
                sb.Append(",[Fusional_VL_BH],[Fusional_VP_BE],[Fusional_VP_BH],[Fusional_VL_BI],[Fusional_VL_BB],[Fusional_VP_BI],[Fusional_VP_BB],[PRC1],[PRC2]");
                sb.Append(",[PRC_Nose],[Retinoscopy],[Retinoscopy_OD1],[Retinoscopy_OD2],[Retinoscopy_OD3],[Retinoscopy_OD4],[Retinoscopy_OS1],[Retinoscopy_OS2]");
                sb.Append(",[Retinoscopy_OS3],[Retinoscopy_OS4],[Retinoscopy_MAV_OD1],[Retinoscopy_MAV_OD2],[Retinoscopy_MAV_OD3],[Retinoscopy_MAV_OD4]");
                sb.Append(",[Retinoscopy_MAV_ODOS],[Retinoscopy_MAV_OS1],[Retinoscopy_MAV_OS2],[Retinoscopy_MAV_OS3],[Retinoscopy_MAV_OS4],[Retinoscopy_ACC]");
                sb.Append(",[Retinoscopy_Ordiarp],[Retinoscopy_Addfinal],[Retinoscopy_Vertex],[Keratometry_OD1],[Keratometry_OD2],[Keratometry_OD3],[Keratometry_OS1]");
                sb.Append(",[Keratometry_OS2],[Keratometry_OS3],[Keratometry_Recall],[Keratometry_Fees],[Centralvisualfield],[Peripheralvisualfield],[Biomicroscopy]");
                sb.Append(",[Fundus],[Fundus_OD_RF1],[Fundus_OD_RF2],[Fundus_OS_RF1],[Fundus_OS_RF2],[Periphery],[Tonometry_OD],[Tonometry_OS],[Tonometry_A1]");
                sb.Append(",[Tonometry_A2],[Tonometry],[Drug_benoxinate],[Drug_proparacaine],[Drug_tropicamide],[Drug_phenylephrine],[Drug_cyclopentolate]");
                sb.Append(",[Drug_A1],[Drug_A2],[DiagnosticsTxt],[DiagnosticsChk],[PlanTxt],[PlanChk],[TemplateType],[MasterId]) ");
                sb.Append("VALUES(" + Txt_Examno.Text + ",'" + DDL_Patient.SelectedValue + "','" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",'" + txt_CaseHistory.Text + "','" + RxScope + "','" + txt_RxScopeOD1.Text + "','" + txt_RxScopeOD2.Text + "','" + txt_RxScopeOD3.Text + "'");
                sb.Append(",'" + txt_RxScopeOD3.Text + "','" + txt_RxScopeOs1.Text + "','" + txt_RxScopeOS2.Text + "'");
                sb.Append(",'" + txt_RxScopeOS3.Text + "','" + txt_RxScopeOS4.Text + "','" + RxFinal + "','" + RxFinalOD1.Text + "'");
                sb.Append(",'" + RxFinalOD2.Text + "','" + RxFinalOD3.Text + "','" + RxFinalOD4.Text + "','" + RxFinalOS1.Text + "','" + RxFinalOS2.Text + "'");
                sb.Append(",'" + RxFinalOS3.Text + "','" + RxFinalOS4.Text + "'");
                sb.Append(",'" + txt_Prism1_OD.Text + "','" + txt_Orientation1_OD.Text + "','" + txt_Decentration11_OD.Text + "'");
                sb.Append(",'" + txt_Decentration12_OD.Text + "','" + txt_Prism1_OS.Text + "','" + txt_Orientation1_OS.Text + "','" + txt_Decentration11_OS.Text + "'");
                sb.Append(",'" + txt_Decentration12_OS.Text + "','" + txt_Prism2_OD.Text + "','" + txt_Orientation2_OD.Text + "','" + txt_Decentration21_OD.Text + "'");
                sb.Append(",'" + txt_Decentration22_OD.Text + "','" + txt_Prism2_OS.Text + "','" + txt_Orientation2_OS.Text + "','" + txt_Decentration21_OS.Text + "'");
                sb.Append(",'" + txt_Decentration22_OS.Text + "','" + txt_Riv.Text + "','" + ddl_Sop.SelectedValue + "','" + ddl_Sgp.SelectedValue + "'");
                sb.Append(",'" + ddl_Sogf.SelectedValue + "','" + txt_Meds.Text + "','" + txt_All.Text + "','" + VisualAcuity + "','" + txt_VisualAcuity_VL1.Text + "'");
                sb.Append(",'" + txt_VisualAcuity_VP1.Text + "','" + txt_VisualAcuity_VLVP.Text + "','" + txt_VisualAcuity_VL2.Text + "','" + txt_VisualAcuity_VP2.Text + "'");
                sb.Append(",'" + AA + "','" + txt_AAOD.Text + "','" + txt_AAOS.Text + "','" + txt_IshiharaOD.Text + "','" + txt_IshiharaOS.Text + "'");
                sb.Append(",'" + txt_StereoscopyAnimals.Text + "','" + txt_StereoscopyCircles.Text + "','" + txt_OcularDominance.Text + "','" + txt_ScreentestVL.Text + "'");
                sb.Append(",'" + txt_ScreentestVP.Text + "','" + txt_Wards1.Text + "','" + txt_Wards2.Text + "','" + txt_Wards3.Text + "','" + txt_Wards4.Text + "'");
                sb.Append(",'" + Eyemotilites + "','" + Phoria + "','" + txt_PhoriaVL.Text + "','" + txt_PhoriaVP.Text + "','" + txt_Fusional_VL.Text + "'");
                sb.Append(",'" + txt_Fusional_VP.Text + "','" + txt_Fusional_VL_BE.Text + "','" + txt_Fusional_VL_BH.Text + "','" + txt_Fusional_VP_BE.Text + "'");
                sb.Append(",'" + txt_Fusional_VP_BH.Text + "','" + txt_Fusional_VL_BI.Text + "','" + txt_Fusional_VL_BB.Text + "','" + txt_Fusional_VP_BI.Text + "'");
                sb.Append(",'" + txt_Fusional_VP_BB.Text + "','" + txt_PRC1.Text + "','" + txt_PRC2.Text + "','" + PRC_Nose + "','" + Retinoscopy + "'");
                sb.Append(",'" + txt_Retinoscopy_OD1.Text + "','" + txt_Retinoscopy_OD2.Text + "','" + txt_Retinoscopy_OD3.Text + "','" + txt_Retinoscopy_OD4.Text + "'");
                sb.Append(",'" + txt_Retinoscopy_OS1.Text + "','" + txt_Retinoscopy_OS2.Text + "','" + txt_Retinoscopy_OS3.Text + "','" + txt_Retinoscopy_OS4.Text + "'");
                sb.Append(",'" + txt_Retinoscopy_MAV_OD1.Text + "','" + txt_Retinoscopy_MAV_OD2.Text + "','" + txt_Retinoscopy_MAV_OD3.Text + "'");
                sb.Append(",'" + txt_Retinoscopy_MAV_OD4.Text + "','" + txt_Retinoscopy_MAV_ODOS.Text + "','" + txt_Retinoscopy_MAV_OS1.Text + "'");
                sb.Append(",'" + txt_Retinoscopy_MAV_OS2.Text + "','" + txt_Retinoscopy_MAV_OS3.Text + "','" + txt_Retinoscopy_MAV_OS4.Text + "'");
                sb.Append(",'" + txt_Retinoscopy_ACC.Text + "','" + txt_Retinoscopy_Ordiarp.Text + "','" + txt_Retinoscopy_Addfinal.Text + "'");
                sb.Append(",'" + txt_Retinoscopy_Vertex.Text + "','" + txt_Keratometry_OD1.Text + "','" + txt_Keratometry_OD2.Text + "','" + txt_Keratometry_OD3.Text + "'");
                sb.Append(",'" + txt_Keratometry_OS1.Text + "','" + txt_Keratometry_OS2.Text + "','" + txt_Keratometry_OS3.Text + "','" + txt_Keratometry_Recall.Text + "'");
                sb.Append(",'" + txt_Keratometry_Fees.Text + "','" + Centralvisualfield + "','" + Peripheralvisualfield + "','" + Biomicroscopy + "'");
                sb.Append(",'" + Fundus + "','" + txt_Fundus_OD_RF1.Text + "','" + txt_Fundus_OD_RF2.Text + "','" + txt_Fundus_OS_RF1.Text + "'");
                sb.Append(",'" + txt_Fundus_OS_RF2.Text + "','" + Periphery + "','" + txt_Tonometry_OD.Text + "','" + txt_Tonometry_OS.Text + "'");
                sb.Append(",'" + txt_Tonometry_A1.Text + "','" + txt_Tonometry_A2.Text + "','" + Tonometry + "','" + txt_Drug_benoxinate.Text + "'");
                sb.Append(",'" + txt_Drug_proparacaine.Text + "','" + txt_Drug_tropicamide.Text + "','" + txt_Drug_phenylephrine.Text + "'");
                sb.Append(",'" + txt_Drug_cyclopentolate.Text + "','" + txt_Drug_A1.Text + "','" + txt_Drug_A2.Text + "','" + txt_Diagnostics.Text + "'");
                sb.Append(",'" + DiagnosticsChk + "','" + txt_Plan.Text + "','" + PlanChk + "','General','" + MasterId + "')");

                objdal.EXECUTE_DML(sb.ToString());
                Session["lblMessage"] = "Exam template is added successfully.";

            }

            Response.Redirect("~/examtemplate.aspx", true);
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
}