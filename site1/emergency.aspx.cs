﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dilationtemplates : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // set title
        string title = "Emergency";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();
        objdal = new Dal();
        SetExamNo();
       
        if (!IsPostBack)
        {
             PatientFill();
           //  SetPatientDetails();
             if (Request.QueryString["id"] != null)
            {
                Button_Submit.Text = "Update";
                GetTemplateData(Request.QueryString["id"].ToString());
            }
             if (Request.QueryString["pid"] != null)
             {
                 DDL_Patient.SelectedValue = Request.QueryString["pid"].ToString();
             }
        }
    }

    //Fill patient DDl
    protected void PatientFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT [PatientId], [FirstName]+' '+[LastName] +' ('+ SUBSTRING([Gender], 1, 1) +') '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Patient] "
                                 +" FROM [tbl_Patient] where [tbl_Patient].MasterId='"+ MasterId +"' ORDER BY [Patient]");
        DDL_Patient.DataSource = dt;
        DDL_Patient.DataValueField = "PatientId";
        DDL_Patient.DataTextField = "Patient";
        DDL_Patient.DataBind();
    }

    //set Max Exam no
    protected void SetExamNo()
    {
       Txt_Examno.Text = objdal.Get_SingleValue("select isnull(max(EmergencyExamtemplatesId),0)+1 as fileno from tbl_EmergencyExamtemplates");
    }

    // Set Address And Case History
    //protected void SetPatientDetails()
    //{
    //    string patientid = DDL_Patient.SelectedValue.ToString();
    //    DataTable dt = new DataTable();
    //    dt = objdal.GET_STOREPROCEDUREDATA("SpGetPatientDetails", patientid);             
    //    string Item = "Last invoice item: "+ dt.Rows[0]["Item"].ToString();
    //    string Service= "Last service: "+ dt.Rows[0]["Service"].ToString();
    //    string ThirdPartyBalance = "Third party balance: "+ Convert.ToDouble(dt.Rows[0]["ThirdPartyBalance"]).ToString("n");
    //    string PatientBalance ="Patient balance: "+ Convert.ToDouble(dt.Rows[0]["PatientBalance"]).ToString("n");
    //    string FeatureEvent = "Next appointment: " + "-";
    //    if (dt.Rows[0]["FeatureEvent"].ToString()!="")
    //    {
    //        FeatureEvent = "Next appointment: " + Convert.ToDateTime(dt.Rows[0]["FeatureEvent"]).ToString("dd-MM-yyyy");
    //    }         
    //    txt_CaseHistory.Text = FeatureEvent + "\n" + Item + "\n" + Service + "\n" + ThirdPartyBalance + "\n" + PatientBalance;
    //}

    protected void SDSPatient_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@MasterId"].Value = MasterId;
    }

    // Save and update template
    protected void Button_Submit_Click(object sender, EventArgs e)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            // for AgentsDiagnostic
            CheckBox[] checkboxes = new CheckBox[] { Chk_Proparacaine, Chk_Benoxinate, Chk_Tropicamide, Chk_Cyclogyl, Chk_Phenylephrine };
            List<string> check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string AgentsDiagnostic = String.Join(",", check);

            // for AgentsDiagnostic
            checkboxes = new CheckBox[] { chk_Electro, chk_ET, chk_confront };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string Visualfields = String.Join(",", check);

            // for Periphery OD
            checkboxes = new CheckBox[] { chk_OdNoholes, chk_OdNottearing, chk_OdNodetachment };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string PeripheryOD = String.Join(",", check);

            // for Periphery OS
            checkboxes = new CheckBox[] { chk_OsNoholes, chk_OsNottearing, chk_OsNodetachment };
            check = new List<string>();
            foreach (CheckBox checkbox in checkboxes)
            {
                if (checkbox.Checked)
                {
                    check.Add(checkbox.ID);
                }
            }
            string PeripheryOS = String.Join(",", check);


            if (Request.QueryString["id"] != null)
            {
                //UPDATE [dbo].[tbl_EmergencyExamtemplates]  SET [PatientId] = '',[ExamDate] = '',[CaseHistory] = ''
                //,[VisualAcuity_OD_AvlwithRx] = '',[VisualAcuity_OD_AvlwithVl] = '',[VisualAcuity_OD_TS] = ''
                //,[VisualAcuity_OD_VP] = '',[VisualAcuity_OS_AvlwithRx] = '',[VisualAcuity_OS_AvlwithVl] =''
                //,[VisualAcuity_OS_TS] = '',[VisualAcuity_OS_VP] = '',[ReflexesPupillaries_OD_MM] = ''
                //,[ReflexesPupillaries_OD_LG] = '',[ReflexesPupillaries_OD_MG] ='',[ReflexesPupillaries_OS_MM] = ''
                //,[ReflexesPupillaries_OS_LG] = '',[ReflexesPupillaries_OS_MG] ='',[ReflexesPupillaries_Notes] =''
                //,[TonometriesGoldmann_OD] = '',[TonometriesGoldmann_OS] = '',[TonometriesGoldmann_OD_H] = ''
                //,[TonometriesGoldmann_OS_H] = '',[AgentsDiagnostic] = '',[AnteriorTears] = '',[AnteriorEyelids] =''
                //,[AnteriorConjunctiva] = '',[Anteriorcornea] = '',[AnteriorIt] = '',[AnteriorIris] = '',[AnteriorCrystalline] = ''
                //,[VisualFields] = '',[VisualTarget] = '',[PosteriorEnvironments] = '',[PosteriorPapilla] = ''
                //,[PosteriorEp] = '',[PosteriorPvs] = '',[PosteriorAv] = '',[PosteriorMacula] = '',[PeripheryOD] = ''
                //,[PeripheryOS] = '',[TemplateType] = '',[MasterId] = '' WHERE             


                sb.Append("UPDATE [dbo].[tbl_EmergencyExamtemplates] SET [PatientId] = '" + DDL_Patient.SelectedValue + "'");
                sb.Append(",[ExamDate] = '" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "',[CaseHistory] = '" + txt_CaseHistory.Text + "'");
                sb.Append(",[VisualAcuity_OD_AvlwithRx] = '" + txt_OD_Avlwithrx.Text + "',[VisualAcuity_OD_AvlwithVl] = '" + txt_OD_Avlwithvl.Text + "'");
                sb.Append(",[VisualAcuity_OD_TS] = '" + txt_OD_Ts.Text + "',[VisualAcuity_OD_VP] = '" + txt_OD_VP.Text + "'");
                sb.Append(",[VisualAcuity_OS_AvlwithRx] = '" + txt_OS_Avlwithrx.Text + "',[VisualAcuity_OS_AvlwithVl] = '" + txt_OS_Avlwithvl.Text + "'");
                sb.Append(",[VisualAcuity_OS_TS] = '" + txt_OS_Ts.Text + "',[VisualAcuity_OS_VP] = '" + txt_OS_VP.Text + "'");
                sb.Append(",[ReflexesPupillaries_OD_MM] = '" + Txt_OD_MM.Text + "',[ReflexesPupillaries_OD_LG] = '" + txt_OD_LG.Text + "'");
                sb.Append(",[ReflexesPupillaries_OD_MG] = '" + txt_OD_MG.Text + "',[ReflexesPupillaries_OS_MM] = '" + Txt_OS_MM.Text + "'");
                sb.Append(",[ReflexesPupillaries_OS_LG] = '" + txt_OS_LG.Text + "',[ReflexesPupillaries_OS_MG] ='" + txt_OS_MG.Text + "'");
                sb.Append(",[ReflexesPupillaries_Notes] = '" + txt_Notes.Text + "',[TonometriesGoldmann_OD] = '" + txt_OD_Tonometries.Text + "'");
                sb.Append(",[TonometriesGoldmann_OS] = '" + txt_OS_Tonometries.Text + "',[TonometriesGoldmann_OD_H] = '" + txt_OD_h.Text + "'");
                sb.Append(",[TonometriesGoldmann_OS_H] = '" + txt_OS_h.Text + "',[AgentsDiagnostic] = '" + AgentsDiagnostic + "'");
                sb.Append(",[AnteriorTears] = '" + txt_Tears.Text + "',[AnteriorEyelids] ='" + txt_Eyelids.Text + "'");
                sb.Append(",[AnteriorConjunctiva] = '" + txt_Conjunctiva.Text + "',[Anteriorcornea] = '" + txt_cornea.Text + "'");
                sb.Append(",[AnteriorIt] = '" + txt_It.Text + "',[AnteriorIris] = '" + txt_Iris.Text + "',[AnteriorCrystalline] = '" + txt_Crystalline.Text + "'");
                sb.Append(",[VisualFields] = '" + Visualfields + "',[VisualTarget] = '" + txt_Target.Text + "',[PosteriorEnvironments] = '" + txt_Environments.Text + "'");
                sb.Append(",[PosteriorPapilla] = '" + txt_Papilla.Text + "',[PosteriorEp] = '" + txt_Ep.Text + "',[PosteriorPvs] = '" + txt_Pvs.Text + "'");
                sb.Append(",[PosteriorAv] = '" + txt_Av.Text + "',[PosteriorMacula] = '" + txt_Macula.Text + "',[PeripheryOD] = '" + PeripheryOD + "'");
                sb.Append(",[PeripheryOS] = '" + PeripheryOS + "' where EmergencyExamtemplatesId='" + Request.QueryString["id"].ToString() + "'");
                
                objdal.EXECUTE_DML(sb.ToString());
                Session["lblMessage"] = "Exam template is updated successfully.";
            }
            else
            {


            // INSERT INTO [dbo].[tbl_EmergencyExamtemplates]([EmergencyExamtemplatesId],[PatientId],[ExamDate]
            //,[CaseHistory],[VisualAcuity_OD_AvlwithRx],[VisualAcuity_OD_AvlwithVl],[VisualAcuity_OD_TS]
            //,[VisualAcuity_OD_VP],[VisualAcuity_OS_AvlwithRx],[VisualAcuity_OS_AvlwithVl],[VisualAcuity_OS_TS]
            //,[VisualAcuity_OS_VP],[ReflexesPupillaries_OD_MM],[ReflexesPupillaries_OD_LG],[ReflexesPupillaries_OD_MG]
            //,[ReflexesPupillaries_OS_MM],[ReflexesPupillaries_OS_LG],[ReflexesPupillaries_OS_MG],[ReflexesPupillaries_Notes]
            //,[TonometriesGoldmann_OD],[TonometriesGoldmann_OS],[TonometriesGoldmann_OD_H],[TonometriesGoldmann_OS_H]
            //,[AgentsDiagnostic],[AnteriorTears],[AnteriorEyelids],[AnteriorConjunctiva],[Anteriorcornea]
            //,[AnteriorIt],[AnteriorIris],[AnteriorCrystalline],[VisualFields],[VisualTarget],[PosteriorEnvironments]
            //,[PosteriorPapilla],[PosteriorEp],[PosteriorPvs],[PosteriorAv],[PosteriorMacula],[PeripheryOD],[PeripheryOS]
            //,[TemplateType],[MasterId])VALUES
               

                sb.Append("INSERT INTO [dbo].[tbl_EmergencyExamtemplates]([EmergencyExamtemplatesId],[PatientId],[ExamDate],[CaseHistory]");
                sb.Append(",[VisualAcuity_OD_AvlwithRx],[VisualAcuity_OD_AvlwithVl],[VisualAcuity_OD_TS],[VisualAcuity_OD_VP],[VisualAcuity_OS_AvlwithRx]");
                sb.Append(",[VisualAcuity_OS_AvlwithVl],[VisualAcuity_OS_TS],[VisualAcuity_OS_VP],[ReflexesPupillaries_OD_MM],[ReflexesPupillaries_OD_LG]");
                sb.Append(",[ReflexesPupillaries_OD_MG],[ReflexesPupillaries_OS_MM],[ReflexesPupillaries_OS_LG],[ReflexesPupillaries_OS_MG],[ReflexesPupillaries_Notes]");
                sb.Append(",[TonometriesGoldmann_OD],[TonometriesGoldmann_OS],[TonometriesGoldmann_OD_H],[TonometriesGoldmann_OS_H],[AgentsDiagnostic]");
                sb.Append(",[AnteriorTears],[AnteriorEyelids],[AnteriorConjunctiva],[Anteriorcornea],[AnteriorIt],[AnteriorIris],[AnteriorCrystalline]");
                sb.Append(",[VisualFields],[VisualTarget],[PosteriorEnvironments],[PosteriorPapilla],[PosteriorEp],[PosteriorPvs],[PosteriorAv]");
                sb.Append(",[PosteriorMacula],[PeripheryOD],[PeripheryOS],[TemplateType],[MasterId])");                
                sb.Append("VALUES("+ Txt_Examno.Text +",'" + DDL_Patient.SelectedValue + "','" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",'" + txt_CaseHistory.Text + "','" + txt_OD_Avlwithrx.Text + "','" + txt_OD_Avlwithvl.Text + "'");
                sb.Append(",'" + txt_OD_Ts.Text + "','" + txt_OD_VP.Text + "','" + txt_OS_Avlwithrx.Text + "','" + txt_OS_Avlwithvl.Text + "','" + txt_OS_Ts.Text + "'");
                sb.Append(",'" + txt_OS_VP.Text + "','" + Txt_OD_MM.Text + "','" + txt_OD_LG.Text + "','" + txt_OD_MG.Text + "','" + Txt_OS_MM.Text + "'");
                sb.Append(",'" + txt_OS_LG.Text + "','" + txt_OS_MG.Text + "','" + txt_Notes.Text + "','" + txt_OD_Tonometries.Text + "','" + txt_OS_Tonometries.Text + "'");
                sb.Append(",'" + txt_OD_h.Text + "','" + txt_OS_h.Text + "','" + AgentsDiagnostic + "','" + txt_Tears.Text +"','" + txt_Eyelids.Text +"'");
                sb.Append(",'" + txt_Conjunctiva.Text +"','" + txt_cornea.Text +"','" + txt_It.Text +"','" + txt_Iris.Text +"','" + txt_Crystalline.Text +"'");
                sb.Append(",'" + Visualfields + "','" + txt_Target.Text +"','" + txt_Environments.Text +"','" + txt_Papilla.Text +"','" + txt_Ep.Text +"'");
                sb.Append(",'" + txt_Pvs.Text + "','" + txt_Av.Text + "','" + txt_Macula.Text + "','" + PeripheryOD + "','" + PeripheryOS + "'");               
                sb.Append(",'Emergency','" + MasterId + "')");
                objdal.EXECUTE_DML(sb.ToString());
                Session["lblMessage"] = "Exam template is added successfully.";
            }
           

            Response.Redirect("~/examtemplate.aspx", true);
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    protected void GetTemplateData(string Editid)
    { 
    // SELECT [EmergencyExamtemplatesId],[PatientId],[ExamDate],[CaseHistory],[VisualAcuity_OD_AvlwithRx],[VisualAcuity_OD_AvlwithVl]
    //,[VisualAcuity_OD_TS],[VisualAcuity_OD_VP],[VisualAcuity_OS_AvlwithRx],[VisualAcuity_OS_AvlwithVl],[VisualAcuity_OS_TS]
    //,[VisualAcuity_OS_VP],[ReflexesPupillaries_OD_MM],[ReflexesPupillaries_OD_LG],[ReflexesPupillaries_OD_MG]
    //,[ReflexesPupillaries_OS_MM],[ReflexesPupillaries_OS_LG],[ReflexesPupillaries_OS_MG],[ReflexesPupillaries_Notes]
    //,[TonometriesGoldmann_OD],[TonometriesGoldmann_OS],[TonometriesGoldmann_OD_H],[TonometriesGoldmann_OS_H]
    //,[AgentsDiagnostic],[AnteriorTears],[AnteriorEyelids],[AnteriorConjunctiva],[Anteriorcornea],[AnteriorIt]
    //,[AnteriorIris],[AnteriorCrystalline],[VisualFields],[VisualTarget],[PosteriorEnvironments],[PosteriorPapilla]
    //,[PosteriorEp],[PosteriorPvs],[PosteriorAv],[PosteriorMacula],[PeripheryOD],[PeripheryOS],[TemplateType]
    //,[MasterId]  FROM [dbo].[tbl_EmergencyExamtemplates]

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_EmergencyExamtemplates] where EmergencyExamtemplatesId='" + Editid + "'");
        Txt_Examno.Text = dt.Rows[0]["EmergencyExamtemplatesId"].ToString();
        DDL_Patient.SelectedValue = dt.Rows[0]["PatientId"].ToString();      
        Txt_Date.Text = Convert.ToDateTime(dt.Rows[0]["ExamDate"]).ToString("dd-MM-yyyy");       
        txt_CaseHistory.Text = dt.Rows[0]["CaseHistory"].ToString();
        txt_OD_Avlwithrx.Text = dt.Rows[0]["VisualAcuity_OD_AvlwithRx"].ToString();
        txt_OD_Avlwithvl.Text = dt.Rows[0]["VisualAcuity_OD_AvlwithVl"].ToString();
        txt_OD_Ts.Text = dt.Rows[0]["VisualAcuity_OD_TS"].ToString();
        txt_OD_VP.Text = dt.Rows[0]["VisualAcuity_OD_VP"].ToString();
        txt_OS_Avlwithrx.Text = dt.Rows[0]["VisualAcuity_OS_AvlwithRx"].ToString();
        txt_OS_Avlwithvl.Text = dt.Rows[0]["VisualAcuity_OS_AvlwithVl"].ToString();
        txt_OS_Ts.Text = dt.Rows[0]["VisualAcuity_OS_TS"].ToString();
        txt_OS_VP.Text = dt.Rows[0]["VisualAcuity_OS_VP"].ToString();
        Txt_OD_MM.Text = dt.Rows[0]["ReflexesPupillaries_OD_MM"].ToString();
        txt_OD_LG.Text = dt.Rows[0]["ReflexesPupillaries_OD_LG"].ToString();
        txt_OD_MG.Text = dt.Rows[0]["ReflexesPupillaries_OD_MG"].ToString();
        Txt_OS_MM.Text = dt.Rows[0]["ReflexesPupillaries_OS_MM"].ToString();
        txt_OS_LG.Text = dt.Rows[0]["ReflexesPupillaries_OS_LG"].ToString();
        txt_OS_MG.Text = dt.Rows[0]["ReflexesPupillaries_OS_MG"].ToString();
        txt_Notes.Text = dt.Rows[0]["ReflexesPupillaries_Notes"].ToString();
        txt_OD_Tonometries.Text = dt.Rows[0]["TonometriesGoldmann_OD"].ToString();
        txt_OS_Tonometries.Text = dt.Rows[0]["TonometriesGoldmann_OS"].ToString();
        txt_OD_h.Text = dt.Rows[0]["TonometriesGoldmann_OD_H"].ToString();
        txt_OS_h.Text = dt.Rows[0]["TonometriesGoldmann_OS_H"].ToString();
        txt_Tears.Text = dt.Rows[0]["AnteriorTears"].ToString();
        txt_Eyelids.Text = dt.Rows[0]["AnteriorEyelids"].ToString();
        txt_Conjunctiva.Text = dt.Rows[0]["AnteriorConjunctiva"].ToString();
        txt_cornea.Text = dt.Rows[0]["Anteriorcornea"].ToString();
        txt_It.Text = dt.Rows[0]["AnteriorIt"].ToString();
        txt_Iris.Text = dt.Rows[0]["AnteriorIris"].ToString();
        txt_Crystalline.Text = dt.Rows[0]["AnteriorCrystalline"].ToString();
        txt_Target.Text = dt.Rows[0]["VisualTarget"].ToString();
        txt_Environments.Text = dt.Rows[0]["PosteriorEnvironments"].ToString();
        txt_Papilla.Text = dt.Rows[0]["PosteriorPapilla"].ToString();
        txt_Ep.Text = dt.Rows[0]["PosteriorEp"].ToString();
        txt_Pvs.Text = dt.Rows[0]["PosteriorPvs"].ToString();
        txt_Av.Text = dt.Rows[0]["PosteriorAv"].ToString();
        txt_Macula.Text = dt.Rows[0]["PosteriorMacula"].ToString();

        string AgentsDiagnostic = dt.Rows[0]["AgentsDiagnostic"].ToString();
        if (AgentsDiagnostic.Contains("Chk_Proparacaine"))
        {
            Chk_Proparacaine.Checked = true;
        }
        if (AgentsDiagnostic.Contains("Chk_Tropicamide"))
        {
            Chk_Tropicamide.Checked = true;
        }
        if (AgentsDiagnostic.Contains("Chk_Benoxinate"))
        {
            Chk_Benoxinate.Checked = true;
        }
        if (AgentsDiagnostic.Contains("Chk_Cyclogyl"))
        {
            Chk_Cyclogyl.Checked = true;
        }
        if (AgentsDiagnostic.Contains("Chk_Phenylephrine"))
        {
            Chk_Phenylephrine.Checked = true;
        }

        string Visualfields = dt.Rows[0]["Visualfields"].ToString();
        if (Visualfields.Contains("chk_Electro"))
        {
            chk_Electro.Checked = true; 
        }
        if (Visualfields.Contains("chk_ET"))
        {
            chk_ET.Checked = true;
        }
        if (Visualfields.Contains("chk_confront"))
        {
            chk_confront.Checked = true;
        }

        string PeripheryOD = dt.Rows[0]["PeripheryOD"].ToString();
        if (PeripheryOD.Contains("chk_OdNoholes"))
        {
            chk_OdNoholes.Checked = true;
        }
        if (PeripheryOD.Contains("chk_OdNottearing"))
        {
            chk_OdNottearing.Checked = true;
        }
        if (PeripheryOD.Contains("chk_OdNodetachment"))
        {
            chk_OdNodetachment.Checked = true;
        }

        string PeripheryOS = dt.Rows[0]["PeripheryOS"].ToString();
        if (PeripheryOS.Contains("chk_OsNoholes"))
        {
            chk_OsNoholes.Checked = true;
        }
        if (PeripheryOS.Contains("chk_OsNottearing"))
        {
            chk_OsNottearing.Checked = true;
        }
        if (PeripheryOS.Contains("chk_OsNodetachment"))
        {
            chk_OsNodetachment.Checked = true;
        }

    }

    
}