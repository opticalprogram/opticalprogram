﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site : System.Web.UI.MasterPage
{
    public string heading = string.Empty;


    Dal objdal;
    protected void Page_Init(object sender, EventArgs e)
    {
        // Display error if any
        //if (Session["lblError"]!=null)
        //{
        //    lblError.Visible = true;
        //    lblError.Text = Session["lblError"].ToString();
        //    Session.Remove("lblError");
        //}

        objdal = new Dal();
        // if (IsPostBack)
        {
            // Check other user's permission
            if (Page.User.Identity.IsAuthenticated)
            {
                if (Page.User.IsInRole("Master") || Page.User.IsInRole("Admin"))
                {
                    if (Session["MasterId"] == null)
                    {
                        Session["MasterId"] = Membership.GetUser().ProviderUserKey.ToString();
                    }
                }
                else if (Page.User.IsInRole("Other"))
                {
                    if (Session["MasterId"] == null)
                    {
                        string masterId = objdal.Get_SingleValue("SELECT MasterId FROM tbl_MasterChildAccount where tbl_MasterChildAccount.UserId='" + Membership.GetUser().ProviderUserKey.ToString() + "'");
                        if (Membership.GetUser(Guid.Parse(masterId)).IsApproved)
                        {
                            Session["MasterId"] = masterId;
                        }
                    }
                    checkPermission();

                }

            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["MasterId"] != null)
        {
            setTopAccessPatient();
        }        
    }

    // set top five patient in side bar
    protected void setTopAccessPatient()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select top 5 PatientId,FirstName+' '+LastName from tbl_Patient where MasterId='" + Session["MasterId"].ToString() + "' order by LastAccess desc");

        if (dt.Rows.Count >= 1)
        {
            Patient1.InnerText = dt.Rows[0][1].ToString();
            Patient1.HRef = "patient.aspx?id=" + dt.Rows[0][0].ToString();
        }
        if (dt.Rows.Count >= 2)
        {
            Patient2.InnerText = dt.Rows[1][1].ToString();
            Patient2.HRef = "patient.aspx?id=" + dt.Rows[1][0].ToString();
        }
        if (dt.Rows.Count >= 3)
        {
            Patient3.InnerText = dt.Rows[2][1].ToString();
            Patient3.HRef = "patient.aspx?id=" + dt.Rows[2][0].ToString();
        }
        if (dt.Rows.Count >= 4)
        {
            Patient4.InnerText = dt.Rows[3][1].ToString();
            Patient4.HRef = "patient.aspx?id=" + dt.Rows[3][0].ToString();
        }
        if (dt.Rows.Count >= 5)
        {
            Patient5.InnerText = dt.Rows[4][1].ToString();
            Patient5.HRef = "patient.aspx?id=" + dt.Rows[4][0].ToString();
        }
    }

    protected void checkPermission()
    {
        string pageName = this.ContentPlaceHolder1.Page.GetType().FullName.Replace("ASP.", "").Replace("_", ".");  //Get Content Pagename

        if (pageName != "home.aspx")
        {
            string userName = Membership.GetUser().UserName;

            switch (pageName)
            {
                case "patient.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionPatientAdd = 1") == "0")
                    {
                        RedirectToHome("Patient");
                    }
                    break;

                case "managepatient.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionPatientAdd = 1") == "0")
                    {
                        RedirectToHome("Patient");
                    }
                    break;


                case "agenda.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionAgendaAdd = 1") == "0")
                    {
                        RedirectToHome("Agenda");
                    }
                    break;

                case "inventory.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissioninventoryAdd = 1") == "0")
                    {
                        RedirectToHome("Inventory");
                    }
                    break;
                case "manageinventory.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissioninventoryAdd = 1") == "0")
                    {
                        RedirectToHome("Inventory");
                    }
                    break;
                case "managesupplier.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissioninventoryAdd = 1") == "0")
                    {
                        RedirectToHome("Inventory");
                    }
                    break;
                case "supplier.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissioninventoryAdd = 1") == "0")
                    {
                        RedirectToHome("Inventory");
                    }
                    break;
                case "invoice.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionInvoiceAdd = 1") == "0")
                    {
                        RedirectToHome("invoice");
                    }
                    break;
                case "manageinvoice.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionInvoiceAdd = 1") == "0")
                    {
                        RedirectToHome("invoice");
                    }
                    break;
                case "reports.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionStatistics = 1") == "0")
                    {
                        RedirectToHome("reports");
                    }
                    break;
                case "examtemplate.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionExamTemplateAdd = 1") == "0")
                    {
                        RedirectToHome("examtemplate");
                    }
                    break;
                case "general.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionExamTemplateAdd = 1") == "0")
                    {
                        RedirectToHome("examtemplate");
                    }
                    break;
                case "emergency.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionExamTemplateAdd = 1") == "0")
                    {
                        RedirectToHome("examtemplate");
                    }
                    break;
                case "dilation.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionExamTemplateAdd = 1") == "0")
                    {
                        RedirectToHome("examtemplate");
                    }
                    break;
                case "prescription.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and [PermissionPrescriptionAdd] = 1") == "0")
                    {
                        RedirectToHome("prescription");
                    }
                    break;
                case "contactlens.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and [PermissionPrescriptionAdd] = 1") == "0")
                    {
                        RedirectToHome("prescription");
                    }
                    break;
                case "glass.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and [PermissionPrescriptionAdd] = 1") == "0")
                    {
                        RedirectToHome("prescription");
                    }
                    break;
                case "printcontanctlens.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and [PermissionPrescriptionAdd] = 1") == "0")
                    {
                        RedirectToHome("prescription");
                    }
                    break;
                case "printglass.aspx": if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and [PermissionPrescriptionAdd] = 1") == "0")
                    {
                        RedirectToHome("prescription");
                    }
                    break;

                default: break;
            }
        }
    }

    public void RedirectToHome(string module)
    {
        Session["lblError"] = "Access denied. You are not permitted to access " + module + " module(s)!";
        Response.Redirect("~/home.aspx", true);
    }
    protected void LoginStatus_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Session.Abandon();
    }
}
