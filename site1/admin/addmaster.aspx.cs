﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_addmaster : System.Web.UI.Page
{
    Dal objdal;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Admin";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;

        objdal = new Dal();
    }
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        MembershipUser usr = Membership.GetUser(CreateUserWizard1.UserName);
        usr.Comment = ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("TxtCompany")).Text
                       + "^default.jpg^"
                       + ((TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("tbTotalLogins")).Text
                       + "^10:00^18:00^60";
        if (((CheckBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Chk_Active")).Checked)
        {
            usr.IsApproved = true;
        }
        else
        {
            usr.IsApproved = false;
        }

        Membership.UpdateUser(usr);
        Roles.AddUserToRoles(CreateUserWizard1.UserName, new string[] { "Master", "Other" });

        Response.Redirect("~/admin/addmaster.aspx", true);
    }
   
    //protected void GVMasterUser_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{       
    //    MembershipUser user = Membership.GetUser(GVMasterUser.DataKeys[e.RowIndex].Value.ToString());
    //    GridViewRow row = GVMasterUser.Rows[e.RowIndex];
    //    if (((CheckBox)row.Cells[4].Controls[0]).Checked)
    //    {
    //        user.IsApproved = true;
    //    }
    //    else
    //    {
    //        user.IsApproved = false;
    //    }
        
    //    Membership.UpdateUser(user);
    //}

    
   
    protected void GVMasterUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //User id 
        Label DeleteId = (Label)GVMasterUser.Rows[e.RowIndex].FindControl("lbl_UserId");
        Guid userIdToDelete = new Guid(DeleteId.Text);
        //Delete User with its id
        Membership.DeleteUser(Membership.GetUser(userIdToDelete).UserName, true);
        //Membership.DeleteUser(GVMasterUser.DataKeys[e.RowIndex].Value.ToString());
    }

    protected void GVMasterUser_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            GVMasterUser.SelectedIndex = e.NewSelectedIndex;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            string UserName = GVMasterUser.SelectedValue.ToString();
            DataTable dt = new DataTable();
            dt = objdal.GET_DATATABLE("SELECT aspnet_Membership.Comment, aspnet_Membership.IsApproved FROM aspnet_Membership INNER JOIN "
                                   + "aspnet_Users ON aspnet_Membership.UserId = aspnet_Users.UserId where aspnet_Users.UserName='" + UserName + "'");
            HF_Comment.Value = dt.Rows[0]["Comment"].ToString();
            Txt_UpdateUserName.Text = UserName;
            Txt_UpdateTotalLogins.Text = dt.Rows[0]["Comment"].ToString().Split( new char[] {'^'})[2];
            if (dt.Rows[0]["IsApproved"].ToString() == "True")
            {
                Chk_UpdateActive.Checked = true;
            }
            else
            {
                Chk_UpdateActive.Checked = false;
            }
        }
        catch (Exception ex)
        {
            
        }  
    }
    protected void Btn_UpdateMaster_Click(object sender, EventArgs e)
    {
        MembershipUser user = Membership.GetUser(Txt_UpdateUserName.Text);
        string[] commentArray = HF_Comment.Value.Split('^');
        string finalcomment=string.Empty;
        for (int i = 0; i < commentArray.Length; i++)
        {
            if (i==2)
            {
                finalcomment += Txt_UpdateTotalLogins.Text + "^";
            }
            else
            {
                finalcomment += commentArray[i] + "^";
            }
        }
        user.Comment = finalcomment.Remove(finalcomment.Length-1);

        if (Chk_UpdateActive.Checked)
        {
            user.IsApproved = true;
        }
        else
        {
            user.IsApproved = false;
        }

        Membership.UpdateUser(user);       
        Response.Redirect("~/admin/addmaster.aspx", true);
    }
}