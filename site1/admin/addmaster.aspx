﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="addmaster.aspx.cs" Inherits="admin_addmaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        td, th {
            padding: 2px 10px;
        }
    </style>
    <script type="text/javascript">
        function openModal() {
            $('#UpdateUserModal').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <span class="glyphicon glyphicon-user btn btn-sm btn-default LinkButton" data-toggle="modal" style="cursor: pointer;" data-target="#CreateMasterModal"><span style="font-family: Arial;">&nbsp;Create master</span></span>
        </li>
    </ul>

    <div class="table-responsive col-lg-12" style="padding-left: 0px !important; margin-top: 10px;">
        <asp:GridView ID="GVMasterUser" runat="server" DataKeyNames="UserName" GridLines="None" Width="800px" CssClass="table table-hover table-striped" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EmptyDataText="No data found!" DataSourceID="SDSMasterUser" OnRowDeleting="GVMasterUser_RowDeleting" OnSelectedIndexChanging="GVMasterUser_SelectedIndexChanging">
            <Columns>
                 <asp:TemplateField HeaderText="UserId" SortExpression="UserId" Visible="False">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Bind("UserId") %>' ID="lbl_UserId"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserName" HeaderText="Name" SortExpression="UserName"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"></asp:BoundField>
                <asp:BoundField DataField="CreateDate" HeaderText="Create Date" SortExpression="CreateDate" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="LastActivityDate" HeaderText="Last Activity Date" DataFormatString="{0:dd-MM-yyyy}" SortExpression="LastActivityDate"></asp:BoundField>
                <asp:TemplateField HeaderText="Total logins" SortExpression="Comment">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text=<%# Eval("Comment").ToString().Split(new char[] { '^' })[2] %>></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CheckBoxField DataField="IsActive" HeaderText="Active" SortExpression="IsActive"></asp:CheckBoxField>
                <asp:CommandField ShowSelectButton="true" SelectText="Edit" ControlStyle-CssClass="btn-success btn btn-xs"></asp:CommandField>
                <asp:CommandField ShowDeleteButton="True" ControlStyle-CssClass="btn-danger btn btn-xs" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource runat="server" ID="SDSMasterUser" ConnectionString='<%$ ConnectionStrings:CN-OpticalProgram %>'
            DeleteCommand="DELETE FROM aspnet_Users WHERE 1=2"
            SelectCommand="SELECT aspnet_Users.UserId, aspnet_Users.UserName, aspnet_Membership.Email, aspnet_Membership.IsApproved AS IsActive, aspnet_Membership.Comment,aspnet_Membership.CreateDate, aspnet_Users.LastActivityDate FROM aspnet_Users INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId WHERE (aspnet_Users.UserId <> 'a249106c-3e97-4395-b867-33e7716f7a3e') AND (aspnet_Users.UserId IN (SELECT UserId FROM aspnet_UsersInRoles WHERE (RoleId IN (SELECT RoleId FROM aspnet_Roles WHERE (RoleName = 'master')))))"></asp:SqlDataSource>
    </div>

    <!-- Modal CreateUser -->
    <div id="ModalCreateMaster" runat="server">
        <div class="modal fade modal-open" id="CreateMasterModal" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal Exam type content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create new master</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" CompleteSuccessText="Master account has been successfully created." ContinueDestinationPageUrl="~/home.aspx" LoginCreatedUser="False" OnCreatedUser="CreateUserWizard1_CreatedUser">
                                <CreateUserButtonStyle CssClass="btn btn-primary" />
                                <WizardSteps>
                                    <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="UserName" Width="200" runat="server"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" Display="Dynamic" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="Password" Width="200" runat="server" TextMode="Password"></asp:TextBox><br />
                                                         <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" Display="Dynamic" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="Regex2" runat="server" ControlToValidate="Password" Display="Dynamic"
                                                            ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$"
                                                            ErrorMessage="Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="ConfirmPassword" Width="200" runat="server" TextMode="Password"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" Display="Dynamic" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="Email" Width="200" runat="server"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="EmailRequired" runat="server" Display="Dynamic" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="LblCompany" runat="server" AssociatedControlID="TxtCompany">Company:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="TxtCompany" Width="200" runat="server"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" Display="Dynamic" ControlToValidate="TxtCompany" ErrorMessage="Company name is required." ToolTip="Company name is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblTotalLogins" runat="server" AssociatedControlID="Email">Total Logins:</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="tbTotalLogins" Width="100" Text="1" runat="server" MaxLength="1"></asp:TextBox><br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="tbTotalLogins" ErrorMessage="Total Logins is required." ToolTip="Total Logins is required." ValidationGroup="CreateUserWizard1"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="LblActive" runat="server" AssociatedControlID="Email">Active:</asp:Label>
                                                        &nbsp; 
                                                        <asp:CheckBox ID="Chk_Active" Checked="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="LblMasterImage" runat="server" AssociatedControlID="TxtCompany">Image:</asp:Label>
                                                        <br />
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/default.jpg" Width="100%" />
                                                        <br />
                                                        <label style="font-weight: normal; color: gray;">This is a default image; can be changed by a Master.</label>
                                                    </td>
                                                </tr>

                                                <%--<tr>
                            <td align="left">
                                <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question">Security Question:</asp:Label>
                            <br />
                                <asp:TextBox ID="Question" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" ControlToValidate="Question" ErrorMessage="Security question is required." ToolTip="Security question is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer:</asp:Label>
                            <br />
                                <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer" ErrorMessage="Security answer is required." ToolTip="Security answer is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2" style="color: Red;">
                                                        <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:CreateUserWizardStep>
                                    <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                                    </asp:CompleteWizardStep>
                                </WizardSteps>
                            </asp:CreateUserWizard>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--<asp:Button ID="Button_Examtype" runat="server" CssClass="btn btn-default" ValidationGroup="Button_Examtype_Group" Text="Save" OnClientClick="addexamtype();" />--%>
                        <%--<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal Update User -->

    <div id="ModalUpdateUser" runat="server">
        <div class="modal fade modal-open" id="UpdateUserModal" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal Exam type content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Master</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <label for="Txt_UpdateUserName">User Name:</label>
                            <asp:TextBox ID="Txt_UpdateUserName" Enabled="false" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div>
                            <label class="labelspace" for="Txt_UpdateTotalLogins">Total Logins:</label>
                            <asp:TextBox ID="Txt_UpdateTotalLogins" Width="100" Text="1" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_UpdateTotalLogins" Display="Dynamic" runat="server" ControlToValidate="Txt_UpdateTotalLogins" ErrorMessage="Total Logins is required!"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <label class="labelspace" for="Txt_UpdateActive">Active:</label>
                            <asp:CheckBox ID="Chk_UpdateActive" Checked="true" runat="server" />
                            <asp:HiddenField ID="HF_Comment" runat="server" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Btn_UpdateMaster" CssClass="btn btn-default" runat="server" Text="Save" OnClick="Btn_UpdateMaster_Click" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

