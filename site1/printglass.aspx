﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printglass.aspx.cs" EnableEventValidation="false" Inherits="printglass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Print</title>
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png"/>

     <!-- jQuery -->
    <script src="<%= ResolveUrl("~/js/jquery-2.1.4.min.js") %>"></script>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="<%= ResolveUrl("~/js/SomeeAdsRemover.js") %>"></script>
    <script src="js/sketch.min.js"></script>
    <script src="js/printprescription.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<%= ResolveUrl("~/js/bootstrap.min.js") %>"></script>  
    <link href="css/print.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
          <div class="container">
        <h4>Glass prescription</h4>
             <div class="table-responsive">     
            <table border="1" class="table">
                <tr>
                    <td>Prescription no</td>
                    <td>Date</td>
                     <td>Professional</td>
                     <td>Patient</td>
                     <td>Exam</td>
                </tr>
                <tr>
                     <td><asp:Label ID="lbl_PrescriptionNo" runat="server"></asp:Label></td>                  
                    <td><asp:Label ID="lbl_Date" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Professional" runat="server"></asp:Label></td>                   
                    <td><asp:Label ID="lbl_Patient" runat="server"></asp:Label></td>
                     <td><asp:Label ID="lbl_Exam" runat="server"></asp:Label></td>
                </tr>           
            </table>
            </div>
            <h4>Rx</h4>
            <div class="table-responsive">     
            <table border="1" class="table">
                <tr>
                    <td>&nbsp;</td>
                    <td align="center">Sphere</td>
                    <td align="center">Cylinder</td>
                    <td align="center">Axis</td>
                    <td align="center">Addition</td>
                    <td align="center">Segh</td>
                    <td align="center">Bc</td>
                </tr>
                <tr>
                    <td>&nbsp;OD:</td>
                    <td><asp:Label ID="lbl_Sphere_OD" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Cylinder_OD" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Axis_OD" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Addition_OD" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Segh_OD" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Bc_OD" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;OS:</td>
                    <td><asp:Label ID="lbl_Sphere_OS" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Cylinder_OS" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Axis_OS" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Addition_OS" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Segh_OS" runat="server"></asp:Label></td>
                    <td><asp:Label ID="lbl_Bc_OS" runat="server"></asp:Label></td>
                </tr>
            </table>
               </div>     
         <h4>P.D.</h4>
            <div class="table-responsive">     
            <table border="1" class="table">      
            <tr>
                <td>&nbsp;</td>
                <td align="center">Far</td>
                <td align="center">Near</td>
                <td align="center">Height</td>
                <td align="center">Vertex</td>
                <td align="center">Wrap angle</td>
                <td align="center">Pantoscopic angle</td>
            </tr>
            <tr>
                <td>&nbsp; &nbsp;<label>OD:</label></td>
                <td><asp:Label ID="lbl_PDFar_OD" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDNear_OD" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDHeight_OD" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDVertex_OD" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDWrapangle_OD" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDPantoscopicangle_OD" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp; &nbsp;<label>OS:</label></td>
                <td><asp:Label ID="lbl_PDFar_OS" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDNear_OS" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDHeight_OS" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDVertex_OS" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDWrapangle_OS" runat="server"></asp:Label></td>
                <td><asp:Label ID="lbl_PDPantoscopicangle_OS" runat="server"></asp:Label></td>
            </tr>            
        </table>
                  </div>  
              <h4>Signature</h4>
                 <canvas id="colors_sketch" width="300" height="100" style="border: 1px solid;"></canvas>
                <br /> <asp:Label ID="lbl_Professional2" runat="server"></asp:Label>                
                <br />
                <asp:Button ID="Button_Print" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="Print" OnClick="Button_Print_Click" />
               <input type="hidden" id="array_store" name="ArrayStore" value='<%=this.ArrayStore %>' />
              </div>
    </form>
</body>
</html>
