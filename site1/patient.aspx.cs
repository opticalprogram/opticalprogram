﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Text;
using System.Web.Security;

public partial class patient : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        string title = "Patient";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        objdal = new Dal();

        SetFileNo();
        //TextBox_OpeningDate.Text = DateTime.Now.ToString("d-M-yyyy");

        if (!IsPostBack)
        {
            //Label_Company.Text = Membership.GetUser().Comment.Split(new char[] { '^' })[0];
            Label_Company.Text = objdal.Get_SingleValue("select aspnet_Membership.Comment from aspnet_Membership where aspnet_Membership.UserId='" + MasterId + "'").Split(new char[] { '^' })[0];

            ProvinceFill();
            TitleFill();
            CityFill();
            OccupationFill();
            LanguageFill();

            if (Request.QueryString["id"] != null)
            {
                Button_Submit.Text = "Update";
                PageHeading.InnerHtml = "<span class='glyphicon glyphicon-user'></span> Edit Patient</a>";

                DivInvoice.Style.Add("display", "block");
                DivExams.Style.Add("display", "block");
                DivPrescription.Style.Add("display", "block");              
                //Hl_Prescription.NavigateUrl = "~/prescription.aspx?id=" + Request.QueryString["id"].ToString() + "";
                Hl_Manageinvoice.HRef = "~/manageinvoice.aspx?id=" + Request.QueryString["id"].ToString() + "";
                Hl_Invoice.HRef = "~/invoice.aspx?pid=" + Request.QueryString["id"].ToString() + "";
                Hl_ManageExamtemplate.HRef = "~/examtemplate.aspx?id=" + Request.QueryString["id"].ToString() + "";
                Hl_AddGeneral.HRef = "~/general.aspx?pid=" + Request.QueryString["id"].ToString() + "";
                Hl_AddEmergency.HRef = "~/emergency.aspx?pid=" + Request.QueryString["id"].ToString() + "";
                Hl_AddDilation.HRef = "~/dilation.aspx?pid=" + Request.QueryString["id"].ToString() + "";
                Hl_ManagePrescription.HRef = "~/prescription.aspx?id=" + Request.QueryString["id"].ToString() + "";
                Hl_AddGlass.HRef = "~/glass.aspx?pid=" + Request.QueryString["id"].ToString() + "";
                Hl_AddContact.HRef = "~/contactlens.aspx?pid=" + Request.QueryString["id"].ToString() + "";
                GetPatientData(Request.QueryString["id"].ToString());
            }
        }
    }

    //for city Title
    protected void TitleFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from [tbl_Title] where MasterId='" + MasterId + "' order by Title");
        ddl_Title.DataSource = dt;
        ddl_Title.DataValueField = "TitleId";
        ddl_Title.DataTextField = "Title";
        ddl_Title.DataBind();
    }

    //for city fill
    protected void CityFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from tbl_City where MasterId='" + MasterId + "' order by CityName");
        DropDownList_City.DataSource = dt;
        DropDownList_City.DataValueField = "CityId";
        DropDownList_City.DataTextField = "CityName";
        DropDownList_City.DataBind();
    }

    //for Province fill
    protected void ProvinceFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from tbl_Province where MasterId='" + MasterId + "' order by ProvinceName");
        DropDownList_Province.DataSource = dt;
        DropDownList_Province.DataValueField = "ProvinceId";
        DropDownList_Province.DataTextField = "ProvinceName";
        DropDownList_Province.DataBind();
    }

    //for Occupation fill

    protected void OccupationFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT * FROM [tbl_Occupation] where MasterId='" + MasterId + "' order by OccupationName");
        DropDownList_Occupation.DataSource = dt;
        DropDownList_Occupation.DataValueField = "OccupationId";
        DropDownList_Occupation.DataTextField = "OccupationName";
        DropDownList_Occupation.DataBind();
    }

    // For Fill Language
    protected void LanguageFill()
    {
        List<KeyValuePair<string, string>> languages = new List<KeyValuePair<string, string>>
            {              
            new KeyValuePair<string,string>("ar", "Arabic"),
            new KeyValuePair<string,string>("bs-Latn", "Bosnian (Latin)"),
            new KeyValuePair<string,string>("bg", "Bulgarian"),
            new KeyValuePair<string,string>("ca", "Catalan"),
            new KeyValuePair<string,string>("zh-CHS", "Chinese Simplified"),
            new KeyValuePair<string,string>("zh-CHT", "Chinese Traditional"),
            new KeyValuePair<string,string>("hr", "Croatian"),
            new KeyValuePair<string,string>("cs", "Czech"),
            new KeyValuePair<string,string>("da", "Danish"),
            new KeyValuePair<string,string>("nl", "Dutch"),
            new KeyValuePair<string,string>("en", "English"),
            new KeyValuePair<string,string>("et", "Estonian"),
            new KeyValuePair<string,string>("fi", "Finnish"),
            new KeyValuePair<string,string>("fr", "French"),
            new KeyValuePair<string,string>("de", "German"),
            new KeyValuePair<string,string>("el", "Greek"),
            new KeyValuePair<string,string>("ht", "Haitian Creole"),
            new KeyValuePair<string,string>("he", "Hebrew"),
            new KeyValuePair<string,string>("hi", "Hindi"),
            new KeyValuePair<string,string>("mww", "Hmong Daw"),
            new KeyValuePair<string,string>("hu", "Hungarian"),
            new KeyValuePair<string,string>("id", "Indonesian"),
            new KeyValuePair<string,string>("it", "Italian"),
            new KeyValuePair<string,string>("ja", "Japanese"),
            new KeyValuePair<string,string>("tlh", "Klingon"),
            new KeyValuePair<string,string>("tlh-Qaak", "Klingon (pIqaD)"),
            new KeyValuePair<string,string>("ko", "Korean"),
            new KeyValuePair<string,string>("lv", "Latvian"),
            new KeyValuePair<string,string>("lt", "Lithuanian"),
            new KeyValuePair<string,string>("ms", "Malay"),
            new KeyValuePair<string,string>("mt", "Maltese"),
            new KeyValuePair<string,string>("no", "Norwegian"),
            new KeyValuePair<string,string>("fa", "Persian"),
            new KeyValuePair<string,string>("pl", "Polish"),
            new KeyValuePair<string,string>("pt", "Portuguese"),
            new KeyValuePair<string,string>("otq", "Querétaro Otomi"),
            new KeyValuePair<string,string>("ro", "Romanian"),
            new KeyValuePair<string,string>("ru", "Russian"),
            new KeyValuePair<string,string>("sr-Cyrl", "Serbian (Cyrillic)"),
            new KeyValuePair<string,string>("sr-Latn", "Serbian (Latin)"),
            new KeyValuePair<string,string>("sk", "Slovak"),
            new KeyValuePair<string,string>("sl", "Slovenian"),
            new KeyValuePair<string,string>("es", "Spanish"),
            new KeyValuePair<string,string>("sv", "Swedish"),
            new KeyValuePair<string,string>("th", "Thai"),
            new KeyValuePair<string,string>("tr", "Turkish"),
            new KeyValuePair<string,string>("uk", "Ukrainian"),
            new KeyValuePair<string,string>("ur", "Urdu"),
            new KeyValuePair<string,string>("vi", "Vietnamese"),
            new KeyValuePair<string,string>("cy", "Welsh"),
            new KeyValuePair<string,string>("yua", "Yucatec Maya")
            };

        DropDownList_spokenlanguage.DataValueField = "key";
        DropDownList_spokenlanguage.DataTextField = "value";


        DropDownList_spokenlanguage.DataSource = languages;
        DropDownList_spokenlanguage.SelectedValue = "en";
        DropDownList_spokenlanguage.DataBind();
    }

    protected void SetFileNo()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select isnull(max(fileno),0)+1 as fileno from tbl_Patient");
        TextBox_FileNo.Text = dt.Rows[0]["fileno"].ToString();
    }

    //Submit Form
    protected void Button_Submit_Click(object sender, EventArgs e)
    {
        try
        {
            // For insert patient
            StringBuilder sb = new StringBuilder();


            if (Request.QueryString["id"] != null)
            {
                /* UPDATE [tbl_Patient]
                   SET [FileNo] = '',[OpeningDate] ='',[Title] = '',[FirstName] = '',[LastName] = '',[DateOfBirth] = '',[Gender] = '',[Medicare] = ''
                  ,[SpokenLanguage] = '',[PrimaryContact] = '',[Address] = '',[City] = '',[PostalCode] = '',[Province] = '',[Country] = '',[HomePhone] =''
                  ,[WorkPhone] = '',[Extension] = '',[CellPhone] = '',[Email] = '',[Preference] = '',[Optometrist] = '',[Ophthalmologist] = '',[Optician] = ''
                 ,[Assistant] = '',[Occupation] = '',[OtherDetail] = '',[DoNotRecall] = '',[ThirdPartyBalance] = '',[PatientBalance] = '',[Insurer] = '',[PlanNumber] = ''
                 WHERE [PatientId] = 'd66e995f-e8bf-45b0-bdb8-0d4ecee67f30'; */               

                sb.Append("UPDATE [tbl_Patient] SET [OpeningDate] ='" + DateTime.ParseExact(TextBox_OpeningDate.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",[TitleId] = '" + ddl_Title.SelectedValue + "',[FirstName] = '" + TextBox_FirstName.Text.Replace("'", "''") + "'");
                sb.Append(",[LastName] = '" + TextBox_LastName.Text.Replace("'", "''") + "'");
                sb.Append(",[DateOfBirth] = '" + DateTime.ParseExact(TextBox_DateOfBirth.Text, "dd-MM-yyyy", null) + "',[Gender] = '" + DropDownList_Gender.SelectedValue + "'");
                sb.Append(",[Medicare] = '" + TextBox_Medicare.Text + "',[SpokenLanguage] = '" + DropDownList_spokenlanguage.SelectedValue + "'");
                sb.Append(",[PrimaryContact] = '" + TextBox_PrimaryContact.Text + "',[Address] = '" + TextBox_Address.Text + "',[CityId] = '" + DropDownList_City.SelectedValue + "'");
                sb.Append(",[PostalCode] = '" + TextBox_PostalCode.Text + "',[ProvinceId] ='" + DropDownList_Province.SelectedValue + "',[Country] = '" + DropDownList_Country.SelectedValue + "'");
                sb.Append(",[HomePhone] ='" + TextBox_HomePhone.Text + "',[WorkPhone] = '" + TextBox_WorkPhone.Text + "',[Extension] = '" + TextBox_Extension.Text + "'");
                sb.Append(",[CellPhone] = '" + TextBox_CellPhone.Text + "',[Email] = '" + TextBox_Email.Text + "',[Preference] = '" + DropDownList_Preference.SelectedValue + "'");
                sb.Append(",[Optometrist] = '" + TextBox_Optometrist.Text + "',[Ophthalmologist] = '" + TextBox_Ophthalmologist.Text + "',[Optician] = '" + TextBox_Optician.Text + "'");
                sb.Append(",[Assistant] = '" + TextBox_Assistant.Text + "',[OccupationId] = '" + DropDownList_Occupation.SelectedValue + "',[OtherDetail] = '" + TextBox_OtherDetail.Text + "'");
                sb.Append(",[DoNotRecall] = '" + TextBox_DoNotRecall.Text + "',[ThirdPartyBalance] = " + TextBox_ThirdPartyBalance.Text + "");
                sb.Append(",[PatientBalance] = " + TextBox_PatientBalance.Text + ",[Insurer] = '" + TextBox_Insurer.Text + "'");
                sb.Append(",[PlanNumber] = '" + TextBox_PlanNumber.Text + "',[LastAccess]='" + DateTime.Now.ToString() + "'");
                sb.Append(",[MasterId]= '" + MasterId + "' WHERE [PatientId] = '" + Request.QueryString["id"].ToString() + "'");

                objdal.EXECUTE_DML(sb.ToString());
                Session["lblMessage"] = "Patient is updated successfully.";
            }
            else
            {
                /* 
            INSERT INTO [tbl_Patient]
              ([PatientId],[FileNo],[OpeningDate],[Title],[FirstName],[LastName],[DateOfBirth],[Gender],[Medicare]
              ,[SpokenLanguage],[PrimaryContact],[Address],[City],[PostalCode],[Province],[Country],[HomePhone]
              ,[WorkPhone],[Extension],[CellPhone],[Email],[Preference],[Optometrist],[Ophthalmologist],[Optician]
              ,[Assistant],[Occupation],[OtherDetail],[DoNotRecall],[ThirdPartyBalance],[PatientBalance],[Insurer],[PlanNumber])
               VALUES
              (NEWID(),'','','','','','','','','','','','','','','','','','','','','','','','','','','','',0.0,0.0,'','')

            */

                sb.Append("INSERT INTO [tbl_Patient]([PatientId],[FileNo],[OpeningDate],[TitleId],[FirstName],[LastName],[DateOfBirth],[Gender],[Medicare]");
                sb.Append(",[SpokenLanguage],[PrimaryContact],[Address],[CityId],[PostalCode],[ProvinceId],[Country],[HomePhone]");
                sb.Append(",[WorkPhone],[Extension],[CellPhone],[Email],[Preference],[Optometrist],[Ophthalmologist],[Optician]");
                sb.Append(",[Assistant],[OccupationId],[OtherDetail],[DoNotRecall],[ThirdPartyBalance],[PatientBalance],[Insurer],[PlanNumber],[LastAccess],[MasterId])");
                sb.Append(" VALUES(NEWID()," + TextBox_FileNo.Text + ",'" + DateTime.ParseExact(TextBox_OpeningDate.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",'" + ddl_Title.SelectedValue + "','" + TextBox_FirstName.Text.Replace("'", "''") + "','" + TextBox_LastName.Text.Replace("'", "''") + "'");
                sb.Append(",'" + DateTime.ParseExact(TextBox_DateOfBirth.Text, "dd-MM-yyyy", null) + "'");
                sb.Append(",'" + DropDownList_Gender.SelectedValue + "','" + TextBox_Medicare.Text + "','" + DropDownList_spokenlanguage.SelectedValue + "'");
                sb.Append(",'" + TextBox_PrimaryContact.Text + "','" + TextBox_Address.Text + "','" + DropDownList_City.SelectedValue + "','" + TextBox_PostalCode.Text + "'");
                sb.Append(",'" + DropDownList_Province.SelectedValue + "','" + DropDownList_Country.SelectedValue + "','" + TextBox_HomePhone.Text + "','" + TextBox_WorkPhone.Text + "'");
                sb.Append(",'" + TextBox_Extension.Text + "','" + TextBox_CellPhone.Text + "','" + TextBox_Email.Text + "','" + DropDownList_Preference.SelectedValue + "'");
                sb.Append(",'" + TextBox_Optometrist.Text + "','" + TextBox_Ophthalmologist.Text + "','" + TextBox_Optician.Text + "','" + TextBox_Assistant.Text + "'");
                sb.Append(",'" + DropDownList_Occupation.SelectedValue + "','" + TextBox_OtherDetail.Text + "','" + TextBox_DoNotRecall.Text + "'");
                sb.Append("," + TextBox_ThirdPartyBalance.Text + "," + TextBox_PatientBalance.Text + ",'" + TextBox_Insurer.Text + "','" + TextBox_PlanNumber.Text + "'");
                sb.Append(",'" + DateTime.Now.ToString() + "','" + MasterId + "')");

                objdal.EXECUTE_DML(sb.ToString());

                Session["lblMessage"] = "Patient is added successfully.";
            }

            Response.Redirect("managepatient.aspx", true);
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    

    //Get patient data 
    protected void GetPatientData(string id)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            sb.Append("select * from tbl_Patient where PatientId='" + id + "'");
            dt = objdal.GET_DATATABLE(sb.ToString());
            TextBox_FileNo.Text = dt.Rows[0]["FileNo"].ToString();
            TextBox_OpeningDate.Text = Convert.ToDateTime(dt.Rows[0]["OpeningDate"]).ToString("dd-MM-yyyy");
            ddl_Title.SelectedValue = dt.Rows[0]["TitleId"].ToString();
            TextBox_FirstName.Text = dt.Rows[0]["FirstName"].ToString();
            TextBox_LastName.Text = dt.Rows[0]["LastName"].ToString();
            TextBox_DateOfBirth.Text = Convert.ToDateTime(dt.Rows[0]["DateOfBirth"]).ToString("dd-MM-yyyy");
            ScriptManager.RegisterStartupScript(this, GetType(), "calculateAge", "calculateAge();", true);
            DropDownList_Gender.SelectedValue = dt.Rows[0]["Gender"].ToString();
            TextBox_Medicare.Text = dt.Rows[0]["Medicare"].ToString();
            DropDownList_spokenlanguage.SelectedValue = dt.Rows[0]["SpokenLanguage"].ToString();
            TextBox_PrimaryContact.Text = dt.Rows[0]["PrimaryContact"].ToString();
            TextBox_Address.Text = dt.Rows[0]["Address"].ToString();
            DropDownList_City.SelectedValue = dt.Rows[0]["CityId"].ToString();
            TextBox_PostalCode.Text = dt.Rows[0]["PostalCode"].ToString();
            DropDownList_Province.SelectedValue = dt.Rows[0]["ProvinceId"].ToString();
            DropDownList_Country.SelectedValue = dt.Rows[0]["Country"].ToString();
            TextBox_HomePhone.Text = dt.Rows[0]["HomePhone"].ToString();
            TextBox_WorkPhone.Text = dt.Rows[0]["WorkPhone"].ToString();
            TextBox_Extension.Text = dt.Rows[0]["Extension"].ToString();
            TextBox_CellPhone.Text = dt.Rows[0]["CellPhone"].ToString();
            TextBox_Email.Text = dt.Rows[0]["Email"].ToString();
            DropDownList_Preference.SelectedValue = dt.Rows[0]["Preference"].ToString();
            TextBox_Optometrist.Text = dt.Rows[0]["Optometrist"].ToString();
            TextBox_Ophthalmologist.Text = dt.Rows[0]["Ophthalmologist"].ToString();
            TextBox_Optician.Text = dt.Rows[0]["Optician"].ToString();
            TextBox_Assistant.Text = dt.Rows[0]["Assistant"].ToString();
            DropDownList_Occupation.SelectedValue = dt.Rows[0]["OccupationId"].ToString();
            TextBox_OtherDetail.Text = dt.Rows[0]["OtherDetail"].ToString();
            TextBox_DoNotRecall.Text = dt.Rows[0]["DoNotRecall"].ToString();
            TextBox_ThirdPartyBalance.Text = Convert.ToDouble(dt.Rows[0]["ThirdPartyBalance"]).ToString("n");
            TextBox_PatientBalance.Text = Convert.ToDouble(dt.Rows[0]["PatientBalance"]).ToString("n");
            TextBox_Insurer.Text = dt.Rows[0]["Insurer"].ToString();
            TextBox_PlanNumber.Text = dt.Rows[0]["PlanNumber"].ToString();
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;

        }

    }

    //TODO: bhadresh
    protected void Button_City_Click(object sender, EventArgs e)
    {
        try
        {
            ModalCity.Style.Add("display", "none");
            //INSERT INTO [tbl_City]([CityName])VALUES(CityName)            
            objdal.EXECUTE_DML("INSERT INTO [tbl_City]([CityName],[MasterId])VALUES('" + TextBox_City.Text + "','" + MasterId + "')");
            CityFill();          
            ModalCity.Style.Add("display", "inline");
         }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    //TODO: bhadresh
    protected void Button_Province_Click(object sender, EventArgs e)
    {
        try
        {
            ModalProvince.Style.Add("display", "none");
            //INSERT INTO [tbl_Province]([ProvinceName])VALUES(ProvinceName)            
            objdal.EXECUTE_DML("INSERT INTO [tbl_Province]([ProvinceName],[MasterId])VALUES('" + TextBox_Province.Text + "','" + MasterId + "')");
            ProvinceFill();
            ModalProvince.Style.Add("display", "inline");
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
    protected void Button_Occupation_Click(object sender, EventArgs e)
    {
        try
        {
            ModalOccupation.Style.Add("display", "none");
            //INSERT INTO [tbl_Occupation]([OccupationName],[MasterId])VALUES()
            objdal.EXECUTE_DML("INSERT INTO [tbl_Occupation]([OccupationName],[MasterId])VALUES('" + TextBox_Occupation.Text + "','" + MasterId + "')");
            OccupationFill();
            ModalOccupation.Style.Add("display", "inline");
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    protected void Button_Title_Click(object sender, EventArgs e)
    {
        try
        {
            ModalTitle.Style.Add("display", "none");
            //INSERT INTO [tbl_Occupation]([OccupationName],[MasterId])VALUES()
            objdal.EXECUTE_DML("INSERT INTO [dbo].[tbl_Title]([Title],[MasterId]) VALUES('" + TextBox_Title.Text + "','" + MasterId + "')");
            TitleFill();
            ModalTitle.Style.Add("display", "inline");
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
}