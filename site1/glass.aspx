﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="glass.aspx.cs" Inherits="glass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/ProjectHelper.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script>
        $(document).ready(function () {

            var id = getUrlVars()["id"];
            if (id == null) {
                $("#Txt_Date").val(currentdate());
                $("#Txt_Datecalled").val(currentdate());
                $("#Txt_Delivery").val(currentdate());
                $("#txt_Printedon_OD").val(currentdate());
                $("#txt_Printedon_OS").val(currentdate());
            }

        });
        //Masking       
        jQuery(function ($) {
            $("#Txt_Date").mask("99-99-9999");
            $("#Txt_Datecalled").mask("99-99-9999");
            $("#Txt_Delivery").mask("99-99-9999");
            $("#txt_Printedon_OD").mask("99-99-9999");
            $("#txt_Printedon_OS").mask("99-99-9999");
            $("#Txt_Axis_OD").mask("999");
            $("#Txt_Axis_OS").mask("999");
        });

        function CalTotal() {
            if ($("#txt_Price_OD").val() != "" && $("#txt_Price_OS").val() != "" && $("#Txt_Professionalfees").val() != "" && $("#txt_FramePrice").val() != "") {
                var Price_OD = parseFloat($("#txt_Price_OD").val());
                var Price_OS = parseFloat($("#txt_Price_OS").val());
                var Professionalfees = parseFloat($("#Txt_Professionalfees").val())
                var FramePrice = parseFloat($("#txt_FramePrice").val());
                $("#Txt_Total").val(Price_OD + Price_OS + Professionalfees + FramePrice);
            }
        }

        function CalFarTotal() {
            if ($("#txt_PDFar_OD").val() != "" && $("#txt_PDFar_OS").val() != "") {
                var Far_OD = parseFloat($("#txt_PDFar_OD").val());
                var Far_OS = parseFloat($("#txt_PDFar_OS").val());
                $("#txt_PDFar_Total").val(Far_OD + Far_OS);
            }
        }

        function CalNearTotal() {
            if ($("#txt_PDNear_OD").val() != "" && $("#txt_PDNear_OS").val() != "") {
                var Near_OD = parseFloat($("#txt_PDNear_OD").val());
                var Near_OS = parseFloat($("#txt_PDNear_OS").val());
                $("#txt_PDNear_Total").val(Near_OD + Near_OS);
            }
        }

        function AxisRange() {
            if ($("#Txt_Axis_OD").val() > 180) {
                $("#Txt_Axis_OD").val('180');
            }
            if ($("#Txt_Axis_OS").val() > 180) {
                $("#Txt_Axis_OS").val('180');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_Prescription" NavigateUrl="~/prescription.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Prescription</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_manageinventory" NavigateUrl="~/manageinventory.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Manage Inventory</asp:HyperLink></li>
        <li>
            <asp:HyperLink ID="HL_Supplier" NavigateUrl="~/managesupplier.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-user"></span>&nbsp;Manage Supplier</asp:HyperLink></li>
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Label_Company">Company:</label>
            <asp:Label ID="Label_Company" runat="server" CssClass="form-control" disabled="disabled"></asp:Label>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_PrescriptionNo">Prescription no:</label>
            <asp:TextBox ID="Txt_PrescriptionNo" ClientIDMode="Static" disabled="disabled" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Date">Date:</label>
            <asp:TextBox ID="Txt_Date" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="txt_Professional">Professional:</label>
            <asp:TextBox ID="txt_Professional" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <asp:UpdatePanel ID="UpdatePanelPatient" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                    <label for="DDL_Patient">Patient:</label>&nbsp; 
                  <asp:DropDownList ID="DDL_Patient" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDL_Patient_SelectedIndexChanged" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-4">
                    <label for="ddl_Exam">Exam:</label>
                    <asp:DropDownList ID="ddl_Exam" CssClass="form-control" OnSelectedIndexChanged="ddl_Exam_SelectedIndexChanged" runat="server">
                    </asp:DropDownList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="txt_LabOrderno_OD">Lab invoice no.:</label>
            <asp:TextBox ID="txt_LabOrderno_OD" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="txt_TrayNo_OD">Tray Number:</label>
            <asp:TextBox ID="txt_TrayNo_OD" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="table-responsive form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Prescription</h4>
        <asp:UpdatePanel ID="UpdatePanelPrescription" runat="server">
            <ContentTemplate>
                <table style="min-width: 400px;">
                    <tr>
                        <td colspan="9">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="center">Sphere</td>
                        <td align="center">Cylinder</td>
                        <td align="center">Axis</td>
                        <td>&nbsp;</td>
                        <td align="center">Addition</td>
                        <td align="center">Segh</td>
                        <td>&nbsp;</td>
                        <td align="center">Bc</td>
                    </tr>
                    <tr>
                        <td>
                            <label>&nbsp;OD:</label></td>
                        <td>
                            <asp:TextBox ID="Txt_Sphere_OD" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Cylinder_OD" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Axis_OD" ClientIDMode="Static" onkeyup="AxisRange();" oninput="AxisRange();" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_Addition_OD" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Segh_OD" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_Bc_OD" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>&nbsp;OS:</label></td>
                        <td>
                            <asp:TextBox ID="Txt_Sphere_OS" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Cylinder_OS" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Axis_OS" CssClass="form-control" onkeyup="AxisRange();" oninput="AxisRange();" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_Addition_OS" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="Txt_Segh_OS" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:TextBox ID="Txt_Bc_OS" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <div class="table-responsive form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <%-- <h4>&nbsp;&nbsp;P.D.</h4>--%>
        <table style="min-width: 400px;">
            <tr>
                <td colspan="7">&nbsp;&nbsp;<label>P.D.</label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">Far</td>
                <td align="center">Near</td>
                <td align="center">Height</td>
                <td align="center">Vertex</td>
                <td align="center">Wrap angle</td>
                <td align="center">Pantoscopic angle</td>
            </tr>
            <tr>
                <td>&nbsp; &nbsp;<label>OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_PDFar_OD" CssClass="form-control" ClientIDMode="Static" onkeyup="CalFarTotal();" oninput="CalFarTotal();" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDNear_OD" CssClass="form-control" ClientIDMode="Static" onkeyup="CalNearTotal();" oninput="CalNearTotal();" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDHeight_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDVertex_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDWrapangle_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDPantoscopicangle_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>&nbsp; &nbsp;<label>OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_PDFar_OS" CssClass="form-control" ClientIDMode="Static" onkeyup="CalFarTotal();" oninput="CalFarTotal();" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDNear_OS" CssClass="form-control" ClientIDMode="Static" onkeyup="CalNearTotal();" oninput="CalNearTotal();" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDHeight_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDVertex_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDWrapangle_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDPantoscopicangle_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>&nbsp; &nbsp;<label>Total:</label>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_PDFar_Total" CssClass="form-control" ClientIDMode="Static" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_PDNear_Total" CssClass="form-control" ClientIDMode="Static" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td colspan="4">&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Ophtalmic lens OD</h4>
        <asp:UpdatePanel ID="UpdatePanel_Product_OD" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Keyword_OD">Keyword:</label>&nbsp;<asp:LinkButton ID="LinkButton_Keyword_OD" runat="server" OnClick="LinkButton_Keyword_OD_Click"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                    <asp:TextBox ID="txt_Keyword_OD" MaxLength="10" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="ddl_Product_OD">Product:</label>
                    <asp:DropDownList ID="ddl_Product_OD" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddl_Product_OD_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <%--<div class="form-group col-lg-6 col-md-6 col-sm-6">
                    <label for="txt_Detail_OD">Detail:</label>
                    <asp:TextBox ID="txt_Detail_OD" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>--%>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Type_OD">Type:</label>
                    <asp:TextBox ID="txt_Type_OD" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Material_OD">Material:</label>
                    <asp:TextBox ID="txt_Material_OD" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Treatments_OD">Treatments:</label>
                    <asp:TextBox ID="txt_Treatments_OD" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Warranty_OD">Warranty:</label>
                    <asp:TextBox ID="txt_Warranty_OD" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Supplier_OD">Supplier:</label>
                    <asp:TextBox ID="txt_Supplier_OD" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Price_OD">Price:</label>
                    <asp:TextBox ID="txt_Price_OD" ClientIDMode="Static" Enabled="false" CssClass="form-control" onkeyup="CalTotal();" oninput="CalTotal();" Text="0.00" runat="server"></asp:TextBox>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <!--not used -->
            <label for="txt_Printedon_OD">Printed on:</label>
            <asp:TextBox ID="txt_Printedon_OD" CssClass="form-control" ClientIDMode="Static" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <!--not used -->
            <label for="txt_Orderno_OD">Order no.:</label>
            <asp:TextBox ID="txt_Orderno_OD" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>

    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Ophtalmic lens OS</h4>
        <asp:UpdatePanel ID="UpdatePanel_Product_OS" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Keyword_OS">Keyword:</label>&nbsp;<asp:LinkButton ID="LinkButton_Keyword_OS" runat="server" OnClick="LinkButton_Keyword_OS_Click"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                    <asp:TextBox ID="txt_Keyword_OS" MaxLength="10" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="ddl_Product_OS">Product:</label>
                    <asp:DropDownList ID="ddl_Product_OS" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddl_Product_OS_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Type_OS">Type:</label>
                    <asp:TextBox ID="txt_Type_OS" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Material_OS">Material:</label>
                    <asp:TextBox ID="txt_Material_OS" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Treatments_OS">Treatments:</label>
                    <asp:TextBox ID="txt_Treatments_OS" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Warranty_OS">Warranty:</label>
                    <asp:TextBox ID="txt_Warranty_OS" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Supplier_OS">Supplier:</label>
                    <asp:TextBox ID="txt_Supplier_OS" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                    <label for="txt_Price_OS">Price:</label>
                    <asp:TextBox ID="txt_Price_OS" ClientIDMode="Static" Enabled="false" CssClass="form-control" onkeyup="CalTotal();" oninput="CalTotal();" Text="0.00" runat="server"></asp:TextBox>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <!--not used -->
            <label for="txt_Printedon_OS">Printed on:</label>
            <asp:TextBox ID="txt_Printedon_OS" CssClass="form-control" ClientIDMode="Static" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <!--not used -->
            <label for="txt_Orderno_OS">Order no.:</label>
            <asp:TextBox ID="txt_Orderno_OS" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <!--not used -->
            <label for="txt_LabOrderno_OS">Lab Order no.:</label>
            <asp:TextBox ID="txt_LabOrderno_OS" CssClass="form-control" ClientIDMode="Static" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-4 col-md-4 col-sm-4" style="display: none;">
            <!--not used -->
            <label for="txt_TrayNo_OS">Tray Number:</label>
            <asp:TextBox ID="txt_TrayNo_OS" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div style="clear: both;"></div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Frame</h4>
        <asp:UpdatePanel ID="UpdatePanel_frame" runat="server">
            <ContentTemplate>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_Keyword_frame">Keyword:</label>&nbsp;<asp:LinkButton ID="LinkButton_frame" runat="server" OnClick="LinkButton_frame_Click"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                    <asp:TextBox ID="txt_Keyword_frame" MaxLength="10" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="ddl_frame">Product:</label>
                    <asp:DropDownList ID="ddl_frame" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddl_frame_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_FrameType">Type:</label>
                    <asp:TextBox ID="txt_FrameType" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_FrameGlass">Glass:</label>
                    <asp:TextBox ID="txt_FrameGlass" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_Color">Color:</label>
                    <asp:TextBox ID="txt_Color" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_FrameSupplier">Supplier:</label>
                    <asp:TextBox ID="txt_FrameSupplier" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="Txt_Brand">Brand:</label>
                    <asp:TextBox ID="Txt_Brand" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="Txt_Model">Model:</label>
                    <asp:TextBox ID="Txt_Model" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-2 col-md-3 col-sm-4">
                    <label for="txt_FramePrice">Price:</label>
                    <asp:TextBox ID="txt_FramePrice" ClientIDMode="Static" Enabled="false" CssClass="form-control" onkeyup="CalTotal();" oninput="CalTotal();" Text="0.00" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-2">
                    <h4>Size</h4>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-2">
                    <label for="Txt_SizeA">A:</label>
                    <asp:TextBox ID="Txt_SizeA" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-2">
                    <label for="Txt_SizeB">B:</label>
                    <asp:TextBox ID="Txt_SizeB" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-2">
                    <label for="Txt_SizeSides">Sides:</label>
                    <asp:TextBox ID="Txt_SizeSides" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-2">
                    <label for="Txt_SizeH">H:</label>
                    <asp:TextBox ID="Txt_SizeH" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-lg-1 col-md-1 col-sm-2">
                    <label for="Txt_SizeDBL">DBL:</label>
                    <asp:TextBox ID="Txt_SizeDBL" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Ready">Ready:</label>
            <asp:TextBox ID="Txt_Ready" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Adviceclient">Advice client:</label>
            <asp:TextBox ID="Txt_Adviceclient" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Followup">Follow-up:</label>
            <asp:TextBox ID="Txt_Followup" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Datecalled">Date called:</label>
            <asp:TextBox ID="Txt_Datecalled" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Delivery">Delivery date:</label>
            <asp:TextBox ID="Txt_Delivery" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Notes">Notes:</label>
            <asp:TextBox ID="Txt_Notes" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div style="clear: both;"></div>

    <div style="display: none;" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td colspan="3">
                    <h4>&nbsp;&nbsp;Ophtalmic lens</h4>
                </td>
                <td align="center">
                    <label>Price</label></td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Ophtalmiclens_OD" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Ophtalmiclens_OS" ClientIDMode="Static" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <div style="clear: both;"></div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Professionalfees">Professional fees:</label>
            <asp:TextBox ID="Txt_Professionalfees" ClientIDMode="Static" Text="0.00" onkeyup="CalTotal();" oninput="CalTotal();" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Total">Total:</label>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="Txt_Total" ClientIDMode="Static" Text="0.00" CssClass="form-control" runat="server"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <br />
            <asp:Button ID="Button_SAVE" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" OnClick="Button_SAVE_Click" runat="server" Text="Save" />
            <asp:Button ID="Button_Invoice" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" Text="Invoice" OnClick="Button_Invoice_Click" />
        </div>
    </div>
</asp:Content>

