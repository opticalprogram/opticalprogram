﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class invoice : System.Web.UI.Page
{
    Dal objdal;    
    string MasterId;
    double OldThirdPartyBalance, OldPatientBalance;
    protected void Page_Load(object sender, EventArgs e)
    {
        objdal = new Dal();        

        // Set title
        string title = "Invoice";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();

        SetInvoiceNumber();
        if (!IsPostBack)
        {
            PatientFill();
            setOptometrist();
            Session["InvoiceMasterId"] = "55555555-5555-5555-5555-555555555555";

            if (Request.QueryString["id"] != null)
            {
                Session["InvoiceMasterId"] = Request.QueryString["id"].ToString();
                SetTotal();
                getInvoiceData(Request.QueryString["id"].ToString());
                checkPermission();
            }

            if (Request.QueryString["pid"] != null)
            {
                DDL_Patient.SelectedValue = Request.QueryString["pid"].ToString();
            }
        }
        

    }

   

    //Check User Permission
    protected void checkPermission()
    {
        string userName = Membership.GetUser().UserName;
        if (!User.IsInRole("Admin") && !User.IsInRole("Master"))
        {
            if (objdal.Get_SingleValue("select count(*) from tbl_Permission where tbl_Permission.UserName='" + userName + "' and PermissionInvoiceEdit = 1") == "0")
            {
                GVInvoice.Columns[GVInvoice.Columns.Count - 1].ControlStyle.CssClass = "btn btn-danger btn-xs disabled";
                AddProduct.Attributes["class"] = "glyphicon glyphicon-user btn btn-sm btn-default disabled";
                AddProduct.Attributes["data-toggle"] = "";
                AddService.Attributes["class"] = "glyphicon glyphicon-user btn btn-sm btn-default disabled";
                AddService.Attributes["data-toggle"] = "";
                Button_Submit.Enabled = false;
            }
        }
    }

    //for Patient fill
    protected void PatientFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT [PatientId], [FirstName]+' '+[LastName]+' '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Patient] FROM [tbl_Patient] where [tbl_Patient].MasterId='" + MasterId + "' ORDER BY [Patient]");
        DDL_Patient.DataSource = dt;
        DDL_Patient.DataValueField = "PatientId";
        DDL_Patient.DataTextField = "Patient";
        DDL_Patient.DataBind();
        setOptometrist();
    }

    // Max Invoice Number Set
    protected void SetInvoiceNumber()
    {
        try
        {
            Txt_InvoiceNumber.Text = objdal.Get_SingleValue("select isnull(max(InvoiceNo),0)+1 as MaxInvoiceNumber from tbl_InvoiceMaster");
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    protected void Button_Patient_Click(object sender, EventArgs e)
    {
        try
        {
            // INSERT INTO [tbl_Patient]([FileNo],[OpeningDate],[FirstName],[LastName],[DateOfBirth][Optometrist]) VALUES('','','','','','')
            StringBuilder sb = new StringBuilder();
            string fileno = objdal.Get_SingleValue("select isnull(max(fileno),0)+1 as fileno from tbl_Patient");
            sb.Append("INSERT INTO [tbl_Patient]([FileNo],[OpeningDate],[FirstName],[LastName],[DateOfBirth],[HomePhone],[Optometrist],[MasterId])");
            sb.Append(" VALUES('" + fileno + "','" + DateTime.Now.ToString() + "'");
            sb.Append(",'" + TextBox_FirstName.Text + "','" + TextBox_LastName.Text + "'");
            sb.Append(",'" + DateTime.ParseExact(TextBox_DateOfBirth.Text, "dd-MM-yyyy", null) + "'");
            sb.Append(",'" + TextBox_HomePhone.Text + "','" + TextBox_Optometrist.Text + "','" + MasterId + "')");

            objdal.EXECUTE_DML(sb.ToString());
            PatientFill();

        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
    protected void DDL_Patient_SelectedIndexChanged(object sender, EventArgs e)
    {
        setOptometrist();
    }

    //Set optometrist on patient change
    protected void setOptometrist()
    {
        if (DDL_Patient.SelectedValue != "")
        {
            Txt_Optometrist.Text = objdal.Get_SingleValue("select Optometrist from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");
        }
        else
        {
            lblMessage.Visible = true;
            lblMessage.CssClass = "alert message alert-warning";
            lblMessage.Text = "Alert: Invoice need a patient data. Please add a patient!";
        }
    }
    protected void Button_ItemSearch_Click(object sender, EventArgs e)
    {
        SDSInventory.SelectCommand = "SELECT tbl_Item.ItemId ,tbl_Item.ItemName, tbl_Item.ItemBrand, tbl_Item.ItemQuantity, tbl_Item.ItemModel,tbl_Item.ItemSalePrice "
                           + " FROM tbl_Item where " + DDL_SerchBy.SelectedValue + " like '%" + TextBox_Item_Keyword.Text + "%' and tbl_Item.MasterId='" + MasterId + "' ";

        TextBox_Item_Keyword.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddProductModel();", true);
    }
    // for item add
    protected void GVInventory_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {

            GVInventory.SelectedIndex = e.NewSelectedIndex;

            InvoiceMasterInsert();

            //INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ItemId],[InvoiceDetailDescription],[InvoiceDetailQuantity],
            //[InvoiceDetailDiscount],[InvoiceDetailTax],[InvoiceDetailUnitprice])VALUES(,,,,,)
             Label Quantity = (Label)GVInventory.Rows[GVInventory.SelectedIndex].FindControl("Lbl_Quantity");
            var quantity = (Quantity.Text ?? "");
            if (string.IsNullOrWhiteSpace(quantity))
            {
                quantity = "0";
            }
            if (Convert.ToInt16(txt_ItemQuantity.Text) <= Convert.ToInt16(quantity))
            {
                StringBuilder sb = new StringBuilder();

                string ItemId = GVInventory.SelectedValue.ToString();
                string UnitPrice = objdal.Get_SingleValue("SELECT tbl_Item.ItemSalePrice FROM tbl_Item where tbl_Item.ItemId='" + ItemId + "'");
                sb.Append("INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ItemId],[InvoiceDetailDescription],[InvoiceDetailQuantity]");
                sb.Append(",[InvoiceDetailDiscount],[InvoiceDetailTax],[InvoiceDetailUnitprice])VALUES('" + Session["InvoiceMasterId"].ToString() + "'");
                sb.Append(",'" + ItemId + "','" + txt_ItemDescription.Text + "','" + txt_ItemQuantity.Text + "'");
                sb.Append(",'" + txt_ItemDiscount.Text + "','" + txt_ItemTax.Text + "','" + UnitPrice + "')");

                objdal.EXECUTE_DML(sb.ToString());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "distroymodal();", true);


                GvInvoiceFill();
                SetTotal();
               
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "distroymodal();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Quantity is not available!');", true);                                 
            }
            
        }
        catch (Exception ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }

    }

    //for service add
    protected void Btn_Service_Click(object sender, EventArgs e)
    {
        try
        {

            InvoiceMasterInsert();

            StringBuilder sb = new StringBuilder();

            sb.Append("INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ExamTypeId],[InvoiceDetailDescription],[InvoiceDetailQuantity]");
            sb.Append(",[InvoiceDetailDiscount],[InvoiceDetailTax],[InvoiceDetailUnitprice])VALUES('" + Session["InvoiceMasterId"].ToString() + "'");
            sb.Append(",'" + DDL_ExamType.SelectedValue + "','" + txt_ServiceDescription.Text + "','" + txt_ServiceQuantity.Text + "'");
            sb.Append(",'" + txt_ServiceDiscount.Text + "','" + txt_ServiceTax.Text + "','" + txt_UnitPrice.Text + "')");

            objdal.EXECUTE_DML(sb.ToString());

            GvInvoiceFill();
            SetTotal();
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    private void InvoiceMasterInsert()
    {
        if (Session["InvoiceMasterId"].ToString() == "55555555-5555-5555-5555-555555555555")
        {
            StringBuilder sb = new StringBuilder();
            string InvoiceMasterId = Guid.NewGuid().ToString();
            string CurrentuserId = Membership.GetUser().ProviderUserKey.ToString();

            //INSERT INTO [dbo].[tbl_InvoiceMaster]([InvoiceMasterId],[PatientId],[InvoiceMasterDate],[InvoiceMasterGrandTotal],[InvoiceMasterNote]
            //,[InvoiceMasterIsTemporary],[MasterId]) VALUES(,,,,,,)

            sb.Append("INSERT INTO [dbo].[tbl_InvoiceMaster]([InvoiceMasterId],[InvoiceNo],[PatientId],[InvoiceMasterDate],[MasterId],[InvoiceMasterEnterydById])");
            sb.Append(" VALUES('" + InvoiceMasterId + "','" + Txt_InvoiceNumber.Text + "','" + DDL_Patient.SelectedValue + "'");
            sb.Append(",'" + DateTime.ParseExact(Txt_InvoiceDate.Text, "dd-MM-yyyy", null) + "','" + MasterId + "','" + CurrentuserId + "')");

            objdal.EXECUTE_DML(sb.ToString());

            Session["InvoiceMasterId"] = InvoiceMasterId;
        }
    }

    // Set Invoice Total
    private void SetTotal()
    {

        Txt_Total.Text = Convert.ToDouble(objdal.Get_SingleValue("SELECT ISNULL(sum((tbl_InvoiceDetail.InvoiceDetailQuantity * tbl_InvoiceDetail.InvoiceDetailUnitprice -tbl_InvoiceDetail.InvoiceDetailDiscount )"
                                                 + "+ tbl_InvoiceDetail.InvoiceDetailTax),0.00) AS Total "
                                                 + " From tbl_InvoiceDetail where InvoiceMasterId='" + Session["InvoiceMasterId"].ToString() + "'")).ToString("n");
    }

    //fill invoice grid
    private void GvInvoiceFill()
    {
        SDSInvoice.SelectCommand = "SELECT tbl_InvoiceDetail.InvoiceDetailId,tbl_InvoiceDetail.InvoiceDetailDescription, tbl_InvoiceDetail.InvoiceDetailQuantity, "
                                    + "   tbl_InvoiceDetail.InvoiceDetailUnitprice, tbl_InvoiceDetail.InvoiceDetailQuantity * tbl_InvoiceDetail.InvoiceDetailUnitprice AS SubTotal,"
                                    + "   tbl_InvoiceDetail.InvoiceDetailDiscount, tbl_InvoiceDetail.InvoiceDetailTax,"
                                    + "   ((tbl_InvoiceDetail.InvoiceDetailQuantity * tbl_InvoiceDetail.InvoiceDetailUnitprice) - tbl_InvoiceDetail.InvoiceDetailDiscount)"
                                    + "   + (tbl_InvoiceDetail.InvoiceDetailTax) AS Total,tbl_Patient.Optometrist,tbl_Patient.FirstName+' '+tbl_Patient.LastName as patientname, tbl_InvoiceMaster.InvoiceMasterId,"
                                    + "   tbl_InvoiceDetail.InvoiceDetailDeliveryDate FROM tbl_InvoiceMaster INNER JOIN tbl_InvoiceDetail "
                                    + "   ON tbl_InvoiceMaster.InvoiceMasterId = tbl_InvoiceDetail.InvoiceMasterId"
                                    + "   INNER JOIN tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId"
                                    + "  where tbl_InvoiceMaster.InvoiceMasterId='" + Session["InvoiceMasterId"].ToString() + "'";
    }

    // for save/update invoice and patient 
    protected void Button_Submit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToDouble(Txt_Total.Text) == Convert.ToDouble(Txt_ThirdpartyBalance.Text) + Convert.ToDouble(Txt_PatientBalance.Text) + Convert.ToDouble(Txt_Payment.Text))
            {
                //UPDATE [dbo].[tbl_InvoiceMaster] SET [InvoiceMasterGrandTotal] = ''
                //,[InvoiceMasterNote] = '',[InvoiceMasterIsTemporary] = '' 
                //WHERE [InvoiceMasterId] =''         
                StringBuilder sb = new StringBuilder();
                sb.Append("UPDATE [dbo].[tbl_InvoiceMaster] SET [InvoiceMasterGrandTotal] = '" + Txt_Total.Text + "'");
                sb.Append(",[InvoiceMasterPayment]='" + Txt_Payment.Text + "',[InvoiceMasterThirdPartyBalance]='" + Txt_ThirdpartyBalance.Text + "'");
                sb.Append(",[InvoiceMasterPatientBalance]='" + Txt_PatientBalance.Text + "'");
                sb.Append(",[InvoiceMasterNote] = '" + Txt_Note.Text + "',[InvoiceMasterIsTemporary] = '0'");
                sb.Append("WHERE [InvoiceMasterId] ='" + Session["InvoiceMasterId"].ToString() + "'");

                objdal.EXECUTE_DML(sb.ToString());

                sb = new StringBuilder();
                //UPDATE [dbo].[tbl_Patient] SET [ThirdPartyBalance] = '',[PatientBalance] ='' WHERE

                string ThirdPartyBalance = string.Empty, PatientBalance = string.Empty;
                if (Request.QueryString["id"] != null)
                {
                    //difference between oldThirpartybal and newthirdpartybal in invoice
                    // add or minues from patient bal 
                    //if equal do nothing
                    double NewThirdPartyBalance = Convert.ToDouble(Txt_ThirdpartyBalance.Text);
                    double NewPatienceBalance = Convert.ToDouble(Txt_PatientBalance.Text);
                    double diff3rdParty, diffPatBal;
                    OldThirdPartyBalance = (double)ViewState["OldThirdPartyBalance"];
                    OldPatientBalance = (double)ViewState["OldPatientBalance"];

                    if (NewThirdPartyBalance != OldThirdPartyBalance || NewPatienceBalance != OldPatientBalance)
                    {
                        if (NewThirdPartyBalance > OldThirdPartyBalance)
                        {
                            diff3rdParty = NewThirdPartyBalance - OldThirdPartyBalance;
                            ThirdPartyBalance = objdal.Get_SingleValue("select isnull(ThirdPartyBalance,0)+'" + diff3rdParty + "' from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");

                        }
                        else if (NewThirdPartyBalance < OldThirdPartyBalance)
                        {
                            diff3rdParty = OldThirdPartyBalance - NewThirdPartyBalance;
                            ThirdPartyBalance = objdal.Get_SingleValue("select isnull(ThirdPartyBalance,0)-'" + diff3rdParty + "' from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");
                        }

                        if (NewPatienceBalance > OldPatientBalance)
                        {
                            diffPatBal = NewPatienceBalance - OldPatientBalance;
                            PatientBalance = objdal.Get_SingleValue("select isnull(PatientBalance,0)+'" + diffPatBal + "' from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");

                        }
                        else if (NewPatienceBalance < OldPatientBalance)
                        {
                            diffPatBal = OldPatientBalance - NewPatienceBalance;
                            PatientBalance = objdal.Get_SingleValue("select isnull(PatientBalance,0)-'" + diffPatBal + "' from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");
                        }



                        sb.Append("UPDATE [dbo].[tbl_Patient] SET ");
                        sb.Append("[ThirdPartyBalance] = '" + ThirdPartyBalance + "',[PatientBalance] ='" + PatientBalance + "'");
                        sb.Append(" WHERE PatientId='" + DDL_Patient.SelectedValue + "'");

                        objdal.EXECUTE_DML(sb.ToString());
                    }
                }
                else
                {
                    ThirdPartyBalance = objdal.Get_SingleValue("select isnull(ThirdPartyBalance,0)+'" + Txt_ThirdpartyBalance.Text + "' from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");
                    PatientBalance = objdal.Get_SingleValue("select isnull(PatientBalance,0)+'" + Txt_PatientBalance.Text + "' from tbl_Patient where PatientId='" + DDL_Patient.SelectedValue + "'");

                    sb.Append("UPDATE [dbo].[tbl_Patient] SET ");
                    sb.Append("[ThirdPartyBalance] = '" + ThirdPartyBalance + "',[PatientBalance] ='" + PatientBalance + "'");
                    sb.Append(" WHERE PatientId='" + DDL_Patient.SelectedValue + "'");

                    objdal.EXECUTE_DML(sb.ToString());
                }


                Session.Remove("InvoiceMasterId");
                Response.Redirect("~/manageinvoice.aspx", true);
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Error: Total not matched with the sum of Payment, Third-party Balance, and Patient Balance!";
            }
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    protected void getInvoiceData(string id)
    {
        //SELECT tbl_InvoiceMaster.InvoiceNo, tbl_InvoiceMaster.InvoiceMasterDate, tbl_Patient.PatientId, tbl_Patient.Optometrist,
        //tbl_InvoiceMaster.InvoiceMasterPayment, tbl_InvoiceMaster.InvoiceMasterThirdPartyBalance,
        //tbl_InvoiceMaster.InvoiceMasterPatientBalance, tbl_InvoiceMaster.InvoiceMasterNote
        //FROM tbl_InvoiceMaster INNER JOIN tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId
        //where InvoiceMasterIsTemporary='0' and tbl_InvoiceMaster.InvoiceMasterId=''
        Button_Submit.Text = "Update";

        StringBuilder sb = new StringBuilder();
        sb.Append(
            "SELECT tbl_InvoiceMaster.InvoiceNo, tbl_InvoiceMaster.InvoiceMasterDate, tbl_Patient.PatientId, tbl_Patient.Optometrist,");
        sb.Append("tbl_InvoiceMaster.InvoiceMasterPayment, tbl_InvoiceMaster.InvoiceMasterThirdPartyBalance,");
        sb.Append("tbl_InvoiceMaster.InvoiceMasterPatientBalance, tbl_InvoiceMaster.InvoiceMasterNote");
        sb.Append(
            " FROM tbl_InvoiceMaster INNER JOIN tbl_Patient ON tbl_InvoiceMaster.PatientId = tbl_Patient.PatientId");
        sb.Append(" where tbl_InvoiceMaster.InvoiceMasterId='" + id.ToString() + "'");

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE(sb.ToString());
        if (dt.Rows.Count > 0)
        {
            Txt_InvoiceNumber.Text = dt.Rows[0]["InvoiceNo"].ToString();
            Txt_InvoiceDate.Text = Convert.ToDateTime(dt.Rows[0]["InvoiceMasterDate"].ToString()).ToString("dd-MM-yyyy");
            DDL_Patient.SelectedValue = dt.Rows[0]["PatientId"].ToString();
            Txt_Optometrist.Text = dt.Rows[0]["Optometrist"].ToString();
            Txt_Payment.Text = Convert.ToDouble(dt.Rows[0]["InvoiceMasterPayment"]).ToString("n");

            ViewState["OldThirdPartyBalance"] =
                OldThirdPartyBalance = Convert.ToDouble(dt.Rows[0]["InvoiceMasterThirdPartyBalance"]);
            Txt_ThirdpartyBalance.Text = OldThirdPartyBalance.ToString("n");

            ViewState["OldPatientBalance"] =
                OldPatientBalance = Convert.ToDouble(dt.Rows[0]["InvoiceMasterPatientBalance"]);
            Txt_PatientBalance.Text = OldPatientBalance.ToString("n");

            Txt_Note.Text = dt.Rows[0]["InvoiceMasterNote"].ToString();
        }
    }

    protected void SDSInvoice_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        SetTotal();
    }

    //Delivery date update in gvinvoice
    protected void GVInvoice_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string DelivaryDate = ((TextBox)GVInvoice.Rows[e.RowIndex].FindControl("Txt_DeliveryDate")).Text;
        GVInvoice.SelectedIndex = e.RowIndex;
        string query = "UPDATE [dbo].[tbl_InvoiceDetail] SET [InvoiceDetailDeliveryDate] = '" + DateTime.ParseExact(DelivaryDate, "dd-MM-yyyy", null) + "' "
                       + " WHERE InvoiceDetailId='"+ GVInvoice.SelectedValue.ToString() +"'";
        objdal.EXECUTE_DML(query);
    }
}