﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class glass : System.Web.UI.Page
{
    Dal objdal;
    string MasterId;
    protected void Page_Load(object sender, EventArgs e)
    {
        // set title
        string title = "Glass";
        Page.Title = title + " | " + ConfigurationManager.AppSettings["ApplicationName"];
        ((Site)Page.Master).heading = title;
        if (Session["MasterId"] == null)
        {
            Response.Redirect("~/home.aspx", true);
        }
        MasterId = Session["MasterId"].ToString();
        objdal = new Dal();
        SetPrescriptionNo();

        try
        {
            if (!IsPostBack)
            {
                ItemFill();
                FrameFill();
                if (ddl_Product_OD.Items.Count>0)
                {
                    ODItemDetails();
                    OSItemDetails();
                }
                if (ddl_frame.Items.Count>0)
                {
                    FrameDetails();
                }
                Label_Company.Text = objdal.Get_SingleValue("select aspnet_Membership.Comment from aspnet_Membership where aspnet_Membership.UserId='" + MasterId + "'").Split(new char[] { '^' })[0];
                txt_Professional.Text = Membership.GetUser().UserName;
                PatientFill();
                if (Request.QueryString["pid"] != null)
                {
                    DDL_Patient.SelectedValue = Request.QueryString["pid"].ToString();
                }
                if (DDL_Patient.Items.Count > 0)
                {
                    FillExam();
                }
                if (Request.QueryString["id"] != null)
                {
                    txt_Professional.Enabled = false;
                    Button_SAVE.Text = "Update";                   
                    GetPrescriptionData(Request.QueryString["id"].ToString());
                }               
            }
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    //set Max Prescription no
    protected void SetPrescriptionNo()
    {
        Txt_PrescriptionNo.Text = objdal.Get_SingleValue("select isnull(max(GlassId),0)+1  from tbl_Glass");
    }

    // fill Item 
    protected void ItemFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
                                 + " where tbl_Item.ItemType='Ophtalmic lens' and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        ddl_Product_OD.DataSource = dt;
        ddl_Product_OD.DataValueField = "ItemId";
        ddl_Product_OD.DataTextField = "Item";
        ddl_Product_OD.DataBind();

        ddl_Product_OS.DataSource = dt;
        ddl_Product_OS.DataValueField = "ItemId";
        ddl_Product_OS.DataTextField = "Item";
        ddl_Product_OS.DataBind();
    }

    // Fill Frame details
    protected void FrameFill()
    {
        DataTable dt = new DataTable();
        if (Request.QueryString["id"] != null)
        {
            dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
                                  + " where tbl_Item.ItemType='Frame' and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        }
        else
        {
            dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
                                 + " where tbl_Item.ItemQuantity>0 and  tbl_Item.ItemType='Frame' and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        }        
        ddl_frame.DataSource = dt;
        ddl_frame.DataValueField = "ItemId";
        ddl_frame.DataTextField = "Item";
        ddl_frame.DataBind();        
    }

    // Set Item OD Item Details
    protected void ODItemDetails()
    {
        //select 'lens:'+tbl_Item.OphtalmicType +', Material:'+tbl_Item.OphtalmicMaterial + ',Treatments:' + tbl_Item.OphtalmicTreatments 
        //+', Warranty:'+tbl_Item.OphtalmicWarranty +', Supplier:'+tbl_Supplier.SupplierName  as [Detail] 
        //from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select tbl_Item.OphtalmicType, tbl_Item.OphtalmicMaterial,tbl_Item.OphtalmicTreatments,tbl_Item.OphtalmicWarranty , "
                                  + "tbl_Supplier.SupplierName,ItemSalePrice "
                                  + " from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId"
                                  + " where tbl_Item.ItemId='" + ddl_Product_OD.SelectedValue + "'");
        txt_Type_OD.Text = dt.Rows[0]["OphtalmicType"].ToString();
        txt_Material_OD.Text = dt.Rows[0]["OphtalmicMaterial"].ToString();
        txt_Treatments_OD.Text = dt.Rows[0]["OphtalmicTreatments"].ToString();
        txt_Warranty_OD.Text = dt.Rows[0]["OphtalmicWarranty"].ToString();
        txt_Supplier_OD.Text = dt.Rows[0]["SupplierName"].ToString();
        txt_Price_OD.Text = Convert.ToDouble(dt.Rows[0]["ItemSalePrice"]).ToString("n");

        SetTotal();
       
    }

    // Set Item OD Item Details
    protected void OSItemDetails()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select tbl_Item.OphtalmicType, tbl_Item.OphtalmicMaterial,tbl_Item.OphtalmicTreatments,tbl_Item.OphtalmicWarranty , "
                                  + "tbl_Supplier.SupplierName,ItemSalePrice "
                                  + " from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId"
                                  + " where tbl_Item.ItemId='" + ddl_Product_OS.SelectedValue + "'");
        txt_Type_OS.Text = dt.Rows[0]["OphtalmicType"].ToString();
        txt_Material_OS.Text = dt.Rows[0]["OphtalmicMaterial"].ToString();
        txt_Treatments_OS.Text = dt.Rows[0]["OphtalmicTreatments"].ToString();
        txt_Warranty_OS.Text = dt.Rows[0]["OphtalmicWarranty"].ToString();
        txt_Supplier_OS.Text = dt.Rows[0]["SupplierName"].ToString();
        txt_Price_OS.Text = Convert.ToDouble(dt.Rows[0]["ItemSalePrice"]).ToString("n");
        SetTotal();
    }

    // set frame details
    protected void FrameDetails()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select tbl_Item.ItemFrame, tbl_Item.ItemGlass, tbl_Item.ItemColor , "
                                + " tbl_Supplier.SupplierName , tbl_item.ItemBrand, tbl_item.ItemModel, tbl_item.ItemSalePrice, "
                                + " tbl_item.ItemSizeA, tbl_item.ItemSizeB, tbl_item.ItemSizeSides, tbl_item.ItemSizeH, tbl_item.ItemSizeDBL"
                                + " from tbl_Item inner JOIN tbl_Supplier on tbl_Item.ItemSupplierId=tbl_Supplier.SupplierId "
                                + " where tbl_Item.ItemId='" + ddl_frame.SelectedValue + "'");

        txt_FrameType.Text = dt.Rows[0]["ItemFrame"].ToString();
        txt_FrameGlass.Text = dt.Rows[0]["ItemGlass"].ToString();
        txt_Color.Text = dt.Rows[0]["ItemColor"].ToString();
        txt_FrameSupplier.Text = dt.Rows[0]["SupplierName"].ToString();
        txt_FramePrice.Text = Convert.ToDouble(dt.Rows[0]["ItemSalePrice"]).ToString("n");
        Txt_Brand.Text = dt.Rows[0]["ItemBrand"].ToString();
        Txt_SizeA.Text = dt.Rows[0]["ItemSizeA"].ToString();
        Txt_SizeB.Text = dt.Rows[0]["ItemSizeB"].ToString();
        Txt_SizeSides.Text = dt.Rows[0]["ItemSizeSides"].ToString();
        Txt_SizeH.Text = dt.Rows[0]["ItemSizeH"].ToString();
        Txt_SizeDBL.Text = dt.Rows[0]["ItemSizeDBL"].ToString();

        SetTotal();
    }

    // set Total
    protected void SetTotal()
    {
        Txt_Total.Text = (Convert.ToDouble(txt_Price_OD.Text) + Convert.ToDouble(txt_Price_OS.Text) + Convert.ToDouble(Txt_Professionalfees.Text) + Convert.ToDouble(txt_FramePrice.Text)).ToString();
    }
    
    //Fill patient DDl
    protected void PatientFill()
    {
        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("SELECT [PatientId], [FirstName]+' '+[LastName] +' ('+ SUBSTRING([Gender], 1, 1) +') '+CONVERT(VARCHAR(11),DateOfBirth,105) AS [Patient] "
                                 + " FROM [tbl_Patient] where [tbl_Patient].MasterId='" + MasterId + "' ORDER BY [Patient]");
        DDL_Patient.DataSource = dt;
        DDL_Patient.DataValueField = "PatientId";
        DDL_Patient.DataTextField = "Patient";
        DDL_Patient.DataBind();
    }

    // fill Exam on patient selection
    protected void FillExam()
    {
        DataTable dt = new DataTable();
        StringBuilder sb = new StringBuilder();

        // SELECT    CONVERT(varchar, GeneralEyeExamETemplatesId) + ' ' +TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam
        // FROM   tbl_GeneralEyeExamETemplates where PatientId=''
        // union all
        //SELECT    CONVERT(varchar, EmergencyExamtemplatesId) + ' ' +TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam
        //FROM   tbl_EmergencyExamtemplates where PatientId=''
        // union all
        // SELECT    CONVERT(varchar, EyeDilationExamtemplatesId) + ' ' +TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam
        // FROM   tbl_EyeDilationExamtemplates where PatientId=''

        sb.Append("SELECT CONVERT(varchar, GeneralEyeExamETemplatesId) +' '+ TemplateType as Template, TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam");
        sb.Append(" FROM   tbl_GeneralEyeExamETemplates where PatientId='" + DDL_Patient.SelectedValue.ToString() + "'");
        sb.Append(" union all ");
        sb.Append("SELECT CONVERT(varchar, EmergencyExamtemplatesId) +' '+ TemplateType as Template, TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam");
        sb.Append(" FROM   tbl_EmergencyExamtemplates where PatientId='" + DDL_Patient.SelectedValue.ToString() + "'");
        sb.Append(" union all ");
        sb.Append("SELECT CONVERT(varchar, EyeDilationExamtemplatesId) +' '+ TemplateType as Template, TemplateType+ ' ' +CONVERT(varchar, ExamDate,105) as Exam");
        sb.Append(" FROM   tbl_EyeDilationExamtemplates where PatientId='" + DDL_Patient.SelectedValue.ToString() + "'");

        dt = objdal.GET_DATATABLE(sb.ToString());
        ddl_Exam.DataSource = dt;
        ddl_Exam.DataValueField = "Template";
        ddl_Exam.DataTextField = "Exam";
        ddl_Exam.DataBind();
        setPrescription();
    }

    // Set Prescription through exam selection
    protected void setPrescription()
    {
        if (ddl_Exam.SelectedValue.Contains("General"))
        {
            DataTable dt = new DataTable();
            dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_GeneralEyeExamETemplates] "
                                     + " where [GeneralEyeExamETemplatesId]='" + ddl_Exam.SelectedValue.Split(new char[] { ' ' })[0] + "'");
            Txt_Sphere_OD.Text = dt.Rows[0]["RxScopeOD1"].ToString();
            Txt_Cylinder_OD.Text = dt.Rows[0]["RxScopeOD2"].ToString();
            Txt_Axis_OD.Text = dt.Rows[0]["RxScopeOD3"].ToString();
            Txt_Addition_OD.Text = dt.Rows[0]["RxScopeOD4"].ToString();
            Txt_Sphere_OS.Text = dt.Rows[0]["RxScopeOs1"].ToString();
            Txt_Cylinder_OS.Text = dt.Rows[0]["RxScopeOS2"].ToString();
            Txt_Axis_OS.Text = dt.Rows[0]["RxScopeOS3"].ToString();
            Txt_Addition_OS.Text = dt.Rows[0]["RxScopeOS4"].ToString();

        }
        else
        {
            Txt_Sphere_OD.Text = "";
            Txt_Cylinder_OD.Text = "";
            Txt_Axis_OD.Text = "";
            Txt_Addition_OD.Text = "";
            Txt_Sphere_OS.Text = "";
            Txt_Cylinder_OS.Text = "";
            Txt_Axis_OS.Text = "";
            Txt_Addition_OS.Text = "";

        }
    }


    protected void DDL_Patient_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillExam();
    }

    // Get Prescription Data
    protected void GetPrescriptionData(string EditId)
    {
       
        // INSERT INTO [dbo].[tbl_Glass]([GlassId],[PatientId],[Date],[ExamTemplate],[Professional],[Sphere_OD],[Cylinder_OD]
        //,[Axis_OD],[Addition_OD],[Segh_OD],[Bc_OD],[Sphere_OS],[Cylinder_OS],[Axis_OS],[Addition_OS],[Segh_OS],[Bc_OS]
        //,[PDFar_OD],[PDNear_OD],[PDHeight_OD],[PDVertex_OD],[PDWrapangle_OD],[PDPantoscopicangle_OD],[PDFar_OS],[PDNear_OS]
        //,[PDHeight_OS],[PDVertex_OS],[PDWrapangle_OS],[PDPantoscopicangle_OS],[PDFar_Total],[PDNear_Total],[ItemId_OD]
        //,[Printedon_OD],[Orderno_OD],[LabOrderno_OD],[TrayNo_OD],[ItemId_OS],[Printedon_OS],[Orderno_OS],[LabOrderno_OS]
        //,[TrayNo_OS],[Ready],[Adviceclient],[Followup],[Datecalled],[Delivery],[Notes],[Ophtalmiclens_OD],[Price_OD]
        //,[Ophtalmiclens_OS],[Price_OS],[Professionalfees],[Total],[PrescriptionType],[MasterId])

        DataTable dt = new DataTable();
        dt = objdal.GET_DATATABLE("select * from [dbo].[tbl_Glass] where GlassId='" + EditId + "'");
        Txt_PrescriptionNo.Text = dt.Rows[0]["GlassId"].ToString();
        DDL_Patient.SelectedValue = dt.Rows[0]["PatientId"].ToString();
        FillExam();
        Txt_Date.Text = Convert.ToDateTime(dt.Rows[0]["Date"]).ToString("dd-MM-yyyy");
        ddl_Exam.SelectedValue = dt.Rows[0]["ExamTemplate"].ToString();
        txt_Professional.Text = dt.Rows[0]["Professional"].ToString();
        Txt_Sphere_OD.Text = dt.Rows[0]["Sphere_OD"].ToString();
        Txt_Cylinder_OD.Text = dt.Rows[0]["Cylinder_OD"].ToString();
        Txt_Axis_OD.Text = dt.Rows[0]["Axis_OD"].ToString();
        Txt_Addition_OD.Text = dt.Rows[0]["Addition_OD"].ToString();
        Txt_Segh_OD.Text = dt.Rows[0]["Segh_OD"].ToString();
        Txt_Bc_OD.Text = dt.Rows[0]["Bc_OD"].ToString();
        Txt_Sphere_OS.Text = dt.Rows[0]["Sphere_OS"].ToString();
        Txt_Cylinder_OS.Text = dt.Rows[0]["Cylinder_OS"].ToString();
        Txt_Axis_OS.Text = dt.Rows[0]["Axis_OS"].ToString();
        Txt_Addition_OS.Text = dt.Rows[0]["Addition_OS"].ToString();
        Txt_Segh_OS.Text = dt.Rows[0]["Segh_OS"].ToString();
        Txt_Bc_OS.Text = dt.Rows[0]["Bc_OS"].ToString();
        txt_PDFar_OD.Text = dt.Rows[0]["PDFar_OD"].ToString();
        txt_PDNear_OD.Text = dt.Rows[0]["PDNear_OD"].ToString();
        txt_PDHeight_OD.Text = dt.Rows[0]["PDHeight_OD"].ToString();
        txt_PDVertex_OD.Text = dt.Rows[0]["PDVertex_OD"].ToString();
        txt_PDWrapangle_OD.Text = dt.Rows[0]["PDWrapangle_OD"].ToString();
        txt_PDPantoscopicangle_OD.Text = dt.Rows[0]["PDPantoscopicangle_OD"].ToString();
        txt_PDFar_OS.Text = dt.Rows[0]["PDFar_OS"].ToString();
        txt_PDNear_OS.Text = dt.Rows[0]["PDNear_OS"].ToString();
        txt_PDHeight_OS.Text = dt.Rows[0]["PDHeight_OS"].ToString();
        txt_PDVertex_OS.Text = dt.Rows[0]["PDVertex_OS"].ToString();
        txt_PDWrapangle_OS.Text = dt.Rows[0]["PDWrapangle_OS"].ToString();
        txt_PDPantoscopicangle_OS.Text = dt.Rows[0]["PDPantoscopicangle_OS"].ToString();
        txt_PDFar_Total.Text = dt.Rows[0]["PDFar_Total"].ToString();
        txt_PDNear_Total.Text = dt.Rows[0]["PDNear_Total"].ToString();
        ddl_Product_OD.SelectedValue = dt.Rows[0]["ItemId_OD"].ToString();
        txt_Printedon_OD.Text = Convert.ToDateTime(dt.Rows[0]["Printedon_OD"]).ToString("dd-MM-yyyy");
        txt_Orderno_OD.Text = dt.Rows[0]["Orderno_OD"].ToString();
        txt_LabOrderno_OD.Text = dt.Rows[0]["LabOrderno_OD"].ToString();
        txt_TrayNo_OD.Text = dt.Rows[0]["TrayNo_OD"].ToString();
        ddl_Product_OS.SelectedValue = dt.Rows[0]["ItemId_OS"].ToString();
        txt_Printedon_OS.Text = Convert.ToDateTime(dt.Rows[0]["Printedon_OS"]).ToString("dd-MM-yyyy");
        txt_Orderno_OS.Text = dt.Rows[0]["Orderno_OS"].ToString();
        txt_LabOrderno_OS.Text = dt.Rows[0]["LabOrderno_OS"].ToString();       
        txt_TrayNo_OS.Text = dt.Rows[0]["TrayNo_OS"].ToString();
        Txt_Ready.Text = dt.Rows[0]["Ready"].ToString();
        Txt_Adviceclient.Text = dt.Rows[0]["Adviceclient"].ToString();
        Txt_Followup.Text = dt.Rows[0]["Followup"].ToString();
        Txt_Datecalled.Text = Convert.ToDateTime(dt.Rows[0]["Datecalled"]).ToString("dd-MM-yyyy");
        Txt_Delivery.Text = Convert.ToDateTime(dt.Rows[0]["Delivery"]).ToString("dd-MM-yyyy");
        Txt_Notes.Text = dt.Rows[0]["Notes"].ToString();
        txt_Ophtalmiclens_OD.Text = dt.Rows[0]["Ophtalmiclens_OD"].ToString();
        //txt_Price_OD.Text = Convert.ToDouble(dt.Rows[0]["Price_OD"]).ToString("n");
        txt_Ophtalmiclens_OS.Text = dt.Rows[0]["Ophtalmiclens_OS"].ToString();
        //txt_Price_OS.Text = Convert.ToDouble(dt.Rows[0]["Price_OS"]).ToString("n");
        Txt_Professionalfees.Text = Convert.ToDouble(dt.Rows[0]["Professionalfees"]).ToString("n");
        Txt_Total.Text = Convert.ToDouble(dt.Rows[0]["Total"]).ToString("n");
        ddl_frame.SelectedValue = dt.Rows[0]["ItemId_Frame"].ToString();
        ODItemDetails();
        OSItemDetails();
        FrameDetails();
    }

    // Save and Update 
    protected void Button_SAVE_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["id"] != null)
            {
                UpdateGlass();
                Session["lblMessage"] = "Prescription is updated successfully.";
            }
            else
            {
                InsertGlass();
                Session["lblMessage"] = "Prescription is added successfully.";
            }

            Response.Redirect("~/prescription.aspx", true);
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    // Insert Glass
    protected void InsertGlass()
    {
        StringBuilder sb = new StringBuilder();

        // INSERT INTO [dbo].[tbl_Glass]([GlassId],[PatientId],[Date],[ExamTemplate],[Professional],[Sphere_OD],[Cylinder_OD]
        //,[Axis_OD],[Addition_OD],[Segh_OD],[Bc_OD],[Sphere_OS],[Cylinder_OS],[Axis_OS],[Addition_OS],[Segh_OS],[Bc_OS]
        //,[PDFar_OD],[PDNear_OD],[PDHeight_OD],[PDVertex_OD],[PDWrapangle_OD],[PDPantoscopicangle_OD],[PDFar_OS],[PDNear_OS]
        //,[PDHeight_OS],[PDVertex_OS],[PDWrapangle_OS],[PDPantoscopicangle_OS],[PDFar_Total],[PDNear_Total],[ItemId_OD]
        //,[Printedon_OD],[Orderno_OD],[LabOrderno_OD],[TrayNo_OD],[ItemId_OS],[Printedon_OS],[Orderno_OS],[LabOrderno_OS]
        //,[TrayNo_OS],[Ready],[Adviceclient],[Followup],[Datecalled],[Delivery],[Notes],[Ophtalmiclens_OD],[Price_OD]
        //,[Ophtalmiclens_OS],[Price_OS],[Professionalfees],[Total],[PrescriptionType],[MasterId])
        //VALUES()

        sb.Append("INSERT INTO [dbo].[tbl_Glass]([GlassId],[PatientId],[Date],[ExamTemplate],[Professional],[Sphere_OD],[Cylinder_OD]");
        sb.Append(",[Axis_OD],[Addition_OD],[Segh_OD],[Bc_OD],[Sphere_OS],[Cylinder_OS],[Axis_OS],[Addition_OS],[Segh_OS],[Bc_OS]");
        sb.Append(",[PDFar_OD],[PDNear_OD],[PDHeight_OD],[PDVertex_OD],[PDWrapangle_OD],[PDPantoscopicangle_OD],[PDFar_OS],[PDNear_OS]");
        sb.Append(",[PDHeight_OS],[PDVertex_OS],[PDWrapangle_OS],[PDPantoscopicangle_OS],[PDFar_Total],[PDNear_Total],[ItemId_OD]");
        sb.Append(",[Printedon_OD],[Orderno_OD],[LabOrderno_OD],[TrayNo_OD],[ItemId_OS],[Printedon_OS],[Orderno_OS],[LabOrderno_OS]");
        sb.Append(",[TrayNo_OS],[Ready],[Adviceclient],[Followup],[Datecalled],[Delivery],[Notes],[Ophtalmiclens_OD],[Price_OD]");
        sb.Append(",[Ophtalmiclens_OS],[Price_OS],[Professionalfees],[Total],[ItemId_Frame],[PrescriptionType],[MasterId])");
        sb.Append("VALUES('" + Txt_PrescriptionNo.Text + "','" + DDL_Patient.SelectedValue + "','" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",'" + ddl_Exam.SelectedValue + "','" + txt_Professional.Text + "','" + Txt_Sphere_OD.Text + "'");
        sb.Append(",'" + Txt_Cylinder_OD.Text + "','" + Txt_Axis_OD.Text + "','" + Txt_Addition_OD.Text + "'");
        sb.Append(",'" + Txt_Segh_OD.Text + "','" + Txt_Bc_OD.Text + "','" + Txt_Sphere_OS.Text + "'");
        sb.Append(",'" + Txt_Cylinder_OS.Text + "','" + Txt_Axis_OS.Text + "','" + Txt_Addition_OS.Text + "'");
        sb.Append(",'" + Txt_Segh_OS.Text + "','" + Txt_Bc_OS.Text + "','" + txt_PDFar_OD.Text + "'");
        sb.Append(",'" + txt_PDNear_OD.Text + "','" + txt_PDHeight_OD.Text + "','" + txt_PDVertex_OD.Text + "'");
        sb.Append(",'" + txt_PDWrapangle_OD.Text + "','" + txt_PDPantoscopicangle_OD.Text + "','" + txt_PDFar_OS.Text + "'");
        sb.Append(",'" + txt_PDNear_OS.Text + "','" + txt_PDHeight_OS.Text + "','" + txt_PDVertex_OS.Text + "'");
        sb.Append(",'" + txt_PDWrapangle_OS.Text + "','" + txt_PDPantoscopicangle_OS.Text + "','" + txt_PDFar_Total.Text + "'");
        sb.Append(",'" + txt_PDNear_Total.Text + "','" + ddl_Product_OD.SelectedValue + "','" + DateTime.ParseExact(txt_Printedon_OD.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",'" + txt_Orderno_OD.Text + "','" + txt_LabOrderno_OD.Text + "','" + txt_TrayNo_OD.Text + "'");
        sb.Append(",'" + ddl_Product_OS.SelectedValue + "','" + DateTime.ParseExact(txt_Printedon_OS.Text, "dd-MM-yyyy", null) + "','" + txt_Orderno_OS.Text + "'");
        sb.Append(",'" + txt_LabOrderno_OS.Text + "','" + txt_TrayNo_OS.Text + "','" + Txt_Ready.Text + "'");
        sb.Append(",'" + Txt_Adviceclient.Text + "','" + Txt_Followup.Text + "','" + DateTime.ParseExact(Txt_Datecalled.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",'" + DateTime.ParseExact(Txt_Delivery.Text, "dd-MM-yyyy", null) + "','" + Txt_Notes.Text + "','" + txt_Ophtalmiclens_OD.Text + "'");
        sb.Append(",'" + txt_Price_OD.Text + "','" + txt_Ophtalmiclens_OS.Text + "','" + txt_Price_OS.Text + "'");
        sb.Append(",'" + Txt_Professionalfees.Text + "','" + Txt_Total.Text + "','" + ddl_frame.SelectedValue + "','Glass'");
        sb.Append(",'" + MasterId + "')");

        objdal.EXECUTE_DML(sb.ToString());
    }

     // Insert Glass
    protected void UpdateGlass()
    {
        StringBuilder sb = new StringBuilder();
        //UPDATE [dbo].[tbl_Glass] SET [GlassId] = '',[PatientId] = '',[Date] = '',[ExamTemplate] = '',[Professional] = '',[Sphere_OD] = ''
        //,[Cylinder_OD] = '',[Axis_OD] = '',[Addition_OD] = '',[Segh_OD] = '',[Bc_OD] = '',[Sphere_OS] = '',[Cylinder_OS] = '',[Axis_OS] = ''
        //,[Addition_OS] = '',[Segh_OS] = '',[Bc_OS] = '',[PDFar_OD] = '',[PDNear_OD] = '',[PDHeight_OD] = '',[PDVertex_OD] = '',[PDWrapangle_OD] = ''
        //,[PDPantoscopicangle_OD] = '',[PDFar_OS] = '',[PDNear_OS] = '',[PDHeight_OS] = '',[PDVertex_OS] = '',[PDWrapangle_OS] = '',[PDPantoscopicangle_OS] = ''
        //,[PDFar_Total] = '',[PDNear_Total] = '',[ItemId_OD] = '',[Printedon_OD] = '',[Orderno_OD] = '',[LabOrderno_OD] = '',[TrayNo_OD] = '',[ItemId_OS] = ''
        //,[Printedon_OS] = '',[Orderno_OS] = '',[LabOrderno_OS] = '',[TrayNo_OS] = '',[Ready] = '',[Adviceclient] = '',[Followup] ='',[Datecalled] = ''
        //,[Delivery] = '',[Notes] = '',[Ophtalmiclens_OD] = '',[Price_OD] = '',[Ophtalmiclens_OS] = '',[Price_OS] = '',[Professionalfees] = ''
        //,[Total] = '',[PrescriptionType] = ''      
        //WHERE 
      

        sb.Append("UPDATE [dbo].[tbl_Glass] SET [PatientId] = '" + DDL_Patient.SelectedValue + "',[Date] = '" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[ExamTemplate] = '" + ddl_Exam.SelectedValue + "',[Professional] = '" + txt_Professional.Text + "',[Sphere_OD] = '" + Txt_Sphere_OD.Text + "'");
        sb.Append(",[Cylinder_OD] = '" + Txt_Cylinder_OD.Text + "',[Axis_OD] = '" + Txt_Axis_OD.Text + "',[Addition_OD] = '" + Txt_Addition_OD.Text + "'");
        sb.Append(",[Segh_OD] = '" + Txt_Segh_OD.Text + "',[Bc_OD] = '" + Txt_Bc_OD.Text + "',[Sphere_OS] = '" + Txt_Sphere_OS.Text + "'");
        sb.Append(",[Cylinder_OS] = '" + Txt_Cylinder_OS.Text + "',[Axis_OS] = '" + Txt_Axis_OS.Text + "',[Addition_OS] = '" + Txt_Addition_OS.Text + "'");
        sb.Append(",[Segh_OS] = '" + Txt_Segh_OS.Text + "',[Bc_OS] = '" + Txt_Bc_OS.Text + "',[PDFar_OD] = '" + txt_PDFar_OD.Text + "'");
        sb.Append(",[PDNear_OD] = '" + txt_PDNear_OD.Text + "',[PDHeight_OD] = '" + txt_PDHeight_OD.Text + "',[PDVertex_OD] = '" + txt_PDVertex_OD.Text + "'");
        sb.Append(",[PDWrapangle_OD] = '" + txt_PDWrapangle_OD.Text + "',[PDPantoscopicangle_OD] = '" + txt_PDPantoscopicangle_OD.Text + "'");
        sb.Append(",[PDFar_OS] = '" + txt_PDFar_OS.Text + "',[PDNear_OS] = '" + txt_PDNear_OS.Text + "',[PDHeight_OS] = '" + txt_PDHeight_OS.Text + "'");
        sb.Append(",[PDVertex_OS] = '" + txt_PDVertex_OS.Text + "',[PDWrapangle_OS] = '" + txt_PDWrapangle_OS.Text + "'");
        sb.Append(",[PDPantoscopicangle_OS] = '" + txt_PDPantoscopicangle_OS.Text + "',[PDFar_Total] = '" + txt_PDFar_Total.Text + "'");
        sb.Append(",[PDNear_Total] = '" + txt_PDNear_Total.Text + "',[ItemId_OD] = '" + ddl_Product_OD.SelectedValue + "'");
        sb.Append(",[Printedon_OD] = '" + DateTime.ParseExact(txt_Printedon_OD.Text, "dd-MM-yyyy", null) + "',[Orderno_OD] = '" + txt_Orderno_OD.Text + "'");
        sb.Append(",[LabOrderno_OD] = '" + txt_LabOrderno_OD.Text + "',[TrayNo_OD] = '" + txt_TrayNo_OD.Text + "'");
        sb.Append(",[ItemId_OS] = '" + ddl_Product_OS.SelectedValue + "',[Printedon_OS] = '" + DateTime.ParseExact(txt_Printedon_OS.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Orderno_OS] = '" + txt_Orderno_OS.Text + "',[LabOrderno_OS] = '" + txt_LabOrderno_OS.Text + "',[TrayNo_OS] = '" + txt_TrayNo_OS.Text + "'");
        sb.Append(",[Ready] = '" + Txt_Ready.Text + "',[Adviceclient] = '" + Txt_Adviceclient.Text + "',[Followup] ='" + Txt_Followup.Text + "'");
        sb.Append(",[Datecalled] = '" + DateTime.ParseExact(Txt_Datecalled.Text, "dd-MM-yyyy", null) + "'");
        sb.Append(",[Delivery] = '" + DateTime.ParseExact(Txt_Delivery.Text, "dd-MM-yyyy", null) + "',[Notes] = '" + Txt_Notes.Text + "'");
        sb.Append(",[Ophtalmiclens_OD] = '" + txt_Ophtalmiclens_OD.Text + "',[Price_OD] = '" + txt_Price_OD.Text + "'");
        sb.Append(",[Ophtalmiclens_OS] = '" + txt_Ophtalmiclens_OS.Text + "',[Price_OS] = '" + txt_Price_OS.Text + "'");
        sb.Append(",[Professionalfees] = '" + Txt_Professionalfees.Text + "',[Total] = '" + Txt_Total.Text + "'");
        sb.Append(",[ItemId_Frame]='" + ddl_frame.SelectedValue + "' WHERE [GlassId] = '" + Request.QueryString["id"].ToString() + "'");

        objdal.EXECUTE_DML(sb.ToString());
    }

    protected void ddl_Exam_SelectedIndexChanged(object sender, EventArgs e)
    {
        setPrescription();
    }

    // click on Invoice
    protected void Button_Invoice_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["id"] != null)
            {
                UpdateGlass();
                var invoiceMasterId = objdal.Get_SingleValue("select InvoiceMasterId from tbl_InvoiceMaster where PatientId='" + DDL_Patient.SelectedValue + "'");
                //Response.Redirect("invoice.aspx?Id=" + DDL_Patient.SelectedValue, true);
                Response.Redirect("invoice.aspx?Id=" + invoiceMasterId, true);
            }
            else
            {
                InsertGlass();
                InsertInvoice();                
            }
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
        
    }

    protected void InsertInvoice()
    {
        StringBuilder sb = new StringBuilder();
        string InvoiceMasterId = Guid.NewGuid().ToString();
        string CurrentuserId = Membership.GetUser().ProviderUserKey.ToString();
        string maxInvoice = objdal.Get_SingleValue("select isnull(max(InvoiceNo),0)+1 as MaxInvoiceNumber from tbl_InvoiceMaster");

            //INSERT INTO [dbo].[tbl_InvoiceMaster]([InvoiceMasterId],[InvoiceNo],[PatientId],[InvoiceMasterDate]
            //,[InvoiceMasterGrandTotal],[InvoiceMasterPayment],[InvoiceMasterThirdPartyBalance],[InvoiceMasterPatientBalance]
            //,[InvoiceMasterNote],[InvoiceMasterIsTemporary],[MasterId],[InvoiceMasterEnterydById])VALUES         


        sb.Append("INSERT INTO [dbo].[tbl_InvoiceMaster]([InvoiceMasterId],[InvoiceNo],[PatientId],[InvoiceMasterDate],[InvoiceMasterGrandTotal],[MasterId]");
        sb.Append(",[InvoiceMasterEnterydById]) VALUES('" + InvoiceMasterId + "','" + maxInvoice + "','" + DDL_Patient.SelectedValue + "'");
        sb.Append(",'" + DateTime.ParseExact(Txt_Date.Text, "dd-MM-yyyy", null) + "','"+ Txt_Total.Text +"'");
        sb.Append(",'" + MasterId + "','" + CurrentuserId + "')");

        objdal.EXECUTE_DML(sb.ToString());

      
       // INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceDetailId],[InvoiceMasterId],[ItemId],[ExamTypeId]
       //,[InvoiceDetailDescription],[InvoiceDetailQuantity],[InvoiceDetailDiscount],[InvoiceDetailTax]
       //,[InvoiceDetailUnitprice],[InvoiceDetailDeliveryDate])VALUES
        sb = new StringBuilder();
        // for left lens
        sb.Append("INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ItemId],[InvoiceDetailQuantity],[InvoiceDetailDescription],[InvoiceDetailUnitprice])");
        sb.Append("VALUES('" + InvoiceMasterId + "','" + ddl_Product_OD.SelectedValue + "','1','Ophtalmic lens OD','" + txt_Price_OD.Text + "')");
        objdal.EXECUTE_DML(sb.ToString());

        sb = new StringBuilder();
        //for right lens
        sb.Append("INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ItemId],[InvoiceDetailQuantity],[InvoiceDetailDescription],[InvoiceDetailUnitprice])");
        sb.Append("VALUES('" + InvoiceMasterId + "','" + ddl_Product_OS.SelectedValue + "','1','Ophtalmic lens OS','" + txt_Price_OS.Text + "')");
        objdal.EXECUTE_DML(sb.ToString());

        sb = new StringBuilder();
        //for right lens
        sb.Append("INSERT INTO [dbo].[tbl_InvoiceDetail]([InvoiceMasterId],[ItemId],[InvoiceDetailQuantity],[InvoiceDetailDescription],[InvoiceDetailUnitprice])");
        sb.Append("VALUES('" + InvoiceMasterId + "','" + ddl_frame.SelectedValue + "','1','Glass frame','" + txt_FramePrice.Text + "')");
        objdal.EXECUTE_DML(sb.ToString());

        Response.Redirect("invoice.aspx?Id=" + InvoiceMasterId, true);        
    }

   

    protected void ddl_Product_OS_SelectedIndexChanged(object sender, EventArgs e)
    {
        OSItemDetails();
    }
    protected void ddl_Product_OD_SelectedIndexChanged(object sender, EventArgs e)
    {
        ODItemDetails();
    }
    protected void ddl_frame_SelectedIndexChanged(object sender, EventArgs e)
    {
        FrameDetails();
    }
    protected void LinkButton_Keyword_OD_Click(object sender, EventArgs e)
    {
        try
        {
            SearchById(ddl_Product_OD, txt_Keyword_OD, "Ophtalmic lens");
            ODItemDetails();
        }
        catch (Exception ex)
        {
            
            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
    protected void LinkButton_Keyword_OS_Click(object sender, EventArgs e)
    {
        try
        {
            SearchById(ddl_Product_OS, txt_Keyword_OS, "Ophtalmic lens");
            OSItemDetails();
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }

    private void SearchById(DropDownList ddlId,TextBox TbId,string Type)
    {
        DataTable dt = new DataTable();
        if (Type=="Frame")
        {
            dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
                               + " where tbl_Item.ItemType='" + Type + "' and CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName like '%" + TbId.Text + "%' "
                               + " and tbl_Item.ItemQuantity>0 and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        }
        else
        {
            dt = objdal.GET_DATATABLE("select tbl_Item.ItemId, CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName as Item from tbl_Item  "
                                + " where tbl_Item.ItemType='" + Type + "' and CONVERT(varchar, tbl_Item.ItemId)+' '+tbl_Item.ItemName like '%" + TbId.Text + "%' "
                                + " and [tbl_Item].MasterId='" + MasterId + "' ORDER BY [ItemName]");
        }      
        ddlId.DataSource = dt;
        ddlId.DataValueField = "ItemId";
        ddlId.DataTextField = "Item";
        ddlId.DataBind();
    }
    protected void LinkButton_frame_Click(object sender, EventArgs e)
    {
        try
        {
            SearchById(ddl_frame, txt_Keyword_frame, "Frame");
            FrameDetails();
        }
        catch (Exception ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = "Error: " + ex.Message;
        }
    }
}