﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="general.aspx.cs" Inherits="general" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/ProjectHelper.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script>
        $(document).ready(function () {

            var id = getUrlVars()["id"];
            if (id == null) {
                $("#Txt_Date").val(currentdate());
            }

        });

        //Masking       
        jQuery(function ($) {
            $("#Txt_Date").mask("99-99-9999");
            $("#txt_RxScopeOD3").mask("999");
            $("#txt_RxScopeOS3").mask("999");
        });

        function AxisRange() {
            if ($("#txt_RxScopeOD3").val() > 180) {
                $("#txt_RxScopeOD3").val('180');
            }
            if ($("#txt_RxScopeOS3").val() > 180) {
                $("#txt_RxScopeOS3").val('180');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ul class="nav nav-pills PageLink">
        <li>
            <asp:HyperLink ID="HL_examtemlate" NavigateUrl="~/examtemplate.aspx" CssClass="btn btn-sm btn-default" runat="server"><span class="glyphicon glyphicon-file"></span>&nbsp;Exam templates</asp:HyperLink></li>
    </ul>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="alert alert-danger message"></asp:Label>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12">
        <asp:ValidationSummary ID="ValidationSummary" CssClass="alert alert-danger summarymessage" runat="server" />
    </div>

    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Examno">Exam no:</label>
            <asp:TextBox ID="Txt_Examno" ClientIDMode="Static" disabled="disabled" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="Txt_Date">Date:</label>
            <asp:TextBox ID="Txt_Date" ClientIDMode="Static" CssClass="form-control" placeholder="DD-MM-YYYY" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-3 col-md-3 col-sm-4">
            <label for="DDL_Patient">Patient:</label>&nbsp; 
                 <asp:DropDownList ID="DDL_Patient" CssClass="form-control" runat="server"></asp:DropDownList>
        </div>
        <div class="form-group col-lg-12 col-md-12 col-sm-12">
            <label for="txt_CaseHistory">Case history:</label>
            <asp:TextBox ID="txt_CaseHistory" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Rx final</label>
                </td>
                <td colspan="3">Same as mav
                    <asp:CheckBox ID="chk_Rxfinal" runat="server" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <label>Base curve</label></td>
                <td colspan="2">&nbsp;
                </td>
                <td colspan="2" align="center">
                    <label>Diameter</label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">Base1</td>
                <td align="center">Base2</td>
                <td>&nbsp;</td>
                <td align="center">Horiz.</td>
                <td align="center">Vert.</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="RxFinalOD1" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="RxFinalOD2" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="RxFinalOD3" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="RxFinalOD4" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="RxFinalOS1" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="RxFinalOS2" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="RxFinalOS3" onkeypress='validate(event)' MaxLength="6" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="RxFinalOS4" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>


    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Rx scope</label>
                </td>
                <td colspan="6">Simple
                    <asp:CheckBox ID="chk_simple" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Progressive
                    <asp:CheckBox ID="chk_progressive" runat="server" />
                    &nbsp;&nbsp;Bifocal
                    <asp:CheckBox ID="chk_bifocal" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">Sphere</td>
                <td align="center">Cylinder</td>
                <td align="center">Axis</td>
                <td>&nbsp;</td>
                <td align="center">Addition</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOD1" ClientIDMode="Static" MaxLength="6" onkeypress='validate(event)' CssClass="form-control" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOD2" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOD3" CssClass="form-control" onkeyup="AxisRange();" oninput="AxisRange();" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOD4" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOs1" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOS2" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOS3" CssClass="form-control" onkeyup="AxisRange();" oninput="AxisRange();" ClientIDMode="Static" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_RxScopeOS4" CssClass="form-control" MaxLength="6" onkeypress='validate(event)' runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Prism</h4>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td align="center">Prism 1</td>
                <td>&nbsp;</td>
                <td align="center">Orientation 1</td>
                <td>&nbsp;</td>
                <td align="center" colspan="2">Decentration 1</td>

            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Prism1_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Orientation1_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Decentration11_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_Decentration12_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Prism1_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Orientation1_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Decentration11_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_Decentration12_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <h4>&nbsp;&nbsp;Prism</h4>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td align="center">Prism 2</td>
                <td>&nbsp;</td>
                <td align="center">Orientation 2</td>
                <td>&nbsp;</td>
                <td align="center" colspan="2">Decentration 2</td>

            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Prism2_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Orientation2_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Decentration21_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_Decentration22_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Prism2_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Orientation2_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Decentration21_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txt_Decentration22_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>


    <div style="clear: both;"></div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 category">
        <div class="form-group col-lg-2 col-md-2 col-sm-3">
            <label for="txt_Riv">Riv:</label>
            <asp:TextBox ID="txt_Riv" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-sm-3">
            <label for="ddl_Sop">Sop:</label>
            <asp:DropDownList ID="ddl_Sop" CssClass="form-control" runat="server">
                <asp:ListItem>Diplopia</asp:ListItem>
                <asp:ListItem>Flashes</asp:ListItem>
                <asp:ListItem>Floaters</asp:ListItem>
                <asp:ListItem>Chx</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-sm-3">
            <label for="ddl_Sgp">Sgp:</label>
            <asp:DropDownList ID="ddl_Sgp" CssClass="form-control" runat="server">
                <asp:ListItem>Htn</asp:ListItem>
                <asp:ListItem>Chol</asp:ListItem>
                <asp:ListItem>Diabetes</asp:ListItem>
                <asp:ListItem>Mdt</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-sm-3">
            <label for="ddl_Sogf">Sogf:</label>
            <asp:DropDownList ID="ddl_Sogf" CssClass="form-control" runat="server">
                <asp:ListItem>Glc</asp:ListItem>
                <asp:ListItem>Dmla</asp:ListItem>
                <asp:ListItem>Blindness</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-sm-3">
            <label for="txt_Meds">Meds :</label>
            <asp:TextBox ID="txt_Meds" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group col-lg-2 col-md-2 col-sm-3">
            <label for="txt_All">All:</label>
            <asp:TextBox ID="txt_All" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Visual Acuity</label>
                </td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td>&nbsp;VL:</td>
                <td>&nbsp;Sc&nbsp;<asp:CheckBox ClientIDMode="Static" ID="chk_VLSc" runat="server" />&nbsp;&nbsp;Cc&nbsp;<asp:CheckBox ID="chk_VLCc" runat="server" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;VP:</td>
                <td>&nbsp;Sc&nbsp;<asp:CheckBox ID="chk_VPSc" ClientIDMode="Static" runat="server" />&nbsp;&nbsp;Cc&nbsp;<asp:CheckBox ID="chk_VPCc" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;20/</td>
                <td>
                    <asp:TextBox ID="txt_VisualAcuity_VL1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td align="right">M@</td>
                <td>
                    <asp:TextBox ID="txt_VisualAcuity_VP1" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="right">&nbsp;20/</td>
                <td>
                    <asp:TextBox ID="txt_VisualAcuity_VLVP" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;20/</td>
                <td>
                    <asp:TextBox ID="txt_VisualAcuity_VL2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td align="right">M@</td>
                <td>
                    <asp:TextBox ID="txt_VisualAcuity_VP2" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12 category">
        <table>
            <tr>
                <td align="center">
                    <label>&nbsp;AA</label></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;Donders&nbsp;<asp:CheckBox ClientIDMode="Static" ID="chk_AADonders" runat="server" />
                    &nbsp;&nbsp;Sheard&nbsp;<asp:CheckBox ID="chk_AASheard" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;OD&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_AAOD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;OS&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_AAOS" Style="margin-bottom: 13px;" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
        &nbsp;
    </div>

    <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td align="center">
                    <label>&nbsp;Ishihara</label></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;OD</td>
                <td>
                    <asp:TextBox ID="txt_IshiharaOD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;OS</td>
                <td>
                    <asp:TextBox ID="txt_IshiharaOS" CssClass="form-control" Style="margin-bottom: 13px;" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td align="center">
                    <label>&nbsp;Stereoscopy</label></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;Animals:</td>
                <td>
                    <asp:TextBox ID="txt_StereoscopyAnimals" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/3&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;Circles:</td>
                <td>
                    <asp:TextBox ID="txt_StereoscopyCircles" Style="margin-bottom: 13px;" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/9&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td colspan="2">
                    <label>&nbsp;Ocular dominance</label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_OcularDominance" CssClass="form-control" Style="margin-left: 10px; margin-bottom: 26px;" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Screen test</label>
                </td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td>&nbsp;VL</td>
                <td>
                    <asp:TextBox ID="txt_ScreentestVL" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>&nbsp;VP</td>
                <td>
                    <asp:TextBox ID="txt_ScreentestVP" CssClass="form-control" Style="margin-bottom: 6px;" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Wards</label>
                </td>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Wards1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>mm R</td>
                <td>
                    <asp:TextBox ID="txt_Wards2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>it MG</td>
                <td>
                    <asp:TextBox ID="txt_Wards3" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>-</td>
                <td>
                    <asp:TextBox ID="txt_Wards4" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>+</td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Eye motilites</label>
                </td>
                <td colspan="3">&nbsp;</td>
            </tr>

            <tr>
                <td colspan="5">&nbsp;&nbsp;Flexible and complete OU&nbsp;<asp:CheckBox ID="chk_Flexibleandcomplete" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
        </table>
    </div>


    <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Phoria</label>
                </td>
                <td colspan="3">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;TM&nbsp;<asp:CheckBox ClientIDMode="Static" ID="chk_PhoriaTm" runat="server" />
                    &nbsp;&nbsp;Von graefe&nbsp;<asp:CheckBox ID="chk_PhoriaVongraefe" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;VL</td>
                <td>
                    <asp:TextBox ID="txt_PhoriaVL" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td>&nbsp;VP</td>
                <td>
                    <asp:TextBox ID="txt_PhoriaVP" CssClass="form-control" Style="margin-bottom: 10px;" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="4">
                    <label>Fusional reserves</label>
                </td>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;VL</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VL" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="3">&nbsp;</td>
                <td>&nbsp;VP</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VP" CssClass="form-control" Style="margin-bottom: 6px;" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>&nbsp;BE</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VL_BE" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;BH</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VL_BH" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;BE</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VP_BE" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;BH</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VP_BH" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>&nbsp;BI</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VL_BI" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;BB</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VL_BB" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;&nbsp;</td>
                <td>&nbsp;BI</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VP_BI" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;BB</td>
                <td>
                    <asp:TextBox ID="txt_Fusional_VP_BB" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12 category">
        <table>
            <tr>
                <td>&nbsp;<label>PRC</label></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_PRC1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_PRC2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;cm</td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">&nbsp;Nose&nbsp;<asp:CheckBox ClientIDMode="Static" ID="chk_PRC_Nose" runat="server" /></td>
            </tr>
        </table>
    </div>


    <div class="form-group table-responsive col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table style="min-width: 400px;">
            <tr>
                <td align="center" colspan="2">
                    <label>Retinoscopy</label>
                </td>
                <td colspan="8">Static
                    <asp:CheckBox ID="chk_Retinoscopy_Static" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;L-M
                    <asp:CheckBox ID="chk_Retinoscopy_LM" runat="server" />
                    &nbsp;&nbsp;Auto
                    <asp:CheckBox ID="chk_Retinoscopy_Auto" runat="server" /></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OD1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OD2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;X&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OD3" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <label>&nbsp;20/&nbsp;</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OD4" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OS1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OS2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;X&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OS3" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <label>&nbsp;20/&nbsp;</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_OS4" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="10">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="10">&nbsp;<label>MAV</label></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OD1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OD2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;X&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OD3" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <label>&nbsp;20/&nbsp;</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OD4" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
                <td>
                    <label>&nbsp;20/&nbsp;</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_ODOS" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OS1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OS2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;X&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OS3" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>
                    <label>&nbsp;20/&nbsp;</label></td>
                <td>
                    <asp:TextBox ID="txt_Retinoscopy_MAV_OS4" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">&nbsp;Acc. CX:</td>
                <td colspan="2">
                    <asp:TextBox ID="txt_Retinoscopy_ACC" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;Ordiarp:</td>
                <td colspan="2">
                    <asp:TextBox ID="txt_Retinoscopy_Ordiarp" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;</td>

            </tr>
            <tr>
                <td colspan="2" align="center">&nbsp;Add final:</td>
                <td colspan="2">
                    <asp:TextBox ID="txt_Retinoscopy_Addfinal" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;Vertex:</td>
                <td colspan="2">
                    <asp:TextBox ID="txt_Retinoscopy_Vertex" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td align="center" colspan="2">
                    <label>Keratometry</label>
                </td>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OD:</label></td>
                <td>
                    <asp:TextBox ID="txt_Keratometry_OD1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Keratometry_OD2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;X&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Keratometry_OD3" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <label>&nbsp;OS:</label></td>
                <td>
                    <asp:TextBox ID="txt_Keratometry_OS1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;/&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Keratometry_OS2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;X&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Keratometry_OS3" CssClass="form-control" runat="server"></asp:TextBox></td>

            </tr>

            <tr>
                <td align="center">&nbsp;Recall:</td>
                <td colspan="2" align="center">
                    <asp:TextBox ID="txt_Keratometry_Recall" CssClass="form-control" runat="server"></asp:TextBox></td>

                <td align="right">&nbsp;Fees:</td>
                <td colspan="2">
                    <asp:TextBox ID="txt_Keratometry_Fees" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;<br />
                    <br />
                </td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td align="center">
                    <label>&nbsp;Central visual field</label>
                </td>
                <td>&nbsp;&nbsp;Complete and flawless OD, OS
                    <asp:CheckBox ID="Chk_Centralvisualfield" ClientIDMode="Static" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td align="center">
                    <label>&nbsp;Peripheral visual field</label>
                </td>
                <td>&nbsp;&nbsp;Complete and flawless OD, OS
                    <asp:CheckBox ID="chk_Peripheralvisualfield" ClientIDMode="Static" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    <div class="form-group table-responsive col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table style="min-width: 400px;">
            <tr>
                <td colspan="3">&nbsp;<label>Biomicroscopy</label></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<label>OD</label></td>
                <td>&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;<label>OS</label></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Healthy
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Eyelids" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Eyelids/Eyelashes
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Healthy
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Eyelids" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Lisselcair
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Connective" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Conjunctivitis
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Lisselcair
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Connective" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Clear
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Cornea" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cornea
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Clear
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Cornea" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Normal
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Tearfilm" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tear film/Goal
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Normal
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Tearfilm" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Quiet
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Chambreant" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chambreant
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Quiet
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Chambreant" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Dish
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Iris" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Iris
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Dish
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Iris" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;Clear
                    <asp:CheckBox ID="chk_Biomicroscopy_OD_Crystalline" ClientIDMode="Static" runat="server" /></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Crystalline
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;Clear
                    <asp:CheckBox ID="chk_Biomicroscopy_OS_Crystalline" ClientIDMode="Static" runat="server" /></td>
            </tr>
        </table>
    </div>

    <div class="form-group table-responsive col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table style="min-width: 460px;">
            <tr>
                <td colspan="9">&nbsp;<label>Fundus</label>
                    &nbsp;&nbsp;Ophthalmoscope
                    <asp:CheckBox ID="chk_Fundus_Ophthalmoscope" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Volk
                    <asp:CheckBox ID="chk_Fundus_Volk" ClientIDMode="Static" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;&nbsp;<label>OD</label></td>
                <td colspan="3">Report E/P(HxV)</td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;<label>OS</label></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Sharp edges
                    <asp:CheckBox ID="chk_Fundus_OD_Papilla" ClientIDMode="Static" runat="server" /></td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Papilla
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="3">&nbsp;&nbsp;Sharp edges
                    <asp:CheckBox ID="chk_Fundus_OS_Papilla" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Pink
                    <asp:CheckBox ID="chk_Fundus_OD_Anr" ClientIDMode="Static" runat="server" /></td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Anr
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="3">&nbsp;&nbsp;Pink
                    <asp:CheckBox ID="chk_Fundus_OS_Anr" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Healthy
                    <asp:CheckBox ID="chk_Fundus_OD_Macula" ClientIDMode="Static" runat="server" /></td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Macula
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="3">&nbsp;&nbsp;Healthy
                    <asp:CheckBox ID="chk_Fundus_OS_Macula" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Fundus_OD_RF1" CssClass="form-control" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;+&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Fundus_OD_RF2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;-&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RF
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Fundus_OS_RF1" CssClass="form-control" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;+&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Fundus_OS_RF2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;-&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Healthy
                    <asp:CheckBox ID="chk_Fundus_OD_Vessels" ClientIDMode="Static" runat="server" /></td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vessels
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="3">&nbsp;&nbsp;Healthy
                    <asp:CheckBox ID="chk_Fundus_OS_Vessels" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Clear
                    <asp:CheckBox ID="chk_Fundus_OD_Window" ClientIDMode="Static" runat="server" /></td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Window
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="3">&nbsp;&nbsp;Clear
                    <asp:CheckBox ID="chk_Fundus_OS_Window" ClientIDMode="Static" runat="server" /></td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12 category">
        <table>
            <tr>
                <td>
                    <label>&nbsp; Periphery</label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;Bio
                    <asp:CheckBox ID="chk_Periphery_Bio" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;3-Mirrors
                    <asp:CheckBox ID="chk_Periphery_Mirrors" runat="server" /></td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;(Hole, Tear, Abruption)OU 
                    <asp:CheckBox ID="chk_Periphery_OU" ClientIDMode="Static" runat="server" />
                </td>
            </tr>

        </table>
    </div>

    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12 category">
        <table>
            <tr>
                <td colspan="5">
                    <label>&nbsp; Tonometry</label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;OD</td>
                <td colspan="3">
                    <asp:TextBox ID="txt_Tonometry_OD" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;mmHG</td>
            </tr>
            <tr>
                <td>&nbsp;OS</td>
                <td colspan="3">
                    <asp:TextBox ID="txt_Tonometry_OS" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;mmHG</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Tonometry_A1" CssClass="form-control" runat="server"></asp:TextBox>
                </td>
                <td align="center">@</td>
                <td>
                    <asp:TextBox ID="txt_Tonometry_A2" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="5">&nbsp;Goldmann
                    <asp:CheckBox ID="chk_Tonometry_Goldmann" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Nct
                    <asp:CheckBox ID="chk_Tonometry_Nct" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;<br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>

    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
    </div>

    <div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td colspan="3">
                    <label>&nbsp; Drug</label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Drug_benoxinate" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;gtt benoxinate 0.4% OU</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Drug_proparacaine" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;gtt proparacaine 0.5% OU</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Drug_tropicamide" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;gtt tropicamide 1% OU</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Drug_phenylephrine" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;gtt phenylephrine 2.5% OU</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Drug_cyclopentolate" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td colspan="2">&nbsp;gtt cyclopentolate 1% OU</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txt_Drug_A1" CssClass="form-control" runat="server"></asp:TextBox></td>
                <td>&nbsp;@&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Drug_A2" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td colspan="2">
                    <label>&nbsp; Diagnostics</label>
                </td>
                <td>
                    <asp:TextBox ID="txt_Diagnostics" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;Hyperopia
                    <asp:CheckBox ID="chk_Farsightedness" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Myopia
                    <asp:CheckBox ID="chk_Myopia" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Astigmatism
                    <asp:CheckBox ID="chk_Astigmatism" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;Cataract
                    <asp:CheckBox ID="chk_Cataract" ClientIDMode="Static" runat="server" />
                    &nbsp; &nbsp;Presbyopia
                    <asp:CheckBox ID="chk_Presbyopia" ClientIDMode="Static" runat="server" />
                    &nbsp; &nbsp;Blepharitis
                    <asp:CheckBox ID="chk_Blepharitis" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;Dry eye
                    <asp:CheckBox ID="chk_Dryeye" ClientIDMode="Static" runat="server" />
                    &nbsp; &nbsp;Macular degeneration
                    <asp:CheckBox ID="Chk_Macular" ClientIDMode="Static" runat="server" />
                    &nbsp; &nbsp;Glaucoma
                    <asp:CheckBox ID="Chk_Glaucoma" ClientIDMode="Static" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>

    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 category">
        <table>
            <tr>
                <td colspan="2">
                    <label>&nbsp; Plan/Recommendations</label>
                </td>
                <td>
                    <asp:TextBox ID="txt_Plan" CssClass="form-control" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Change rx
                    <asp:CheckBox ID="chk_Changerx" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Artificial tears
                    <asp:CheckBox ID="chk_Artificialtears" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Cold compresses
                    <asp:CheckBox ID="chk_Coldcompresses" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;&nbsp;Warm compresses
                    <asp:CheckBox ID="chk_Warmcompresses" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Eyelid hygiene
                    <asp:CheckBox ID="chk_Eyelidhygiene" ClientIDMode="Static" runat="server" />
                    &nbsp;&nbsp;Preventive expansion
                    <asp:CheckBox ID="chk_Preventiveexpansion" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;Pt advised of the signs and symptoms of DR rtc if emergency floaters / flashes
                    <asp:CheckBox ID="chk_Ptadvised" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;Reyoir emergency if condition worsens or does not dici sameliore
                    <asp:CheckBox ID="chk_Reyoiremergency" ClientIDMode="Static" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    &nbsp;
                    <asp:Button ID="Button_Submit" ClientIDMode="Static" CssClass="btn btn-primary btnSubmit" runat="server" OnClick="Button_Submit_Click" Text="SAVE" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

